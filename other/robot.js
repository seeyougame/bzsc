
class StaticGuaJiTask {
    static name = "StaticGuaJiTask";
    arrGuaJiMap = null;
    state = false;
    curGuaJiMap = null;
    uid = 0;
    instance = null;
    stopTask = false;
    findWayIsStop = true;
    attackIsStop = true;
    stopFindWay = false;
    hashCode = null;
    diePos = null;

    setMaps(arrMap) {
        this.arrGuaJiMap = arrMap;
    }

    setCurGuaJiMap(map) {
        this.curGuaJiMap = map;
    }
	
	gotoTarget(monster) {
		try {
			if (!GlobalUtil.isAttackDistance(monster.currentX, monster.o_NI3)) {
				let targetPos = GlobalUtil.getTargetPos(monster.currentX, monster.o_NI3);
				if (targetPos) {
					setCurMonster(monster);
					SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
					return check();
				}
				else {
					SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
				}
			}	
		}
		catch (ex) {}		
	}

    async stop() {
        // console.warn(`准备任务停止${this.constructor.name}  uid=>${this.uid}`);
        // if (!this.isRunning()) return;
        this.curMapName = null;
        this.curGuaJiMap = null;
        try {
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            clearCurMonster();
        } catch (ex) {
            console.error(ex);
        }
        await this.stopTaskComplete();
        this.diePos = null;
        console.warn(`任务停止${this.constructor.name}  uid=>${this.uid}`);
    }

    stopTaskComplete() {
        let self = this;
        function check() {
            return xSleep(100).then(function () {
                if (!curMonster) {
                    self.attackIsStop = true;
                }
                if (self.findWayIsStop == true) {
                    self.state = false;
                }
                if (self.attackIsStop && self.findWayIsStop) {
                    self.stopTask = true;
                    return;
                }
                return check();
            });
        }
        return check();
    }

    enableParams() {

    }

    initState() {
        this.stopTask = false;
        this.findWayIsStop = true;
        this.attackIsStop = true;
        this.state = true;
		this.diePos = null;
		clearCurMonster();
    }

    setHashCode() {
        let hashCode = new Date().getTime();
        this.hashCode = hashCode;
        GlobalUtil.lastTaskHashCode = hashCode;
    }

    async start() {
        if (this.isRunning()) return;
        this.initState();
        this.enableParams();
        this.run();
        console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
    }

    isRunning() {
        // return this.stopTask != true && this.state == true;
        return this.state == true;
    }

    startUtils() {
        try {
            QuickPickUpNormal.start();
            BlacklistMonitors.start();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    stopUtils() {
        try {
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            this.loopCancleNoBelongMonster();
            this.loopAttackMonter();
            this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    reviveFinish() {
        function check() {
            return xSleep(1000).then(function () {
                if (PlayerActor.o_NLN.curHP > 0) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }

    async revive() {
        try {
            SingletonMap.o_eaX.o_e5d(0);
            await this.reviveFinish();
            console.warn(`人物复活...`);
            await xSleep(1000);
            await this.backToMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            // if (!isSafeArea()) return;
            console.warn(`回到挂机地图...`);
            await this.enterMap(this.curGuaJiMap);
        } catch (ex) {
            console.error(ex);
        }
    }

    loopCancleNoBelongMonster() {
        let self = this;
        try {
            if (curMonster && curMonster.belongingName != "" && curMonster.belongingName != SingletonMap.RoleData.name
                && !BlacklistMonitors.isWhitePlayer(curMonster.belongingName)) { // 不打非归属怪
                //callbackObj.writelog('不打非归属怪');
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
            else if (curMonster && curMonster.masterName.indexOf(SingletonMap.RoleData.name) > -1) { // 是自己宠物
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
            else if (this.filterMonster && !this.filterMonster(curMonster)) {
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
        if (this.isRunning()) {
            setTimeout(() => {
                self.loopCancleNoBelongMonster();
            }, 100);
        }
    }

    findMonter() {
        let monster = null;
        try {
            // 归属怪优先
            if (curMonster && curMonster.belongingName == SingletonMap.RoleData.name && this.filterMonster(curMonster)) {
                let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (belongingMonster) {
                    monster = belongingMonster;
                    // console.warn(`发现归属怪 ==>curMonster.$hashCode=>${monster.roleName}`);
                }
            }
            //已选中，还没有归属
            if (curMonster && curMonster.belongingName == "" && this.filterMonster(curMonster)) {
                let noBelongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (noBelongingMonster) {
                    monster = noBelongingMonster;
                }
            }
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof Monster && !M.masterName && M.curHP > 100 && this.filterMonster(M)) {
                        // 没有归属或归属自己
                        if (M.belongingName == '' || M.belongingName == SingletonMap.RoleData.name) {
                            if (targetMonster == null) {
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                            else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                                // 距离近的优先
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                        }
                    }
                }

                if (targetMonster) {
                    monster = targetMonster;
                    // console.warn(`发现怪物 ==>$hashCode=>${monster.roleName}`);
                }
            }
            //  先打黑名单
            if (BlacklistMonitors.badPlayer != null) {
                monster = null;
                // console.warn(`发现黑名单 ==>`);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    async loopAttackMonter() {
        try {
            if (!mainRobotIsRunning()) {
                return;
            }
            if (curMonster) {
                curMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode && item.curHP > 100);
            }
            if (curMonster && curMonster.masterName.indexOf(SingletonMap.RoleData.name) > -1) { // 是自己宠物
                clearCurMonster();
            }
            if (!curMonster) {
                curMonster = this.findMonter();
            }
            if (curMonster && this.filterMonster(curMonster)) {
                // console.warn(`发现怪物 => ${curMonster.roleName}=${curMonster.$hashCode}`);
                setCurMonster(curMonster);
                SingletonMap.o_OKA.o_Pc(curMonster, true);
                await this.attackMonster(curMonster);
            }
        } catch (ex) {
            console.error(ex);
        }
        let self = this;
        if (self.loopAttackMonterTtimeoutID) {
            clearTimeout(self.loopAttackMonterTtimeoutID);
        }
        if (this.isRunning()) {
            self.loopAttackMonterTtimeoutID = setTimeout(() => {
                this.loopAttackMonter();
            }, 100);
        }
    }

    async enterMap(map) {
        await xSleep(1000);
    }

    getMapPointConfig(mapId) {

    }

    hasMonster() {
        let self = this;
        function check() {
            return xSleep(500).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                if (curMonster) {
                    curMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode && item.curHP > 100);
                }
                if (curMonster && curMonster.masterName.indexOf(SingletonMap.RoleData.name) > -1) { // 是自己宠物
                    clearCurMonster();
                }
                let monster = curMonster || self.findMonter();
                if (monster == null || !SingletonMap.o_h7.o_lDb(monster)) {
                    // console.warn('hasMonster...没有怪物了...');
                    return;
                }
                if (monster && (!self.filterMonster(monster) || GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length > 20)) {
                    // console.warn('hasMonster...距离超出范围...');
                    return;
                }
                if (monster) {
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    await self.attackMonster(monster);
                }
                return check();
            });
        }
        return check();
    }

    hasBadPlayer() {
        let self = this;
        function check() {
            return xSleep(500).then(function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                if (BlacklistMonitors.badPlayer == null) {
                    return;
                }
                return check();
            });
        }
        return check();
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                let map = this.arrGuaJiMap[i].split(',');
                // 第一次进图或者当前地图不是目标地图
                //if (this.curGuaJiMap == null || !(this.curGuaJiMap[0] == map[0] && this.curGuaJiMap[1] == map[1])) {
                SingletonMap.o_lAu.stop();
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                //callbackObj.writelog(`进入地图 map==>${map}`);
                console.warn(`进入地图 map==>${map}`);
                // }
                let mapId = this.arrGuaJiMap[i];
                //callbackObj.writelog(`mapId==>${mapId}`);
                let pointConfig = this.getMapPointConfig(mapId);
                while (mainRobotIsRunning() && pointConfig.length > 0) {
                    this.enableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
                    // console.warn(`任务${this.constructor.name}  hashCode=>${this.hashCode}  pointConfig length ==> ${pointConfig.length}`);
                    let minPos = pointConfig[0];
                    if (pointConfig.length > 1) {
                        minPos = pointConfig.reduce(function (a, b) {
                            let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
                            // let aDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, a.x, a.y);
                            let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
                            // let bDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b.x, b.y);
                            return path1.length >= path2.length ? b : a;
                        });
                    }
                    // 没有怪物后再寻路
                    await this.hasMonster();
                    //console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  前往最近的坐标 ==> ${JSON.stringify(minPos)}`);
                    await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
                    //console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  到达最近的坐标 ==> ${JSON.stringify(minPos)}`);
                    // 删除已经用掉的坐标
                    pointConfig = pointConfig.filter(M => {
                        return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
                    });
                    // 没有怪物后再离开
                    await this.hasMonster();
                    //callbackObj.writelog(`没有怪物了...`);

                    this.disableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }

                    // 剩余数量小于8,换图
                    //let monsterNum = getShenYuMonsterNum();
                    //if (monsterNum && monsterNum < 8) {
                    //    break;
                    //}
                }

                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        if (this.isRunning() && mainRobotIsRunning()) {
            console.warn('打完一轮了，再次执行挂机 loopStartFlow...');
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }

    existsMonster(monster) {
        try {
            if (monster == null) {
                return false;
            }
            return SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == monster.$hashCode && item.curHP > 100) != undefined;
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    attackMonster(monster) {
        let self = this;
        if (!monster) return;
        this.attackIsStop = false;
        // if (curMonster && curMonster.$hashCode != monster.$hashCode) return;
        function check() {
            return xSleep(50).then(function () {
                SingletonMap.o_lAu.stop();
                self.attackIsStop = false;
                if (!mainRobotIsRunning()) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    //console.warn('attackMonster...1');
                    return;
                }

                // 收到停止指令
                if (self.stopTask == true) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    // console.warn('attackMonster...2');
                    return;
                }

                // 任务停止
                if (self.isRunning && !self.isRunning()) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    // console.warn('attackMonster...3');
                    return;
                }

                if (self.attackIsStop) {
                    // console.warn('attackMonster...3-1');
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.attackIsStop = true;
                //    clearCurMonster();
                //   console.warn('attackMonster...4');
                //    return;
                //}
                if (monster && monster.masterName) { // 是宠物
                    clearCurMonster();
                    self.attackIsStop = true;
                    // console.warn('attackMonster...5');
                    return;
                }

                // 不是想打的怪
                //if (!self.filterMonster(monster)) {
                //    self.attackIsStop = true;
                //     clearCurMonster();
                //    console.warn('attackMonster...6');
                //     return;
                // }

                if (monster && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name
                    && !BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 不打非归属怪
                    //callbackObj.writelog('不打非归属怪');
                    clearCurMonster();
                    self.attackIsStop = true;
                    SingletonMap.o_lAu.stop();
                    // console.warn('attackMonster...7');
                    return;
                }

                // 发现黑名单
                if (BlacklistMonitors.badPlayer != null) {
                    //callbackObj.writelog(`发现黑名单，优先攻击黑名单`);
                    //  console.warn('attackMonster...8');
                    return;
                }

                // 怪物消失
                if (!self.existsMonster(monster)) {
                    //callbackObj.writelog(`怪物消失，停止攻击`);
                    // clearCurMonster();
                    //   console.warn('attackMonster...9');
                    return;
                }

                // 离怪物太远
                if (!GlobalUtil.isAttackDistance(monster.currentX, monster.o_NI3)) {
                    let targetPos = GlobalUtil.getTargetPos(monster.currentX, monster.o_NI3);
                    if (targetPos) {
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                        return check();
                    }
                    else {
                        SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
						return check();
                    }
                }
                // PlayerActor.o_NLN.o_lkM();
                setCurMonster(monster);
                SingletonMap.o_OKA.o_Pc(monster, true);
                return check();
            });
        }
        return check();
    }

    disableFindWay() {
        this.stopFindWay = true;
    }

    enableFindWay() {
        this.stopFindWay = false;
    }

    existsMonsterById(monsterId) {
        let result = null;
        try {
            if (monsterId) {
                result = SingletonMap.o_OFM.o_Nx1.find(item => item.curHP > 100 && !item.masterName && item.cfgId == monsterId);
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async findWayByPos(posX, posY, type, map, monsterId) {
        console.warn(`任务${this.constructor.name} hashCode=>${this.uid} 开始寻路..x:${posX}_y:${posY}...type:${type}...map=>${map}...monsterId=>${monsterId}`);
        let self = this;
        this.findWayIsStop = false;
        function check() {
            return xSleep(500).then(function () {
                if (!mainRobotIsRunning()) {
                    self.findWayIsStop = true;
                    console.warn('停止寻路...1');
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    self.findWayIsStop = true;
                    console.warn('停止寻路...2');
                    return;
                }

                // 任务停止
                if (self.isRunning && !self.isRunning()) {
                    self.findWayIsStop = true;
                    console.warn('停止寻路...3');
                    return;
                }

                if (self.findWayIsStop) {
                    console.warn('停止寻路...3-1');
                    return;
                }

                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return;
                }

                // 发现黑名单
                if (BlacklistMonitors.badPlayer != null) {
                    //callbackObj.writelog(`发现黑名单，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    return check();
                }


                // 发现怪物
                let targetMonster = self.existsMonsterById(monsterId);
                if (targetMonster && GlobalUtil.compterFindWayPos(posX, posY).length < 7) {
                    self.findWayIsStop = true;
                    console.warn('停止寻路...3-1');
                    // PlayerActor.o_NLN.o_lkM();
                    setCurMonster(targetMonster);
                    SingletonMap.o_OKA.o_Pc(targetMonster, true);
                    return;
                }

                if (GlobalUtil.compterFindWayPos(posX, posY).length < 5) {
                    //callbackObj.writelog(`接近目标，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    self.findWayIsStop = true;
                    console.warn('停止寻路...8');
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    console.warn('停止寻路...4');
                //    return;
                //}


                // 换图了
                //if (map != SingletonMap.MapData.curMapName) {
                //    self.findWayIsStop = true;
                //    console.warn('停止寻路...7');
                //    return;
                //}

                //  死亡回城了
                //if (SingletonMap.MapData.curMapName == '神界主城') {
                //PlayerActor.o_NLN.o_lkM();
                // SingletonMap.o_lAu.stop();
                //    return check();
                //}


                if (curMonster && !self.filterMonster(curMonster)) {
                    clearCurMonster();
                }

                // 正在打怪,暂停寻路
                if (self.existsMonster(curMonster)) {
                    console.log(`锁定怪物，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    self.findWayIsStop = true;
                    // PlayerActor.o_NLN.o_lkM();
                    return;
                }

                //if (curMonster && type != 'monster') {
                // 在寻怪，普通寻路暂停
                //    return check();
                //}

                //if (PlayerActor.o_NLN.o_NWJ != true) {
                //PlayerActor.o_NLN.o_lkM();
                //SingletonMap.o_lAu.stop();
                SingletonMap.o_OKA.o_NSw(posX, posY);
                //}
                return check();
            });
        }
        return check();
    }

    filterMonster() {
        return true;
    }
}

// 神界精英怪
class ShenJieGuaJiTask extends StaticGuaJiTask {
    static name = "ShenJieGuaJiTask";

    needRun() {
        if (!mainRobotIsRunning()) return false;
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (!SingletonModelUtil.getRunningTask() && curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieMap) {
                    let arrGuaJiMap = appConfig.ShenJieMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await gotoShenJie();
            await xSleep(1000);
            if (!mainRobotIsRunning()) {
                await this.stop();
            }
            else {
                super.start();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(map) {
        try {
            await gotoShenJie();
            await xSleep(500);
            if (map) {
                SingletonMap.o_Uij.o_UX4(map[0], map[1]);
                this.curMap = map;
            }
            await xSleep(500);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster(monster) {
    }

    filterMonster(monster) {
        if (!monster) return false;
        return this.isSuperMonster(monster.cfgId);
    }
    // 高暴
    isSuperMonster(cfgId) {
        if (!(cfgId >= 169001 && cfgId <= 169005) && !(cfgId >= 167001 && cfgId <= 167045)) {
            // console.log(cfgId + '==>true');
            return true;
        }
        //console.log(cfgId + '==>false');
        return false;
    }

    getMapPointConfig(mapId) {
        let pos = shenJieCaiLiaoMonsterConfig[mapId];
        let arrPos = pos.split("|");
        let arrResult = [];
        for (var i = 0; i < arrPos.length; i++) {
            let posItem = arrPos[i].split(",");
            arrResult.push({
                x: posItem[0],
                y: posItem[1]
            });
        }
        return arrResult;
    }

    existsMonster(monster) {
        let self = this;
        let isExists = false;
        try {
            if (monster == null) {
                return false;
            }
            isExists = SingletonMap.o_OFM.o_Nx1.find(item => item.curHP > 100 && !item.masterName && item.$hashCode == monster.$hashCode && self.isSuperMonster(monster.cfgId)) != undefined;
        } catch (ex) {
            console.error(ex);
        }
        return isExists;
    }
}

// 神界普通挂机
class ShenJieGuaJiNormalTask extends StaticGuaJiTask {
    static name = "ShenJieGuaJiNormalTask";

    needRun() {
        if (!mainRobotIsRunning()) return false;
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (!SingletonModelUtil.getRunningTask() && curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieNormalMap) {
                    let arrGuaJiMap = appConfig.ShenJieNormalMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await gotoShenJie();
            await xSleep(1000);
            if (!mainRobotIsRunning()) {
                await this.stop();
            }
            else {
                super.start();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(map) {
        try {
            await gotoShenJie();
            await xSleep(500);
            if (map) {
                SingletonMap.o_Uij.o_UX4(map[0], map[1]);
                this.curMap = map;
            }
            await xSleep(500);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster(monster) {
    }

    filterMonster(monster) {
        if (!monster) return false;
        return true;
    }
    // 高暴
    isSuperMonster(cfgId) {
        if (!(cfgId >= 169001 && cfgId <= 169005) && !(cfgId >= 167001 && cfgId <= 167045)) {
            // console.log(cfgId + '==>true');
            return true;
        }
        //console.log(cfgId + '==>false');
        return false;
    }

    getMapPointConfig(mapId) {
        let pos = shenJieCaiLiaoMonsterConfig[mapId];
        let arrPos = pos.split("|");
        let arrResult = [];
        for (var i = 0; i < arrPos.length; i++) {
            let posItem = arrPos[i].split(",");
            arrResult.push({
                x: posItem[0],
                y: posItem[1]
            });
        }
        return arrResult;
    }

    existsMonster(monster) {
        let self = this;
        let isExists = false;
        try {
            if (monster == null) {
                return false;
            }
            isExists = super.existsMonster(monster);
        } catch (ex) {
            console.error(ex);
        }
        return isExists;
    }
}

// 神魔边境
class shenMoBianJinTask extends StaticGuaJiTask {
    static name = "shenMoBianJinTask";
    arrMapPosTemplate = [{ x: 68, y: 73 }, { x: 80, y: 90 }, { x: 97, y: 111 }, { x: 119, y: 115 }, { x: 141, y: 114 }, { x: 163, y: 111 }, { x: 166, y: 83 }, { x: 139, y: 69 }, { x: 123, y: 36 }];
    attackBoos = false;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && curHours == 23 && curMinutes >= 30) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.state == true) return;
            SingletonMap.o_yyA.o_yDG();
            this.initState();
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                this.attackBoos = appConfig.EnableShenMoBianJinBoss == 'TRUE' ? true : false;
                if (appConfig.EnableShenMoBianJin == "") {
                    await this.stop();
                }
                else {
                    this.hashCode = new Date().getTime();
                    await gotoShenMoBianJin();
                    super.run();
                    console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
                }
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
            SingletonMap.o_eaX.o_lKj(0, TeleportPos.NewWorldCommon); //进神魔边境
            await (500);
        } catch (ex) {
            console.error(ex);
        }
    }
    afterFindMonster() { }

    filterMonster(monster) {
        // 不打boss
        if (monster && !this.attackBoos) {
            return monster.roleName.indexOf('★') == -1;
        }
        return true;
    }

    getLiveBoss() {
        let result = null;
        try {
            for (let key in SingletonMap.o_eB9.o_yHl) {
                let bossTime = SingletonMap.o_eB9.o_yHl[key];
                if (bossTime < SingletonMap.ServerTimeManager.o_eYF) {
                    result = SingletonMap.o_O.o_yU9.find(item => item.index == key);
                    break;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async loopStartFlow() {
        let self = this;
        try {
            let pointConfig = Object.assign([], this.arrMapPosTemplate);
            while (pointConfig.length > 0) {
                this.enableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                // console.warn(`任务${this.constructor.name}  hashCode=>${this.hashCode}  pointConfig length ==> ${pointConfig.length}`);

                // 勾选了boss
                if (this.attackBoos) {
                    let liveBoss = this.getLiveBoss();
                    if (liveBoss != null) {
                        await this.findWayByPos(liveBoss.bossPos[0], liveBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        await this.hasMonster();
                    }
                }
                let minPos = pointConfig[0];
                if (pointConfig.length > 1) {
                    minPos = pointConfig.reduce(function (a, b) {
                        let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
                        let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
                        return path1.length >= path2.length ? b : a;
                    });
                }
                // 没有怪物后再寻路
                await this.hasMonster();
                //console.log(`任务${this.constructor.name}  uid=>${this.uid}  前往最近的坐标 ==> ${JSON.stringify(minPos)}`);
                await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
                //console.log(`任务${this.constructor.name}  uid=>${this.uid}  到达最近的坐标 ==> ${JSON.stringify(minPos)}`);
                // 删除已经用掉的坐标
                pointConfig = pointConfig.filter(M => {
                    return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
                });
                // 没有怪物后再离开
                await this.hasMonster();
                //callbackObj.writelog(`没有怪物了...`);

                this.disableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        if (self.loopStartFlowTtimeoutID) {
            clearTimeout(self.loopStartFlowTtimeoutID);
        }
        if (this.isRunning() && mainRobotIsRunning()) {
            self.loopStartFlowTtimeoutID = setTimeout(() => {
                console.log(`loopStartFlow....${self.constructor.name}`);
                self.loopStartFlow();
            }, 1500);
        }
        else {
            console.log(`${self.constructor.name}停止任务`);
            await this.stop();
        }
    }
}

class XinKongZhiLiTask extends StaticGuaJiTask {
    static name = "XinKongZhiLiTask";

    arrBaseXinKongBoss = ["超级男人", "雷电之子", "机械先锋"];
    arrXinKongBoss = [];
    mapPointConfigTemplate = [{ x: 61, y: 82 }, { x: 63, y: 69 }, { x: 58, y: 60 }, { x: 54, y: 43 }, { x: 37, y: 34 }, { x: 27, y: 41 }, { x: 15, y: 29 }];
    mapPointConfig = [];

    needRun() {
        if (this.isRunning()) return false;
        if (!mainRobotIsRunning()) return false;
        return true;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            this.initState();
            this.arrXinKongBoss = Object.assign([], this.arrBaseXinKongBoss); // 重置配置
            this.mapPointConfig = Object.assign([], this.mapPointConfigTemplate);
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XingKongZhiLi) {
                    let monsterNames = appConfig.XingKongZhiLi.split('|');
                    this.arrXinKongBoss = this.arrXinKongBoss.concat(monsterNames);
                    // 不打boss不走boss坐标
                    if (this.arrXinKongBoss.join(",").indexOf("绿色巨人") == -1) {
                        this.mapPointConfig.splice(3, 1);
                    }
                }
                else {// 一条都没勾选
                    this.mapPointConfig.splice(3, 1);
                }

                let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                let curHours = curDate.getHours();
                let curDay = curDate.getDate();

                let openDate = new Date(SingletonMap.o_N42.o_N4K);
                let openDay = openDate.getDate();

                if (curHours >= 10 && curHours < 23) { //跨服时段先进幻境
                    if (SingletonMap.o_Ntl.o_NxW != 1) {
                        SingletonMap.o_Ozj.o_N5I(); // 开启玛法别墅
                        await xSleep(1000);
                    }
                    await gotoMafaHuanJin();
                    // SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq); // 进入幻境
                    // await awaitToMap("玛法幻境");
                    // console.warn(`进入幻境...`);
                }
                else { // 通宵时段直接从王城进入
                    await gotoWangChen(); // 先回王城
                    await awaitToMap("王城");
                }
                this.run();
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    run() {
        try {
            QuickPickUpNormal.start();
            // this.loopCheckHp();
            this.loopCancleNoBelongMonster();
            this.loopAttackMonter();
            this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        try {
            console.warn('开始别墅loopStartFlow...');
            await this.enterMap();
            await xSleep(100);
            SingletonMap.o_lAu.stop();
            for (let i = 0; i < this.mapPointConfig.length; i++) {
                let pos = this.mapPointConfig[i];
                await this.findWayByPos(pos.x, pos.y, 'findWay', SingletonMap.MapData.curMapName);
                SingletonMap.o_lAu.stop();
                await this.hasMonster();
                QuickPickUpNormal.runPickUp();
                console.warn('没有怪物了...');
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                await xSleep(300);
            }
            await xSleep(300);
        }
        catch (ex) {
            console.error(ex);
        }
        if (this.isRunning()) {
            await this.exitMap();
            await xSleep(300);
            this.loopStartFlow();
        }
    }

    async stop() {
        //if (!this.isRunning()) return;
        try {
            MoveLock.lock();
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            await this.exitMap();
            console.warn(`${this.constructor.name}任务停止了。。。。。`);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stopTaskComplete();
        MoveLock.unLock();
    }

    async exitMap() {
        try {
            SingletonMap.o_Ozj.o_Vn();
            SingletonMap.o_N42.o_ytx();
            await xSleep(300);
            await awaitToMap("玛法幻境|王城");
            console.warn('退出都市...');
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
            //SingletonMap.o_N42.o_N5U = 8;
            //SingletonMap.o_Ozj.o_NC5(4, 2);
            //await waitMafaDuShi2Map();
            await gotoMafaVilla(4, 2);
        } catch (ex) {
            console.error(ex);
        }
    }

    getMapPointConfig() {
        return this.mapPointConfig;
    }

    afterFindMonster() { }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrXinKongBoss.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }
}

class StaticMafaBossTask extends StaticGuaJiTask {
    static name = "StaticMafaBossTask";
    state = false;
    arrBossPosTemplate = [];
    arrBossName = [];
    arrBossPos = [];
    diePos = null;
    curMapName = null;
    nextRunTime = null;
    bossId = null;

    bosIsAlive() {
        let result = false;
        try {
            let bossTime = villaMafaBossTime[this.bossId];
            if (!bossTime) {
                result = true;
            }
            else {
                let M = Math.floor((bossTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                result = M <= 0 ? true : false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    updateNextRunTime(time) {
        this.nextRunTime = time;
        let nextRunDateStr = new Date(this.nextRunTime).toLocaleString();
        console.warn(`任务${this.constructor.name}下次执行时间${nextRunDateStr}`);
    }

    needRun() {
        if (this.isRunning()) return false;
        let result = true;
        try {
            if (this.bossId != null) {
                result = this.bosIsAlive();
            }
            else if (this.nextRunTime != null) {
                let M = Math.floor((this.nextRunTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                result = M <= 0 ? true : false;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curDay = curDate.getDate();

            let openDate = new Date(SingletonMap.o_N42.o_N4K);
            let openDay = openDate.getDate();

            await gotoWangChen(); // 先回王城
            await awaitToMap("王城");
            if (curHours >= 10 && curHours < 23) {
                if (SingletonMap.o_Ntl.o_NxW != 1) {
                    SingletonMap.o_Ozj.o_CL(); // 开启玛法别墅
                    console.warn(`开启别墅...UID=${this.uid}`);
                    await xSleep(2000);
                }
                //SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq); // 进入幻境
                await gotoMafaHuanJin();
                //await awaitToMap("玛法幻境");
                //console.warn(`进入幻境...UID=${this.uid}`);
            }

            if (this.arrBossPosTemplate != null) {
                this.arrBossPos = Object.assign([], this.arrBossPosTemplate);
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                this.loopAttackMonter();
                this.run();
                console.warn(`${this.constructor.name}任务开始了。。。。。UID=${this.uid}`);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
            await this.enterMap();
            await awaitLeaveMap('玛法幻境');
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;
            // console.log("this.curMapName=>" + this.curMapName);
            // console.log("this.arrBossName=>" + this.arrBossName.join(','));
            while (this.arrBossPos.length > 0) {
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                await this.waitBossKill();
                let bossPos = Object.assign({}, this.arrBossPos[0]);
                await this.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
                await this.waitBossKill();
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                await xSleep(500);
                this.arrBossPos.splice(0, 1);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    async waitBossKill() {
        let self = this;
        function check() {
            return xSleep(1500).then(function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    return;
                //}
                if (self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }

    async stop() {
        if (!this.isRunning()) return;
        try {
            this.diePos = null;
            this.curMapName = null;
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            await this.exitMap();
            console.warn(`${this.constructor.name}任务停止了。。。。。`);
            // let taskQueueManage = SingletonModelUtil.getInstance(TaskQueueManage);
            // taskQueueManage.removeTask(this.constructor.name);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stopTaskComplete();
    }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossName.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async enterMap() {
        await xSleep(1000);
    }


    async exitMap() {
        try {
            SingletonMap.o_Ozj.o_Vn();
            SingletonMap.o_N42.o_ytx();
            await gotoWangChen();
            // await awaitToMap("玛法幻境|王城");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

class villaDuShiBossTask extends StaticMafaBossTask {
    bossId = 157115;
    static name = "villaDuShiBossTask";
    arrBossPosTemplate = [{ x: 53, y: 48 }, { x: 9, y: 27 }];
    arrBossName = ['绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 8;
            SingletonMap.o_Ozj.o_NC5(4, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaDuShiStar1Task extends StaticMafaBossTask {
    static name = "villaDuShiStar1Task";
    arrBossPosTemplate = [{ x: 22, y: 74 }, { x: 42, y: 54 }, { x: 35, y: 38 }, { x: 49, y: 26 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 7;
            SingletonMap.o_Ozj.o_NC5(4, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaDuShiStar2Task extends StaticMafaBossTask {
    static name = "villaDuShiStar2Task";
    arrBossPosTemplate = [{ x: 28, y: 71 }, { x: 44, y: 55 }, { x: 47, y: 66 }, { x: 56, y: 75 }, { x: 56, y: 87 }, { x: 77, y: 69 }, { x: 86, y: 66 }, { x: 72, y: 57 }, { x: 34, y: 42 }, { x: 47, y: 27 }, { x: 28, y: 28 }, { x: 4, y: 23 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 8;
            SingletonMap.o_Ozj.o_NC5(4, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaHuoLongBossTask extends StaticMafaBossTask {
    bossId = 157030;
    static name = "villaHuoLongBossTask";
    arrBossPosTemplate = [{ x: 59, y: 19 }, { x: 13, y: 13 }, { x: 9, y: 21 }];
    arrBossName = ['火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 6;
            SingletonMap.o_Ozj.o_NC5(3, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaHuoLongStar2Task extends StaticMafaBossTask {
    static name = "villaHuoLongStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 43 }, { x: 50, y: 32 }, { x: 56, y: 22 }, { x: 63, y: 30 }, { x: 47, y: 18 }, { x: 51, y: 67 }, { x: 62, y: 71 }, { x: 71, y: 55 }, { x: 11, y: 43 }, { x: 17, y: 20 }, { x: 25, y: 8 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 6;
            SingletonMap.o_Ozj.o_NC5(3, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaHuoLongStar1Task extends StaticMafaBossTask {
    static name = "villaHuoLongStar1Task";
    arrBossPosTemplate = [{ x: 107, y: 219 }, { x: 137, y: 179 }, { x: 82, y: 113 }, { x: 133, y: 100 }, { x: 57, y: 63 }, { x: 57, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 5;
            SingletonMap.o_Ozj.o_NC5(3, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaMoLongBossTask extends StaticMafaBossTask {
    bossId = 157020;
    static name = "villaMoLongBossTask";
    arrBossPosTemplate = [{ x: 86, y: 40 }, { x: 63, y: 50 }, { x: 71, y: 39 }];
    arrBossName = ['‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 4;
            SingletonMap.o_Ozj.o_NC5(2, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaMoLongStar1Task extends StaticMafaBossTask {
    static name = "villaMoLongStar1Task";
    arrBossPosTemplate = [{ x: 39, y: 222 }, { x: 52, y: 180 }, { x: 76, y: 106 }, { x: 88, y: 55 }, { x: 88, y: 21 }, { x: 144, y: 24 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 3;
            SingletonMap.o_Ozj.o_NC5(2, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaMoLongStar2Task extends StaticMafaBossTask {
    static name = "villaMoLongStar2Task";
    arrBossPosTemplate = [{ x: 17, y: 126 }, { x: 36, y: 139 }, { x: 55, y: 114 }, { x: 44, y: 97 }, { x: 53, y: 90 }, { x: 31, y: 86 }, { x: 53, y: 65 }, { x: 67, y: 62 }, { x: 82, y: 41 }, { x: 98, y: 26 }, { x: 103, y: 45 }, { x: 83, y: 71 }, { x: 98, y: 98 }, { x: 109, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 4;
            SingletonMap.o_Ozj.o_NC5(2, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaChiYueBossTask extends StaticMafaBossTask {
    bossId = 157010;
    static name = "villaChiYueBossTask";
    arrBossPosTemplate = [{ x: 81, y: 131 }, { x: 53, y: 64 }, { x: 52, y: 45 }, { x: 37, y: 45 }];
    arrBossName = ['绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 2;
            SingletonMap.o_Ozj.o_NC5(1, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaChiYueStar2Task extends StaticMafaBossTask {
    static name = "villaChiYueStar2Task";
    arrBossPosTemplate = [{ x: 22, y: 108 }, { x: 36, y: 112 }, { x: 66, y: 118 }, { x: 67, y: 140 }, { x: 88, y: 133 }, { x: 94, y: 106 }, { x: 96, y: 84 }, { x: 91, y: 46 }, { x: 66, y: 59 }, { x: 49, y: 63 }, { x: 75, y: 81 }, { x: 20, y: 60 }, { x: 23, y: 20 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 2;
            SingletonMap.o_Ozj.o_NC5(1, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaChiYueStar1Task extends StaticMafaBossTask {
    static name = "villaChiYueStar1Task";
    arrBossPosTemplate = [{ x: 19, y: 27 }, { x: 83, y: 21 }, { x: 89, y: 45 }, { x: 78, y: 135 }, { x: 52, y: 98 }, { x: 65, y: 84 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 1;
            SingletonMap.o_Ozj.o_NC5(1, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}


class villaXianLingBossTask extends StaticMafaBossTask {
    bossId = 157215;
    static name = "villaXianLingBossTask";
    arrBossPosTemplate = [{ x: 66, y: 120 }, { x: 52, y: 54 }];
    arrBossName = ['‖神王‖★★持柱天王★', '‖大罗‖★★十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 10;
            SingletonMap.o_Ozj.o_NC5(5, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaXianLingStar2Task extends StaticMafaBossTask {
    static name = "villaXianLingStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 62 }, { x: 67, y: 59 }, { x: 39, y: 38 }, { x: 6, y: 73 }, { x: 8, y: 79 }, { x: 12, y: 84 }, { x: 35, y: 95 }, { x: 40, y: 130 }, { x: 24, y: 156 }, { x: 49, y: 161 }, { x: 62, y: 154 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 10;
            SingletonMap.o_Ozj.o_NC5(5, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaXianLingStar1Task extends StaticMafaBossTask {
    static name = "villaXianLingStar1Task";
    arrBossPosTemplate = [{ x: 24, y: 93 }, { x: 41, y: 118 }, { x: 29, y: 140 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 9;
            SingletonMap.o_Ozj.o_NC5(5, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaShenLongZhuanZhuTask extends StaticGuaJiTask {
    static name = 'villaShenLongZhuanZhuTask';
    state = false;
    diePos = null;
    curMapName = null;
    arrBossPos = [];
    arrBossId = [];

    needRun() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
            }
            if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1)) {
                result = true;
            }
            else {
                if (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1 && SingletonMap.o_eB9.o_N3B[22] < SingletonMap.ServerTimeManager.o_eYF) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }

        if (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1) { // 庄主
            if (SingletonMap.o_eB9.o_N3B[22] < SingletonMap.ServerTimeManager.o_eYF) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[21].bossPos[0],
                    y: SingletonMap.o_O.o_N38[21].bossPos[1],
                    index: 21
                });
            }
            this.arrBossId.push(SingletonMap.o_O.o_N38[21].bossId);
        }
    }

    async start() {
        try {
            if (this.state == true) return;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            await gotoWangChen(); // 先回王城
            if (curHours >= 10 && curHours < 23) {
                if (SingletonMap.o_eB9.o_N3F == 0) { // 开启神龙别墅
                    SingletonMap.o_wB.o_CL();
                    await xSleep(1000);
                }
                await gotoCanYueDao(); // 进入苍月岛
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                this.loopAttackMonter();
                this.run();
                console.log(`${this.constructor.name}任务开始了。。。。。`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
            await this.enterMap();
            await xSleep(1000);
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;

            this.init();
            while (this.arrBossPos.length > 0) {
                let bossPos = Object.assign({}, this.arrBossPos[0]);
                if (SingletonMap.o_eB9.o_N3B[bossPos.index + 1] < SingletonMap.ServerTimeManager.o_eYF) {
                    await this.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
                    await xSleep(500);
                    await this.waitBossKill();
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    await xSleep(500);
                }
                this.arrBossPos.splice(0, 1);
            }
            await xSleep(500);
            await this.stop();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async stop() {
        //if (!this.isRunning()) return;
        try {
            this.diePos = null;
            this.curMapName = null;
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            await this.exitMap();
            this.state = false;
            console.log(`任务停止了${this.constructor.name}。。。。。`);
            // let taskQueueManage = SingletonModelUtil.getInstance(TaskQueueManage);
            // taskQueueManage.removeTask(this.constructor.name);
        }
        catch (ex) {
            console.warn(ex);
        }
        await this.stopTaskComplete();
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    async enterMap() {
        try {
            SingletonMap.o_wB.o_N5o();
            await awaitToMap("神龙别墅");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            gameObjectInstMap.ZjmTaskConView.o_lmT();
            await awaitToMap("苍月岛");
            await gotoWangChen();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async waitBossKill() {
        let self = this;
        function check() {
            return xSleep(1500).then(function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}
                if (self.isRunning() && self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null && self.diePos == null) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }


    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossId.find(item => monster.cfgId == item && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            await this.findWayByPos(this.diePos.x, this.diePos.y);
            this.diePos = null;
        } catch (ex) {
            console.error(ex);
        }
    }
}

class villaShenLongWangTask extends villaShenLongZhuanZhuTask {
    static name = 'villaShenLongWangTask';

    needRun() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
            }

            // 第一次进入
            if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1)) {
                result = true;
            }
            else {
                if (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1) {
                    for (let i = 18; i < 22; i++) {
                        if (SingletonMap.o_eB9.o_N3B[i] < SingletonMap.ServerTimeManager.o_eYF) {
                            result = true;
                            break;
                        }
                    }
                }

            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }

        if (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1) {
            for (let i = 17; i < 21; i++) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[i].bossPos[0],
                    y: SingletonMap.o_O.o_N38[i].bossPos[1],
                    index: i
                });
                this.arrBossId.push(SingletonMap.o_O.o_N38[i].bossId);
            }
        }
    }
}


class villaShenLongStarTask extends villaShenLongZhuanZhuTask {
    static name = 'villaShenLongStarTask';

    needRun() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
            }

            // 第一次进入
            if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1)) {
                result = true;
            }
            else {

                if (appConfig.VillaBoss.indexOf('villaShenLongStarTask') > -1) {
                    for (let i = 0; i < 18; i++) {
                        if (SingletonMap.o_eB9.o_N3B[i + 1] < SingletonMap.ServerTimeManager.o_eYF) {
                            result = true;
                            break;
                        }
                    }
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }
        if (appConfig.VillaBoss.indexOf('villaShenLongStarTask') > -1) {
            for (let i = 0; i < 17; i++) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[i].bossPos[0],
                    y: SingletonMap.o_O.o_N38[i].bossPos[1],
                    index: i
                });
                this.arrBossId.push(SingletonMap.o_O.o_N38[i].bossId);
            }
        }
    }
}

// 天界boss
class tianJieMatchGuaJiTask extends StaticGuaJiTask {
    static name = "tianJieMatchGuaJiTask";

    constructor() {
        try {
            super();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.TianJieMap) {
                    let arrGuaJiMap = appConfig.TianJieMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await gotoWangChen(); // 先回王城
            this.hashCode = new Date().getTime();
            this.enableParams();
            this.run();
            console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
        } catch (ex) {
            console.error(ex);
        }
    }

    filterMonster() {
        return true;
    }
    async enterMap(curMap) {
        try {
            if (curMap[0].indexOf('kf') != -1) {
                let mapId = curMap[0].replace('kf', '');
                SingletonMap.o_Yr.o_NZ8(KfActivity.XJOUTDOORBOSS, mapId, curMap[1]);
                await checkServerCrossMap();
                await xSleep(1000);
            }
            else {
                if (SingletonMap.ServerCross.o_ePe) {
                    SingletonMap.o_Yr.o_NZ8();
                    await checkLocalServeMap();
                }
                SingletonMap.Boss2Sys.o_OV4(curMap[0], curMap[1]);
                SingletonMap.Boss2Sys.o_NqN(curMap[0]);
                await xSleep(1000);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            SingletonMap.o_lHL.o_OVF();
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster() {
    }

    getBossCount(map) {
		return SingletonMap.o_eB9.o_NI2(map[0], SingletonMap.MapData.mapId)
        // return SingletonMap.o_eB9.o_XI;
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                let map = this.arrGuaJiMap[i].split(',');
                //  跨服地图，并且非跨服时段
                if (isCrossMap(map[0]) && !isCrossTime()) {
                    continue;
                }
                console.warn('进入地图=>' + map);
				console.warn('开始挂机.....怪物数量=>' + this.getBossCount(map));
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                while (this.getBossCount(map) >= 7 || self.findMonter()) { // 少于7只换图
				console.warn('开始挂机.....怪物数量=>' + this.getBossCount(map));
                    await this.hasMonster();
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    //if (!SingletonMap.o_lAu.o_lcD) {
                    //    SingletonMap.o_lAu.start();
                    //}
                    await this.hasMonster();
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    await xSleep(1000);
                    await this.enterMap(map);
                    await xSleep(1000);
                }
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        if (this.isRunning() && mainRobotIsRunning()) {
            console.log(`loopStartFlow....${this.constructor.name}`);
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }

    needRun() {
        if (this.isRunning()) return false;
        return true;
    }
}

// 野外boss
class YeWaiGuaJiTask extends tianJieMatchGuaJiTask {
    static name = "YeWaiGuaJiTask";

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.YeWaiBoss) {
                    let arrGuaJiMap = appConfig.YeWaiBoss.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await gotoWangChen(); // 先回王城
            this.run();
            console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
        } catch (ex) {
            console.error(ex);
        }
    }

    async refreshBossCount() {
        try {
            let bossMap = SingletonMap.BossProvider.o_Ncc();
            for (var i = 0; i < bossMap.length; i++) {
                SingletonMap.o_wB.o_lyp(bossMap[i].id);
                xSleep(100);
            }
        }
        catch (ex) {
        }
    }

    async enterMap(curMap) {
        try {
            this.refreshBossCount();
            if (curMap[0].indexOf('kf') != -1) {
                let mapId = curMap[0].replace('kf', '');
                SingletonMap.o_Yr.o_NZ8(KfActivity.o_NdN, mapId, curMap[1]);
                await checkServerCrossMap();
                await xSleep(1000);
            }
            else {
                if (SingletonMap.ServerCross.o_ePe) {
                    SingletonMap.o_Yr.o_NZ8();
                    await checkLocalServeMap();
                }
                SingletonMap.o_wB.o_l3N(curMap[0], curMap[1])
                await xSleep(1000);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }
    filterMonster() {
        return true;
    }
    afterFindMonster() {
    }

    getBossCount(curMap) {
        try {
            let mapInfo = Object.values(SingletonMap.o_eB9.o_eYr).find(M => Object.keys(M).find(C => C == SingletonMap.MapData.sceneId));
            //let mapId = curMap[0].replace('kf', '');
            if (mapInfo && mapInfo[SingletonMap.MapData.sceneId] != undefined) {
                return mapInfo[SingletonMap.MapData.sceneId];
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return 10;
    }

}

class xiYouMatchGuaJiTask extends StaticGuaJiTask {
    static name = 'xiYouMatchGuaJiTask';
    state = false;
    diePos = null;
    curMapName = null;

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (!SingletonModelUtil.getRunningTask() && curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouMap) {
                this.arrGuaJiMap = appConfig.XiYouMap.split('|');
            }
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                this.loopAttackMonter();
                this.init();
                this.hashCode = new Date().getTime();
                this.enableParams();
                await gotoXiYouZhuChen();
                this.run();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster() { }

    filterMonster() {
        return true;
    }

    async enterMap(map) {
        try {
            await gotoXiYouZhuChen();
            if (map) {
                SingletonMap.o_yKf.o_yda(map[0], map[1]);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                this.disableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                let map = this.arrGuaJiMap[i].split(',');
                console.warn('进入地图=>' + map);
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                while (SingletonMap.o_eB9.o_ysG > 7) {
                    this.enableFindWay();
                    await this.hasMonster();
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
                    await this.hasMonster();
                    await this.hasBadPlayer();
                    this.disableFindWay();
                    clearCurMonster();
                    await xSleep(1000);
                    useShuiJiProperty();
                    await xSleep(1000);
                }
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1500);
        if (this.isRunning() && mainRobotIsRunning()) {
            console.log(`loopStartFlow....${this.constructor.name}`);
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }
}

class xiyouJiuChongTianGuaJiBoss extends StaticGuaJiTask {
    static name = "xiyouJiuChongTianGuaJiBoss";
    curRefreshBoss = null;

    refrehBossTime() {
        try {
            const arrMapIds = SingletonMap.o_O.o_yd4.reduce((result, cur) => {
                let arrMap = cur.Layer.map((layer, idx) => {
                    return layer.mapId;
                });
                result = result.concat(arrMap);
                return result;
            }, []);
            arrMapIds.forEach(async (mapId) => {
                SingletonMap.o_yKf.o_ydb(mapId);
                await xSleep(100);
            });
        }
        catch (e) {
            console.error(e);
        }
    }

    getBossMapInfo(bossId) {
        let bossMapInfo = null;
        try {
            for (let i = 0; i < SingletonMap.o_O.o_yd4.length; i++) {
                let bossInfo = SingletonMap.o_O.o_yd4[i];
                let arrLayer = bossInfo.Layer;
                let bossLayer = arrLayer.find((item) => {
                    return item.bossList.includes(bossId);
                });
                if (bossLayer) {
                    let bossIndex = bossLayer.bossList.findIndex(item => item == bossId);
                    bossMapInfo = {
                        id: bossLayer.id,
                        layer: bossLayer.layer,
                        mapId: bossLayer.mapId,
                        bossId: bossId,
                        bossPos: bossLayer.bossPosList[bossIndex]
                    }
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return bossMapInfo;
    }

    getBossRefreshTime(mapId, bossId) {
        if (SingletonMap.o_eB9.o_ysJ[mapId]) {
            return SingletonMap.o_eB9.o_ysJ[mapId][bossId];
        }
        return null;
    }

    getRefreshBoss() {
        let boss = null;
        if (this.arrGuaJiMap) {
            // 先找已经刷新的
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                try {
                    let bossId = parseInt(this.arrGuaJiMap[i]);
                    if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                        let bossMapInfo = this.getBossMapInfo(bossId);
                        let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.mapId, bossId);
                        if (bossRefreshTime == 0) { // 已经刷新
                            boss = bossMapInfo;
                            break;
                        }
                    }
                }
                catch (e) {
                    console.error(e);
                }
            }

            // 没有已经刷新的，接着找60秒内刷新的
            if (boss == null) {
                for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                    try {
                        let bossId = parseInt(this.arrGuaJiMap[i]);
                        if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                            let bossMapInfo = this.getBossMapInfo(bossId);
                            let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.mapId, bossId);
                            if (bossRefreshTime == null) {
                                continue;
                            }
                            if (bossRefreshTime == 0 || bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) { // 已经刷新或30秒后刷新
                                boss = bossMapInfo;
                                break;
                            }
                        }
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
            }
        }
        return boss;
    }

    needRun() {
        if (this.isRunning()) return false;
        this.init();
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_ysJ)) {
                    result = true;
                }
                else if (this.getRefreshBoss() != null) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouJiuChongTianBoss) {
                this.arrGuaJiMap = appConfig.XiYouJiuChongTianBoss.split('|');
            }
        }
    }

    run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.init();
                this.hashCode = new Date().getTime();
                await gotoXiYouZhuChen();
                this.run();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(map) {
        try {
            await gotoXiYouZhuChen();
            if (map) {
                SingletonMap.o_yKf.o_yda(map[0], map[1]);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            // await gotoWangChen();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        //if (this.arrGuaJiMap.includes(monster.cfgId + "") && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) { // 没归属或归属是自己
        //    return true;
        //}
        //else if (this.arrGuaJiMap.includes(monster.cfgId + "") && BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属是黑名单成员
        //    return true;
        //}
        return true;
    }

    async backToMap() {
        try {
            console.warn('回到boss地图...');
            let curRefreshBoss = this.curRefreshBoss || this.getRefreshBoss();
            if (curRefreshBoss) {
                if (curRefreshBoss.id && curRefreshBoss.layer) {
                    await this.enterMap([curRefreshBoss.id, curRefreshBoss.layer]);
                    await xSleep(300);
                }
                if (curRefreshBoss && curRefreshBoss.bossPos) {
                    await this.findWayByPos(curRefreshBoss.bossPos[0], curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        this.diePos = null;
        // await this.stop();
    }

    async loopStartFlow() {
        try {
            await this.refrehBossTime();
            await xSleep(2000);
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                // 进入boss地图
                await this.enterMap([this.curRefreshBoss.id, this.curRefreshBoss.layer]);
                await xSleep(100);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                // 前往boss坐标
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                // await xSleep(100);
                await this.killBoss(this.curRefreshBoss);
                await xSleep(1000);
                await gotoXiYouZhuChen();
                this.refrehBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    async killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(30).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
				
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    // console.warn('角色死亡...');
                    return check();
                }

                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length > 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    return check();
                }

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    setCurMonster(BlacklistMonitors.badPlayer);
                    //self.attackMonster(BlacklistMonitors.badPlayer);
                    SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    return check();
                }

                // 被打飞了
                if (SingletonMap.MapData.curMapName == '西游降魔') {
                    await self.enterMap([refreshBoss.id, refreshBoss.layer]);
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    console.warn('jiuchongtiang  killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        bossNextWatchTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                    else {
                        return check();
                    }
                }
                // 刷新boss时间
                self.refrehBossTime();
                let bossRefreshTime = SingletonMap.o_eB9.o_ysJ[refreshBoss.mapId][refreshBoss.bossId];
                // 刷新时间小于30秒，等待刷新
                if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
                    return check();
                }
                // 刷新时间大于30秒
                if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
                    // console.log('killBoss...2.');
                    return;
                }
                return check();
            });
        }
        return check();
    }
}

// 西游天牢boss
class XiyouTianLaoBossTask extends StaticGuaJiTask {
    static name = "XiyouTianLaoBossTask";
    curRefreshBoss = null;

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_ySw)) {
                    result = true;
                }
                else if (this.getRefreshBoss() != null) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiyouTianLaoBoss) {
                this.arrGuaJiMap = appConfig.XiyouTianLaoBoss.split('|');
            }
            else {
                this.arrGuaJiMap = "";
            }
        }
        else {
            this.arrGuaJiMap = "";
        }
    }

    getBossMapInfo(bossId) {
        let bossMapInfo = null;
        try {
            for (let i = 0; i < SingletonMap.o_O.o_ykE.length; i++) {
                let layerInfo = SingletonMap.o_O.o_ykE[i];
                let arrLayerBoss = layerInfo.boss;
                let bossLayer = arrLayerBoss.find((item) => {
                    return item.bossId == bossId;
                });
                if (bossLayer) {
                    bossMapInfo = {
                        index: bossLayer.index,
                        layer: layerInfo.layer,
                        mapId: layerInfo.mapId,
                        bossId: bossId,
                        bossPos: bossLayer.bossPos,
                        bossLayer: bossLayer
                    }
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return bossMapInfo;
    }

    getBossRefreshTime(layerId, bossIndex) {
        try {
            if (SingletonMap.o_eB9.o_ySw[layerId]) {
                return SingletonMap.o_eB9.o_ySw[layerId][bossIndex];
            }
        }
        catch (e) {
            console.error(e);
        }
        return null;
    }

    getRefreshBoss() {
        this.init();
        let boss = null;
        if (this.arrGuaJiMap) {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                try {
                    let bossId = parseInt(this.arrGuaJiMap[i]);
                    if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                        let bossMapInfo = this.getBossMapInfo(bossId);
                        let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.layer, bossMapInfo.index);
                        if (bossRefreshTime == 0 && bossMapInfo.bossLayer.cost.count <= SingletonMap.RoleData.o_eNf) { // 已经刷新
                            boss = bossMapInfo;
                            break;
                        }
                    }
                }
                catch (e) {
                    console.error(e);
                }
            }
        }
        return boss;
    }

    refrehBossTime() {
        try {
            const arrMapIds = SingletonMap.o_O.o_ykE.map(M => M.layer);
            arrMapIds.forEach(async (mapId) => {
                SingletonMap.o_yKf.o_yZe(mapId);
                await xSleep(300);
            });
        }
        catch (e) {
            console.error(e);
        }
    }

    run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.init();
                this.hashCode = new Date().getTime();
                await gotoXiYouZhuChen();
                this.run();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(layer) {
        try {
            await gotoXiYouZhuChen();
            if (layer) {
                SingletonMap.o_yKf.o_yZ5(layer);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            // await gotoWangChen();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        try {
            await gotoXiYouZhuChen();
            await this.refrehBossTime();
            await xSleep(1000);
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                // 进入boss地图
                await this.enterMap(this.curRefreshBoss.layer);
                await xSleep(1500);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                // 前往boss坐标
                const curMapName = SingletonMap.MapData.curMapName;
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', curMapName);
                await xSleep(100);
                await this.killBoss(this.curRefreshBoss);
                await xSleep(1500);
                await gotoXiYouZhuChen();
                this.refrehBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        return true;
    }

    async backToMap() {
        try {
            SingletonMap.o_eaX.o_lKj(0, TeleportPos.XiYouArea);
            if (this.curRefreshBoss) {
                await this.enterMap(this.curRefreshBoss.layer);
                await xSleep(500);
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }

    async killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(200).then(function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                // }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    return check();
                }
                if (SingletonMap.MapData.curMapName == '西游降魔') {
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                    setCurMonster(monster);
                    self.attackMonster(monster);
                    return check();
                }
                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    // console.log('killBoss...1.');
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        bossNextWatchTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                    else {
                        return check();
                    }
                }
                let bossRefreshTime = self.getBossRefreshTime(refreshBoss.layer, refreshBoss.index);
                // 刷新时间小于30秒，等待刷新
                if (bossRefreshTime && bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    return check();
                }
                // 刷新时间大于30秒
                if (bossRefreshTime && bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 30000) {
                    // console.log('killBoss...2.');
                    return;
                }
                return check();
            });
        }
        return check();
    }
}


// 个人经验boss
class BossExpTask extends StaticGuaJiTask {
    static name = 'BossExpTask';
    state = false;

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (SingletonMap.RoleData.o_yOG > 150 * 10000 && isFreeTime() && !this.isRunning() && SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE) < SingletonMap.BossProvider.o_lG4().dailyCount) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.state = true;
            let curCount = SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE);
            let dailyCount = SingletonMap.BossProvider.o_lG4().dailyCount;
            while (curCount < dailyCount) {
                await gotoWangChen();
                if (SingletonMap.MapData.curMapName == "王城") {
                    await xSleep(2000);
                    SingletonMap.o_n.o_EG(BossWin, [1, 2]);
                    await xSleep(2000);
                    SingletonMap.o_wB.o_OtG(gameObjectInstMap.BossExpView.curCfg.index, 1);
                    await xSleep(2000);
                }
                await awaitToMap("王城");
                if (!mainRobotIsRunning()) {
                    break;
                }
                if (!this.isRunning()) {
                    break;
                }
                curCount = SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE);
                await xSleep(1000);
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        gameObjectInstMap.BossExpView && SingletonMap.o_n.o_eAN(gameObjectInstMap.BossExpView);
        await xSleep(1000);
        await super.stop();
    }
}

// 行会boss
class GuildBossTask extends StaticGuaJiTask {
    static name = 'GuildBossTask';
    state = false;

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (isFreeTime() && !this.isRunning()
                && SingletonMap.o_OTs.o_OO3().guildBossDayChallengeNum - SingletonMap.GuildDataMgr.o_lYO > 0) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    killBoss() {
        let self = this;
        function check() {
            return xSleep(50).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}
                // 怪物消失-捡东西-退出
                if (SingletonMap.o_O.o_lLU.filter(B => SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == B.bossId)).length == 0) {
                    // console.warn('击败行会boss.......');
                    SingletonMap.o_lAu.start();
                    if (!bossNextWatchTime[GuildBossTask.name]) {
                        // console.warn('击败行会boss.......1');
                        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                        let nextWatchTime = curDate.getTime() + 20 * 1000; // 20秒后
                        bossNextWatchTime[GuildBossTask.name] = nextWatchTime;
                        return check();
                    }
                    else if (bossNextWatchTime[GuildBossTask.name] <= SingletonMap.ServerTimeManager.o_eYF) { // 20秒已到
                        // console.warn('击败行会boss.......2');
                        return;
                    }
                    else if (SingletonMap.MapData.curMapName == "王城") {
                        await xSleep(1000);
                        return;
                    }
                    else {
                        return check();
                    }
                }
                if (SingletonMap.MapData.curMapName == "王城") {
                    await xSleep(1000);
                    return;
                }
                SingletonMap.o_lAu.start();
                return check();
            });
        }
        return check();
    }

    async start() {
        try {
            if (this.isRunning()) return;
            await gotoWangChen();
            this.state = true;
            bossNextWatchTime[GuildBossTask.name] = null;
            if (SingletonMap.o_OTs.o_OO3().guildBossDayChallengeNum - SingletonMap.GuildDataMgr.o_lYO > 0) {
                if (mainRobotIsRunning() && this.isRunning()) {
                    SingletonMap.o_lWu.o_lnz();
                    await xSleep(2000);
                    await this.killBoss();
                }
                await xSleep(1000);
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        if (SingletonMap.MapData.curMapName != "王城") {
            gameObjectInstMap.ZjmTaskConView && gameObjectInstMap.ZjmTaskConView.o_lmT();
        }
        await xSleep(1000);
        closeGameWin("GuildWin");
        await super.stop();
    }
}

class MineTask extends StaticGuaJiTask {
    static name = 'MineTask';
    state = false;

    needRun() {
        let result = false;
        try {
            if (isFreeTime() && !this.isRunning()
                && SingletonMap.o_e_w.o_NgZ > 0 && !SingletonMap.o_e_w.o_Nnz()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            let mineLevel = 1;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.MineLevel) {
                    mineLevel = parseInt(appConfig.MineLevel);
                }
            }
            await gotoWangChen();
            this.state = true;
            SingletonMap.o_OzT.o_Nxc(); //进入矿洞
            await awaitToMap("黑暗矿洞");
            await xSleep(500);
            // 有奖励先领
            if (SingletonMap.o_e_w.o_eew()) {
                SingletonMap.o_OzT.o_eIR();
                await xSleep(100);
            }
            await this.foundMine();
            await xSleep(1000);
            if (SingletonMap.o_n.isOpen("MineChooseWin")) {
                while (mineLevel > SingletonMap.o_e_w.o_bN && totalBagItemByName("矿工令") > 0) {
                    await xSleep(300);
                    SingletonMap.o_OzT.o_Nsf(0);
                }
                await xSleep(300);
                SingletonMap.o_n.isOpen("MineChooseWin").o_N0D();
                await xSleep(200);
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    foundMine() {
        let self = this;
        function check() {
            return xSleep(500).then(function () {
                // 辅助停止
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                if (SingletonMap.o_n.isOpen("MineChooseWin")) {
                    return;
                }
                SingletonMap.o_e_w.o_eAo(); // 找矿
                return check();
            });
        }
        return check();
    }

    async stop() {
        //if (!this.isRunning()) return;
        gameObjectInstMap.ZjmTaskConView && gameObjectInstMap.ZjmTaskConView.o_lmT();
        await xSleep(1000);
        await super.stop();
    }
}

// 神界五行源地boss
class WuXingBossTask extends StaticGuaJiTask {
    static name = 'WuXingBossTask';
    state = false;
    curLayer = -1;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                if (this.getBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
        }
        return result;
    }

    getBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieWuXingBoss != "") {
                    let arrShenJieWuXingBoss = appConfig.ShenJieWuXingBoss.split("|");
                    for (let i = 0; i < arrShenJieWuXingBoss.length; i++) {
                        let layer = arrShenJieWuXingBoss[i];
                        for (let key in SingletonMap.o_UuC.bossData.o_UaE) {
                            let arrBossData = SingletonMap.o_UuC.bossData.o_UaE[key];
                            boss = arrBossData.find(M => M.time == 0 && layer == M.layer + ""
                                && (!bossNextWatchTime[M.bossId] || bossNextWatchTime[M.bossId] <= SingletonMap.ServerTimeManager.o_eYF));
                            if (boss) {
                                break;
                            }
                        }
                        if (boss) {
                            let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == boss.layer);
                            let bossCfg = layerBoss.boss[0];
                            if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                                boss = null;
                            }
                            else {
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    hasBossCost(bossId) {
        let result = false;
        try {
            let boss = null;
            for (let key in SingletonMap.o_UuC.bossData.o_UaE) {
                let arrBossData = SingletonMap.o_UuC.bossData.o_UaE[key];
                boss = arrBossData.find(M => M.bossId == bossId);
                if (boss) {
                    break;
                }
            }
            if (boss) {
                let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == boss.layer);
                let bossCfg = layerBoss.boss[0];
                if (SingletonMap.RoleData.o_UPf > bossCfg.cost.count) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async backToMap() {
        try {
            await this.enterMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
            SingletonMap.o_Uaq.o_UaV(this.curLayer);
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    initState() {
        this.curLayer = -1;
        super.initState();
    }

    getBossCfg(layer, bossId) {
        let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == layer);
        return layerBoss.boss.find(M => M.bossIds.includes(bossId));
    }

    run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.hashCode = new Date().getTime();
                await gotoShenJie();
                this.run();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        try {
            let curBoss = this.getBoss();
            while (curBoss != null) {
                if (this.curLayer != curBoss.layer) {
                    this.curLayer = curBoss.layer;
                    SingletonMap.o_Uaq.o_UaV(curBoss.layer);
                }
                await xSleep(1500);
                let bossCfg = this.getBossCfg(curBoss.layer, curBoss.bossId);
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                if (bossCfg) {
                    // 九幽值不够了
                    if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                        break;
                    }
                    SingletonMap.o_lAu.stop();
                    clearCurMonster();
                    await this.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    if (!mainRobotIsRunning() || !this.isRunning()) {
                        break;
                    }
                    await this.killBoss(curBoss.bossId);
                }
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                await xSleep(1000);
                curBoss = this.getBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async killBoss(bossId) {
        let self = this;
        function check() {
            return xSleep(20).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}

                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }

                let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                if (!monster) {
                    return;
                }

                try {
                    let curBoss = self.getBoss();
                    if (!curBoss) {
                        return;
                    }
                    let bossCfg = self.getBossCfg(curBoss.layer, curBoss.bossId);
                    // 九幽令不足
                    if (!bossCfg) {
                        return;
                    }
                    else if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                        return;
                    }
                    else if (GlobalUtil.compterFindWayPos(bossCfg.bossPos[0], bossCfg.bossPos[1]).length > 3) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
                }
                catch (ex) {
                    console.error(ex);
                    return;
                }

                if (monster && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        // console.warn('killBoss...1.');
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        bossNextWatchTime[bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                    else if (BlacklistMonitors.badPlayer != null) { // 开启了黑名单
                        return check();
                    } else { // 没开黑名单
                        bossNextWatchTime[bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
                // 没有归属，发起攻击
                if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    // console.warn('killBoss...2');
                    setCurMonster(monster);
                    self.attackMonster(monster);
                    return check();
                }
                return check();
            });
        }
        return check();
    }

    async stop() {
        //if (!this.isRunning()) return;
        gameObjectInstMap.ZjmTaskConView && gameObjectInstMap.ZjmTaskConView.o_lmT();
        await xSleep(500);
        await super.stop();
    }
}

class CrossPublicBossTask extends StaticGuaJiTask {
    static name = "CrossPublicBossTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime() && SingletonMap.RoleData.o_lYC > 0) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_NQV)) {
                    result = true;
                }
                else if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        try {
            if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                return null;
            }
            let arrBossCfg = SingletonMap.BossProvider.o_lGJ();
            let lastLayer = arrBossCfg[arrBossCfg.length - 1];
            SingletonMap.o_wB.o_Ohs(lastLayer);

            for (let key in SingletonMap.o_eB9.o_NQV) {
                let arrLayerBossTime = SingletonMap.o_eB9.o_NQV[key];
                // 先找已经刷新的
                let bossTime = arrLayerBossTime.find(M => M.time == 0 && (!CrossPublicBossRefreshTime[key + '_' + M.index] || CrossPublicBossRefreshTime[key + '_' + M.index] <= SingletonMap.ServerTimeManager.o_eYF));
                if (bossTime) {
                    let arrLayerBoss = arrBossCfg.find(M => M.layer == key);
                    if (arrLayerBoss) {
                        let bossCfg = arrLayerBoss.boss.find(M => M.index == bossTime.index);
                        if (bossCfg) {
                            result = bossCfg;
                            break;
                        }
                    }
                }
            }

            if (result == null) {
                let mintimeBoss = null;
                for (let key in SingletonMap.o_eB9.o_NQV) {
                    let arrLayerBossTime = SingletonMap.o_eB9.o_NQV[key];
                    // 找30秒内刷新的
                    let bossTime = arrLayerBossTime.find(M => M.time - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[key + '_' + M.index] || CrossPublicBossRefreshTime[key + '_' + M.index] <= SingletonMap.ServerTimeManager.o_eYF));
                    if (bossTime) {
                        if (mintimeBoss == null || bossTime.time < mintimeBoss.time) {
                            mintimeBoss = bossTime;
                            mintimeBoss.layer = key;
                        }
                    }
                }
                if (mintimeBoss) {
                    let arrLayerBoss = arrBossCfg.find(M => M.layer == mintimeBoss.layer);
                    if (arrLayerBoss) {
                        let bossCfg = arrLayerBoss.boss.find(M => M.index == mintimeBoss.index);
                        if (bossCfg) {
                            result = bossCfg;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async enterMap() {
        try {
			await this.findWayByPos(41, 33, 'findWay', SingletonMap.MapData.curMapName);
            if (this.curNextBoss) {
                SingletonMap.o_wB.o_OiN(this.curNextBoss.layer);
                await awaitToMap("轮回秘境");
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            gameObjectInstMap.ZjmTaskConView.o_lmT();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await xSleep(500);
            if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                this.diePos = null;
                await this.stop();
            }
            else {
                let nextBoss = this.getNextBoss();
                if (nextBoss) {
                    this.curNextBoss = nextBoss;
                    SingletonMap.o_lAu.stop();
                    SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
                    await xSleep(500);
                    await this.findWayByPos(41, 33, 'findWay', SingletonMap.MapData.curMapName);
                    SingletonMap.o_lAu.stop();
                    if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') == -1) {
                        if (this.curNextBoss) {
                            SingletonMap.o_wB.o_OiN(this.curNextBoss.layer);
                        }
                    }
                    if (this.diePos) {
                        await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                    }
                    this.diePos = null;
                }
                else {
                    this.diePos = null;
                    await this.stop();
                }
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            if (this.isActivityTime()) {
                await gotoCanYueDao();
                this.run();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async stop() {
        QuickPickUpNormal.runPickUp();
        if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') > -1) {
            gameObjectInstMap.ZjmTaskConView.o_lmT();
        }
        await super.stop();
    }

    async run() {
        try {
            await xSleep(500);
            SingletonMap.o_lAu.stop();
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
                this.curNextBoss = nextBoss;
                await this.enterMap();
                await this.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                SingletonMap.o_lAu.stop();
                await this.waitBossKill(nextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                nextBoss = this.getNextBoss();
                QuickPickUpNormal.runPickUp();
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    isWaitingTime(curBoss) {
        let result = false;
        try {
            if (curBoss) {
                if (!CrossPublicBossRefreshTime[curBoss.layer + '_' + curBoss.index]
                    && SingletonMap.o_eB9.o_NQV[curBoss.layer].find(M => M.index == curBoss.index).time - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    result = true;
                }
                else if (CrossPublicBossRefreshTime[curBoss.layer + '_' + curBoss.index] - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    filterMonster() {
        return true;
    }

    async waitBossKill(curBoss) {
        let self = this;
        function check() {
            return xSleep(50).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                if (!self.isActivityTime()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 还没回到boss地图
                if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') == -1) {
					await self.enterMap();
                    return check();
                }

                if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                    return;
                }
                let nextBoss = self.getNextBoss();
                // 没有boss了
                if (!nextBoss) {
                    return;
                }

                SingletonMap.o_lAu.stop();
                if (GlobalUtil.compterFindWayPos(curBoss.bossPos[0], curBoss.bossPos[1]).length > 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(curBoss.bossPos[0], curBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    return check();
                }

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3)) {
                        setCurMonster(BlacklistMonitors.badPlayer);
                        SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }

                let bossMonster = GlobalUtil.getMonsterById(curBoss.bossId);
                let monster = bossMonster || self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                // 归属不是自己
                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) {
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        CrossPublicBossRefreshTime[curBoss.layer + '_' + curBoss.index] = SingletonMap.ServerTimeManager.o_eYF + 60000 * 5; //5分钟内不打
                        return;
                    }
                    else {
                        return check();
                    }
                }

                if (self.isWaitingTime(curBoss)) {
                    return check();
                }

                if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') > -1 && self.findMonter() == null) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }
}

class xiyouXianfuDayTask extends StaticGuaJiTask {
    static name = "xiyouXianfuDayTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();

            if (curHours >= 22 && curHours < 23
                && (!bossNextWatchTime[xiyouXianfuDayTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[xiyouXianfuDayTask.name])) {
                result = true;
            }
            else if (curHours == 22 && curMinutes >= 30 && !xiyouXianfuDayTaskLastTime[curMonth + '_' + curDateNum]) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);

            // 领取仙府每日任务
            SingletonMap.o_n.o_EG("XiyouXianfuWin", null, false);
            await xSleep(500);
            let arrDayTask = SingletonMap.o_n.isOpen("XiyouXianfuWin").viewCon.$children.find(M => M.__class__ == 'XiyouXianfuView').o_OhJ._source.filter(M => M.o_Nz_ == true);
            for (var i = 0; i < arrDayTask.length; i++) {
                SingletonMap.o_lQH.o_lIV(arrDayTask[i].stdQuest.id);
                await xSleep(500);
            }
            if (SingletonMap.o_n.isOpen("XiyouXianfuWin")) {
                SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouXianfuWin"));
                await xSleep(500);
            }

            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            // 22点最后一次进入
            if (curHours >= 22) {
                xiyouXianfuDayTaskLastTime[curMonth + '_' + curDateNum] = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 60 * 1000; // 8小时1分钟后再次执行
        bossNextWatchTime[xiyouXianfuDayTask.name] = nextWatchTime;
        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouXianfuWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouXianfuWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 仙府炼丹
class xiyouXianfuLianDanTask extends StaticGuaJiTask {
    static name = "xiyouXianfuLianDanTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23
                && (!bossNextWatchTime[xiyouXianfuLianDanTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[xiyouXianfuLianDanTask.name])) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);

            // 炼丹炉
            SingletonMap.o_n.o_EG("XiyouLianDanWin", null, false);
            await xSleep(1000);
            // 先领奖励
            let arrLianDanLu = SingletonMap.o_n.isOpen("XiyouLianDanWin").viewCon.$children.find(M => M.__class__ == 'XiyouLiandanView').list.$children.filter(M => M.data.o_yOc && M.endTime != 0 && M.endTime < SingletonMap.ServerTimeManager.o_eYF);
            for (var i = 0; i < arrLianDanLu.length; i++) {
                SingletonMap.o_yKf.o_Und(arrLianDanLu[i].data.index);
                await xSleep(500);
                SingletonMap.o_n.isOpen("XiyouLiandanResultWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLiandanResultWin"));
                await xSleep(500);
            }
            // 开始炼丹
            arrLianDanLu = SingletonMap.o_n.isOpen("XiyouLianDanWin").viewCon.$children.find(M => M.__class__ == 'XiyouLiandanView').list.$children.filter(M => M.data.o_yOc && M.endTime == 0);
            for (var i = 0; i < arrLianDanLu.length; i++) {
                arrLianDanLu[i].o_lP6();
                await xSleep(500);
                for (var j = SingletonMap.o_O.o_UPH.length; j > 0; j--) {
                    // 可以炼丹
                    if (SingletonMap.o_Unw.o_Uh8(SingletonMap.o_O.o_UPH.find(M => M.sort == j).id)) {
                        SingletonMap.o_n.isOpen("XiyouDanfangView").o_O1y({ itemIndex: j - 1 }); // 选中tab
                        await xSleep(500);
                        SingletonMap.o_n.isOpen("XiyouDanfangView").o_eKW(); // 炼丹
                        await xSleep(500);
                        break;
                    }
                }
                await xSleep(500);
            }
            SingletonMap.o_n.isOpen("XiyouLianDanWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLianDanWin"));// 关闭炼丹窗体

            let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 60 * 1000; // 8小时1分钟后再次执行
            bossNextWatchTime[xiyouXianfuLianDanTask.name] = nextWatchTime;
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouLianDanWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLianDanWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 西游仙府 - 神魔谷地
let xiyouShenMoGuDiTaskLastTime = {};
class XiyouShenMoGuDiTask extends StaticGuaJiTask {
    static name = "XiyouShenMoGuDiTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            if (curHours >= 10 && curHours < 23) {
                if (!bossNextWatchTime[XiyouShenMoGuDiTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[XiyouShenMoGuDiTask.name]) {
                    result = true;
                }
                else if (curHours == 22 && curMinutes >= 30 && !xiyouShenMoGuDiTaskLastTime[curMonth + '_' + curDateNum]) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);


            // 神魔谷地
            SingletonMap.o_n.o_EG("XiyouShenMoGuDiWin", null, false);
            await xSleep(500);
            // 领取奖励
            if (SingletonMap.XiyouMgr.o_UMf()) {
                SingletonMap.o_yKf.o_UMT();
                await xSleep(500);
            }
            let XiyouShenMoGuDiView = SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin").viewCon.$children.find(M => M.__class__ == 'XiyouShenMoGuDiView');
            // 开始历练
            if (XiyouShenMoGuDiView.startBtn.label == LangManager.o_Ur1[0]) {
                SingletonMap.o_yKf.o_UMM(XiyouShenMoGuDiView.curIdx + 1);
                await xSleep(500);
            }
            // 22点先停止，重新开始
            if (curHours >= 22) {
                xiyouShenMoGuDiTaskLastTime[curMonth + '_' + curDateNum] = true;
                SingletonMap.o_yKf.o_Ur0(); // 停止历练
                await xSleep(500);
                // 领取奖励
                if (SingletonMap.XiyouMgr.o_UMf()) {
                    SingletonMap.o_yKf.o_UMT();
                    await xSleep(500);
                }
                SingletonMap.o_yKf.o_UMM(XiyouShenMoGuDiView.curIdx + 1);
                await xSleep(500);
            }

            SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin"));// 关闭神魔谷地
            let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 5000; // 8小时后
            bossNextWatchTime[XiyouShenMoGuDiTask.name] = nextWatchTime;
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 西游仙府-炼器
class XiyouXFLianqiTask extends StaticGuaJiTask {
    static name = "XiyouXFLianqiTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23
                && (!bossNextWatchTime[XiyouXFLianqiTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[XiyouXFLianqiTask.name])) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);

            // 炼器
            SingletonMap.o_n.o_EG("XiyouXFLianqiWin", null, false);
            var M = Math.floor((SingletonMap.o_UMF.data.o_UV5().endTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
            // 先领取奖励
            if (M <= 0) {
                SingletonMap.o_UnK.o_URk();
                await xSleep(500);
                closeGameWin("XiyouXFLianqiResultWin");
                await xSleep(500);
            }

            let lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 3); // 先找高阶
            if (!lianQiCfg) {
                if (SingletonMap.o_UMF.data.o_UQZ > 0) { // 可以免费刷新
                    SingletonMap.o_UnK.o_UM4();
                    await xSleep(1000);
                    lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 3);
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 2);
                    }
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 1); // 低阶
                    }
                }
                else {
                    lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 2); // 找中阶
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 1); // 低阶
                    }
                }
            }
            // 开始炼器
            if (lianQiCfg) {
                SingletonMap.o_UnK.o_Uno(lianQiCfg.index, []);
                if (lianQiCfg.level == 3) {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 8 * 60 * 60 * 1000 + 5000; // 8小时后
                }
                else if (lianQiCfg.level == 2) {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 6 * 60 * 60 * 1000 + 5000; // 6小时后
                }
                else {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 4 * 60 * 60 * 1000 + 5000; // 4小时后
                }
            }
            closeGameWin("XiyouXFLianqiWin"); // 关闭炼器
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        closeGameWin("XiyouXFLianqiWin");
        await super.stop();
    }
}

// 西游化缘
class XiyouHuayuanTask extends StaticGuaJiTask {
    huayuanAnswer = [{ 0: 1, 1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1 },
    { 0: 0, 1: 0, 2: 0, 3: 2, 4: 1, 5: 1, 6: 1 },
    { 0: 2, 1: 2, 2: 2, 3: 2, 4: 2 }];

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            if (curHours >= 10 && curHours < 23) {
                let arrFinishEvent = this.getFinishEvent();
                if (!bossNextWatchTime[XiyouHuayuanTask.name] || bossNextWatchTime[XiyouHuayuanTask.name] <= SingletonMap.ServerTimeManager.o_eYF) {
                    if (this.hasTimes() || (arrFinishEvent && arrFinishEvent.length > 0)) { // 有次数或有已完成的事件
                        result = true;
                    }
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    hasTimes() {
        let result = false;
        try {
            let count = SingletonMap.o_O.o_yCJ.baseBegNum;
            let useTimes = SingletonMap.o_NL6.getCountById(EntimesType.XIYOUHUAYUANTIMES, 1);
            let buyTimes = SingletonMap.o_NL6.getCountById(EntimesType.XIYOUHUAYUANTIMES, 2);
            if (count + buyTimes - useTimes > 0) {
                return true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getFinishEvent() {
        let result = null;
        try {
            result = SingletonMap.o_yCo.o_yE6.events.filter(M => !M.o_ykq);
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }



    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);
            let arrFinishEvent = this.getFinishEvent();
            SingletonMap.o_n.o_EG("XiyouHuayuanWin", null, false);
            if (arrFinishEvent && arrFinishEvent.length > 0) { // 所有事件都完成了，提交事件
                for (var i = 0; i < arrFinishEvent.length; i++) {
                    let eventIndex = arrFinishEvent[i].eventId - 1;
                    let eventItemIndex = arrFinishEvent[i].o_yCc - 1;
                    let answerId = this.huayuanAnswer[eventIndex][eventItemIndex];
                    SingletonMap.o_yCD.o_yCU(arrFinishEvent[i].itemIndex, answerId);
                    await xSleep(500);
                    closeGameWin("XiyouHuayuanTalkEndWin");
                }
            }
            await xSleep(1000);
            arrFinishEvent = this.getFinishEvent();
            if (this.hasTimes() && (arrFinishEvent == null || arrFinishEvent.length == 0)) { //没有进行中的事件，并且有次数
                SingletonMap.o_yCD.o_ys_(SingletonMap.o_yCo.o_yE6.o_yCe); // 开始化缘
            }
            await xSleep(1000);
        }
        catch (ex) {
            console.error(ex);
        }
        bossNextWatchTime[XiyouHuayuanTask.name] = curDate.getTime() + 30 * 60 * 1000;// 30分钟后再次执行
        await this.stop();
    }

    async stop() {
        //if (!this.isRunning()) return;
        closeGameWin("XiyouHuayuanWin");
        await super.stop();
    }
}

// 本服皇城争霸
class LocalServerHuangChengZhengBaTask extends StaticGuaJiTask {
    static name = "LocalServerHuangChengZhengBaTask";
    OnlyVitality = false;
    lastTaskTime = "";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            // 每周六20:00 - 20:30
            if (this.lastTaskTime == "" || this.lastTaskTime < SingletonMap.ServerTimeManager.o_eYF) {
                if (weekDay == 6 && curHours == 20 && (curMinutes >= 0 && curSeconds >= 10) && curMinutes < 30) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (weekDay == 6 && curHours == 20 && curMinutes >= 0 && curMinutes < 30) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    initConfig() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.LocalServerHuangChengZhengBaOnlyVitality == 'TRUE') {
                    this.OnlyVitality = true;
                }
                else {
                    this.OnlyVitality = false;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            this.initConfig();
            await gotoWangChen(); // 回王城
            await xSleep(500);
            SingletonMap.o_eaX.o_lxG(3); // 去皇宫
            await xSleep(500);
            await awaitToMap("皇城");
            // this.loopCheckHp();
            if (!this.OnlyVitality) {
                await this.run();
            }
            this.lastTaskTime = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 60;
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async run() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    self.findWayIsStop = false;
                    if (!mainRobotIsRunning()) {
                        self.findWayIsStop = true;
                        console.log('mainRobot not Running.........');
                        return;
                    }
                    // 收到停止指令
                    if (self.stopTask == true) {
                        self.findWayIsStop = true;
                        console.log('stopTask.........');
                        return;
                    }

                    // 任务停止
                    if (self.isRunning && !self.isRunning()) {
                        console.log('not isRunning.........');
                        self.findWayIsStop = true;
                        return;
                    }

                    if (!self.isActivityTime()) {
                        self.findWayIsStop = true;
                        console.log('not activity time.........');
                        return;
                    }
                    // 人物死亡
                    if (AliveCheckLock.isLock()) {
                        return check();
                    }
                    if (self.diePos != null) {
                        return check();
                    }

                    if (SingletonMap.MapData.curMapName == '皇城') {
                        SingletonMap.o_OKA.o_NSw(47, 42);
                        return check();
                    }

                    if (SingletonMap.MapData.curMapName == '皇宫') {
                        // 寻找非本行会的人
                        let monster = self.findMonter();
                        if (monster) {
                            // 发起攻击
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else {
                            clearCurMonster();
                            self.attackIsStop = true;
                            SingletonMap.o_OKA.o_NSw(23, 20); // 前往皇座附近
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    findMonter() {
        let monster = null;
        try {
            if (curMonster && curMonster.curHP > 0 && this.existsMonster(curMonster)) {
                monster = curMonster;
            }
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof PlayerActor && !M.masterName && M.curHP > 0 && M.guildName != PlayerActor.o_NLN.guildName) {
                        if (targetMonster == null) {
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                        else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                            // 距离近的优先
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                    }
                }
                if (targetMonster) {
                    monster = targetMonster;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return monster;
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        //if (!this.isRunning()) return;
        await super.stop();
    }
}

// 烧猪
class shaoZhuTask extends StaticGuaJiTask {
    static name = "shaoZhuTask";
    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (isFreeTime() && this.getTimes() > 0) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getTimes() {
        let result = 0;
        try {
            result = SingletonMap.o_OiP.times - SingletonMap.o_OiP.o_lth;
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            let times = this.getTimes();
            for (var i = 0; i < times; i++) {
                SingletonMap.o_lHn.o_NTD(SingletonMap.o_OiP.curLayer);
                await awaitToMap("经验副本");
                await awaitToMap("王城");
                await xSleep(1000);
                if (!mainRobotIsRunning()) {
                    break;
                }
                if (!this.isRunning()) {
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        this.stop();
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        //if (!this.isRunning()) return;
        closeGameWin("ExpFbBuyWin");
        await super.stop();
    }
}

class XunBaoTask extends StaticGuaJiTask {
	static name = "XunBaoTask";
    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (isFreeTime() && this.hasTimes()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	hasTimes() {
        let result = false;
        try {
            result = SingletonMap.o_Nlv.baseCfg.num > SingletonMap.o_Nlv.o_eWD();
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        this.stop();
    }
	
	waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (SingletonMap.MapData.curMapName == '藏宝阁') return check();
                    if (!self.hasTimes()) {
                        return;
                    }
                    if (gameObjectInstMap.ZjmView.compassView.visible == false) {
						SingletonMap.o_Nlv.o_OOL(true);
					}
					else if (gameObjectInstMap.ZjmView.compassView.barCon.visible == false) {
						gameObjectInstMap.ZjmView.compassView.o_eFC();
					}
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	async enterMap() {
        await xSleep(500);
    }

    async stop() {
		try {
			closeGameWin("XunbaotuAutoEndWin");
			await xSleep(300);
			gameObjectInstMap.ZjmTaskConView.o_lmT();
		}
		catch(ex) {}
        await super.stop();
    }
}

// 膜拜城主
class MoBaiChenZhuTask extends StaticGuaJiTask {
    static name = "MoBaiChenZhuTask";
    moBaiState = false;

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();

            if (this.isActivityTime() && this.moBaiState == false) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 12 && curMinutes >= 0 && curMinutes < 10) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isOnlyOnlyVitality() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.MoBaiChenZhuOnlyVitality == 'TRUE') {
                    result = true;
                }
                else {
                    result = false;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.moBaiState = false;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (SingletonMap.o_lAh.o_exz() && self.moBaiState != true) {
                        SingletonMap.o_eqY.o_NGG(0);
                        self.moBaiState = true;
                    }
                    else if (self.isOnlyOnlyVitality() && self.moBaiState) { // 只拿活跃度
                        return;
                    }
                    if (!self.isActivityTime() && self.moBaiState) {
                        return;
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep();
    }

    async stop() {
        //if (!this.isRunning()) return;
        await super.stop();
    }
}

// 西游蟠桃
class XiyouPanTaoTask extends StaticGuaJiTask {
    static name = "XiyouPanTaoTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (this.isActivityTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 15) {
                if (curMinutes == 30 && curSeconds >= 10) {
                    result = true;
                }
                else if (curMinutes > 30) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoXiYouZhuChen();
            await xSleep(1000);
			BlacklistMonitors.stop();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 活动时间结束
                        return true;
                    }
                    if (!SingletonMap.XiyouMgr.o_yWb()) { // 不在西游地图
                        await gotoXiYouZhuChen();
                        await xSleep(500);
                    }
                    else if (SingletonMap.MapData.curMapName == '西游降魔') {
                        SingletonMap.o_yKf.o_yZ3();
                        await xSleep(500);
                    }
                    else if (SingletonMap.MapData.curMapName != '蟠桃盛宴') {
                        await gotoXiYouZhuChen();
                        await xSleep(500);
                    }
                    else if (SingletonMap.MapData.curMapName == '蟠桃盛宴') {
                        if (curMonster && !BlacklistMonitors.isBadPlayer(curMonster.roleName)) {
                            clearCurMonster();
                        }
                        if (SingletonMap.XiyouMgr.o_yZ7 == 2) { // 没座                            
                            let seatCfg = self.getTargetSeatCfg();
                            if (seatCfg) { // 有空位
                                clearCurMonster();
                                await self.sitSeat(seatCfg);
                            }
                            else if (GlobalUtil.compterFindWayPos(35, 70).length > 8) { // 前往中心位置
                                await self.findWayByPos(35, 70, 'findWay', SingletonMap.MapData.curMapName);
                            }
                            else {
                                let badSeatPlayer = self.getBadSeatPlayer(); // 杀人
                                while (badSeatPlayer) {
                                    let seatCfg = self.getTargetSeatCfg(); // 有空座了，退出杀人状态
                                    if (seatCfg) {
                                        clearCurMonster();
                                        await self.sitSeat(seatCfg);
                                        break;
                                    }
                                    if (curMonster && !BlacklistMonitors.isBadPlayer(curMonster.roleName)) {
                                        clearCurMonster();
                                    }
                                    setCurMonster(badSeatPlayer);
                                    SingletonMap.o_OKA.o_Pc(badSeatPlayer, true);
                                    if (!GlobalUtil.existsMonster(badSeatPlayer)) {
                                        badSeatPlayer = self.getBadSeatPlayer();
                                    }
                                    await xSleep(100);
                                }
                            }
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    sitSeat(seatCfg) {
        let self = this;
        function check() {
            return xSleep(300).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (!self.isActivityTime()) { // 活动时间结束
                    return true;
                }
                if (SingletonMap.XiyouMgr.o_yZ7 != 2) { // 坐上了
                    return true;
                }
                if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] == 1) { // 被别人占领了
                    return true;
                }

                if (PlayerActor.o_NLN.currentX != seatCfg.posTbl.x && seatCfg.posTbl.y != PlayerActor.o_NLN.o_NI3) {
                    SingletonMap.o_OKA.o_NSw(seatCfg.posTbl.x, seatCfg.posTbl.y);
                    return check();
                }
                else {
                    let seat = self.getSeatById(seatCfg.monId);
                    SingletonMap.o_OKA.o_Pc(seat, true);
                    return check();
                }
            });
        }
        return check();
    }

    getTargetSeatCfg() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouPanTao != "") {
                    let arrSeatId = appConfig.XiyouPanTao.split('|');
                    for (let i = 0; i < arrSeatId.length; i++) {
                        let seatId = parseInt(arrSeatId[i]);
                        let seatCfg = SingletonMap.o_O.o_ykw.find(M => M.monId == seatId);
                        if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] != 1) {
                            result = seatCfg;
                            break;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
    getSeatById(seatId) {
        return SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = seatId);
    }

    getSeatPlayer(posX, posY) {
        return SingletonMap.o_OFM.o_Nx1.find(M => M.currentX == posX && M.o_NI3 == posY && PlayerActor.o_NLN.guildName != M.guildName && BlacklistMonitors.isBadPlayer(M.roleName));
    }
    getBadSeatPlayer() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouPanTao != "") {
                    let arrSeatId = appConfig.XiyouPanTao.split('|');
                    for (let i = 0; i < arrSeatId.length; i++) {
                        let seatId = arrSeatId[0];
                        let seatCfg = SingletonMap.o_O.o_ykw.find(M => M.monId == seatId);
                        // 先找座位上的
                        if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] == 1) {
                            let seatPlayer = this.getSeatPlayer(seatCfg.posTbl.x, seatCfg.posTbl.y);
                            if (seatPlayer && BlacklistMonitors.isBadPlayer(seatPlayer.roleName)) {
                                result = seatPlayer;
                                break;
                            }
                        }
                    }

                    if (!result) {
                        result = BlacklistMonitors.getBadPlayer();
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        if (result) console.warn(result.roleName + ':' + BlacklistMonitors.isBadPlayer(result.roleName));
        return result;
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        //if (!this.isRunning()) return;
        // 回到西游主城
        await super.stop();
    }
}

class XiyouCityBossTask extends StaticGuaJiTask {
    static name = "XiyouCityBossTask";
    originalEnableMovePickup = false;

    needRun() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();

            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_yGa)) {
                    result = true;
                }
                else if (this.getNextActivityBoss()) {
                    result = true;
                }
                else if (curHours == 10 && curMinutes >= 30 && !xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_30']) {
                    result = true;
                }
                else if (curHours == 16 && !xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_16_00']) {
                    result = true;
                }
                else if (curHours == 10 && curMinutes < 10 && !xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00']) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
                if (curHours == 10) {
                    result = true;
                }
                else if (curHours == 16) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextActivityBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouCityBoss != "") {
                    let XiyouCityBoss = appConfig.XiyouCityBoss.split("|");
                    let bossCfg = SingletonMap.o_eB9.o_yGa || {};
                    for (var i = 0; i < XiyouCityBoss.length; i++) {
                        let bossId = parseInt(XiyouCityBoss[i]);
                        let boss = SingletonMap.o_O.o_yGM.find(M => M.bossId == bossId);
                        if (bossCfg[boss.index] == 0) {
                            result = boss;
                            break;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getActivityBossById(bossId) {
        let result = null;
        try {
            let boss = SingletonMap.o_O.o_yGM.find(M => M.bossId == bossId);
            let bossCfg = SingletonMap.o_eB9.o_yGa || {};
            if (bossCfg[boss.index] == 0) {
                result = boss;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
            this.initState();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            await gotoXiYouZhuChen();
            await xSleep(1000);
            let curBoss = this.getNextActivityBoss();
            while (curBoss) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                // 前往boss坐标
                await this.findWayByPos(curBoss.bossPos[0], curBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(100);
                await this.killBoss(curBoss);
                curBoss = this.getNextActivityBoss();
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 16) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00'] = true;
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_30'] = true;
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_16_00'] = true;
            }
            else if (curHours >= 10 && curMinutes < 30) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00'] = true;
            }
            else if (curHours >= 10 && curMinutes >= 30) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_30'] = true;
            }
        }
        catch (e) {
            console.error(e);
        }

        await this.stop();
    }

    async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
        await super.stop();
    }

    killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(100).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }

                let importantGoods = getImportantGoods();
                if (importantGoods) {
					if (isFastSuction == true && enableMovePickup == false) {
						SingletonMap.o_lAu.stop();
						startMovePickup(true);
					}
					else if (enableMovePickup == false && !SingletonMap.o_lAu.o_lcD){
						SingletonMap.o_lAu.start();
					}
                    return check();
                }

                if (SingletonMap.MapData.curMapName != '西游降魔') { // 不在西游主城
                    await gotoXiYouZhuChen();
                    return check();
                }

                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length > 3) {
                    // 离boss超过3步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }

                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer) {	// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget();
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    // self.attackMonster(badPlayer);
                    return check();
                }

                let monster = SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == refreshBoss.bossId);
                if (!monster) { // 怪物消失
                    clearCurMonster();
                    if (self.getActivityBossById(refreshBoss.bossId)) { // 怪物还在，返回
                        await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        monster = SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == refreshBoss.bossId);
                        if (!monster && self.getActivityBossById(refreshBoss.bossId)) { //怪物处于刷新状态但是目的地没有怪物
                            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                            let curMonth = curDate.getMonth();
                            let curDateNum = curDate.getDate();
                            let curHours = curDate.getHours();
                            let curMinutes = curDate.getMinutes();
                            if (curHours == 10 && curMinutes < 30) {
                                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00'] = true;
                                return;
                            }
                        }
                    }
                    else {
                        return;
                    }
                }
                else {
                    BlacklistMonitors.start();
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                }
                return check();
            });
        }
        return check();
    }

    async enterMap() {
        await xSleep(500);
    }

    async backToMap() {
        try {
            if (SingletonMap.MapData.curMapName != '西游降魔') {
                await gotoXiYouZhuChen();
            }
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

//  跨服1V1
class Ladder1v1Task extends StaticGuaJiTask {
    static name = "Ladder1v1Task";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            let beginTime1 = 1431;
            let endTime1 = 1530;
            let beginTime2 = 1931;
            let endTime2 = 2000;
            try {
                if (appConfig != "") {
                    appConfig = JSON.parse(appConfig);
                    if (appConfig.Ladder1v1Time1 != "") {
                        beginTime1 = parseInt(appConfig.Ladder1v1Time1.replace(":", ""));
                    }
                    if (appConfig.Ladder1v1Time2 != "") {
                        beginTime2 = parseInt(appConfig.Ladder1v1Time2.replace(":", ""));
                    }
                }
            }
            catch (e) {
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (SingletonMap.Ladder1v1Mgr.o_NVM() && SingletonMap.Ladder1v1Mgr.o_ehV() > 0) {
                let curTime = parseInt(curHours + "" + (curMinutes >= 10 ? curMinutes : '0' + curMinutes));
                if (curTime >= beginTime1 && curTime < endTime1) {
                    result = true;
                }
                else if (curTime >= beginTime2 && curTime < endTime2) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(2000).then(function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }

                if (!self.isActivityTime() && SingletonMap.MapData.curMapName != '1V1巅峰竞赛') { // 不是活动时间或次数用完了
                    return;
                }

                if (SingletonMap.o_n.isOpen("Ladder1v1ResultWin")) {
                    closeGameWin("Ladder1v1ResultWin");
                }
                else if (!SingletonMap.Ladder1v1Mgr.o_O59 && !SingletonMap.o_n.isOpen("Ladder1v1MateWin")
                    && !SingletonMap.o_n.isOpen("Ladder1v1LoadWin")
                    && !SingletonMap.o_n.isOpen("Ladder1v1FinalsResultWin")
                    && SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                    console.warn('开始1v1匹配...');
                    SingletonMap.o_lVP.sendLadder1v1Mate();
                }
                else if (SingletonMap.MapData.curMapName == '1V1巅峰竞赛') { // 进入赛场
                    console.warn('1v1比赛进行中...');
                    let monster = curMonster && curMonster.curHP > 0 ? curMonster : SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.visible && !M.masterName && M.curHP > 0);
                    if (monster) {
                        setCurMonster(monster);
                        if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
                            SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
                        }
                        else {
                            try {
                                SingletonMap.o_OKA.o_Pc(monster, true);
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                    else if (GlobalUtil.compterFindWayPos(28, 23).length > 3) {
                        SingletonMap.o_OKA.o_NSw(28, 23);
                    }
                    else {
                        clearCurMonster();
                    }
                }
                return check();
            });
        }
        return check();
    }
}

//  跨服3V3
class Ladder3v3Task extends StaticGuaJiTask {
    static name = "Ladder3v3Task";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            result = this.isActivityTime() && !SingletonMap.Ladder3v3Mgr.is3v3Map();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            let beginTime1 = 1431;
            let endTime1 = 1530;
            let beginTime2 = 1931;
            let endTime2 = 2000;
            try {
                if (appConfig != "") {
                    appConfig = JSON.parse(appConfig);
                    if (appConfig.Ladder1v1Time1 != "") {
                        beginTime1 = parseInt(appConfig.Ladder1v1Time1.replace(":", ""));
                    }
                    if (appConfig.Ladder1v1Time2 != "") {
                        beginTime2 = parseInt(appConfig.Ladder1v1Time2.replace(":", ""));
                    }
                }
            }
            catch (e) {
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (SingletonMap.Ladder3v3Mgr.o_NVM() && SingletonMap.Ladder3v3Mgr.o_ehV() > 0) {
                let curTime = parseInt(curHours + "" + (curMinutes >= 10 ? curMinutes : '0' + curMinutes));
                if (curTime >= beginTime1 && curTime < endTime1) {
                    result = true;
                }
                else if (curTime >= beginTime2 && curTime < endTime2) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(2000).then(function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }

                if (PlayerActor.o_NLN.o_LP) { // 处于幽灵状态
                    return check();
                }
                if (!self.isActivityTime() && !SingletonMap.Ladder3v3Mgr.is3v3Map()) { // 不是活动时间或次数用完了
                    console.warn('不是活动时间或次数用完了...');
                    return;
                }
                if (SingletonMap.o_n.isOpen("Ladder3v3ResultWin")) {
                    closeGameWin("Ladder3v3ResultWin");
                }
                else if (SingletonMap.Ladder3v3Mgr.is3v3Map()) { // 进入赛场
                    // 备战中
                    if ((SingletonMap.Ladder3v3Mgr.is3v3Map() || SingletonMap.Ladder1v1Mgr.o_yrY() || SingletonMap.JobWarMgr.o_yrY()) && SingletonMap.SceneManager.o_lsJ) {
                        return check();
                    }
                    let monster = GlobalUtil.existsMonster(curMonster) || SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.visible && M.curHP > 0 && SingletonMap.o_h7.o_lDb(M));
                    if (monster) {
                        if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
                            SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
                        }
                        else {
                            try {
                                PlayerActor.o_NLN.o_lkM();
                                setCurMonster(monster);
                                SingletonMap.o_OKA.o_Pc(monster, true);
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                    else if (GlobalUtil.compterFindWayPos(18, 18).length > 3) {
                        SingletonMap.o_OKA.o_NSw(18, 18);
                    }
                    else {
                        clearCurMonster();
                    }
                }
                else if (!SingletonMap.Ladder3v3Mgr.o_O59 && !SingletonMap.o_n.isOpen("Ladder3v3MateWin")
                    && SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                    SingletonMap.o_lVP.sendLadder3v3Single();
                }
                return check();
            });
        }
        return check();
    }
}

//  职业巅峰赛1V1
class JobWar1v1Task extends StaticGuaJiTask {
    static name = "JobWar1v1Task";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (SingletonMap.JobWarMgr.o_y61 && !(!SingletonMap.JobWarMgr.o_y7G() && !SingletonMap.JobWarMgr.o_y77)) {
                if (curHours == 19 && curMinutes < 30) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        SingletonMap.o_Yr.o_NZ8(); // 退出
        await checkServerCrossMap();
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 不是活动时间或次数用完了
                        return;
                    }
                    if (SingletonMap.o_n.isOpen("JobWarResultWin")) {
                        closeGameWin("JobWarResultWin");
                    }
                    else if (SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                        SingletonMap.o_y6N.o_y54(); // 进入职业巅峰竞赛主城
                    }
                    else if (SingletonMap.MapData.curMapName == '职业巅峰竞赛主城' && SingletonMap.ServerCross.o_ePe) { // 进入赛场
                        if (!SingletonMap.o_n.isOpen("JobWarMateWin")) {
                            SingletonMap.o_y6N.o_y5B(); // 开始匹配
                        }
                    }
                    let monster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.curHP > 0);
                    if (SingletonMap.MapData.curMapName == '职业巅峰竞赛场') {
                        if (monster) {
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else if (GlobalUtil.compterFindWayPos(30, 17).length > 3) {
                            SingletonMap.o_OKA.o_NSw(30, 17);
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 神魔水晶
class ShenMoShuiJinTask extends StaticGuaJiTask {
    static name = "ShenMoShuiJinTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (curHours == 21 && curMinutes >= 1) {
                if (this.getTimes() > 0) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    getTimes() {
        let result = 0;
        try {
            var Q = SingletonMap.o_O.o_yH4.yaSongCount || 3;
            result = Q - SingletonMap.o_yy_.times;
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoShenMoZhuChen();
            await xSleep(1000);
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(2000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (self.getTimes() <= 0) return;
                    let npc = SingletonMap.o_OFM.o_Nx1.find(M => M.roleName == '神奇的水晶');
                    if (SingletonMap.o_n.isOpen("NewWorldCrystalWin")) { // 开始押送
                        if (SingletonMap.o_yy_.o_yqi >= 3) {
                            SingletonMap.o_yyA.o_yH3();
                        }
                        else {
                            SingletonMap.o_yyA.o_yqI();
                        }
                    }
                    else if (SingletonMap.o_n.isOpen("NewWorldCrystalSubmitWin")) { // 提交
                        SingletonMap.o_yyA.o_yHg();
                        closeGameWin("NewWorldCrystalSubmitWin");
                    }
                    else if (SingletonMap.o_yy_.o_eGE) { //在运水晶
                        SingletonMap.o_yy_.o_yq6();
                    }
                    else if (npc) {
                        SingletonMap.o_OKA.o_Pc(npc, true);
                    }
                    else {
                        //前往水晶祭坛
                        SingletonMap.o_yy_.o_yHx();
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        //if (!this.isRunning()) return;
        await super.stop();
    }
}


// 神界boss
class ShenJieBossTask extends StaticGuaJiTask {
    static name = "ShenJieBossTask";
    curRefreshBoss = null;

    needRun() {
        let result = false;
        try {
            this.init();
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_UbI)) {
                    result = true;
                }
                else if (this.getRefreshBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    init() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieBoss) {
                    this.arrGuaJiMap = appConfig.ShenJieBoss.split('|');
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    getRefreshBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieBoss != "") {
                    let arrBossRefreshTimes = this.getArrBossRefreshTimes();
                    let arrShenJieBoss = appConfig.ShenJieBoss.split("|");
                    for (let i = 0; i < arrShenJieBoss.length; i++) {
                        let bossId = parseInt(arrShenJieBoss[i]);
                        if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                            if (arrBossRefreshTimes[bossId] == 0) { // 已经刷新
                                boss = this.getBossCfg(bossId);
                                break;
                            }
                        }
                    }
                    if (boss == null) { // 寻找60秒内刷新的
                        for (let i = 0; i < arrShenJieBoss.length; i++) {
                            try {
                                let bossId = parseInt(arrShenJieBoss[i]);
                                if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                                    let bossRefreshTime = arrBossRefreshTimes[bossId];
                                    if (bossRefreshTime == null) {
                                        continue;
                                    }
                                    if (bossRefreshTime == 0 || bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) { // 已经刷新或60秒后刷新
                                        boss = this.getBossCfg(bossId);
                                        break;
                                    }
                                }
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    getArrBossRefreshTimes() {
        let result = {};
        try {
            let cfg = SingletonMap.o_eB9.o_UbI || {};
            for (var key in cfg) {
                Object.assign(result, cfg[key]);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getArrBossCfg() {
        let arrBossCfg = [];
        try {
            for (var i = 0; i < SingletonMap.o_O.o_Uai.length; i++) {
                for (var j = 0; j < SingletonMap.o_O.o_Uai[i].Layer.length; j++) {
                    arrBossCfg.push(SingletonMap.o_O.o_Uai[i].Layer[j]);
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return arrBossCfg;
    }

    getBossCfg(bossId) {
        let bossCfg = null;
        try {
            let arrBossCfg = this.getArrBossCfg();
            let layerBoss = arrBossCfg.find(M => M.bossList.includes(bossId));
            let bossIndex = layerBoss.bossList.findIndex(M => M == bossId);
            layerBoss.bossPos = layerBoss.bossPosList[bossIndex];
            bossCfg = layerBoss;
            bossCfg.bossId = bossId;
        }
        catch (e) {
            console.error(e);
        }
        return bossCfg;
    }

    async refreshBossTime() {
        try {
            let arrBossCfg = this.getArrBossCfg();
            for (let i = 0; i < arrBossCfg.length; i++) {
                SingletonMap.o_Uij.o_Umd(arrBossCfg[i].mapId);
                await xSleep(100);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async enterMap(map) {
        try {
            await gotoShenJie();
            SingletonMap.o_Uij.o_UX4(map[0], map[1]);
            this.curMap = map;
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            this.initState();
            QuickPickUpNormal.start();
            BlacklistMonitors.start();
            // this.loopCheckHp();
            this.loopCancleNoBelongMonster();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.hashCode = new Date().getTime();
                console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
                await gotoShenJie();
                await this.run();
            }
            else {
                await this.stop();
            }
        }
        catch (e) {
            console.error(e);
        }
        // await this.stop();
    }

    initState() {
        super.initState();
        this.curRefreshBoss = null;
    }

    async run() {
        try {
            await this.refreshBossTime();
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!this.isActivityTime()) {
                    break;
                }
                // 进入boss地图
                await this.enterMap([this.curRefreshBoss.id, this.curRefreshBoss.layer]);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                await xSleep(100);
                // 前往boss坐标
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName, this.curRefreshBoss.bossId);
                await this.killBoss(this.curRefreshBoss);
                await xSleep(1000);
                await gotoShenJie();
                // this.refreshBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        return true;
    }

    async backToMap() {
        try {
            // console.warn('回到boss地图...');
            let curRefreshBoss = this.curRefreshBoss || this.getRefreshBoss();
            if (curRefreshBoss) {
                if (curRefreshBoss.id && curRefreshBoss.layer) {
                    await this.enterMap([curRefreshBoss.id, curRefreshBoss.layer]);
                    await xSleep(500);
                }
                if (curRefreshBoss.bossPos) {
                    await this.findWayByPos(curRefreshBoss.bossPos[0], curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(500);
        this.diePos = null;
        // await this.stop();
    }

    async killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(20).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                if (!self.isActivityTime()) return;
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}
                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    return check();
                }

                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length > 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    return check();
                }

                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }


                // 被打飞了
                if (SingletonMap.MapData.curMapName == '神界主城') {
                    await self.enterMap([refreshBoss.id, refreshBoss.layer]);
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        bossNextWatchTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
                // 刷新boss时间
                self.refreshBossTime();
                try {
                    let bossRefreshTime = SingletonMap.o_eB9.o_UbI[refreshBoss.mapId][refreshBoss.bossId];
                    // 刷新时间小于30秒，等待刷新
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
                        // console.warn('killBoss...2');
                        return check();
                    }
                    // 刷新时间大于30秒
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
                        // console.warn('killBoss...3.');
                        return;
                    }
                }
                catch (exx) {
                    console.error(exx);
                    return;
                }
                return check();
            });
        }
        return check();
    }
}

// 神魔大战
class ShenMoWarTask extends StaticGuaJiTask {
    static name = "ShenMoWarTask";
    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (weekDay == 1 || weekDay == 4 || weekDay == 0) { // 周一、周四、周日
                if (curHours == 20 && curMinutes >= 0) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 不是活动时间或次数用完了
                        return;
                    }
                    if (SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                        SingletonMap.o_y6N.o_y54(); // 进入职业巅峰竞赛主城
                    }
                    else if (SingletonMap.MapData.curMapName == '职业巅峰竞赛主城' && SingletonMap.ServerCross.o_ePe) { // 进入赛场
                        if (!SingletonMap.o_n.isOpen("JobWarMateWin")) {
                            SingletonMap.o_y6N.o_y5B(); // 开始匹配
                        }
                    }
                    if (SingletonMap.MapData.curMapName == '职业巅峰竞赛场') {
                        let monster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.curHP > 0);
                        if (monster) {
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else if (GlobalUtil.compterFindWayPos(30, 17).length > 3) {
                            SingletonMap.o_OKA.o_NSw(30, 17);
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}


// 高爆地图
class GaoBaoGuaJiTask extends StaticGuaJiTask {
    static name = "GaoBaoGuaJiTask";
    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            let runningTask = SingletonModelUtil.getRunningTask();
            if (SingletonMap.o_NP8.o_lkV > 0 && isFreeTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.GaoBaoGuaJi) {
                    SingletonMap.o_lyZ.o_NHl(parseInt(appConfig.GaoBaoGuaJi));
                }
                else {
                    SingletonMap.o_lyZ.o_NHl(1); // 1福利 2高爆1 3高爆2
                }
                await this.waitTaskFinish();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(3000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (SingletonMap.o_NP8.o_lkV <= 0) { // 挂机时间已结束
                        return;
                    }
                    if (!SingletonMap.o_lAu.o_lcD) {
                        SingletonMap.o_lAu.start();
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep();
    }

    async stop() {
        //if (!this.isRunning()) return;
        await super.stop();
    }
}

// 普通玛法
class StaticNormalMafaBossTask extends StaticGuaJiTask {
    static name = "StaticNormalMafaBossTask";
    state = false;
    arrBossPosTemplate = [];
    arrBossName = [];
    arrBossPos = [];
    diePos = null;
    curMapName = null;
    nextRunTime = null;
    bossId = null;

    bosIsAlive() {
        let result = false;
        try {
            let bossTime = normalVillaMafaBossTime[this.bossId];
            if (!bossTime) {
                result = true;
            }
            else {
                let M = Math.floor((bossTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                result = M <= 0 ? true : false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    updateNextRunTime(time) {
        this.nextRunTime = time;
        let nextRunDateStr = new Date(this.nextRunTime).toLocaleString();
        console.warn(`任务${this.constructor.name}下次执行时间${nextRunDateStr}`);
    }

    needRun() {
        if (this.isRunning()) return false;
        let result = true;
        try {
            if (isCrossTime()) {
                if (this.bossId != null) {
                    result = this.bosIsAlive();
                }
                else if (this.nextRunTime != null) {
                    let M = Math.floor((this.nextRunTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                    result = M <= 0 ? true : false;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!isCrossTime()) {
                await this.stop();
            }
            this.initState();
			if (SingletonMap.MapData.curMapName != "玛法幻境") {
				await gotoWangChen(); // 先回王城
				await awaitToMap("王城");
			}
            await gotoMafaHuanJin();
            if (this.arrBossPosTemplate != null) {
                this.arrBossPos = Object.assign([], this.arrBossPosTemplate);
                QuickPickUpNormal.start();
                //BlacklistMonitors.start();
                //// this.loopCheckHp();
                //this.loopAttackMonter();
                this.run();
                console.warn(`${this.constructor.name}任务开始了。。。。。UID=${this.uid}`);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
            await this.enterMap();
            await awaitLeaveMap('玛法幻境');
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;
            while (this.arrBossPos.length > 0) {
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                let bossPos = Object.assign({}, this.arrBossPos[0]);
                await this.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(100);
                await this.waitBossKill(bossPos);
                QuickPickUpNormal.runPickUp();
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                await xSleep(100);
                this.arrBossPos.splice(0, 1);
            }
			QuickPickUpNormal.runPickUp();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    isWaitingTime() {
        let result = false;
        try {
            if (this.bossId && (!this.getBossRefreshTime() || this.getBossRefreshTime() - SingletonMap.ServerTimeManager.o_eYF <= 60000)) {
                result = true; // 60秒内刷新
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async waitBossKill(bossPos) {
        let self = this;
        function check() {
            return xSleep(50).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
					await xSleep(1000).then(function() {
						SingletonMap.o_eaX.o_e5d(0);
					});
                    return check();
                }
				
				if (SingletonMap.MapData.curMapName == '玛法幻境') {
					await self.enterMap();
					return check();					
				}

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3)) {
                        setCurMonster(BlacklistMonitors.badPlayer);
                        SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }
                let bossMonster = null;
                if (self.bossId) {
                    bossMonster = GlobalUtil.getMonsterById(self.bossId);
                }
                let monster = bossMonster || self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster)
                    && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        normalVillaMafaBossTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000;
                        return;
                    }
                    else {
                        return check();
                    }
                }
				
				if (GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length > 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    // await self.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
					SingletonMap.o_OKA.o_NSw(bossPos.x, bossPos.y);
                    return check();
                }

                if (self.isWaitingTime()) {
                    return check();
                }

                if (self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }

    async stop() {
        if (!this.isRunning()) return;
        try {
            this.diePos = null;
            this.curMapName = null;
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            await this.exitMap();
            console.warn(`${this.constructor.name}任务停止了。。。。。`);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stopTaskComplete();
    }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossName.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async enterMap() {
        await xSleep(1000);
    }


    async exitMap() {
        try {
            SingletonMap.o_Ozj.o_elB(); // 回到玛法幻境
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(500);
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

class MafaChiYueBossTask extends StaticNormalMafaBossTask {
    bossId = 157010;
    static name = "MafaChiYueBossTask";
    arrBossPosTemplate = [{ x: 81, y: 131 }, { x: 53, y: 64 }, { x: 52, y: 45 }, { x: 37, y: 45 }];
    arrBossName = ['绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_GW[2][1];
	}
}

class MafaChiYueStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaChiYueStar2Task";
    arrBossPosTemplate = [{ x: 22, y: 108 }, { x: 36, y: 112 }, { x: 66, y: 118 }, { x: 67, y: 140 }, { x: 88, y: 133 }, { x: 94, y: 106 }, { x: 96, y: 84 }, { x: 91, y: 46 }, { x: 66, y: 59 }, { x: 49, y: 63 }, { x: 75, y: 81 }, { x: 20, y: 60 }, { x: 23, y: 20 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_GW[2][1];
	}
}

class MafaChiYueStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaChiYueStar1Task";
    arrBossPosTemplate = [{ x: 19, y: 27 }, { x: 83, y: 21 }, { x: 89, y: 45 }, { x: 78, y: 135 }, { x: 52, y: 98 }, { x: 65, y: 84 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(1);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_GW[2][1];
	}
}
class MafaMoLongBossTask extends StaticNormalMafaBossTask {
    bossId = 157020;
    static name = "MafaMoLongBossTask";
    arrBossPosTemplate = [{ x: 86, y: 40 }, { x: 63, y: 50 }, { x: 71, y: 39 }];
    arrBossName = ['‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_Ng_[2][1];
	}
}
class MafaMoLongStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaMoLongStar1Task";
    arrBossPosTemplate = [{ x: 39, y: 222 }, { x: 52, y: 180 }, { x: 76, y: 106 }, { x: 88, y: 55 }, { x: 88, y: 21 }, { x: 144, y: 24 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(1);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_Ng_[2][1];
	}
}

class MafaMoLongStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaMoLongStar2Task";
    arrBossPosTemplate = [{ x: 17, y: 126 }, { x: 36, y: 139 }, { x: 55, y: 114 }, { x: 44, y: 97 }, { x: 53, y: 90 }, { x: 31, y: 86 }, { x: 53, y: 65 }, { x: 67, y: 62 }, { x: 82, y: 41 }, { x: 98, y: 26 }, { x: 103, y: 45 }, { x: 83, y: 71 }, { x: 98, y: 98 }, { x: 109, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_Ng_[2][1];
	}
}
class MafaHuoLongBossTask extends StaticNormalMafaBossTask {
    bossId = 157030;
    static name = "MafaHuoLongBossTask";
    arrBossPosTemplate = [{ x: 59, y: 19 }, { x: 13, y: 13 }, { x: 9, y: 21 }];
    arrBossName = ['火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_eFv[2][1];
	}
}
class MafaHuoLongStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaHuoLongStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 43 }, { x: 50, y: 32 }, { x: 56, y: 22 }, { x: 63, y: 30 }, { x: 47, y: 18 }, { x: 51, y: 67 }, { x: 62, y: 71 }, { x: 71, y: 55 }, { x: 11, y: 43 }, { x: 17, y: 20 }, { x: 25, y: 8 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_eFv[2][1];
	}
}

class MafaHuoLongStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaHuoLongStar1Task";
    arrBossPosTemplate = [{ x: 107, y: 219 }, { x: 137, y: 179 }, { x: 82, y: 113 }, { x: 133, y: 100 }, { x: 57, y: 63 }, { x: 57, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(1);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_eFv[2][1];
	}
}
class MafaDuShiBossTask extends StaticNormalMafaBossTask {
    bossId = 157115;
    static name = "MafaDuShiBossTask";
    arrBossPosTemplate = [{ x: 53, y: 48 }, { x: 9, y: 27 }, { x: 57, y: 71 }, { x: 78, y: 75 }, { x: 8, y: 73 }];
    arrBossName = ['绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_efW[2][1];
	}
}
class MafaDuShiStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaDuShiStar1Task";
    arrBossPosTemplate = [{ x: 22, y: 74 }, { x: 42, y: 54 }, { x: 35, y: 38 }, { x: 49, y: 26 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(1);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_efW[2][1];
	}
}

class MafaDuShiStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaDuShiStar2Task";
    arrBossPosTemplate = [{ x: 28, y: 71 }, { x: 44, y: 55 }, { x: 47, y: 66 }, { x: 56, y: 75 }, { x: 56, y: 87 }, { x: 77, y: 69 }, { x: 86, y: 66 }, { x: 72, y: 57 }, { x: 34, y: 42 }, { x: 47, y: 27 }, { x: 28, y: 28 }, { x: 4, y: 23 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_efW[2][1];
	}
}
class MafaXianLingBossTask extends StaticNormalMafaBossTask {
    bossId = 157215;
    static name = "MafaXianLingBossTask";
    arrBossPosTemplate = [{ x: 66, y: 120 }, { x: 52, y: 54 }];
    arrBossName = ['‖神王‖★★持柱天王★', '‖大罗‖★★十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_yAb[2][1];
	}
}
class MafaXianLingStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaXianLingStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 62 }, { x: 67, y: 59 }, { x: 39, y: 38 }, { x: 6, y: 73 }, { x: 8, y: 79 }, { x: 12, y: 84 }, { x: 35, y: 95 }, { x: 40, y: 130 }, { x: 24, y: 156 }, { x: 49, y: 161 }, { x: 62, y: 154 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_yAb[2][1];
	}
}

class MafaXianLingStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaXianLingStar1Task";
    arrBossPosTemplate = [{ x: 24, y: 93 }, { x: 41, y: 118 }, { x: 29, y: 140 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(1);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 43 * 60 * 1000; // 43分钟后再次执行
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
	
	getBossRefreshTime() {
		return SingletonMap.o_Ntl.o_yAb[2][1];
	}
}

class ShenLongShanZhuanTask extends StaticGuaJiTask {
    static name = 'ShenLongShanZhuanTask';
    state = false;
    diePos = null;
    curMapName = null;
    arrBossPos = [];
    arrBossId = [];
    arrBossCfgId = {
        1: 152001, 2: 152001, 3: 152001, 4: 152002, 5: 152002, 6: 152002, 7: 152003, 8: 152003, 9: 152003,
        10: 152004, 11: 152004, 12: 152005, 13: 152005, 14: 152006, 15: 152006, 16: 152007, 17: 152008, 18: 152101,
        19: 152102, 20: 152103, 21: 152104, 22: 152105
    };

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (isCrossTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_eXy)) {
                    result = true;
                }
                else if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenLongShanZhuanBoss) {
                    // 先找已经刷新的
                    let arrShenLongShanZhuanBossIndex = appConfig.ShenLongShanZhuanBoss.split('|');
                    for (let i = 0; i < arrShenLongShanZhuanBossIndex.length; i++) {
                        let bossIndex = parseInt(arrShenLongShanZhuanBossIndex[i]);
                        let bossCfg = SingletonMap.o_O.o_N38.find(M => M.index == bossIndex);
                        if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] == 0 &&
                            (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                            result = bossCfg;
                            break;
                        }
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        // 找快要刷新的 时间短的优先
                        for (let i = 0; i < arrShenLongShanZhuanBossIndex.length; i++) {
                            let bossIndex = parseInt(arrShenLongShanZhuanBossIndex[i]);
                            let bossCfg = SingletonMap.o_O.o_N38.find(M => M.index == bossIndex);
                            if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] == 0 &&
                                (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                    || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                                result = bossCfg;
                                break;
                            }
                            else if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] - SingletonMap.ServerTimeManager.o_eYF <= 60000 &&
                                (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                    || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                                if (mintimeBoss == null || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] < ShenLongShanZhuanBossNextWatchTime[mintimeBoss.index]) {
                                    mintimeBoss = bossCfg;
                                }
                            }
                        }
                        if (result == null) {
                            result = mintimeBoss;
                        }
                    }
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenLongShanZhuanBoss) {
                let arrBoss = appConfig.ShenLongShanZhuanBoss.split("|");
                for (let i = 0; i < arrBoss.length; i++) {
                    let bossIndex = parseInt(arrBoss[i]);
                    let bossCfg = SingletonMap.o_O.o_N38[bossIndex - 1];
                    this.arrBossId.push(this.arrBossCfgId[bossCfg.index]);
                }
            }
        }
    }

    async start() {
        try {
            if (this.state == true) return;
            this.initState();
            this.init();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            await gotoWangChen(); // 先回王城
            if (curHours >= 10 && curHours < 23) {
                await gotoCanYueDao(); // 进入苍月岛
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                //// this.loopCheckHp();
                //this.loopAttackMonter();
                this.run();
                console.log(`${this.constructor.name}任务开始了。。。。。`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
            await this.enterMap();
            await xSleep(500);
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
                await this.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                SingletonMap.o_lAu.stop();
                await this.waitBossKill(nextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(500);
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                nextBoss = this.getNextBoss();
            }
            await xSleep(100);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    async stop() {
        try {
            this.diePos = null;
            this.curMapName = null;
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
            await this.exitMap();
            console.log(`任务停止了${this.constructor.name}。。。。。`);
        }
        catch (ex) {
            console.warn(ex);
        }
        await this.stopTaskComplete();
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    async enterMap() {
        try {
            SingletonMap.o_wB.o_FW();
            await awaitToMap("神龙山庄");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
            await awaitToMap("苍月岛");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrBossId.includes(M.cfgId));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        return monster;
    }

    async waitBossKill(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(20).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                QuickPickUpNormal.runPickUp();
                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length > 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    return check();
                }

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3)) {
                        setCurMonster(BlacklistMonitors.badPlayer);
                        SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster)
                    && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        ShenLongShanZhuanBossNextWatchTime[refreshBoss.index] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                    else {
                        return check();
                    }
                }

                try {
                    let bossRefreshTime = SingletonMap.o_eB9.o_eXy[refreshBoss.index];
                    // 刷新时间小于30秒，等待刷新
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
                        // console.warn('killBoss...2');
                        return check();
                    }
                    // 刷新时间大于30秒
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
                        // console.warn('killBoss...3.');
                        return;
                    }
                }
                catch (exx) {
                    console.error(exx);
                    return;
                }
                return check();
            });
        }
        return check();
    }


    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossId.find(item => monster.cfgId == item && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

// 押镖
class EscortTask extends StaticGuaJiTask {
    static name = "EscortTask";
    targetLevel = 1; // 目标等级

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (this.isActivityTime() && SingletonMap.o_eXx.o_OP7 > 0) {
                result = true;
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours == 16 && curMinutes >= 30) {
                result = true;
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EscortLevel != "") {
                    this.targetLevel = parseInt(appConfig.EscortLevel);
                }
            }
        }
        catch (ex) {
            this.targetLevel = 1;
            console.warn(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            this.init();
            await gotoWangChen();
            await this.gotoChanYueDao();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        closeGameWin('EscortGiftWin'); // 押镖奖励窗口
        await this.stop();
    }

    gotoChanYueDao() {
        SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
        function check() {
            return xSleep(200).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    return check();
                }
                if (SingletonMap.MapData.curMapName == '苍月岛') {
                    return;
                }
                return check();
            });
        }
        return check();
    }

    waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(3000).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.o_eXx.o_eGE) { // 押镖中
                    if (SingletonMap.o_eXx.o_NDD(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3)) { // 距离镖车过远
                        SingletonMap.o_eXx.o_ljt(); // 飞到镖车
                        await xSleep(300);
                    }
                    if (SingletonMap.o_eXx.o_eGs) {
                        SingletonMap.o_eXx.o_et3 = true;
                        SingletonMap.o_eXx.o_OYc(true);
                        SingletonMap.o_l.o_Ut1('o_l2e', true);
                        await xSleep(300);
                    }
                }
                else { // 没有镖车
                    if (SingletonMap.o_eXx.o_OP7 <= 0) { // 没次数了
                        return;
                    }
                    closeGameWin('EscortGiftWin'); // 押镖奖励窗口
                    await self.gotoChanYueDao();
                    SingletonMap.o_n.o_EG("EscortWin");
                    let npc = SingletonMap.o_OFM.o_Nx1.find(M => M.roleName == '苍月镖头');
                    SingletonMap.o_OKA.o_Pc(npc, true);
                    let times = 0; // 最多升3次到钻石镖车
                    while (self.targetLevel > SingletonMap.o_eXx.index && self.getEscortCount() > 0 && times < 4) {
                        times++;
                        SingletonMap.o_NpG.o_ekQ();
                        await xSleep(1000);
                    }
                    await xSleep(1000);
                    SingletonMap.o_NpG.o_Npm(0); // 开始运镖
                    await xSleep(1000);
                }
                return check();
            });
        }
        return check();
    }

    getEscortCount() {
        var M = SingletonMap.o_O.o_jv.FreshNeedItem;
        return M > 100 ? SingletonMap.o_Ue.o_Ngt(M) : SingletonMap.RoleData.o_NM3(M);
    }
}

class DeityPrisonTask extends StaticGuaJiTask {
    static name = "DeityPrisonTask";

    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (this.isActivityTime()) {
                result = true;
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours == 10 && curMinutes < 30 && !DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_10_00']) {
                result = true;
            }
            else if (curHours == 15 && curMinutes >= 30 && !DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_15_30']) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_10_00']
            this.initState();
            await gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        if (curHours == 10 && curMinutes < 30) {
            DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_10_00'] = true;
        }
        else if (curHours == 15 && curMinutes >= 30) {
            DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_15_30'] = true;
        }
        await this.stop();
    }


    waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(1500).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.MapData.curMapName == '王城') {
                    SingletonMap.o_lyZ.o_eVD(1);
                    return check();
                }

                try {
                    let arrBossCfg = SingletonMap.o_NP8.o_ecq(SingletonMap.o_lyZ.layer);
                    let boss = SingletonMap.o_NP8.bossList.find(M => M.state != 0);
                    if (boss) { // 有boss未击杀
                        let targetMonster = self.existsMonsterById(boss.bossId);
                        if (targetMonster) { // boss在身边
                            setCurMonster(targetMonster);
                            SingletonMap.o_OKA.o_Pc(targetMonster, true);
                        }
                        else {
                            if (arrBossCfg) {
                                let bossCfg = arrBossCfg.boss.find(M => M.BossID == boss.bossId);
                                // 寻路找boss
                                await self.findWayByPos(bossCfg.BossBornPoint.x, bossCfg.BossBornPoint.y, 'findWay', SingletonMap.MapData.curMapName, boss.bossId);
                            }
                        }
                    }
                    else {

                        // 已经是最后一层
                        if (SingletonMap.o_NP8.isMax) {
                            return;
                        }
                        let nextLayer = SingletonMap.o_lyZ.layer + 1;
                        let nextCfg = SingletonMap.o_NP8.o_ecq(nextLayer);
                        let b = nextCfg.expend[0] ? nextCfg.expend[0] : 0;
                        let r = nextCfg.expend[1] ? nextCfg.expend[1] : 0;
                        let itemId = SingletonMap.o_Ue.o_lbU(b, r);
                        // 神威令牌不足
                        if (itemId > 0 && !SingletonMap.o_NP8.o_esz()) {
                            SingletonMap.o_lAu.start();
                            return check();
                        }
                        // 前往下一层
                        let npcCfg = SingletonMap.o_Oos.o_NIi(arrBossCfg.comp);
                        await self.findWayByPos(npcCfg.pos.x, npcCfg.pos.y, 'findWay', SingletonMap.MapData.curMapName);
                        await xSleep(500);
                        SingletonMap.o_lyZ.o_eVD(nextLayer);
                    }
                }
                catch (ex) {
                    console.error(ex);
                }
                return check();
            });
        }
        return check();
    }
}

// 跨服皇陵
class CrossHuangLingTask extends StaticGuaJiTask {
    static name = "CrossHuangLingTask";

    needRun() {
        if (this.isRunning()) return false;
        if (SingletonMap.RoleData.o_f4 < 20) return false;
        try {
            [1, 2, 3, 4, 5, 6, 7, 8].forEach(async (layer) => {
                SingletonMap.o_wB.o_Gq(layer);
                await xSleep(100);
            });
        }
        catch (e) {
            console.error(e);
        }

        let result = false;
        try {
            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_N2G)) {
                    result = true;
                }
                else if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            if (!isFreeTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getArrBossCfg() {
        let result = [];
        try {
            let bossCfg = SingletonMap.BossProvider.o_Nw8();
            for (var i = 0; i < bossCfg.length; i++) {
                result = result.concat(bossCfg[i].boss);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        if (SingletonMap.RoleData.o_f4 < 20) return null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.CrossHuangLing) {
                    let arrBossCfg = this.getArrBossCfg();
                    let arrBossId = appConfig.CrossHuangLing.split('|');
                    for (let i = 0; i < arrBossId.length; i++) {
                        let bossId = parseInt(arrBossId[i]);
                        let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                        let bossTime = SingletonMap.o_eB9.o_N2G[bossCfg.layer][bossCfg.index];
                        if (bossTime == 0 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) { // 先找已经刷新的
                            result = bossCfg;
                            break;
                        }
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        for (let i = 0; i < arrBossId.length; i++) {
                            let bossId = parseInt(arrBossId[i]);
                            let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                            let bossTime = SingletonMap.o_eB9.o_N2G[bossCfg.layer][bossCfg.index];
                            if (bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) {// 找30秒内刷新的
                                if (mintimeBoss == null || bossTime < mintimeBoss.time) {
                                    mintimeBoss = bossCfg;
                                }
                            }
                        }
                        result = mintimeBoss;
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            await gotoWangChen();
            await xSleep(300);
            let nextBoss = this.getNextBoss();
            if (nextBoss) {
                this.curNextBoss = nextBoss;
                await this.waitTaskFinish(this.curNextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(1500);
            }

        }
        catch (e) {
            console.error(e);
        }
        this.stop();
    }

    waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (SingletonMap.RoleData.o_f4 < 20) { // 皇陵体力少于20
                        return true;
                    }
                    let bossTime = SingletonMap.o_eB9.o_N2G[nextBoss.layer][nextBoss.index];
                    if (bossTime - SingletonMap.ServerTimeManager.o_eYF > 30000) { // 已经击杀
                        return;
                    }

                    if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                        // loading
                        return check();
                    }
                    if (SingletonMap.MapData.curMapName.indexOf('领地') == -1) {
                        SingletonMap.o_wB.o_NGb(nextBoss.layer, nextBoss.index);
                        return check();
                    }

                    let badPlayer = BlacklistMonitors.getBadPlayer();
                    if (badPlayer != null) {// 先打黑名单
                        setCurMonster(badPlayer);
                        SingletonMap.o_OKA.o_Pc(badPlayer, true);
                        return check();
                    }

                    if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length > 5) {
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }

                    let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = nextBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                    if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) {
                        if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                            return check();
                        }
                        if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                            CrossPublicBossRefreshTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                            return;
                        }
                        else {
                            return check();
                        }
                    }

                    // 刷新时间小于30秒，等待刷新
                    if (bossTime > 0 && bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                        return check();
                    }

                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
        QuickPickUpNormal.runPickUp();
        await xSleep(500);
		try {
			SingletonMap.o_wB.o_ll();
			// await checkLocalServeMap();
		}
		catch(ex) {
		}
        await super.stop();
    }
}

// 上古遗址
class ShanGuYiZhiTask extends StaticGuaJiTask {
    static name = "ShanGuYiZhiTask";
	arrBossRefreshTime = null;
	
    needRun() {
        if (this.isRunning()) return false;
        let result = false;
        try {
            [1, 2, 3].forEach(async (layer) => {
                SingletonMap.Boss2Sys.o_yJ(layer);
                await xSleep(100);
            });
        }
        catch (e) {
            console.error(e);
        }
        try {
			if (!SingletonMap.ServerCross.o_ePe) {
				this.arrBossRefreshTime = this.getArrBossRefreshTime();
			}
			if (SingletonMap.RoleData.o_O4Q < 10) return false;
            if (this.isActivityTime()) {
                if ($.isEmptyObject(this.arrBossRefreshTime)) {
                    result = true;
                }
                else if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        return true;
    }

    getArrBossCfg() {
        let result = [];
        try {
            let bossCfg = SingletonMap.BossProvider.o_aQ();
            for (var i = 0; i < bossCfg.length; i++) {
                result = result.concat(bossCfg[i].boss);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getArrBossRefreshTime() {
        try {
            [1, 2, 3].forEach(async (layer) => {
                SingletonMap.Boss2Sys.o_yJ(layer);
                await xSleep(100);
            });
        }
        catch (e) {
            console.error(e);
        }
        let result = [];
        try {
            let bossCfg = SingletonMap.o_eB9.o_l6z;
            for (var i = 1; i <= 3; i++) {
                result = result.concat(bossCfg[i]);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        if (SingletonMap.RoleData.o_O4Q < 10) return null;
		if (!this.arrBossRefreshTime) return null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShanGuYiZhi) {
                    let arrBossCfg = this.getArrBossCfg();
                    let arrBossId = appConfig.ShanGuYiZhi.split('|');
                    // let arrBossRefreshTime = this.getArrBossRefreshTime();
                    for (let i = 0; i < arrBossId.length; i++) {
                        let bossId = parseInt(arrBossId[i]);
                        let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                        let bossTimeCfg = this.arrBossRefreshTime.find(M => M.layer == bossCfg.layer && M.index == bossCfg.index);
                        let bossTime = bossTimeCfg.time;
                        if (bossTime == 0 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) { // 先找已经刷新的
                            result = bossCfg;
                            break;
                        }
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        for (let i = 0; i < arrBossId.length; i++) {
                            let bossId = parseInt(arrBossId[i]);
                            let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                            let bossTimeCfg = this.arrBossRefreshTime.find(M => M.layer == bossCfg.layer && M.index == bossCfg.index);
                            let bossTime = bossTimeCfg.time;
                            if (bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) {// 找30秒内刷新的
                                if (mintimeBoss == null || bossTime < mintimeBoss.time) {
                                    mintimeBoss = bossCfg;
                                }
                            }
                        }
                        result = mintimeBoss;
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            this.initState();
            if (SingletonMap.MapData.curMapName.indexOf('上古遗迹') == -1) {
                await gotoWangChen();
                await xSleep(300);
            }
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
                this.curNextBoss = nextBoss;
                await this.waitTaskFinish(this.curNextBoss);
                QuickPickUpNormal.runPickUp();
                nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
        }
        catch (e) {
            console.error(e);
        }
        QuickPickUpNormal.runPickUp();
        await xSleep(1000);
        this.stop();
    }

    waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (SingletonMap.RoleData.o_O4Q < 10) { // 体力少于10
                        return true;
                    }

                    self.arrBossRefreshTime = self.getArrBossRefreshTime();
                    let bossTimeCfg = self.arrBossRefreshTime.find(M => M.layer == nextBoss.layer && M.index == nextBoss.index);
                    let bossTime = bossTimeCfg.time;
                    if (bossTime - SingletonMap.ServerTimeManager.o_eYF > 30000) { // 已经击杀
                        return;
                    }

                    if (SingletonMap.MapData.curMapName.indexOf('上古遗迹') == -1) {
                        SingletonMap.Boss2Sys.o_hL(nextBoss.layer);
                        return check();
                    }

                    if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                        // loading
                        return check();
                    }                    

                    let badPlayer = BlacklistMonitors.getBadPlayer();
                    if (badPlayer != null) {// 先打黑名单
                        setCurMonster(badPlayer);
                        SingletonMap.o_OKA.o_Pc(badPlayer, true);
                        return check();
                    }

                    if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length > 5) {
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }

                    let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = nextBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                    if (monster && self.filterMonster(monster) && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) {
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (monster && self.existsMonster(monster) && monster.belongingName != '' && monster.belongingName != SingletonMap.RoleData.name) {
                        if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                            return check();
                        }
                        if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                            CrossPublicBossRefreshTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                            return;
                        }
                        else {
                            return check();
                        }
                    }
					
					if (bossTime > 0 && bossTime - SingletonMap.ServerTimeManager.o_eYF < 30000) {  // 等待刷新
                        return check();
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
        QuickPickUpNormal.runPickUp();
        await xSleep(500);
        try {
            gameObjectInstMap.ZjmTaskConView.o_lmT();
            await checkLocalServeMap();
        }
        catch (e) {
            console.error(e);
        }
        await super.stop();
    }
}

async function moveToPos(x, y) {
    return new Promise(async function (resolve) {
        try {
            var b = SingletonMap.o_etq.o_egw(x, y, PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, true);
            if (b && b.length > 0) {
                for (var i = b.length - 1; i >= 0; i--) {
                    if (!enableMovePickup) break;
                    SingletonMap.o_lAu.stop();
                    // await xSleep(100);
                    SingletonMap.o_NZS.o_NiD(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b[i].dir);
                    await xSleep(moveSleep);
                    PlayerActor.o_NLN.moveTo(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b[i].dir);
                    // await xSleep(moveSleep);
                    if (!enableMovePickup) break;
                    // 走不过去
                    if (PlayerActor.o_NLN.currentX != b[i].posX || PlayerActor.o_NLN.o_NI3 != b[i].posY) {
                        break;
                    }
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        resolve();
    });
}
// 全局参数: 死亡不回城
var arrNoBackTaskMap = [MafaChiYueBossTask.name, MafaChiYueStar2Task.name, MafaChiYueStar1Task.name, MafaMoLongBossTask.name, MafaMoLongStar1Task.name,
    MafaMoLongStar2Task.name, MafaHuoLongBossTask.name, MafaHuoLongStar2Task.name, MafaHuoLongStar1Task.name, MafaDuShiBossTask.name, MafaDuShiStar1Task.name,
    MafaDuShiStar2Task.name, MafaXianLingBossTask.name, MafaXianLingStar2Task.name, MafaXianLingStar1Task.name];