function allowAttack(M) {
	if (!M) return false;
	let result = false;
	try {
		if ("PlayerActor" == egret.getQualifiedClassName(M)) {
			if (SingletonMap.o_h7.o_lDb(M)) {
				result = true;
			}
		}
	
		if ("Monster" == egret.getQualifiedClassName(M)) {
			if (SingletonMap.o_h7.o_lDb(M)) {
				result = true;
			}
		}
	}
	catch(ex){
		
	}
	return result;
}

function autoDiscardProp() {// 自动丢弃
	try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
		if (SingletonMap.ServerCross.o_ePe) return; // 跨服不执行
		let propCfg = getAppConfigKeyValue("AutoDiscardJinYanDan");
		if (propCfg) {
			let arrProp = ['50万经验丹', '100万经验丹', '200万经验丹', '300万经验丹', '400万经验丹', '500万经验丹'];
			if (propCfg == "400") {
				arrProp.pop();
			}
			else if (propCfg == "300") {
				arrProp.pop();
				arrProp.pop();
			}
			else if (propCfg == "200") {
				arrProp.pop();
				arrProp.pop();
				arrProp.pop();
			}
			else if (propCfg == "100") {
				arrProp.pop();
				arrProp.pop();
				arrProp.pop();
				arrProp.pop();
			}
			for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
				let item = SingletonMap.o_Ue.o_OFL[i];
				for (let j  = 0; j < arrProp.length; j++) {
					if (item && item.name && arrProp[j] != "" && item.name == arrProp[j]) {
						SingletonMap.o_OPQ.o_OxY(item);
					}
				}
			}
		}
	}
	catch(ex) {
	}
}
function autoSaveWarehouse() {// 存仓库
	try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
		if (SingletonMap.ServerCross.o_ePe) return;// 跨服不执行
		let propCfg = getAppConfigKeyValue("AutoSaveWarehouse");
		if (propCfg) {
			let arrProp = propCfg.split('|');
			for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
				let item = SingletonMap.o_Ue.o_OFL[i];
				for (let j  = 0; j < arrProp.length; j++) {
					if (item && item.name && arrProp[j] != "" && item.name.indexOf(arrProp[j]) != -1) {
						SingletonMap.o_l1X.o_OxI(item.series);
					}
				}
			}
		}
	}
	catch(ex) {
	}
}
		
class SceneRobotLock {
    static state = false;

    static isLock() {
        return SceneRobotLock.state == true;
    }

    static lock() {
        SceneRobotLock.state = true;
    }

    static unLock() {
        SceneRobotLock.state = false;
    }
}
// 拆红包
function chaiHongBao() {
	try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
		if (SingletonMap.ServerCross.o_ePe) return;
		let cfg = getAppConfigKeyValue("EnableChaiHongBao");
		if (cfg != "TRUE") return;
		if (SingletonMap.o_OMb.o_NGN > 0) {
			SingletonMap.o_ez1.o_OUh();
		}
	}catch(e) {}
}

function resetBossTime() {
	try {
		let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
		if ((curHours == 23 || curHours == 10) && !gameCfgResetTime['resetBossTime_' + curMonth + '_' + curDateNum + '_' + curHours]) {
            gameCfgResetTime['resetBossTime_' + curMonth + '_' + curDateNum + '_' + curHours] = true;
			TaskStateManager.clearTaskFlag();
            villaMafaBossTime = {}; // 别墅boss
            normalVillaMafaBossTime = {};  // 普通玛法boss
            SingletonMap.o_eB9.o_eXy = {}; // 神龙山庄boss刷新时间
            SingletonMap.o_eB9.o_yGa = {}; // 西游4帝
			SingletonMap.o_eB9.o_N2G = {}; // 跨服皇陵
			SingletonMap.o_eB9.o_UbI = {}; // 神界boss
            console.warn('重置boss刷新时间...');
        }
	}
	catch(e) {
		console.error(e);
	}	
}

// 越狱
async function breakout() {
    try {
		if (!hasPri()) return;
		if (SingletonMap.MapData.curMapName == '红名监狱') {
			SingletonMap.o_NzX.baseCfg.prisonPKVal = 99999999;
			SingletonMap.o_lHL.o_OVF();
			await awaitToMap("天界主城");
			SingletonMap.o_eaX.o_lxG(2);
		}
    }
    catch (ex) {
		console.error(ex);
    }
}

function resetMafaExecuteTime() {
	try {
		let arrTask = [MafaChiYueStar1Task, MafaChiYueStar2Task, MafaMoLongStar1Task, MafaMoLongStar2Task, MafaHuoLongStar1Task, MafaHuoLongStar2Task, MafaDuShiStar1Task, MafaDuShiStar2Task, MafaXianLingStar1Task, MafaXianLingStar2Task];
		for (let i = 0; i < arrTask.length; i++) {
			let task = SingletonModelUtil.getInstance(arrTask[i]);
			if (task) {
				task.nextRunTime = null;
			}
		}
	}
	catch (e) {
		console.error(e);
	}
}

// 9成血加血
function autoHp() {
    try {
		if (!hasPri()) return;
		if (getAppConfigKeyValue("EnableAutoHp") != "TRUE") return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
        if (typeof PlayerActor != 'undefined' && PlayerActor.o_NLN && PlayerActor.o_NLN.curHP > 0 && (PlayerActor.o_NLN.curHP / PlayerActor.o_NLN.curHPMax <= 0.9)) {
            !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && getSkill('集体治疗术') && SingletonMap.o_lAu.attack(null, getSkill('集体治疗术'), { $hashCode: SingletonMap.o_etq.o_e9H(800, 500).$hashCode, x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 });
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

function autoJobSkill() {
    try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
        if (typeof PlayerActor != 'undefined' && PlayerActor.o_NLN && PlayerActor.o_NLN.o_eaP) {
            let buff = PlayerActor.o_NLN.o_eaP.find(M => M.name == '魔法盾');
            if (!buff && getSkill('魔法护盾')) {
                SingletonMap.o_lAu.attack(null, getSkill('魔法护盾'));
            }
			
			buff = PlayerActor.o_NLN.o_eaP.find(M => M.name.indexOf('神圣防御') > -1);
			if (!buff && getSkill('神圣防御')) {
                SingletonMap.o_lAu.attack(null, getSkill('神圣防御'), { $hashCode: SingletonMap.o_etq.o_e9H(800, 500).$hashCode, x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 })
            }
        }
    } catch (ex) {
    }
}

// 聚宝盆领元宝
function autoJuBaoPen() {
	try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
		if (SingletonMap.ServerCross.o_ePe) return;
        if (SingletonMap.o_OMb.o_eWW(1) && SingletonMap.o_OMb.o_NEz > 0 && SingletonMap.o_OMb.o_lGW > 0 && SingletonMap.o_OMb.o_lwn() <= 0) {
            SingletonMap.o_ez1.o_No_();
        }
	}
	catch(e) {}
}

// 召唤宠物
function autoPet() {
	try {
		if (!hasPri()) return;
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return;
		if (getAppConfigKeyValue("EnableAutoPet") == "TRUE") {
			if (!SingletonMap.PetMgr.o_ewd() 
				&& !SingletonMap.ShenJieMgr.o_UEk()
				&& !SingletonMap.XiyouMgr.o_yWb()
				&& SingletonMap.MapData.curMapName.indexOf("红名监狱") == -1
				&& SingletonMap.MapData.curMapName.indexOf('魔龙密窟') == -1
				&& SingletonMap.MapData.curMapName.indexOf('财神') == -1
				&& SingletonMap.MapData.curMapName.indexOf('巅峰') == -1
				&& SingletonMap.MapData.curMapName.indexOf('皇城') == -1
				&& SingletonMap.MapData.curMapName.indexOf('天梯赛') == -1
				&& SingletonMap.MapData.curMapName.indexOf('峡谷决战') == -1
				&& SingletonMap.MapData.curMapName.indexOf('修罗幻境') == -1
				&& SingletonMap.MapData.curMapName.indexOf('修罗战场') == -1
				&& SingletonMap.MapData.curMapName.indexOf('光明神殿') == -1
				&& SingletonMap.MapData.curMapName.indexOf('刺激战场') == -1
				&& SingletonMap.MapData.curMapName.indexOf('皇宫') == -1) {
				SingletonMap.o_eo.o_89(1);
			}
		}
	}
	catch(e) {}
}
// 领周卡月卡奖励
var weekCardAwardXiyou = false;
var weekCardAward = false;
var monthCardAward = false;
var xiuLuoAward = false;
function getWeekCardDayAward() {
	try {
		if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
            return;
        }
        if (!hasPri()) return;
		if (SingletonMap.XiyouMgr.o_UVf != 0) {
			if (SingletonMap.RoleData.o_OuN() > 0 && SingletonMap.XiyouMgr.o_UVf != 2 && weekCardAwardXiyou == false) {
				weekCardAwardXiyou = true;
				SingletonMap.o_yKf.o_Un6(); // 周卡领取仙府奖励
			}
			
			if (SingletonMap.RoleData.o_OuN() > 0 && weekCardAward == false) {
				weekCardAward = true;
				SingletonMap.o_lK4.o_eLR(1); // 周卡每日奖励
			}
			
			if (SingletonMap.o_Hm_.o_Hmz() > SingletonMap.o_Hm_.o_HuP && !SingletonMap.o_Hm_.o_HmC() && xiuLuoAward == false) { // 修罗每日特权
				xiuLuoAward = true;
				SingletonMap.o_Os4.o_Hm8(2);
			}
		}
		
		if (SingletonMap.RoleData.o_OpO() > 0 && monthCardAward == false) {
			monthCardAward = true;
			SingletonMap.o_lK4.o_eLR(2); // 月卡每日奖励
		}
	}
	catch(e) {}
}

function autoLoopSmallTask() {
	autoPet();
	autoJuBaoPen();	
	autoHp();
	autoJobSkill();
	resetBossTime();
	breakout();
	getWeekCardDayAward();
}

function getMonthDayStr() {
	let result = '';
	try {
		let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
		let curMonth = curDate.getMonth();
		let curDateNum = curDate.getDate();
		result = curMonth + '_' + curDateNum;
	}
	catch(e) {}
	return result;
}

class TaskStateManager {
	static taskMap = {};
	
	static clearTaskFlag() {
		TaskStateManager.taskMap = {};
	}
	
	static getTaskFlag(taskName) {
		let monthDayStr = getMonthDayStr();
		let taskFlag = monthDayStr + "_" + taskName;
		return taskFlag;
	}
	
	static isFirtTimeEnter(taskName) {		
		let taskFlag = TaskStateManager.getTaskFlag(taskName);
		let result = TaskStateManager.taskMap[taskFlag] ? false : true;
		return result;
	}
	
	static updateTaskEnterState(taskName) {
		let taskFlag = TaskStateManager.getTaskFlag(taskName);
		TaskStateManager.taskMap[taskFlag] = true;
	}
}

function canMove(x, y) {
	let result = false;
	try {
		let arr = SingletonMap.o_etq.o_egw(x, y, PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, true);
		if (arr && arr.length > 0) {
			result = true;
		}
	}
	catch(exx) {
	}
	return result;
}

function isShenZunMap() {
	let result = false;
	try {
		result = ['胧月仙境','绿茵幽谷','云雾之巅'].find(M => SingletonMap.MapData.curMapName.indexOf(M)> -1);
	}
	catch(exx) {
	}
	return result;
}

function needExitScene() {
	let result = false;
	try {
		if (['玛法幻境', '苍月岛', '王城', '修罗幻境', '西游降魔', '神界主城', "神族主城", "魔族主城", "天界主城", "红名监狱"].includes(SingletonMap.MapData.curMapName)) {
				result = false;
			}
			else if (isShenZunMap()) {
				result = true;
			}
			else if (SingletonMap.XiyouMgr.o_yWb() && SingletonMap.MapData.curMapName != '西游降魔') {
				result = true;
			}
			else if (SingletonMap.ShenJieMgr.o_UEk() && SingletonMap.MapData.curMapName != "神界主城") {
				result = true;
			}
			else if (SingletonMap.o_Ntl.o_bj() && SingletonMap.MapData.curMapName != "玛法幻境") {
				result = true;
			}
			else if (['神魔深渊一层', '神魔深渊二层',  "上古遗迹一层", "上古遗迹二层", "上古遗迹三层" ].includes(SingletonMap.MapData.curMapName)) {
				result = true;
			}
			else if (['福利地图','高爆地图①','福利地图②','个人BOSS','黑暗矿洞', '藏宝阁', '苍月秘境','寻龙秘境', '神龙秘境', '星空炼狱', '轮回秘境', '八卦神殿', '宠物神殿', 'VIP神殿', '神魔战场', '神龙山庄'].includes(SingletonMap.MapData.curMapName)) {
				result = true;
			}
			else if (['ChongWuBoss','mijingBOSS','gerenBOSS','8-10huolong2','1-13guajisanceng','8-19shenmobianjie','8-23shenmoshenyuan','1-13guajierceng','kuangdong', 'vipBOSS', 'smsd',
			'4-3VIPshendian', 'ShenYuBoss', 'shijieBOSS','ShenYuBoss-KF', 'sjslfb', 'xingkong', 'hasd', 'yonghengmigong', 'jctdbc'].includes(SingletonMap.MapData.curMapFilePath)) {
				result = true;
			}
	}
	catch(ex){
	}
	return result;
}

function exitScene(){
	let self = this;
	SceneRobotLock.lock();
	function check() {
		return xSleep(800).then(async function() {
			try {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				if (!mainRobotIsRunning()) { // 停止挂机
					SceneRobotLock.unLock();
					return;
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					SceneRobotLock.unLock();
					return;
				}
				
				if (needExitScene()) {
					const curMapName = SingletonMap.MapData.curMapName;
					console.warn(`exit scene ${SingletonMap.MapData.curMapName}...`);
					gameObjectInstMap.ZjmTaskConView.o_OiC({ target: gameObjectInstMap.ZjmTaskConView.exitBtn });
					await changeScene(curMapName);
					return check();
				}
				else {
					SceneRobotLock.unLock();
					console.warn('Complete the exit scene 2...');
					return;
				}
			}
			catch(ex){
				console.error(ex);
			}
			return check();
		});
	}
	return check();
}

function changeScene(curMapName){
	let self = this;
	function check() {
		return xSleep(300).then(function () {
			if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
				return check();
			}

			if (!mainRobotIsRunning()) { // 停止挂机
				return;
			}
			
			if (SingletonMap.ShenJieMgr.o_UWs() && SingletonMap.o_n.isOpen("WarnNormalWin")) { // 黑暗神殿
				gameObjectInstMap.ZjmTaskConView.o_lmT();
				closeGameWin("WarnNormalWin");
				return check();
			}
			else if (SingletonMap.ShenJieMgr.o_UWp() && SingletonMap.o_n.isOpen("WarnNormalWin")) { // 光明神殿
				SingletonMap.o_Uij.o_UsE();
				closeGameWin("WarnNormalWin");
				return check();
			}
			
			if (curMapName != SingletonMap.MapData.curMapName) {
				return;
			}
			
			return check();
		});
	}
	return check();
}

var shenJieCaiLiaoMonsterConfig = {
    "1,1": "24,57|50,44|50,71|30,128|62,125|162,79|153,119|143,138|126,28|70,34|95,21",
    "1,2": "24,57|50,44|50,71|30,128|62,125|162,79|153,119|143,138|126,28|70,34|95,21",
    "2,1": "79,33|54,32|24,31|81,53|80,80|67,106|37,109|18,100|25,60|62,74",
    "2,2": "79,33|54,32|24,31|81,53|80,80|67,106|37,109|18,100|25,60|62,74",
    "3,1": "95,30|97,106|60,85|27,101|32,44|47,29",
    "4,1": "35,137|59,109|68,71|28,56|47,27|88,31|124,23|93,52|120,93|93,143",
    "5,1": "45,117|66,112|100,160|110,132|126,112|121,75|131,65|118,42|99,38|66,70|53,56",
    "6,1": "31,107|76,131|127,133|110,98|147,86|86,66|71,40|106,29",
    "7,1": "15,129|24,81|54,18|40,134|117,69|86,175|128,128|143,39",
    "8,1": "22,128|31,71|91,26|39,122|99,171|122,114|136,91|118,41",
    "8,2": "22,128|31,71|91,26|39,122|99,171|122,114|136,91|118,41",
    "9,1": "23,97|53,112|81,96|87,68|31,43|54,25|71,29|88,41|91,10|12,22",
    "9,2": "23,97|53,112|81,96|87,68|31,43|54,25|71,29|88,41|91,10|12,22",
    "10,1": "37,101|69,66|101,31|78,140|139,71|117,178|182,110|110,105|151,144|91,199|53,163|17,125|32,186",
    "10,2": "37,101|69,66|101,31|78,140|139,71|117,178|182,110|110,105|151,144|91,199|53,163|17,125|32,186",
    "11,1": "24,81|33,91|41,78|51,67|66,52|79,33|54,40|36,25",
    "12,1": "26,20|45,34|62,47|81,28|74,67|94,82|117,105|139,88|136,131|103,140|82,111|62,97|40,110|22,118",
    "13,1": "33,69|15,58|48,57|31,41|46,29|25,22",
    "14,1": "58,32|50,46|31,58|13,48|25,23|12,10"
};

async function gotoWangChen(notLock) {
	let curTime = null;
	let arrFlag = {};
	let self = this;
	function check() {
		return xSleep(800).then(async function () {
			try {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;				

				if (SingletonMap.MapData.curMapName == "王城") {
					console.warn("前往王城======>回到王城安全区1");
					SingletonMap.o_eaX.o_lxG(2);
					await awaitToMap('王城');
					return;
				}
				else if (!SingletonMap.ServerCross.o_ePe && SingletonMap.MapData.curMapName != "王城") { // 在本服
					SingletonMap.o_eaX.o_lxG(2);
					await xSleep(100);
					return check();
				}
				else if (SingletonMap.ServerCross.o_ePe) { // 在其它跨服
					console.warn(`前往王城======>在跨服${SingletonMap.MapData.curMapName}，先退出跨服`);
					const curMapName = SingletonMap.MapData.curMapName;
					SingletonMap.o_Yr.o_NZ8();
					await changeScene(curMapName);
				}
			}
			catch (e) {
			}
			return check();
		});
	}
	return check();
}

class BaseTask {
	needExitScene() {
		let result = false;
		try {
			result = needExitScene();
		}
		catch(ex){
		}
		return result;
	}

	exitScene(){
		let self = this;
		function check() {
			return xSleep(800).then(async function() {
				try {
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
						return check();
					}

					if (!mainRobotIsRunning()) { // 停止挂机
						return;
					}
					
					if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
						return;
					}
					
					if (!self.isRunning()) return;
					if (self.stopTask) return;
					
					if (self.needExitScene()) {
						const curMapName = SingletonMap.MapData.curMapName;
						console.warn(`exit scene ${SingletonMap.MapData.curMapName}...`);
						gameObjectInstMap.ZjmTaskConView.o_OiC({ target: gameObjectInstMap.ZjmTaskConView.exitBtn });
						await self.changeScene(curMapName);
						return check();
					}
					else {
						console.warn('Complete the exit scene...');
						return;
					}
				}
				catch(ex){
					console.error(ex);
				}
				return check();
			});
		}
		return check();
	}

	changeScene(curMapName){
		let self = this;
		function check() {
			return xSleep(300).then(function () {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (!self.isRunning()) return;
				if (self.stopTask) return;
				
				if (SingletonMap.ShenJieMgr.o_UWs() && SingletonMap.o_n.isOpen("WarnNormalWin")) {
					gameObjectInstMap.ZjmTaskConView.o_lmT();
					closeGameWin("WarnNormalWin");
					return check();
				}
				else if (SingletonMap.ShenJieMgr.o_UWp() && SingletonMap.o_n.isOpen("WarnNormalWin")) { // 光明神殿
					SingletonMap.o_Uij.o_UsE();
					closeGameWin("WarnNormalWin");
					return check();
				}
				
				if (curMapName != SingletonMap.MapData.curMapName) {
					return;
				}
				
				return check();
			});
		}
		return check();
	}
	//回到王城 
	gotoWangChen(notLock) {
		let self = this;
		let curTime = null;
		let arrFlag = {};
		function check() {
			return xSleep(800).then(async function () {
				try {
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
						return check();
					}

					if (!mainRobotIsRunning()) { // 停止挂机
						return;
					}
					
					if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
						return;
					}
					if (!self.isRunning()) return;
					if (self.stopTask) return;

					if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
						return check();
					}
					curTime = SingletonMap.ServerTimeManager.o_eYF;				

					if (SingletonMap.MapData.curMapName == "王城") {
						SingletonMap.o_eaX.o_lxG(2);
						await xSleep(100);
						await awaitToMap('王城');
						console.warn("前往王城======>回到王城安全区1");
						return;
					}
					else if (!SingletonMap.ServerCross.o_ePe && SingletonMap.MapData.curMapName != "王城") { // 在本服
						SingletonMap.o_eaX.o_lxG(2);
						await xSleep(100);
						return check();
					}
					else if (SingletonMap.ServerCross.o_ePe) { // 在其它跨服
						console.warn(`前往王城======>在跨服${SingletonMap.MapData.curMapName}，先退出跨服`);
						SingletonMap.o_Yr.o_NZ8();
						await self.checkLocalServeMap();
						await xSleep(1000);
					}
				}
				catch (e) {
				}
				return check();
			});
		}
		return check();
	}

	// 进入玛法幻境
	gotoMafaHuanJin() {
		let arrFlag = {};
		let curTime = null;
		let self = this;
		function check() {
			return xSleep(800).then(async function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					// loading
					return check();
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (SingletonMap.MapData.curMapName == "玛法幻境") {
					console.warn(`前往玛法幻境======>到达玛法幻境`);
					await xSleep(100);
					return;
				}
				else if (SingletonMap.o_Ntl.o_bj() && SingletonMap.MapData.curMapName != "玛法幻境") { // 在玛法，不在幻境
					console.warn(`前往玛法幻境===${SingletonMap.MapData.curMapName}===>退出玛法地图`);
					SingletonMap.o_Ozj.o_Vn();
					SingletonMap.o_N42.o_ytx();
					await xSleep(100);
					await awaitToMap('玛法幻境');
				}
				else if (!SingletonMap.ServerCross.o_ePe && SingletonMap.MapData.curMapName != "王城") { // 在本服其它图
					SingletonMap.o_eaX.o_lxG(2);
					await xSleep(100);
					await awaitToMap('王城');
					console.warn(`前往玛法幻境===${SingletonMap.MapData.curMapName}===>回到王城安全区`);
				}
				else if (SingletonMap.MapData.curMapName == "王城") {
					console.warn(`前往玛法幻境======>回到王城安全区`);
					SingletonMap.o_eaX.o_lxG(2);
					await xSleep(100);
					await awaitToMap('王城');
					console.warn("前往玛法幻境======>进入玛法幻境");
					SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq);
					await awaitToMap('玛法幻境');
				}
				else if (SingletonMap.ServerCross.o_ePe && !SingletonMap.o_Ntl.o_bj()) { // 在跨服并且不在玛法
					console.warn(`前往玛法幻境======>在跨服${SingletonMap.MapData.curMapName}，先退出跨服`);
					SingletonMap.o_Yr.o_NZ8();
					await self.checkLocalServeMap();
					await xSleep(1000);
				}
				return check();
			});
		}
		return check();
	}

	gotoMafaVilla(mapId, layerId) {
		//if (MoveLock && MoveLock.isStop && MoveLock.isStop()) return;
		let arrFlag = {};
		let curTime = null;
		let self = this;
		function check() {
			return xSleep(800).then(async function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					// loading
					return check();
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (SingletonMap.MapData.curMapName == "玛法幻境") {
					console.warn(`进入玛法别墅======>进入玛法地图mapId=${mapId},layerId=${layerId}`);
					SingletonMap.o_Ozj.o_NC5(mapId, layerId);
					await xSleep(100);
					return;
				}
				else if (SingletonMap.o_Ntl.o_bj() && SingletonMap.MapData.curMapName != "玛法幻境") { // 在玛法，不在幻境
					console.warn(`进入玛法别墅====${SingletonMap.MapData.curMapName}==>退出幻境地图`);
					SingletonMap.o_Ozj.o_Vn();
					SingletonMap.o_N42.o_ytx();
					await xSleep(100);
				}
				else if (!arrFlag['1'] && SingletonMap.MapData.curMapName == "王城") {
					console.warn("进入玛法别墅======>回到王城安全区");
					arrFlag['1'] = true;
					SingletonMap.o_eaX.o_lxG(2);
					await xSleep(100);
				}
				else if (!SingletonMap.ServerCross.o_ePe && SingletonMap.MapData.curMapName != "王城") { // 在本服
					//const curMapName = SingletonMap.MapData.curMapName;
					console.warn(`进入玛法别墅====${SingletonMap.MapData.curMapName}==>回到王城安全区`);
					SingletonMap.o_eaX.o_lxG(2);
					await xSleep(100);
					await awaitToMap('王城');
				}
				else if (SingletonMap.MapData.curMapName == "王城") {
					let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
					let curHours = curDate.getHours();
					await xSleep(100);
					if (curHours >= 10 && curHours < 23) {
						console.warn(`进入玛法别墅====== 王城==>前往玛法幻境`);
						SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq); // 进入幻境
						await awaitToMap('玛法幻境');
					}
					else { // 通宵直接进别墅
						console.warn(`进入玛法别墅======>进入幻境mapId=${mapId},layerId=${layerId}`);
						SingletonMap.o_Ozj.o_NC5(mapId, layerId);
						await awaitToMap('别墅');
						return;
					}
				}
				else if (SingletonMap.ServerCross.o_ePe && !SingletonMap.o_Ntl.o_bj()) { // 在跨服并且不在玛法
					await xSleep(100);
					console.warn(`进入玛法别墅======>在跨服${SingletonMap.MapData.curMapName}，先退出跨服`);
					SingletonMap.o_Yr.o_NZ8();
					await self.checkLocalServeMap();
					await xSleep(100);
				}
				return check();
			});
		}
		return check();
	}
	
	/**前往苍月岛 **/
	gotoChanYueDao() {
		let arrFlag = {};
		let self = this;
		let curTime = null;
		function check() {
			return xSleep(800).then(async function () {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				// 停止挂机
				if (!mainRobotIsRunning()) {
					return;
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (SingletonMap.MapData.curMapName == "苍月岛") {
					console.warn('前往苍月岛===>到达苍月岛');
					return;
				}
				else if (SingletonMap.MapData.curMapName == "神龙山庄") {
					console.warn('前往苍月岛=== 神龙山庄==>前往苍月岛');
					SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
					await awaitToMap('苍月岛');
					return;
				}
				else if (SingletonMap.MapData.curMapName == "王城") {
					console.warn('前往苍月岛=== 王城==>前往苍月岛');
					SingletonMap.o_Yr.o_NZ8();
					await awaitToMap('苍月岛');
				}
				else {
					await self.gotoWangChen();
				}
				return check();
			});
		}
		return check();
	}

	// 进入神魔大陆
	gotoShenMoZhuChen(notLock) {
		let arrFlag = {};
		let self = this;
		let curTime = null;
		function check() {
			return xSleep(800).then(async function () {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				// 停止挂机
				if (!mainRobotIsRunning()) {
					return;
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;
				
				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (["神族主城", "魔族主城"].includes(SingletonMap.MapData.curMapName)) {
					await xSleep(100);
					console.warn("进入神魔大陆======>到达神魔主城");
					// 关闭进图弹窗
					if (!SingletonMap.o_yy_.o_yqz(0)) {
						SingletonMap.o_yy_.o_yqw(0, true);
					}
					return;
				}
				else if (SingletonMap.MapData.curMapName == "王城") {                
					console.warn("进入神魔大陆======>前往神魔主城");
					SingletonMap.o_Yr.o_NZ8(KfActivity.o_yNg);
					await self.checkServerCrossMap();
					await xSleep(100);
				}
				else if (SingletonMap.MapData.curMapName != "王城") {
					console.warn("进入神魔大陆======>先回王城");
					await self.gotoWangChen();
					await xSleep(100);
				}
				return check();
			});
		}
		return check();
	}

	// 进入神魔边境
	gotoShenMoBianJin() {		
		let arrFlag = {};
		let self = this;
		let curTime = null;
		function check() {
			return xSleep(800).then(async function () {
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}

				// 停止挂机
				if (!mainRobotIsRunning()) {
					return;
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;
				
				
				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (SingletonMap.MapData.curMapName == '神魔边境') {
					console.warn("进入神魔边境======>到达神魔边境");
					return;
				}
				else if (["神族主城", "魔族主城"].includes(SingletonMap.MapData.curMapName)) {
					console.warn("进入神魔边境======>前往神魔边境");
					SingletonMap.o_eaX.o_lKj(0, TeleportPos.NewWorldCommon); //进神魔边境
					await xSleep(100);
				}
				else if (!["神族主城", "魔族主城"].includes(SingletonMap.MapData.curMapName)) {
					console.warn("进入神魔边境======>前往神魔主城");
					await self.gotoShenMoZhuChen(false);
					await xSleep(100);
				}
				return check();
			});
		}
		return check();
	}


	// 进入天界
	gotoTianJie() {
		let arrFlag = {};
		let curTime = null;
		let self = this;
		function check() {
			return xSleep(800).then(async function () {
				if (!mainRobotIsRunning()) {
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}
				
				if (MoveLock && MoveLock.isStop && MoveLock.isStop()) {
					return;
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;
				
				if (SingletonMap.MapData.curMapName == "天界主城") {
					console.warn("前往天界=======>到达天界主城");
					return;
				}
				else if (!SingletonMap.ServerCross.o_ePe) { // 在本服
					console.warn(`前往天界====${SingletonMap.MapData.curMapName}===>本服前往天界`);
					SingletonMap.o_lHL.o_OVF();
					await xSleep(100);
					await awaitToMap("天界主城");
				}
				else if (SingletonMap.ServerCross.o_ePe) { // 在跨服
					console.warn(`前往天界=======>在跨服${SingletonMap.MapData.curMapName}，先退出跨服`);
					SingletonMap.o_Yr.o_NZ8();
					await self.checkLocalServeMap();
					await xSleep(100);
				}
				return check();
			});
		}
		return check();
	}

	// 进入西游主城
	gotoXiYouZhuChen() {
		let curTime = null;
		let self = this;
		function check() {
			return xSleep(800).then(async function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}
				
				if (MoveLock.isStop()) {
					return;
				}

				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;

				if (SingletonMap.MapData.curMapName == '西游降魔') {
					console.warn("前往西游主城=======>到达西游主城1");
					return;
				}
				else if (SingletonMap.XiyouMgr.o_yWb() && SingletonMap.MapData.curMapName != '西游降魔') { // 在西游
					SingletonMap.o_eaX.o_lKj(0, TeleportPos.XiYouArea); // 回西游安全区
					console.warn(`前往西游主城=====${SingletonMap.MapData.curMapName}==>到达西游主城2`);
					await awaitToMap("西游降魔");
					MoveLock.unLock();
					return;
				}
				else if (SingletonMap.MapData.curMapName == '王城') {
					console.warn("前往西游主城=======>前往西游世界");
					SingletonMap.o_Yr.o_NZ8(KfActivity.o_ysk);
					await awaitToMap("西游降魔");
					await xSleep(500);
				}
				else if (!SingletonMap.ServerCross.o_ePe && SingletonMap.MapData.curMapName != '王城') { // 在本服，不在王城
					console.warn(`前往西游主城=====${SingletonMap.MapData.curMapName}==>回王城安全区`);
					SingletonMap.o_eaX.o_lxG(2);
					await awaitToMap("王城");
				}
				else if (SingletonMap.ServerCross.o_ePe && !SingletonMap.XiyouMgr.o_yWb()) { // 在跨服，不在西游
					console.warn(`前往西游主城=======>在跨服${SingletonMap.MapData.curMapName}, 退出跨服`);
					SingletonMap.o_Yr.o_NZ8();
					await self.checkLocalServeMap();
					await xSleep(500);
				}
				return check();
			});
		}
		return check();
	}

	// 进入神界主城
	gotoShenJie() {
		let arrFlag = {};
		let curTime = null;
		let self = this;
		function check() {
			return xSleep(800).then(async function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}
				
				if (MoveLock.isStop()) {
					return;
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;

				if (curTime && SingletonMap.ServerTimeManager.o_eYF - curTime < 1200) {
					return check();
				}
				curTime = SingletonMap.ServerTimeManager.o_eYF;
				
				if (SingletonMap.ShenJieMgr.o_UWs()) { // 在黑暗神殿
					console.warn("前往神界主城=======>退出黑暗神殿");
					gameObjectInstMap.ZjmTaskConView.o_OiC({ target: gameObjectInstMap.ZjmTaskConView.exitBtn }); // 回神界主城
					await awaitToMap("神界主城");
				}
				else if (SingletonMap.ShenJieMgr.o_UEk() && SingletonMap.MapData.curMapName != "神界主城") { // 在神界其它地图
					// await self.exitScene();
					console.warn("前往神界主城=======>前往神界主城1");
					gameObjectInstMap.ZjmTaskConView.o_OiC({ target: gameObjectInstMap.ZjmTaskConView.exitBtn });// 回神界主城
					await awaitToMap("神界主城");
					console.warn("前往神界主城=======>到达神界主城1");
					return;
				}
				else if (SingletonMap.MapData.curMapName == "神界主城") {
					console.warn("前往神界主城=======>到达神界主城2");
					return;
				}
				else if (SingletonMap.MapData.curMapName == "天界主城") {
					console.warn("前往神界主城=======>前往神界");
					SingletonMap.o_Yr.o_NZ8(KfActivity.o_Ui7);
					await awaitToMap("神界主城");
				}
				else if (SingletonMap.ServerCross.o_ePe && !SingletonMap.ShenJieMgr.o_UEk()) { // 在跨服不在神界
					console.warn(`前往神界主城=======>在跨服${SingletonMap.MapData.curMapName}, 退出跨服`);
					SingletonMap.o_Yr.o_NZ8();
					await self.checkLocalServeMap();
				}
				else if (!SingletonMap.ServerCross.o_ePe) { // 在本服,前往天界
					console.warn(`前往神界主城=======>在本服${SingletonMap.MapData.curMapName},前往天界`);
					SingletonMap.o_lHL.o_OVF();
					await awaitToMap("天界主城");
				}
				return check();
			});
		}
		return check();
	}

	/**等待回到本服 */
	checkLocalServeMap() {
		let self = this;
		function check() {
			return xSleep(800).then(function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					return check();
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;
				
				if (MoveLock.isStop()) {
					return;
				}
				if (!SingletonMap.ServerCross.o_ePe) {
					console.warn("前往本服=======>回到本服");
					return;
				}
				else {
					return check();
				}
			});
		}
		return check();
	}

	/**等待进入跨服 */
	checkServerCrossMap() {
		let self = this;
		function check() {
			return xSleep(800).then(function () {
				if (!mainRobotIsRunning()) { // 停止挂机
					return;
				}
				if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
					// loading
					return check();
				}
				if (MoveLock.isStop()) {
					return;
				}
				
				if (!self.isRunning()) return;
				if (self.stopTask) return;
				
				if (SingletonMap.ServerCross.o_ePe) {
					console.warn("前往跨服=======>到达跨服");
					return;
				}
				else {
					return check();
				}
			});
		}
		return check();
	}	
}

class StaticGuaJiTask extends BaseTask {
    static name = "StaticGuaJiTask";
    arrGuaJiMap = null;
    state = false;
    curGuaJiMap = null;
    uid = 0;
    instance = null;
    stopTask = false;
    findWayIsStop = true;
    attackIsStop = true;
    stopFindWay = false;
    hashCode = null;
    diePos = null;
	stopping = false;	

    setMaps(arrMap) {
        this.arrGuaJiMap = arrMap;
    }

    setCurGuaJiMap(map) {
        this.curGuaJiMap = map;
    }
	
	fuzzyFindMonterByName(name) {
		return SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.roleName.indexOf(name) > -1);
	}
	
	isOverTime() {
		let result = false;
		try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (bossNextWatchTime[this.constructor.name + '_' + curMonth + '_' + curDateNum + '_' + curHours] == true) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	updateOverTime() {
		try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			bossNextWatchTime[this.constructor.name + '_' + curMonth + '_' + curDateNum + '_' + curHours] = true;
        }
        catch (ex) {
            console.error(ex);
        }		
	}
	
	gotoTarget(monster) {
		try {
			if (!GlobalUtil.isAttackDistance(monster.currentX, monster.o_NI3)) {
				let targetPos = GlobalUtil.getTargetPos(monster.currentX, monster.o_NI3);
				if (targetPos) {
					setCurMonster(monster);
					SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
				}
				else {
					SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
				}
			}	
		}
		catch (ex) {}		
	}

    async stop() {
		if (!this.isRunning()) return;
		if (this.stopping) return;
		this.stopTask = true;
		this.stopping = true;
		MoveLock.stop();
		FirstKnife.clear();
        this.curMapName = null;
        this.curGuaJiMap = null;
        try {
			clearCurMonster();
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor.o_NLN.o_lkM(); // 取消寻路、自动寻宝
            SingletonMap.o_lAu.stop(); // 取消自带挂机状态
        } catch (ex) {
            console.error(ex);
        }
		try {
			if (this.constructor.name == XunBaoTask.name) {
				// 退出挖宝
				if (SingletonMap.o_NIo.o_OiB) SingletonMap.o_l.o_Ut1(appInstanceMap.o_H.o_O9x);
				SingletonMap.o_NIo.o_Ook = null;
				SingletonMap.o_NIo.o_OiB = false;
				PlayerActor.o_NLN.o_lkM();
				if (SingletonMap.o_Nlv.o_lJL) SingletonMap.o_Nlv.o_lv1();
				gameObjectInstMap.ZjmView.o_Yu();
			}
		}
		 catch (ex) {
            console.error(ex);
        }
		this.diePos = null;
        await this.stopTaskComplete();
		this.state = false;
		MoveLock.resume();
		closeGameWin("ShenJieDarkNpcWin");
		console.warn(`任务停止...${getTaskNameCn(this.constructor.name)}  uid=>${this.uid}`);
    }

    stopTaskComplete() {
        let self = this;
        function check() {
            return xSleep(500).then(function () {
                if (self.attackIsStop && self.findWayIsStop) {
                    return;
                }
                return check();
            });
        }
        return check();
    }

    enableParams() {

    }

    initState() {
        this.stopTask = false;
        this.findWayIsStop = true;
        this.attackIsStop = true;
        this.state = true;
		this.diePos = null;
		this.stopping = false;
		clearCurMonster();
    }

    setHashCode() {
        let hashCode = new Date().getTime();
        this.hashCode = hashCode;
        GlobalUtil.lastTaskHashCode = hashCode;
    }

    async start() {
        if (this.isRunning()) return;
		if (!mainRobotIsRunning()) return false;
		this.initState();
        this.enableParams();
        await this.run();
    }

    isRunning() {
        // return this.stopTask != true && this.state == true;
        return this.state == true;
    }

    startUtils() {
        try {
            QuickPickUpNormal.start();
            BlacklistMonitors.start();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    stopUtils() {
        try {
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
        }
        catch (ex) {
            console.error(ex);
        }
    }

	async run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            this.loopCancleNoBelongMonster();
            this.loopAttackMonter();
            await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    reviveFinish() {
        function check() {
            return xSleep(1000).then(function () {
                if (PlayerActor.o_NLN.curHP > 0) {
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }

    async revive() {
        try {
            SingletonMap.o_eaX.o_e5d(0);
            await this.reviveFinish();
            console.warn(`人物复活...`);
            await xSleep(1000);
            await this.backToMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            // if (!isSafeArea()) return;
            console.warn(`回到挂机地图...`);
            await this.enterMap(this.curGuaJiMap);
        } catch (ex) {
            console.error(ex);
        }
    }

    loopCancleNoBelongMonster() {
        let self = this;
        try {
            if (curMonster && !isNullBelong(curMonster) && !isSelfBelong(curMonster) && !BlacklistMonitors.isWhitePlayer(curMonster.belongingName)) { // 不打非归属怪
                //callbackObj.writelog('不打非归属怪');
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
            else if (isSelfPet(curMonster)) { // 是自己宠物
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
            else if (this.filterMonster && !this.filterMonster(curMonster)) {
                clearCurMonster();
                SingletonMap.o_lAu.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
        if (this.isRunning()) {
            setTimeout(() => {
                self.loopCancleNoBelongMonster();
            }, 100);
        }
    }

    findMonter() {
        let monster = null;
        try {
            // 归属怪优先
            if (curMonster && isSelfBelong(curMonster) && this.filterMonster(curMonster)) {
                let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (belongingMonster) {
                    monster = belongingMonster;
                    // console.warn(`发现归属怪 ==>curMonster.$hashCode=>${monster.roleName}`);
                }
            }
            //已选中，还没有归属
            if (curMonster && isNullBelong(curMonster) && this.filterMonster(curMonster)) {
                let noBelongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (noBelongingMonster) {
                    monster = noBelongingMonster;
                }
            }
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof Monster && !M.masterName && M.curHP > 100 && this.filterMonster(M)) {
                        // 没有归属或归属自己
                        if (isSelfBelong(M) || isNullBelong(M)) {
                            if (targetMonster == null) {
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                            else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                                // 距离近的优先
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                        }
                    }
                }

                if (targetMonster) {
                    monster = targetMonster;
                    // console.warn(`发现怪物 ==>$hashCode=>${monster.roleName}`);
                }
            }
            //  先打黑名单
            if (BlacklistMonitors.badPlayer != null) {
                monster = null;
                // console.warn(`发现黑名单 ==>`);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    async loopAttackMonter() {
        try {
            if (!mainRobotIsRunning()) {
                return;
            }
            if (curMonster) {
                curMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode && item.curHP > 100);
            }
            if (isSelfPet(curMonster)) { // 是自己宠物
                clearCurMonster();
            }
            if (!curMonster) {
                curMonster = this.findMonter();
            }
            if (curMonster && this.filterMonster(curMonster)) {
                // console.warn(`发现怪物 => ${curMonster.roleName}=${curMonster.$hashCode}`);
                setCurMonster(curMonster);
                SingletonMap.o_OKA.o_Pc(curMonster, true);
                await this.attackMonster(curMonster);
            }
        } catch (ex) {
            console.error(ex);
        }
        let self = this;
        if (self.loopAttackMonterTtimeoutID) {
            clearTimeout(self.loopAttackMonterTtimeoutID);
        }
        if (this.isRunning()) {
            self.loopAttackMonterTtimeoutID = setTimeout(() => {
                this.loopAttackMonter();
            }, 100);
        }
    }

    async enterMap(map) {
        await xSleep(1000);
    }

    getMapPointConfig(mapId) {

    }

    hasMonster() {
        let self = this;
        function check() {
            return xSleep(500).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
				// 数量已经打够了
				if (self.isEnough && self.isEnough()) {
					return;
				}
                if (curMonster) {
                    curMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode && item.curHP >= 100);
                }
                if (isSelfPet(curMonster)) { // 是自己宠物
                    clearCurMonster();
					return;
                }
                let monster = self.findMonter();
				if (SingletonMap.MapData.curMapName.indexOf('别墅') == -1) {
					if (monster == null || !SingletonMap.o_h7.o_lDb(monster)) {
						// console.warn('hasMonster...没有怪物了...');
						return;
					}
				}
                if (monster && (!self.filterMonster(monster) || GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 20)) {
                    // console.warn('hasMonster...距离超出范围...');
                    return;
                }
                if (monster) {
					if (canMove(monster.currentX, monster.o_NI3)) {
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						await self.attackMonster(monster);
					}
					else {
						return;
					}
                }
				else {
					return;					
				}
                return check();
            });
        }
        return check();
    }

    hasBadPlayer() {
        let self = this;
        function check() {
            return xSleep(500).then(function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                if (BlacklistMonitors.badPlayer == null) {
                    return;
                }
                return check();
            });
        }
        return check();
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				if (this.stopTask == true) break;
                let map = this.arrGuaJiMap[i].split(',');
                // 第一次进图或者当前地图不是目标地图
                //if (this.curGuaJiMap == null || !(this.curGuaJiMap[0] == map[0] && this.curGuaJiMap[1] == map[1])) {
                SingletonMap.o_lAu.stop();
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                //callbackObj.writelog(`进入地图 map==>${map}`);
                console.warn(`进入地图 map==>${map}`);
                // }
                let mapId = this.arrGuaJiMap[i];
                //callbackObj.writelog(`mapId==>${mapId}`);
                let pointConfig = this.getMapPointConfig(mapId);
                while (mainRobotIsRunning() && pointConfig.length > 0) {
                    this.enableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
					if (this.stopTask == true) break;
                    // console.warn(`任务${this.constructor.name}  hashCode=>${this.hashCode}  pointConfig length ==> ${pointConfig.length}`);
                    let minPos = pointConfig[0];
                    if (pointConfig.length > 1) {
                        minPos = pointConfig.reduce(function (a, b) {
                            let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
                            // let aDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, a.x, a.y);
                            let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
                            // let bDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b.x, b.y);
                            return path1.length >= path2.length ? b : a;
                        });
                    }
                    // 没有怪物后再寻路
                    await this.hasMonster();
                    //console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  前往最近的坐标 ==> ${JSON.stringify(minPos)}`);
                    await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
                    //console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  到达最近的坐标 ==> ${JSON.stringify(minPos)}`);
                    // 删除已经用掉的坐标
                    pointConfig = pointConfig.filter(M => {
                        return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
                    });
                    // 没有怪物后再离开
                    await this.hasMonster();
                    //callbackObj.writelog(`没有怪物了...`);

                    this.disableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
					QuickPickUpNormal.runPickUp();
					await xSleep(500);
                    // 剩余数量小于8,换图
                    //let monsterNum = getShenYuMonsterNum();
                    //if (monsterNum && monsterNum < 8) {
                    //    break;
                    //}
                }

                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        if (this.isRunning() && mainRobotIsRunning()) {
            // console.warn('打完一轮了，再次执行挂机 loopStartFlow...');
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }

    existsMonster(monster) {
        try {
            if (monster == null) {
                return false;
            }
            return SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == monster.$hashCode && item.curHP > 100) != undefined;
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    attackMonster(monster) {
        let self = this;
        if (!monster) return;
        this.attackIsStop = false;
        // if (curMonster && curMonster.$hashCode != monster.$hashCode) return;
        function check() {
            return xSleep(50).then(function () {
                SingletonMap.o_lAu.stop();
                self.attackIsStop = false;
                if (!mainRobotIsRunning()) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    //console.warn('attackMonster...1');
                    return;
                }

                // 收到停止指令
                if (self.stopTask == true) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    // console.warn('attackMonster...2');
                    return;
                }

                // 任务停止
                if (self.isRunning && !self.isRunning()) {
                    self.attackIsStop = true;
                    clearCurMonster();
                    // console.warn('attackMonster...3');
                    return;
                }

                if (self.attackIsStop) {
                    // console.warn('attackMonster...3-1');
                    return;
                }
     
                if (monster && monster.masterName) { // 是宠物
                    clearCurMonster();
                    self.attackIsStop = true;
                    // console.warn('attackMonster...5');
                    return;
                }

                // 不是想打的怪
                //if (!self.filterMonster(monster)) {
                //    self.attackIsStop = true;
                //     clearCurMonster();
                //    console.warn('attackMonster...6');
                //     return;
                // }

                if (monster && !isSelfBelong(monster) && !isNullBelong(monster) && !BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 不打非归属怪
                    //callbackObj.writelog('不打非归属怪');
                    clearCurMonster();
                    self.attackIsStop = true;
                    SingletonMap.o_lAu.stop();
                    // console.warn('attackMonster...7');
                    return;
                }

                // 发现黑名单
                if (BlacklistMonitors.badPlayer != null) {
                    //callbackObj.writelog(`发现黑名单，优先攻击黑名单`);
                    //  console.warn('attackMonster...8');
					self.attackIsStop = true;
                    return;
                }

                // 怪物消失
                if (!self.existsMonster(monster)) {
                    //callbackObj.writelog(`怪物消失，停止攻击`);
                    // clearCurMonster();
                    //   console.warn('attackMonster...9');
					self.attackIsStop = true;
                    return;
                }

                // 离怪物太远
                if (!GlobalUtil.isAttackDistance(monster.currentX, monster.o_NI3)) {
                    let targetPos = GlobalUtil.getTargetPos(monster.currentX, monster.o_NI3);
                    if (targetPos) {
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                        return check();
                    }
                    else {
                        SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
						return check();
                    }
                }
                // PlayerActor.o_NLN.o_lkM();
                setCurMonster(monster);
                SingletonMap.o_OKA.o_Pc(monster, true);
                return check();
            });
        }
        return check();
    }

    disableFindWay() {
        this.stopFindWay = true;
    }

    enableFindWay() {
        this.stopFindWay = false;
    }

    existsMonsterById(monsterId) {
        let result = null;
        try {
            if (monsterId) {
                result = SingletonMap.o_OFM.o_Nx1.find(item => item.curHP > 100 && !item.masterName && item.cfgId == monsterId);
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async findWayByPos(posX, posY, type, map, monsterId) {
        // console.warn(`任务${this.constructor.name} hashCode=>${this.uid} 开始寻路..x:${posX}_y:${posY}...type:${type}...map=>${map}...monsterId=>${monsterId}`);
        let self = this;
        this.findWayIsStop = false;
		GlobalUtil.curTokenId = new Date().getTime();
		this.tokenId = GlobalUtil.curTokenId;
		this.attackIsStop = true;
		clearCurMonster();
        function check() {
            return xSleep(500).then(function () {
                if (!mainRobotIsRunning()) {
                    self.findWayIsStop = true;
                    //console.warn('停止寻路...1');
                    return;
                }
                // 收到停止指令
                if (self.stopTask == true) {
                    self.findWayIsStop = true;
                    //console.warn('停止寻路...2');
                    return;
                }

                // 任务停止
                if (self.isRunning && !self.isRunning()) {
                    self.findWayIsStop = true;
                    //console.warn('停止寻路...3');
                    return;
                }
				
				// 令牌过期
				if (self.tokenId != GlobalUtil.curTokenId) {
					return;			
				}

                if (self.findWayIsStop) {
                    //console.warn('停止寻路...3-1');
                    return;
                }

                // 人物死亡
                if (!isAlive()) {
					self.findWayIsStop = true;
                    return;
                }

                // 发现黑名单
                if (BlacklistMonitors.badPlayer != null) {
                    //callbackObj.writelog(`发现黑名单，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    return check();
                }


                // 发现怪物
                let targetMonster = GlobalUtil.getMonsterById(monsterId);
                if (targetMonster && GlobalUtil.compterFindWayPos(posX, posY).length <= 6) {
                    self.findWayIsStop = true;
                    //console.warn('停止寻路...3-1');
                    // PlayerActor.o_NLN.o_lkM();
                    setCurMonster(targetMonster);
                    SingletonMap.o_OKA.o_Pc(targetMonster, true);
                    return;
                }

                if (GlobalUtil.compterFindWayPos(posX, posY).length <= 3) {
                    //callbackObj.writelog(`接近目标，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    self.findWayIsStop = true;
                    //console.warn('停止寻路...8');
                    return;
                }


                if (curMonster && !self.filterMonster(curMonster)) {
                    clearCurMonster();
                }

                // 正在打怪,暂停寻路
                if (self.existsMonster(curMonster)) {
                    //console.log(`锁定怪物，停止寻路...`);
                    //PlayerActor.o_NLN.o_lkM();
                    //SingletonMap.o_lAu.stop();
                    self.findWayIsStop = true;
                    // PlayerActor.o_NLN.o_lkM();
                    return;
                }
                SingletonMap.o_OKA.o_NSw(posX, posY);
                return check();
            });
        }
        return check();
    }

    filterMonster() {
        return true;
    }
}

// 神界普通挂机
class ShenJieGuaJiNormalTask extends StaticGuaJiTask {
    static name = "ShenJieGuaJiNormalTask";

    needRun() {
        if (!mainRobotIsRunning()) return false;
        if (this.isRunning()) return false;
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (!SingletonModelUtil.getRunningTask() && isCrossTime()) {
				if (this.isEnough()) {
					result = false;
				}
				else {
					result = true;
				}
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isEnough() {
		let result = false;
		try {
			if (getAppConfigKeyValue("EnableShenJieNormalOnlyVitality") == "TRUE" && SingletonMap.o_UuA.o_UE() == 0 && SingletonMap.o_UuA.o_J1() == 0) {
				result = true;
			}
		}
		catch (ex) {
        }
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let appConfig = getAppConfig();
            if (appConfig != "") {
				this.initState();		
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieNormalMap) {
                    let arrGuaJiMap = appConfig.ShenJieNormalMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
				await this.gotoShenJie();
				await xSleep(1000);
				await this.run();
            }
        } catch (ex) {
            console.error(ex);
        }
    }
	
	async run() {
		try {
			this.startUtils();
			// this.loopCheckHp();
			this.loopCancleNoBelongMonster();
			this.loopAttackMonter();
			await this.loopStartFlow();
		} catch (ex) {
			console.error(ex);
		}
		await this.stop();
	}
	
    async enterMap(map) {
        try {
            await this.gotoShenJie();
            await xSleep(500);
            if (map) {
                SingletonMap.o_Uij.o_UX4(map[0], map[1]);
                this.curMap = map;
            }
            await xSleep(500);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster(monster) {
    }

    filterMonster(monster) {
        if (!monster) return false;
        return true;
    }
    // 高暴
    isSuperMonster(cfgId) {
        if (!(cfgId >= 169001 && cfgId <= 169005) && !(cfgId >= 167001 && cfgId <= 167045)) {
            // console.log(cfgId + '==>true');
            return true;
        }
        //console.log(cfgId + '==>false');
        return false;
    }

    getMapPointConfig(mapId) {
		let arrResult = [];
		try {
			let pos = shenJieCaiLiaoMonsterConfig[mapId];
			let arrPos = pos.split("|");
			for (var i = 0; i < arrPos.length; i++) {
				let posItem = arrPos[i].split(",");
				arrResult.push({
					x: posItem[0],
					y: posItem[1]
				});
			}
		}
		catch(ex){
			
		}
        return arrResult;
    }

    existsMonster(monster) {
        let self = this;
        let isExists = false;
        try {
            if (monster == null) {
                return false;
            }
            isExists = super.existsMonster(monster);
        } catch (ex) {
            console.error(ex);
        }
        return isExists;
    }
	
	async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				if (!isCrossTime()) {
					break;
				}
				if (this.stopTask) break;
				if (this.isEnough()) {
					break;
				}
                let map = this.arrGuaJiMap[i].split(',');
                // 第一次进图或者当前地图不是目标地图
                //if (this.curGuaJiMap == null || !(this.curGuaJiMap[0] == map[0] && this.curGuaJiMap[1] == map[1])) {
                SingletonMap.o_lAu.stop();
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                //callbackObj.writelog(`进入地图 map==>${map}`);
                console.warn(`进入地图 map==>${map}`);
                // }
                let mapId = this.arrGuaJiMap[i];
                //callbackObj.writelog(`mapId==>${mapId}`);
                let pointConfig = this.getMapPointConfig(mapId);
				while (mainRobotIsRunning() && pointConfig.length > 0) {
					QuickPickUpNormal.runPickUp();					
					this.enableFindWay();
					if (!mainRobotIsRunning()) {
						break;
					}
					// 停止挂机了
					if (!this.isRunning()) {
						break;
					}
					if (this.stopTask) break;
					if (!isCrossTime()) {
						break;
					}
					if (this.isEnough()) {
						break;
					}
					// console.warn(`任务${this.constructor.name}  hashCode=>${this.hashCode}  pointConfig length ==> ${pointConfig.length}`);
					let minPos = pointConfig[0];
					if (pointConfig.length > 1) {
						minPos = pointConfig.reduce(function (a, b) {
							let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
							// let aDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, a.x, a.y);
							let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
							// let bDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b.x, b.y);
							return path1.length >= path2.length ? b : a;
						});
					}
					// 没有怪物后再寻路
					await this.hasMonster();
					//console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  前往最近的坐标 ==> ${JSON.stringify(minPos)}`);
					await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
					//console.log(`任务${this.constructor.name}  hashCode=>${this.hashCode}  到达最近的坐标 ==> ${JSON.stringify(minPos)}`);
					// 删除已经用掉的坐标
					pointConfig = pointConfig.filter(M => {
						return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
					});
					// 没有怪物后再离开
					await this.hasMonster();
					//callbackObj.writelog(`没有怪物了...`);
					QuickPickUpNormal.runPickUp();
					this.disableFindWay();
					if (!mainRobotIsRunning()) {
						break;
					}
					// 停止挂机了
					if (!this.isRunning()) {
						break;
					}
					if (this.stopTask) break;
					if (!isCrossTime()) {
						break;
					}
					if (this.diePos != null) {
						continue;							
					}
					// 剩余数量小于8,换图
					let bossThreshold = getAppConfigKeyValue("BossThreshold") || 3;
					bossThreshold = parseInt(bossThreshold);
					let monsterNum = getShenYuMonsterNum();
					if (monsterNum && monsterNum < bossThreshold) {
						QuickPickUpNormal.runPickUp();
						await xSleep(500);
						break;
					}
				}

                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
    }
}

// 神界精英怪
class ShenJieGuaJiTask extends StaticGuaJiTask {
    static name = "ShenJieGuaJiTask";

    needRun() {
        if (!mainRobotIsRunning()) return false;
        if (this.isRunning()) return false;
        let result = false;
        try {
            if (isCrossTime() && (!bossNextWatchTime[this.constructor.name] || bossNextWatchTime[this.constructor.name] >= SingletonMap.ServerTimeManager.o_eYF)) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let appConfig = getAppConfig();
            if (appConfig != "") {
				this.initState();			
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieMap) {
                    let arrGuaJiMap = appConfig.ShenJieMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
				await this.gotoShenJie();
				await xSleep(300);
				await this.run();
            }
        } catch (ex) {
            console.error(ex);
        }
		bossNextWatchTime[this.constructor.name] = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 60 * 2; // 2小时
		await this.stop();
    }
	
	async run() {
        try {
            this.startUtils();
            this.loopCancleNoBelongMonster();
            this.loopAttackMonter();
            await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(map) {
        try {
            await this.gotoShenJie();
            await xSleep(500);
            if (map) {
                SingletonMap.o_Uij.o_UX4(map[0], map[1]);
                this.curMap = map;
            }
            await xSleep(500);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster(monster) {
    }

    filterMonster(monster) {
        if (!monster) return false;
        return this.isSuperMonster(monster.cfgId);
    }
    // 高暴
    isSuperMonster(cfgId) {
        if (!(cfgId >= 169001 && cfgId <= 169005) && !(cfgId >= 167001 && cfgId <= 167045)) {
            // console.log(cfgId + '==>true');
            return true;
        }
        //console.log(cfgId + '==>false');
        return false;
    }

    getMapPointConfig(mapId) {
		let arrResult = [];
		try {
			let pos = shenJieCaiLiaoMonsterConfig[mapId];
			let arrPos = pos.split("|");
			for (var i = 0; i < arrPos.length; i++) {
				let posItem = arrPos[i].split(",");
				arrResult.push({
					x: posItem[0],
					y: posItem[1]
				});
			}
		}
		catch(ex) {}
        return arrResult;
    }

    existsMonster(monster) {
        let self = this;
        let isExists = false;
        try {
            if (monster == null) {
                return false;
            }
            isExists = SingletonMap.o_OFM.o_Nx1.find(item => item.curHP > 100 && !item.masterName && item.$hashCode == monster.$hashCode && self.isSuperMonster(monster.cfgId)) != undefined;
        } catch (ex) {
            console.error(ex);
        }
        return isExists;
    }
	
	async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				if (this.stopTask == true) break;
                let map = this.arrGuaJiMap[i].split(',');
                SingletonMap.o_lAu.stop();
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
                console.warn(`进入地图 map==>${map}`);
                // }
                let mapId = this.arrGuaJiMap[i];
                //callbackObj.writelog(`mapId==>${mapId}`);
                let pointConfig = this.getMapPointConfig(mapId);
                while (mainRobotIsRunning() && pointConfig.length > 0) {
                    this.enableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
					if (this.stopTask == true) break;
                    let minPos = pointConfig[0];
                    if (pointConfig.length > 1) {
                        minPos = pointConfig.reduce(function (a, b) {
                            let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
                            let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
                            return path1.length >= path2.length ? b : a;
                        });
                    }
                    // 没有怪物后再寻路
                    await this.hasMonster();
                    await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
                    // 删除已经用掉的坐标
                    pointConfig = pointConfig.filter(M => {
                        return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
                    });
                    // 没有怪物后再离开
                    await this.hasMonster();

                    this.disableFindWay();
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
					QuickPickUpNormal.runPickUp();
					await xSleep(500);
                }

                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
    }
}

// 神魔边境
class shenMoBianJinTask extends StaticGuaJiTask {
    static name = "shenMoBianJinTask";
    arrMapPosTemplate = [{ x: 68, y: 73 }, { x: 80, y: 90 }, { x: 97, y: 111 }, { x: 119, y: 115 }, { x: 141, y: 114 }, { x: 163, y: 111 }, { x: 166, y: 83 }, { x: 139, y: 69 }, { x: 123, y: 36 }];
    attackBoos = false;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) { // 每月1号休战
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && (curHours == 23 && curMinutes >= 30 || curHours == 0 && curMinutes <= 30)) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
            let appConfig = getAppConfig();
            if (appConfig != "") {
				this.initState();			
				SingletonMap.o_yyA.o_yDG();
                appConfig = JSON.parse(appConfig);
                //this.attackBoos = appConfig.EnableShenMoBianJinBoss == 'TRUE' ? true : false;
				this.hashCode = new Date().getTime();
				await this.gotoShenMoBianJin();
				super.run();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
            SingletonMap.o_eaX.o_lKj(0, TeleportPos.NewWorldCommon); //进神魔边境
            await (500);
        } catch (ex) {
            console.error(ex);
        }
    }
    afterFindMonster() { }

    filterMonster(monster) {
        // 不打boss
        if (monster) {
            return monster.roleName.indexOf('★') == -1;
        }
        return true;
    }

    getLiveBoss() {
        let result = null;
        try {
            for (let key in SingletonMap.o_eB9.o_yHl) {
                let bossTime = SingletonMap.o_eB9.o_yHl[key];
                if (bossTime < SingletonMap.ServerTimeManager.o_eYF) {
                    result = SingletonMap.o_O.o_yU9.find(item => item.index == key);
                    break;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async loopStartFlow() {
        let self = this;
        try {
            let pointConfig = Object.assign([], this.arrMapPosTemplate);
            while (pointConfig.length > 0) {
				QuickPickUpNormal.runPickUp();
				await xSleep(500);
                this.enableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
				
				if (!this.isActivityTime()) {
					break;
				}
                // console.warn(`任务${this.constructor.name}  hashCode=>${this.hashCode}  pointConfig length ==> ${pointConfig.length}`);

                // 勾选了boss
                //if (this.attackBoos) {
                //    let liveBoss = this.getLiveBoss();
                //    if (liveBoss != null) {
                //        await this.findWayByPos(liveBoss.bossPos[0], liveBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                //        await this.hasMonster();
                //    }
                //}
                let minPos = pointConfig[0];
                if (pointConfig.length > 1) {
                    minPos = pointConfig.reduce(function (a, b) {
                        let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
                        let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
                        return path1.length >= path2.length ? b : a;
                    });
                }
                // 没有怪物后再寻路
                await this.hasMonster();
                //console.log(`任务${this.constructor.name}  uid=>${this.uid}  前往最近的坐标 ==> ${JSON.stringify(minPos)}`);
                await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
                //console.log(`任务${this.constructor.name}  uid=>${this.uid}  到达最近的坐标 ==> ${JSON.stringify(minPos)}`);
                // 删除已经用掉的坐标
                pointConfig = pointConfig.filter(M => {
                    return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
                });
                // 没有怪物后再离开
                await this.hasMonster();
                //callbackObj.writelog(`没有怪物了...`);
				QuickPickUpNormal.runPickUp();
				await xSleep(500);
                this.disableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        if (this.isRunning() && mainRobotIsRunning()) {
            await self.loopStartFlow();
        }
        else {
            await super.stop();
			await this.gotoWangChen();
        }
    }
}

// 星空之力
class XinKongZhiLiTask extends StaticGuaJiTask {
    static name = "XinKongZhiLiTask";

    arrBaseXinKongBoss = ["超级男人", "雷电之子", "机械先锋"];
    arrXinKongBoss = [];
    mapPointConfigTemplate = [{ x: 61, y: 82 }, { x: 63, y: 69 }, { x: 58, y: 60 }, { x: 54, y: 43 }, { x: 37, y: 34 }, { x: 27, y: 41 }, { x: 15, y: 29 }];
    mapPointConfig = [];

    needRun() {
        if (this.isRunning()) return false;
        if (!mainRobotIsRunning()) return false;
        return true;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
			this.initState();        
            this.arrXinKongBoss = Object.assign([], this.arrBaseXinKongBoss); // 重置配置
            this.mapPointConfig = Object.assign([], this.mapPointConfigTemplate);
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XingKongZhiLi) {
                    let monsterNames = appConfig.XingKongZhiLi.split('|');
                    this.arrXinKongBoss = this.arrXinKongBoss.concat(monsterNames);
                    // 不打boss不走boss坐标
                    if (this.arrXinKongBoss.join(",").indexOf("绿色巨人") == -1) {
                        this.mapPointConfig.splice(3, 1);
                    }
                }
                else {// 一条都没勾选
                    this.mapPointConfig.splice(3, 1);
                }

                let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                let curHours = curDate.getHours();
                let curDay = curDate.getDate();

                let openDate = new Date(SingletonMap.o_N42.o_N4K);
                let openDay = openDate.getDate();

                if (curHours >= 10 && curHours < 23) { //跨服时段先进幻境
                    //if (SingletonMap.o_Ntl.o_NxW != 1) {
                    //    SingletonMap.o_Ozj.o_N5I(); // 开启玛法别墅
                    //    await xSleep(1000);
                    //}
                    await this.gotoMafaHuanJin();
                    // SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq); // 进入幻境
                    // await awaitToMap("玛法幻境");
                    // console.warn(`进入幻境...`);
                }
                else { // 通宵时段直接从王城进入
                    await this.gotoWangChen(); // 先回王城
                    await awaitToMap("王城");
                }
                await this.loopStartFlow();
            }
                
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async loopStartFlow() {
        try {
            // console.warn('开始别墅loopStartFlow...');
            await this.enterMap();
            await xSleep(100);
            SingletonMap.o_lAu.stop();
            for (let i = 0; i < this.mapPointConfig.length; i++) {
				if (!mainRobotIsRunning()) break;
				if (!this.isRunning()) break;
                let pos = this.mapPointConfig[i];
                await this.findWayByPos(pos.x, pos.y, 'findWay', SingletonMap.MapData.curMapName);
				if (!mainRobotIsRunning()) break;
				if (!this.isRunning()) break;
				await xSleep(500);
                await this.waitBossKill();
				SingletonMap.o_lAu.stop();
                QuickPickUpNormal.runPickUp();
                // console.warn('没有怪物了...');
                // 停止挂机了
                if (!this.isRunning()) {
					QuickPickUpNormal.runPickUp();
                    break;
                }
                await xSleep(300);
            }
            await xSleep(300);
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	async waitBossKill() {
		let self = this;
        function check() {
            return xSleep(200).then(function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
				
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return;
                }
				
                let monster = self.findMonter();
                if (monster && self.filterMonster(monster)) {
					if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 3) {
						SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
						return check();
					}
					else {
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
                }
				return;
            });
        }
        return check();
	}

    async exitMap() {
        try {
            await this.exitScene();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
            // SingletonMap.o_N42.o_N5U = 8;
            // SingletonMap.o_Ozj.o_NC5(4, 2);
            // await awaitToMap("三亚海滩");
            await this.gotoMafaVilla(4, 2);
        } catch (ex) {
            console.error(ex);
        }
    }

    getMapPointConfig() {
        return this.mapPointConfig;
    }

    afterFindMonster() { }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrXinKongBoss.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }
}
// 玛法别墅
class StaticMafaBossTask extends StaticGuaJiTask {
    static name = "StaticMafaBossTask";
    state = false;
    arrBossPosTemplate = [];
    arrBossName = [];
    arrBossPos = [];
    diePos = null;
    curMapName = null;
    nextRunTime = null;
    bossId = null;

    bosIsAlive() {
        let result = false;
        try {
            let bossTime = villaMafaBossTime[this.bossId];
            if (!bossTime) {
                result = true;
            }
            else {
                let M = Math.floor((bossTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                result = M <= 0 ? true : false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    updateNextRunTime(time) {
        this.nextRunTime = time;
        let nextRunDateStr = new Date(this.nextRunTime).toLocaleString();
        console.warn(`任务${this.constructor.name}下次执行时间${nextRunDateStr}`);
    }

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			// SingletonMap.RoleData.o_N55() < 1 && SingletonMap.RoleData.o_N5T() < 1   未激活特权
			// !SingletonMap.o_N42.o_N5O() 每天 23点到10点开启别墅
			// 非通宵
			if (isCrossTime()) {
				if (this.bossId != null) {
					result = this.bosIsAlive();
				}
				else if (this.nextRunTime == null) {
					result = true;
				}
				else if (this.nextRunTime != null) {
					let M = Math.floor((this.nextRunTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
					result = M <= 0 ? true : false;
				}
			}
			// SingletonMap.ServerTimeManager.o_eYF < SingletonMap.o_N42.o_N4K == false 表示可以开通宵
			// 通宵 时段  
			else if (SingletonMap.ServerTimeManager.o_eYF < SingletonMap.o_N42.o_N4K) {
				if (this.bossId != null) {
					result = this.bosIsAlive();
				}
				else if (this.nextRunTime == null) {
					result = true;
				}
				else if (this.nextRunTime != null) {
					let M = Math.floor((this.nextRunTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
					result = M <= 0 ? true : false;
				}
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (SingletonMap.RoleData.o_N55() < 1 && SingletonMap.RoleData.o_N5T() < 1) return; // 未激活特权
			this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curDay = curDate.getDate();

            //let openDate = new Date(SingletonMap.o_N42.o_N4K);
            //let openDay = openDate.getDate();

            await this.gotoWangChen(); // 先回王城
            await awaitToMap("王城");
            if (curHours >= 10 && curHours < 23) {
                if (!SingletonMap.o_N42.o_N5O() && SingletonMap.o_Ntl.o_NxW == 0) { // 还没开启
                    SingletonMap.o_Ozj.o_CL(); // 开启玛法别墅
                    console.warn(`开启别墅...UID=${this.uid}`);
                    await xSleep(1000);
                }
                //SingletonMap.o_Yr.o_NZ8(KfActivity.o_OSq); // 进入幻境
                await this.gotoMafaHuanJin();
                //await awaitToMap("玛法幻境");
                //console.warn(`进入幻境...UID=${this.uid}`);
            }

            if (this.arrBossPosTemplate != null) {
                this.arrBossPos = Object.assign([], this.arrBossPosTemplate);
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                // this.loopAttackMonter();
                await this.run();
                console.warn(`${this.constructor.name}任务开始了。。。。。UID=${this.uid}`);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
			await xSleep(800);
            await this.enterMap();
            await awaitLeaveMap('玛法幻境');
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;
            // console.log("this.curMapName=>" + this.curMapName);
            // console.log("this.arrBossName=>" + this.arrBossName.join(','));
            while (this.arrBossPos.length > 0) {
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                let bossPos = Object.assign({}, this.arrBossPos[0]);
                await this.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
                await this.waitBossKill();
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                await xSleep(500);
                this.arrBossPos.splice(0, 1);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop2();
    }
	
	findMonter() {
		let result = null;
		if (this.bossId) {
			result = GlobalUtil.getMonsterById(this.bossId);
		}
		if (result == null) {
			result = super.findMonter();
		}
		return result;
	}

    async waitBossKill() {
        let self = this;
        function check() {
            return xSleep(200).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
				
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
				
                let monster = self.findMonter();
                if (monster && self.filterMonster(monster)) {
					if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 3) {
						await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
					}
					setCurMonster(monster);
					SingletonMap.o_OKA.o_Pc(monster, true);
					return check();
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    return;
                //}
                if (self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null) {
					QuickPickUpNormal.runPickUp();
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }
	
	
	async stop2() {
		if (!this.isRunning()) return false;
		this.exitMap();
        await super.stop();
    }
	
	async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossName.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async enterMap() {
        await xSleep(1000);
    }


    async exitMap() {
        try {
            SingletonMap.o_Ozj.o_Vn();
            SingletonMap.o_N42.o_ytx();
            await this.gotoWangChen();
            // await awaitToMap("玛法幻境|王城");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

class villaDuShiBossTask extends StaticMafaBossTask {
    bossId = 157115;
    static name = "villaDuShiBossTask";
    arrBossPosTemplate = [{ x: 53, y: 48 }, { x: 9, y: 27 }];
    arrBossName = ['绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 8;
            SingletonMap.o_Ozj.o_NC5(4, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaDuShiStar1Task extends StaticMafaBossTask {
    static name = "villaDuShiStar1Task";
    arrBossPosTemplate = [{ x: 22, y: 74 }, { x: 42, y: 54 }, { x: 35, y: 38 }, { x: 49, y: 26 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 7;
            SingletonMap.o_Ozj.o_NC5(4, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaDuShiStar2Task extends StaticMafaBossTask {
    static name = "villaDuShiStar2Task";
    arrBossPosTemplate = [{ x: 28, y: 71 }, { x: 44, y: 55 }, { x: 47, y: 66 }, { x: 56, y: 75 }, { x: 56, y: 87 }, { x: 77, y: 69 }, { x: 86, y: 66 }, { x: 72, y: 57 }, { x: 34, y: 42 }, { x: 47, y: 27 }, { x: 28, y: 28 }, { x: 4, y: 23 }];
    arrBossName = ['★', '变异'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 8;
            SingletonMap.o_Ozj.o_NC5(4, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaHuoLongBossTask extends StaticMafaBossTask {
    bossId = 157030;
    static name = "villaHuoLongBossTask";
    arrBossPosTemplate = [{ x: 59, y: 19 }, { x: 13, y: 13 }, { x: 9, y: 21 }];
    arrBossName = ['火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 6;
            SingletonMap.o_Ozj.o_NC5(3, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaHuoLongStar2Task extends StaticMafaBossTask {
    static name = "villaHuoLongStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 43 }, { x: 50, y: 32 }, { x: 56, y: 22 }, { x: 63, y: 30 }, { x: 47, y: 18 }, { x: 51, y: 67 }, { x: 62, y: 71 }, { x: 71, y: 55 }, { x: 11, y: 43 }, { x: 17, y: 20 }, { x: 25, y: 8 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 6;
            SingletonMap.o_Ozj.o_NC5(3, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaHuoLongStar1Task extends StaticMafaBossTask {
    static name = "villaHuoLongStar1Task";
    arrBossPosTemplate = [{ x: 107, y: 219 }, { x: 137, y: 179 }, { x: 82, y: 113 }, { x: 133, y: 100 }, { x: 57, y: 63 }, { x: 57, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 5;
            SingletonMap.o_Ozj.o_NC5(3, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaMoLongBossTask extends StaticMafaBossTask {
    bossId = 157020;
    static name = "villaMoLongBossTask";
    arrBossPosTemplate = [{ x: 86, y: 40 }, { x: 63, y: 50 }, { x: 71, y: 39 }];
    arrBossName = ['‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 4;
            SingletonMap.o_Ozj.o_NC5(2, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaMoLongStar1Task extends StaticMafaBossTask {
    static name = "villaMoLongStar1Task";
    arrBossPosTemplate = [{ x: 39, y: 222 }, { x: 52, y: 180 }, { x: 76, y: 106 }, { x: 88, y: 55 }, { x: 88, y: 21 }, { x: 144, y: 24 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 3;
            SingletonMap.o_Ozj.o_NC5(2, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaMoLongStar2Task extends StaticMafaBossTask {
    static name = "villaMoLongStar2Task";
    arrBossPosTemplate = [{ x: 17, y: 126 }, { x: 36, y: 139 }, { x: 55, y: 114 }, { x: 44, y: 97 }, { x: 53, y: 90 }, { x: 31, y: 86 }, { x: 53, y: 65 }, { x: 67, y: 62 }, { x: 82, y: 41 }, { x: 98, y: 26 }, { x: 103, y: 45 }, { x: 83, y: 71 }, { x: 98, y: 98 }, { x: 109, y: 111 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 4;
            SingletonMap.o_Ozj.o_NC5(2, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaChiYueBossTask extends StaticMafaBossTask {
    bossId = 157010;
    static name = "villaChiYueBossTask";
    arrBossPosTemplate = [{ x: 81, y: 131 }, { x: 53, y: 64 }, { x: 52, y: 45 }, { x: 37, y: 45 }];
    arrBossName = ['绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 2;
            SingletonMap.o_Ozj.o_NC5(1, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaChiYueStar2Task extends StaticMafaBossTask {
    static name = "villaChiYueStar2Task";
    arrBossPosTemplate = [{ x: 22, y: 108 }, { x: 36, y: 112 }, { x: 66, y: 118 }, { x: 67, y: 140 }, { x: 88, y: 133 }, { x: 94, y: 106 }, { x: 96, y: 84 }, { x: 91, y: 46 }, { x: 66, y: 59 }, { x: 49, y: 63 }, { x: 75, y: 81 }, { x: 20, y: 60 }, { x: 23, y: 20 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 2;
            SingletonMap.o_Ozj.o_NC5(1, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaChiYueStar1Task extends StaticMafaBossTask {
    static name = "villaChiYueStar1Task";
    arrBossPosTemplate = [{ x: 19, y: 27 }, { x: 83, y: 21 }, { x: 89, y: 45 }, { x: 78, y: 135 }, { x: 52, y: 98 }, { x: 65, y: 84 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 1;
            SingletonMap.o_Ozj.o_NC5(1, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}


class villaXianLingBossTask extends StaticMafaBossTask {
    bossId = 157215;
    static name = "villaXianLingBossTask";
    arrBossPosTemplate = [{ x: 66, y: 120 }, { x: 52, y: 54 }];
    arrBossName = ['持柱天王', '十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 10;
            SingletonMap.o_Ozj.o_NC5(5, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }
}

class villaXianLingStar2Task extends StaticMafaBossTask {
    static name = "villaXianLingStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 62 }, { x: 67, y: 59 }, { x: 39, y: 38 }, { x: 6, y: 73 }, { x: 8, y: 79 }, { x: 12, y: 84 }, { x: 35, y: 95 }, { x: 40, y: 130 }, { x: 24, y: 156 }, { x: 49, y: 161 }, { x: 62, y: 154 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 10;
            SingletonMap.o_Ozj.o_NC5(5, 2);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}

class villaXianLingStar1Task extends StaticMafaBossTask {
    static name = "villaXianLingStar1Task";
    arrBossPosTemplate = [{ x: 24, y: 93 }, { x: 41, y: 118 }, { x: 29, y: 140 }];
    arrBossName = ['★'];
    async enterMap() {
        try {
            SingletonMap.o_N42.o_N5U = 9;
            SingletonMap.o_Ozj.o_NC5(5, 1);
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
        super.updateNextRunTime(nextDate);
        await super.exitMap();
    }
}
// 别墅神龙山庄
class villaShenLongZhuanZhuTask extends StaticGuaJiTask {
    static name = 'villaShenLongZhuanZhuTask';
    state = false;
    diePos = null;
    curMapName = null;
    arrBossPos = [];
    arrBossId = [];

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime()) {
				let appConfig = getAppConfig();
				if (appConfig != "") {
					appConfig = JSON.parse(appConfig);
					appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
				}
				if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else {
					if (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1 && SingletonMap.o_eB9.o_N3B[22] < SingletonMap.ServerTimeManager.o_eYF) {
						TaskStateManager.updateTaskEnterState(this.constructor.name);
						result = true;
					}
					else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
						TaskStateManager.updateTaskEnterState(this.constructor.name);
						result = true;
					}
				}
			}
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }

        if (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1) { // 庄主
            if (SingletonMap.o_eB9.o_N3B[22] < SingletonMap.ServerTimeManager.o_eYF) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[21].bossPos[0],
                    y: SingletonMap.o_O.o_N38[21].bossPos[1],
                    index: 21
                });
            }
            this.arrBossId.push(SingletonMap.o_O.o_N38[21].bossId);
        }
    }

    async start() {
        try {
            if (this.state == true) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();          
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            await this.gotoWangChen(); // 先回王城
            if (curHours >= 10 && curHours < 23) {
                if (!SingletonMap.o_N42.o_N5O() && SingletonMap.o_eB9.o_N3F == 0) { // 开启神龙别墅
                    SingletonMap.o_wB.o_CL();
                    await xSleep(1000);
                }
                await this.gotoChanYueDao(); // 进入苍月岛
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                // this.loopAttackMonter();
                this.run();
                console.log(`${this.constructor.name}任务开始了。。。。。`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async run() {
        try {
            await this.enterMap();
            await xSleep(1000);
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;

            this.init();
            while (this.arrBossPos.length > 0) {
				QuickPickUpNormal.runPickUp();
                let bossPos = Object.assign({}, this.arrBossPos[0]);
                if (SingletonMap.o_eB9.o_N3B[bossPos.index + 1] < SingletonMap.ServerTimeManager.o_eYF) {
                    await this.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
                    await xSleep(500);
                    await this.waitBossKill(bossPos);
					QuickPickUpNormal.runPickUp();
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    await xSleep(500);
                }
                this.arrBossPos.splice(0, 1);
            }
            await xSleep(500);
            await this.stop();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	async stop() {
		if (!this.isRunning()) return false;
        try {
            await this.exitMap();
        }
        catch (ex) {
            console.error(ex);
        }
        await super.stop();
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    async enterMap() {
        try {
            SingletonMap.o_wB.o_N5o();
            await awaitToMap("神龙别墅");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
			if (SingletonMap.MapData.curMapName != "王城") {
				gameObjectInstMap.ZjmTaskConView.o_lmT();
			}
            // await awaitToMap("苍月岛");
            await this.gotoWangChen();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async waitBossKill(bossPos) {
        let self = this;
        function check() {
            return xSleep(1500).then(function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
				
				if (GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length >= 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    // await self.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
					SingletonMap.o_OKA.o_NSw(bossPos.x, bossPos.y);
                    return check();
                }
                let bossMonster = GlobalUtil.getMonsterById(bossPos.bossId);
                let monster = bossMonster || self.findMonter();
                if (monster) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                //}
                if (self.isRunning() && self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null && self.diePos == null) {
					QuickPickUpNormal.runPickUp();
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }


    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossId.find(item => monster.cfgId == item && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            await this.findWayByPos(this.diePos.x, this.diePos.y);
            this.diePos = null;
        } catch (ex) {
            console.error(ex);
        }
    }
}

class villaShenLongWangTask extends villaShenLongZhuanZhuTask {
    static name = 'villaShenLongWangTask';

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime()) {
				let appConfig = getAppConfig();
				if (appConfig != "") {
					appConfig = JSON.parse(appConfig);
					appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
				}

				// 第一次进入
				if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else {
					if (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1) {
						for (let i = 18; i < 22; i++) {
							if (SingletonMap.o_eB9.o_N3B[i] < SingletonMap.ServerTimeManager.o_eYF) {
								result = true;
								break;
							}
						}
						
						if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
							TaskStateManager.updateTaskEnterState(this.constructor.name);
							result = true;
						}
					}
				}
			}
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }

        if (appConfig.VillaBoss.indexOf('villaShenLongWangTask') > -1) {
            for (let i = 17; i < 21; i++) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[i].bossPos[0],
                    y: SingletonMap.o_O.o_N38[i].bossPos[1],
                    index: i
                });
                this.arrBossId.push(SingletonMap.o_O.o_N38[i].bossId);
            }
        }
    }
}


class villaShenLongStarTask extends villaShenLongZhuanZhuTask {
    static name = 'villaShenLongStarTask';

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime()) {
				let appConfig = getAppConfig();
				if (appConfig != "") {
					appConfig = JSON.parse(appConfig);
					appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
				}

				// 第一次进入
				if ($.isEmptyObject(SingletonMap.o_eB9.o_N3B) && (appConfig.VillaBoss.indexOf('villaShenLongZhuanZhuTask') > -1)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else {

					if (appConfig.VillaBoss.indexOf('villaShenLongStarTask') > -1) {
						for (let i = 0; i < 18; i++) {
							if (SingletonMap.o_eB9.o_N3B[i + 1] < SingletonMap.ServerTimeManager.o_eYF) {
								result = true;
								break;
							}
						}
						
						if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
							TaskStateManager.updateTaskEnterState(this.constructor.name);
							result = true;
						}
					}
				}
			}
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
        }
        if (appConfig.VillaBoss.indexOf('villaShenLongStarTask') > -1) {
            for (let i = 0; i < 17; i++) {
                this.arrBossPos.push({
                    x: SingletonMap.o_O.o_N38[i].bossPos[0],
                    y: SingletonMap.o_O.o_N38[i].bossPos[1],
                    index: i
                });
                this.arrBossId.push(SingletonMap.o_O.o_N38[i].bossId);
            }
        }
    }
}

// 天界boss
class tianJieMatchGuaJiTask extends StaticGuaJiTask {
    static name = "tianJieMatchGuaJiTask";

    constructor() {
        try {
            super();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();       
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.TianJieMap) {
                    let arrGuaJiMap = appConfig.TianJieMap.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await this.gotoWangChen(); // 先回王城
            this.hashCode = new Date().getTime();
            this.enableParams();
            this.run();
            ////console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
        } catch (ex) {
            console.error(ex);
        }
    }

    filterMonster() {
        return true;
    }
    async enterMap(curMap) {
        try {
            if (curMap[0].indexOf('kf') != -1) {
                let mapId = curMap[0].replace('kf', '');
                SingletonMap.o_Yr.o_NZ8(KfActivity.XJOUTDOORBOSS, mapId, curMap[1]);
                await this.checkServerCrossMap();
                await xSleep(1000);
            }
            else {
                if (SingletonMap.ServerCross.o_ePe) {
                    SingletonMap.o_Yr.o_NZ8();
                    await this.checkLocalServeMap();
                }
                SingletonMap.Boss2Sys.o_OV4(curMap[0], curMap[1]);
                SingletonMap.Boss2Sys.o_NqN(curMap[0]);
                await xSleep(1000);
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            SingletonMap.o_lHL.o_OVF();
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster() {
    }

    getBossCount(map) {
		try {
			return SingletonMap.o_eB9.o_NI2(map[0], SingletonMap.MapData.mapId);
		}
		catch (ex) {
            // console.error(ex);
			return 10;
        }
        // return SingletonMap.o_eB9.o_XI;
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
				if (this.stopTask == true) break;
                let map = this.arrGuaJiMap[i].split(',');
                //  跨服地图，并且非跨服时段
                if (isCrossMap(map[0]) && !isCrossTime()) {
                    continue;
                }
                //console.warn('进入地图=>' + map);
				// console.warn('开始挂机.....怪物数量=>' + this.getBossCount(map));
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
				
				let bossThreshold = getAppConfigKeyValue("BossThreshold") || 7;
				bossThreshold = parseInt(bossThreshold);
                while (this.getBossCount(map) >= bossThreshold || self.findMonter()) { // 少于设定数量换图
					QuickPickUpNormal.runPickUp();
				// console.warn('开始挂机.....怪物数量=>' + this.getBossCount(map));
                    await this.hasMonster();
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
					if (this.stopTask == true) break;
                    //if (!SingletonMap.o_lAu.o_lcD) {
                    //    SingletonMap.o_lAu.start();
                    //}
                    await this.hasMonster();
                    if (!this.isRunning()) {
                        break;
                    }
                    if (!mainRobotIsRunning()) {
                        break;
                    }
                    await xSleep(1000);
                    await this.enterMap(map);
                    await xSleep(1000);
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        if (this.isRunning() && mainRobotIsRunning()) {
            console.log(`loopStartFlow....${this.constructor.name}`);
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }

    needRun() {
        if (this.isRunning()) return false;
        return true;
    }
}

// 野外boss
class YeWaiGuaJiTask extends tianJieMatchGuaJiTask {
    static name = "YeWaiGuaJiTask";

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();          
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.YeWaiBoss) {
                    let arrGuaJiMap = appConfig.YeWaiBoss.split('|');
                    this.setMaps(arrGuaJiMap);
                }
            }
            await this.gotoWangChen(); // 先回王城
            this.run();
            ////console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
        } catch (ex) {
            console.error(ex);
        }
    }

    async refreshBossCount() {
        try {
            let bossMap = SingletonMap.BossProvider.o_Ncc();
            for (var i = 0; i < bossMap.length; i++) {
                SingletonMap.o_wB.o_lyp(bossMap[i].id);
                xSleep(100);
            }
        }
        catch (ex) {
        }
    }

    async enterMap(curMap) {
        try {
            this.refreshBossCount();
            if (curMap[0].indexOf('kf') != -1) {
                let mapId = curMap[0].replace('kf', '');
                SingletonMap.o_Yr.o_NZ8(KfActivity.o_NdN, mapId, curMap[1]);
                await this.checkServerCrossMap();
                await xSleep(1000);
            }
            else {
                if (SingletonMap.ServerCross.o_ePe) {
                    SingletonMap.o_Yr.o_NZ8();
                    await this.checkLocalServeMap();
                }
                SingletonMap.o_wB.o_l3N(curMap[0], curMap[1])
                await xSleep(1000);
            }
        }
        catch (ex) {
        }
    }
    filterMonster() {
        return true;
    }
    afterFindMonster() {
    }

    getBossCount(curMap) {
        try {
            let mapInfo = Object.values(SingletonMap.o_eB9.o_eYr).find(M => Object.keys(M).find(C => C == SingletonMap.MapData.sceneId));
            //let mapId = curMap[0].replace('kf', '');
            if (mapInfo && mapInfo[SingletonMap.MapData.sceneId] != undefined) {
                return mapInfo[SingletonMap.MapData.sceneId];
            }
        }
        catch (ex) {
        }
        return 10;
    }
}
// 西游九重天小怪
class xiYouMatchGuaJiTask extends StaticGuaJiTask {
    static name = 'xiYouMatchGuaJiTask';
    state = false;
    diePos = null;
    curMapName = null;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isCrossTime() && !this.isEnough()) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isEnough() {
		let result = false;
		try {
			if (getAppConfigKeyValue("EnableXiyouNormalOnlyVitality") == "TRUE") {
				// 西游每日任务
				var M = SingletonMap.o_O.o_UiV;
				var i = [];
				var Q = M.xiyouxianfudaytask;
				var E;
				for (var A = 0; A < Q.length; A++) {
					E = SingletonMap.QuestMgr.o_Nx0(Q[A]);
					if (!E) E = SingletonMap.o_eHJ.o_lUO(Q[A]);
					i.push(E)
				}
				if (i.length) i.sort(SingletonMap.QuestMgr.o_k9);
				// 九重天200只怪
				let task200 = i.find(M => M.stdQuest && M.stdQuest.id == 9208 && M.o_Nz_ == true);
				if (task200) {
					result = true;
				}
			}
		}
		catch (ex) {
            console.error(ex);
        }
		return result;
	}

    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouMap) {
                this.arrGuaJiMap = appConfig.XiYouMap.split('|');
            }
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();           
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                QuickPickUpNormal.start();
                BlacklistMonitors.start();
                // this.loopCheckHp();
                this.loopAttackMonter();
                this.init();
                this.hashCode = new Date().getTime();
                this.enableParams();
                await this.gotoXiYouZhuChen();
                this.run();
                ////console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
            else {
                await this.stop();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    afterFindMonster() { }

    filterMonster() {
        return true;
    }

    async enterMap(map) {
        try {
            await this.gotoXiYouZhuChen();
            if (map) {
                SingletonMap.o_yKf.o_yda(map[0], map[1]);
            }
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        let self = this;
        try {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                this.disableFindWay();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				if (!mainRobotIsRunning()) break;
				if (!isCrossTime()) break;
				if (this.stopTask == true) break;
                let map = this.arrGuaJiMap[i].split(',');
                console.warn('进入地图=>' + map);
                this.setCurGuaJiMap(map);
                await xSleep(1000);
                await this.enterMap(map);
                await xSleep(1000);
				let bossThreshold = getAppConfigKeyValue("BossThreshold") || 7;
				bossThreshold = parseInt(bossThreshold);
                while (SingletonMap.o_eB9.o_ysG > bossThreshold) {
                    this.enableFindWay();
                    await this.hasMonster();
                    // 停止挂机了
                    if (!this.isRunning()) {
                        break;
                    }
					if (!mainRobotIsRunning()) break;
					if (!isCrossTime()) break;
					if (this.stopTask == true) break;
                    await this.hasMonster();
                    await this.hasBadPlayer();
                    this.disableFindWay();
                    clearCurMonster();
                    await xSleep(1000);
                    useShuiJiProperty();
                    await xSleep(1000);
					if (this.isEnough()) {
						break;
					}
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
				if (this.isEnough()) {
					break;
				}
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1500);
        if (this.isRunning() && mainRobotIsRunning() && isCrossTime()) {
            console.log(`loopStartFlow....${this.constructor.name}`);
            this.loopStartFlow();
        }
        else {
            await this.stop();
        }
    }
}

class xiyouJiuChongTianGuaJiBoss extends StaticGuaJiTask {
    static name = "xiyouJiuChongTianGuaJiBoss";
    curRefreshBoss = null;

    async refrehBossTime() {
        try {
            const arrMapIds = SingletonMap.o_O.o_yd4.reduce((result, cur) => {
                let arrMap = cur.Layer.map((layer, idx) => {
                    return layer.mapId;
                });
                result = result.concat(arrMap);
                return result;
            }, []);
			for(let i = 0; i < arrMapIds.length; i++) {
				SingletonMap.o_yKf.o_ydb(arrMapIds[i]);
                await xSleep(100);
			}
        }
        catch (e) {
            console.error(e);
        }
    }

    getBossMapInfo(bossId) {
        let bossMapInfo = null;
        try {
            for (let i = 0; i < SingletonMap.o_O.o_yd4.length; i++) {
                let bossInfo = SingletonMap.o_O.o_yd4[i];
                let arrLayer = bossInfo.Layer;
                let bossLayer = arrLayer.find((item) => {
                    return item.bossList.includes(bossId);
                });
                if (bossLayer) {
                    let bossIndex = bossLayer.bossList.findIndex(item => item == bossId);
                    bossMapInfo = {
                        id: bossLayer.id,
                        layer: bossLayer.layer,
                        mapId: bossLayer.mapId,
                        bossId: bossId,
                        bossPos: bossLayer.bossPosList[bossIndex]
                    }
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return bossMapInfo;
    }

    getBossRefreshTime(mapId, bossId) {
        if (SingletonMap.o_eB9.o_ysJ[mapId]) {
            return SingletonMap.o_eB9.o_ysJ[mapId][bossId];
        }
        return null;
    }

    getRefreshBoss() {
        let boss = null;
        if (this.arrGuaJiMap) {
            // 先找已经刷新的
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                try {
                    let bossId = parseInt(this.arrGuaJiMap[i]);
                    if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                        let bossMapInfo = this.getBossMapInfo(bossId);
                        let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.mapId, bossId);
                        if (bossRefreshTime == 0) { // 已经刷新
                            boss = bossMapInfo;
                            break;
                        }
                    }
                }
                catch (e) {
                    console.error(e);
                }
            }

            // 没有已经刷新的，接着找60秒内刷新的
            if (boss == null) {
                for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                    try {
                        let bossId = parseInt(this.arrGuaJiMap[i]);
                        if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                            let bossMapInfo = this.getBossMapInfo(bossId);
                            let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.mapId, bossId);
                            if (bossRefreshTime == null) {
                                continue;
                            }
                            if (bossRefreshTime == 0 || bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) { // 已经刷新或30秒后刷新
                                boss = bossMapInfo;
                                break;
                            }
                        }
                    }
                    catch (e) {
                        console.error(e);
                    }
                }
            }
        }
        return boss;
    }

    needRun() {
        this.init();
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (isCrossTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_ysJ)) {
                    result = true;
                }
                else if (this.getRefreshBoss() != null) {
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					result = true;
				}
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouJiuChongTianBoss) {
                this.arrGuaJiMap = appConfig.XiYouJiuChongTianBoss.split('|');
            }
        }
    }

    async run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            if (isCrossTime()) {
                this.init();
                this.hashCode = new Date().getTime();
                await this.gotoXiYouZhuChen();
                await this.run();
            }
        } catch (ex) {
            console.error(ex);
        }
		TaskStateManager.updateTaskEnterState(this.constructor.name);
		await this.stop();
    }

    async enterMap(map) {
        try {
			await this.gotoXiYouZhuChen();
			if (map) {
				SingletonMap.o_yKf.o_yda(map[0], map[1]);
			}
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            // await this.gotoWangChen();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        //if (this.arrGuaJiMap.includes(monster.cfgId + "") && (monster.belongingName == '' || monster.belongingName == SingletonMap.RoleData.name)) { // 没归属或归属是自己
        //    return true;
        //}
        //else if (this.arrGuaJiMap.includes(monster.cfgId + "") && BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属是黑名单成员
        //    return true;
        //}
        return true;
    }

    async backToMap() {
        try {
            console.warn('回到boss地图...');
            let curRefreshBoss = this.curRefreshBoss || this.getRefreshBoss();
            if (curRefreshBoss) {
                if (curRefreshBoss.id && curRefreshBoss.layer) {
                    await this.enterMap([curRefreshBoss.id, curRefreshBoss.layer]);
                    await xSleep(300);
                }
                if (curRefreshBoss && curRefreshBoss.bossPos) {
                    await this.findWayByPos(curRefreshBoss.bossPos[0], curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(1000);
        this.diePos = null;
        // await this.stop();
    }

    async loopStartFlow() {
        try {
            await this.refrehBossTime();
            await xSleep(2000);
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				// 停止挂机了
                if (this.stopTask) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                // 进入boss地图
                await this.enterMap([this.curRefreshBoss.id, this.curRefreshBoss.layer]);
                await xSleep(100);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                // 前往boss坐标
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                // await xSleep(100);
                await this.killBoss(this.curRefreshBoss);
                await xSleep(1000);
				//if (this.uid == SingletonModelUtil.curUid) {
					await this.gotoXiYouZhuChen();
				//}
                await this.refrehBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
				if (this.stopTask) {
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async killBoss(refreshBoss) {
        let self = this;
		FirstKnife.setTargetCfgId(refreshBoss.bossId);
        function check() {
            return xSleep(300).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
				if (self.stopTask) {
                    return;
                }
				if (!isCrossTime()) return;
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    // console.warn('角色死亡...');
                    return check();
                }

                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 5) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }

                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null && GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length <= 5) {
					setCurMonster(badPlayer);
					SingletonMap.o_OKA.o_Pc(badPlayer, true);
					return check();
                }
				
				if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 5) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }

                // 被打飞了
                if (SingletonMap.MapData.curMapName == '西游降魔') {
                    await self.enterMap([refreshBoss.id, refreshBoss.layer]);
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                if (monster && !isSelfBelong(monster) && !isNullBelong(monster)) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.getBadPlayer()) {
						return check();
					}
                    else if (BlacklistMonitors.isBadPlayer(monster.belongingName)) {
						// 先打黑名单归属者
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (!belongingBadPlayer) { // 归属者不在视界，等待
							return check();
						}
						if (belongingBadPlayer && allowAttack(belongingBadPlayer)) {
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							return check();
						}
						else {
							bossNextWatchTime[refreshBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
							console.warn(`boss归属${monster.belongingName},当前模式不能攻击,离开不打...`);
							return;
						}
                    }
					else {
						await xSleep(2000);
						let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
						if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(monster))) {
							setCurMonster(tmpMonster);
							SingletonMap.o_OKA.o_Pc(tmpMonster, true);
							return check();
						}
						else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
							return check();
						}
						else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName)) {						
							// 先打黑名单归属者
							let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
							if (!belongingBadPlayer) { // 归属者不在视界，等待
								return check();
							}
							if (belongingBadPlayer && allowAttack(belongingBadPlayer)) {
								setCurMonster(belongingBadPlayer);
								SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
								return check();
							}
							else {
								bossNextWatchTime[refreshBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
								console.warn(`boss归属${monster.belongingName},当前模式不能攻击,离开不打...`);
								return;
							}
							return check();
						}
						else {
							bossNextWatchTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // boss消失或是别人归属,5分钟内不打
							return;
						}
					}
                }
                // 刷新boss时间
                // self.refrehBossTime();
				
                let bossRefreshTime = self.getBossRefreshTime(refreshBoss.mapId, refreshBoss.bossId);
                // 刷新时间小于30秒，等待刷新
                if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    return check();
                }
                // 刷新时间大于30秒
                if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 30000) {
                    // console.log('killBoss...2.');
                    return;
                }
                return check();
            });
        }
        return check();
    }
}

// 西游天牢boss
class XiyouTianLaoBossTask extends StaticGuaJiTask {
    static name = "XiyouTianLaoBossTask";
    curRefreshBoss = null;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_ySw)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getRefreshBoss() != null) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    init() {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiyouTianLaoBoss) {
                this.arrGuaJiMap = appConfig.XiyouTianLaoBoss.split('|');
            }
            else {
                this.arrGuaJiMap = "";
            }
        }
        else {
            this.arrGuaJiMap = "";
        }
    }

    getBossMapInfo(bossId) {
        let bossMapInfo = null;
        try {
            for (let i = 0; i < SingletonMap.o_O.o_ykE.length; i++) {
                let layerInfo = SingletonMap.o_O.o_ykE[i];
                let arrLayerBoss = layerInfo.boss;
                let bossLayer = arrLayerBoss.find((item) => {
                    return item.bossId == bossId;
                });
                if (bossLayer) {
                    bossMapInfo = {
                        index: bossLayer.index,
                        layer: layerInfo.layer,
                        mapId: layerInfo.mapId,
                        bossId: bossId,
                        bossPos: bossLayer.bossPos,
                        bossLayer: bossLayer
                    }
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return bossMapInfo;
    }

    getBossRefreshTime(layerId, bossIndex) {
        try {
            if (SingletonMap.o_eB9.o_ySw[layerId]) {
                return SingletonMap.o_eB9.o_ySw[layerId][bossIndex];
            }
        }
        catch (e) {
            console.error(e);
        }
        return null;
    }

    getRefreshBoss() {
        this.init();
        let boss = null;
        if (this.arrGuaJiMap) {
            for (let i = 0; i < this.arrGuaJiMap.length; i++) {
                try {
                    let bossId = parseInt(this.arrGuaJiMap[i]);
                    if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                        let bossMapInfo = this.getBossMapInfo(bossId);
                        let bossRefreshTime = this.getBossRefreshTime(bossMapInfo.layer, bossMapInfo.index);
                        if (bossRefreshTime == 0 && bossMapInfo.bossLayer.cost.count <= SingletonMap.RoleData.o_eNf) { // 已经刷新
                            boss = bossMapInfo;
                            break;
                        }
                    }
                }
                catch (e) {
                    console.error(e);
                }
            }
        }
        return boss;
    }

    refrehBossTime() {
        try {
            const arrMapIds = SingletonMap.o_O.o_ykE.map(M => M.layer);
            arrMapIds.forEach(async (mapId) => {
                SingletonMap.o_yKf.o_yZe(mapId);
                await xSleep(300);
            });
        }
        catch (e) {
            console.error(e);
        }
    }

    async run() {
        try {
            this.startUtils();
            // this.loopCheckHp();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();          
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.init();
                this.hashCode = new Date().getTime();
                await this.gotoXiYouZhuChen();
                await this.run();
                //console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap(layer) {
        try {
			await this.gotoXiYouZhuChen();
			if (!SingletonMap.o_n.isOpen("XiyouTianLaoBossWin")) {
				SingletonMap.o_n.o_EG("XiyouTianLaoBossWin");
				await xSleep(500);
			}
			if (layer) {
				SingletonMap.o_yKf.o_yZ5(layer);
			}
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            // await this.gotoWangChen();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        try {
            await this.gotoXiYouZhuChen();
            await this.refrehBossTime();
            await xSleep(1000);
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                // 进入boss地图
                await this.enterMap(this.curRefreshBoss.layer);
                await xSleep(1500);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                // 前往boss坐标
                const curMapName = SingletonMap.MapData.curMapName;
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', curMapName);
                await xSleep(100);
                await this.killBoss(this.curRefreshBoss);
                await xSleep(1500);
				//if (this.uid == SingletonModelUtil.curUid) {
					await this.gotoXiYouZhuChen();
				//}
                this.refrehBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {
					QuickPickUpNormal.runPickUp();
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        return true;
    }

    async backToMap() {
        try {
            SingletonMap.o_eaX.o_lKj(0, TeleportPos.XiYouArea);
            if (this.curRefreshBoss) {
                await this.enterMap(this.curRefreshBoss.layer);
                await xSleep(500);
                await this.findWayByPos(this.curRefreshBoss.bossPos[0], this.curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }

    async killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(200).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 校验uid
                //if (SingletonModelUtil.curUid != self.uid) {
                //    self.findWayIsStop = true;
                //    return;
                // }
				if (self.getRefreshBoss() == null) return;
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    return check();
                }
                if (SingletonMap.MapData.curMapName == '西游降魔') {
                    return check();
                }
				
				// 先打人
				let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer && GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length <= 7) {
					self.findWayIsStop = true;
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }
				
				if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 7) {
					await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
				}

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                    setCurMonster(monster);
					SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                if (monster && self.existsMonster(monster) && !isSelfBelong(monster) && isNullBelong(monster)) { // 归属不是自己
                    // console.log('killBoss...1.');
                    if (!BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属不是黑名单
                        bossNextWatchTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
						QuickPickUpNormal.runPickUp();
                        return;
                    }
                    else {
                        return check();
                    }
                }				
				
                let bossRefreshTime = self.getBossRefreshTime(refreshBoss.layer, refreshBoss.index);
                // 刷新时间小于30秒，等待刷新
                if (bossRefreshTime && bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    return check();
                }
                // 刷新时间大于30秒
                if (bossRefreshTime && bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 30000) {
                    // console.log('killBoss...2.');
					QuickPickUpNormal.runPickUp();
                    return;
                }
				if (!monster) return;
                return check();
            });
        }
        return check();
    }
}


// 个人经验boss
class BossExpTask extends StaticGuaJiTask {
    static name = 'BossExpTask';
    state = false;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (SingletonMap.RoleData.o_yOG > 150 * 10000 && isFreeTime() && SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE) < SingletonMap.BossProvider.o_lG4().dailyCount) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            let curCount = SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE);
            let dailyCount = SingletonMap.BossProvider.o_lG4().dailyCount;
            while (curCount < dailyCount) {
                await this.gotoWangChen();
                if (SingletonMap.MapData.curMapName == "王城") {
                    await xSleep(2000);
                    SingletonMap.o_n.o_EG(BossWin, [1, 2]);
                    await xSleep(2000);
                    SingletonMap.o_wB.o_OtG(gameObjectInstMap.BossExpView.curCfg.index, 1);
                    await xSleep(2000);
                }
                await awaitToMap("王城");
                if (!mainRobotIsRunning()) {
                    break;
                }
                if (!this.isRunning()) {
                    break;
                }
                curCount = SingletonMap.o_NL6.getCountById(EntimesType.EXPBOSSCHALLENGE);
                await xSleep(1000);
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;		
        await super.stop();
    }
}

function allowPickUp(A) {
	let result = false;	
	try {
		var b = SingletonMap.o_Ue.o_lpC();
		if (!A) return;
        if (!SingletonMap.o_lLF.o_OrJ(A, b)) return;
		return true;
	}
	catch(e) {
		console.error(e);
	}
	return result;
}

// 行会boss
class GuildBossTask extends StaticGuaJiTask {
    static name = 'GuildBossTask';
    state = false;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isFreeTime() && SingletonMap.o_OTs.o_OO3().guildBossDayChallengeNum - SingletonMap.GuildDataMgr.o_lYO > 0) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    killBoss() {
        let self = this;
        function check() {
            return xSleep(50).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                let nextGoods = getNextGoods();
				if (nextGoods) {
					SingletonMap.o_lAu.start();
					return check();
				}
                // 怪物消失-捡东西-退出
                if (SingletonMap.o_O.o_lLU.filter(B => SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == B.bossId)).length == 0) {
                    // console.warn('击败行会boss.......');
                    
					let nextGoods = getNextGoods();
                    if (nextGoods) {
                        return check();
                    }
                    else {
                        return;
                    }
                }
                if (SingletonMap.MapData.curMapName == "王城") {
                    return;
                }
                SingletonMap.o_lAu.start();
                return check();
            });
        }
        return check();
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            await this.gotoWangChen();
            bossNextWatchTime[GuildBossTask.name] = null;
			SingletonMap.o_n.o_EG("GuildWin", [2, 0, true]);
			await xSleep(1000);
            if (SingletonMap.o_OTs.o_OO3().guildBossDayChallengeNum - SingletonMap.GuildDataMgr.o_lYO > 0) {
                if (mainRobotIsRunning() && this.isRunning()) {
                    SingletonMap.o_lWu.o_lnz();
                    await xSleep(2000);
                    await this.killBoss();
                }
                await xSleep(1000);
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
		closeGameWin("GuildBossInfoWin");
		closeGameWin("GuildBossResultWin");
        closeGameWin("GuildWin");
        await super.stop();
    }
}

class MineTask extends StaticGuaJiTask {
    static name = 'MineTask';
    state = false;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isFreeTime() && SingletonMap.o_e_w.o_NgZ > 0 && !SingletonMap.o_e_w.o_Nnz()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
            result = false;
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            let mineLevel = 1;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.MineLevel) {
                    mineLevel = parseInt(appConfig.MineLevel);
                }
            }
            await this.gotoWangChen();
            SingletonMap.o_OzT.o_Nxc(); //进入矿洞
            await awaitToMap("黑暗矿洞");
            await xSleep(500);
            // 有奖励先领
            if (SingletonMap.o_e_w.o_eew()) {
                SingletonMap.o_OzT.o_eIR();
                await xSleep(100);
            }
            await this.foundMine();
            await xSleep(1000);
            if (SingletonMap.o_n.isOpen("MineChooseWin")) {
                while (mineLevel > SingletonMap.o_e_w.o_bN) {
					var M = SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.DIAMOND); // 钻石数量
					let count = this.totalKuanGongLin();
					if (count > 0) {
						SingletonMap.o_OzT.o_Nsf(0);
					}
					else if (getAppConfigKeyValue("EnableDiamondMine") == "TRUE" && M >= SingletonMap.o_O.o_lgc.needDiamond) {
						SingletonMap.o_OzT.o_Nsf(0);
					}
					else {
						break;
					}
					await xSleep(300);
                }
                await xSleep(300);
				if (SingletonMap.o_n.isOpen("MineChooseWin")) {
					SingletonMap.o_n.isOpen("MineChooseWin").o_N0D();
					await xSleep(200);
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	totalKuanGongLin() {
		let result = 0;
		try {
			var i = SingletonMap.o_O.o_lgc.needItemId;
			result = i > 100 ? SingletonMap.o_Ue.o_Ngt(i) : SingletonMap.RoleData.o_NM3(i);
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    foundMine() {
        let self = this;
        function check() {
            return xSleep(500).then(function () {
                // 辅助停止
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                if (SingletonMap.o_n.isOpen("MineChooseWin")) {
                    return;
                }
                SingletonMap.o_e_w.o_eAo(); // 找矿
                return check();
            });
        }
        return check();
    }

    async stop() {
        if (!this.isRunning()) return;
        await super.stop();
    }
}

// 神界五行源地boss
class WuXingBossTask extends StaticGuaJiTask {
    static name = 'WuXingBossTask';
    state = false;
    curLayer = -1;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (isCrossTime()) {
                if (this.getBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
        }
        return result;
    }

    getBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieWuXingBoss != "") {
                    let arrShenJieWuXingBoss = appConfig.ShenJieWuXingBoss.split("|");
                    for (let i = 0; i < arrShenJieWuXingBoss.length; i++) {
                        let layer = arrShenJieWuXingBoss[i];
                        for (let key in SingletonMap.o_UuC.bossData.o_UaE) {
                            let arrBossData = SingletonMap.o_UuC.bossData.o_UaE[key];
                            boss = arrBossData.find(M => M.time == 0 && layer == M.layer + ""
                                && (!bossNextWatchTime[M.bossId] || bossNextWatchTime[M.bossId] <= SingletonMap.ServerTimeManager.o_eYF));
                            if (boss) {
                                break;
                            }
                        }
                        if (boss) {
                            let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == boss.layer);
                            let bossCfg = layerBoss.boss[0];
                            if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                                boss = null;
                            }
                            else {
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    hasBossCost(bossId) {
        let result = false;
        try {
            let boss = null;
            for (let key in SingletonMap.o_UuC.bossData.o_UaE) {
                let arrBossData = SingletonMap.o_UuC.bossData.o_UaE[key];
                boss = arrBossData.find(M => M.bossId == bossId);
                if (boss) {
                    break;
                }
            }
            if (boss) {
                let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == boss.layer);
                let bossCfg = layerBoss.boss[0];
                if (SingletonMap.RoleData.o_UPf >= bossCfg.cost.count) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async backToMap() {
        try {
			if (!this.isRunning()) return;
			if (!mainRobotIsRunning()) return;
            await this.enterMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
			if (!this.isRunning()) return;
			if (!mainRobotIsRunning()) return;
			if (this.needExitScene()) {
				await this.exitScene();
			}
			if (this.curLayer != -1) {
				SingletonMap.o_Uaq.o_UaV(this.curLayer);
			}
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    initState() {
        this.curLayer = -1;
        super.initState();
    }

    getBossCfg(layer, bossId) {
        let layerBoss = SingletonMap.o_O.o_UuB.find(M => M.layer == layer);
        return layerBoss.boss.find(M => M.bossIds.includes(bossId));
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.hashCode = new Date().getTime();
                await this.gotoShenJie();
                await this.loopStartFlow();
                //console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async loopStartFlow() {
        try {
            let curBoss = this.getBoss();
            while (curBoss != null) {
				QuickPickUpNormal.runPickUp();
				if(curBoss.layer != this.curLayer) {
					if (this.needExitScene()) {
						await this.exitScene();
					}
					this.curLayer = curBoss.layer;
					SingletonMap.o_Uaq.o_UaV(curBoss.layer);
					await xSleep(1100);
				}
                let bossCfg = this.getBossCfg(curBoss.layer, curBoss.bossId);
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                if (bossCfg) {
                    // 九幽值不够了
                    if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                        break;
                    }
                    SingletonMap.o_lAu.stop();
                    clearCurMonster();
                    await this.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    if (!mainRobotIsRunning() || !this.isRunning()) {
                        break;
                    }
                    await this.killBoss(curBoss.bossId);
                }
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                curBoss = this.getBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async killBoss(bossId) {
        let self = this;
		let pickup = false;
        function check() {
            return xSleep(200).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }

				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
				
				if (SingletonMap.MapData.curMapName == '神界主城') {
					return;
				}
				
				try {
                    let curBoss = self.getBoss();
                    if (!curBoss) {
						let nextGoods = getNextGoods();
						if (allowPickUp(nextGoods) && pickup == false) {
							QuickPickUpNormal.runPickUp();
							pickup = true;
							await xSleep(1000);
							return check();
						}
						return;
                    }
                    let bossCfg = self.getBossCfg(curBoss.layer, curBoss.bossId);
                    // 九幽令不足
                    if (!bossCfg) {
                        return;
                    }
                    else if (SingletonMap.RoleData.o_UPf < bossCfg.cost.count) {
                        return;
                    }
                    else if (GlobalUtil.compterFindWayPos(bossCfg.bossPos[0], bossCfg.bossPos[1]).length >= 3) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
                }
                catch (ex) {
                    console.error(ex);
                    return;
                }
				
                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }

                let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                if (!monster) {
					QuickPickUpNormal.runPickUp();
					await xSleep(500);
                    return;
                }
				
				 // 没有归属，发起攻击
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    // console.warn('killBoss...2');
                    setCurMonster(monster);
                    self.attackMonster(monster);
                    return check();
                }

                if (monster && !isSelfBelong(monster)) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        // console.warn('killBoss...1.');
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
						return check();
                    }
                    else {
                        bossNextWatchTime[bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
                return check();
            });
        }
        return check();
    }
}

// 神界炼狱
class LianYuBossTask extends StaticGuaJiTask {
    static name = 'LianYuBossTask';
    state = false;
    curLayer = -1;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (isCrossTime()) {
				let arrBossRefreshTime = this.getArrBossRefreshTime();
				if (arrBossRefreshTime.length == 0) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
                else if (this.getBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
        }
        return result;
    }

    getBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.LianYuBoss != "") {
                    let arrShenJieLianYuBoss = appConfig.LianYuBoss.split("|");
					let arrBossRefreshTime = this.getArrBossRefreshTime();
                    for (let i = 0; i < arrShenJieLianYuBoss.length; i++) {
                        let bossId = parseInt(arrShenJieLianYuBoss[i]);
                        let bossCfg = this.getBossCfg(bossId);
						let bossTime = arrBossRefreshTime.find(M => M && M.time == 0 && M.layer == bossCfg.id && M.index == bossCfg.index);
						if (bossTime && this.hasBossCost(bossId) && (!bossNextWatchTime[bossId] || bossNextWatchTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) {
							boss = bossCfg;
							break;
						}
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    hasBossCost(bossId) {
        let result = false;
        try {
            let boss = null;
			let bossCfg = this.getBossCfg(bossId);
            if (bossCfg) {
                if (SingletonMap.RoleData.o_UPf >= bossCfg.cost[0].count) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async backToMap() {
        try {
            await this.enterMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
			if (SingletonMap.MapData.curMapName == "神界主城" && this.curLayer != -1) {
				SingletonMap.o_Uij.o_HJl(this.curLayer);
			}
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    initState() {
        this.curLayer = -1;
        super.initState();
    }
	
	getArrBossCfg() {
		let result = [];
		try {
			for(var i = 0;i < SingletonMap.o_O.o_Hq8.length; i++) {
				result = result.concat(SingletonMap.o_O.o_Hq8[i].list); 
			}
		}
		catch(ex) {
		}
		return result;
	}

    getBossCfg(bossId) {
		let arrBossCfg = this.getArrBossCfg();
        return arrBossCfg.find(M => M.bossId == bossId);
    }
	
	getArrBossRefreshTime() {
		let result = [];
		try {
			for(var key in SingletonMap.o_HvA.o_UaE) {
				result = result.concat(SingletonMap.o_HvA.o_UaE[key]);
			}
		}
		catch(ex) {
		}
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
			this.hashCode = new Date().getTime();
			await this.gotoShenJie();
			await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async loopStartFlow() {
        try {
            let curBoss = this.getBoss();
            while (curBoss != null) {
                if (this.curLayer != curBoss.id) {
					SingletonMap.o_eaX.o_lKj(0, TeleportPos.ShenJieArea);
					await xSleep(500);
                    this.curLayer = curBoss.id;
                    SingletonMap.o_Uij.o_HJl(curBoss.id);
                }
                await xSleep(1000);
                let bossCfg = this.getBossCfg(curBoss.bossId);
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                if (bossCfg) {
                    // 九幽值不够了
                    if (SingletonMap.RoleData.o_UPf < bossCfg.cost[0].count) {
                        break;
                    }
                    SingletonMap.o_lAu.stop();
                    clearCurMonster();
                    await this.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    if (!mainRobotIsRunning() || !this.isRunning()) {
                        break;
                    }
                    await this.killBoss(curBoss.bossId);
                }
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                curBoss = this.getBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async killBoss(bossId) {
        let self = this;
		let pickup = false;
        function check() {
            return xSleep(200).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }

                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
				
				if (SingletonMap.MapData.curMapName == '神界主城') {
					await xSleep(1500);
					await self.backToMap();
					await xSleep(1500);
				}
				
				let nextGoods = getNextGoods();
				if (allowPickUp(nextGoods) && pickup == false) {
					QuickPickUpNormal.runPickUp();
					pickup = true;
					await xSleep(1000);
					return check();
				}
				
				try {
                    let curBoss = self.getBoss();
                    let bossCfg = self.getBossCfg(curBoss.bossId);
                    // 九幽令不足
                    if (!bossCfg) {
                        return;
                    }
                    else if (SingletonMap.RoleData.o_UPf < bossCfg.cost[0].count) {
                        return;
                    }
                    else if (GlobalUtil.compterFindWayPos(bossCfg.bossPos[0], bossCfg.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
                }
                catch (ex) {
                    console.error(ex);
                    return;
                }
				
                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }

                let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = bossId && M instanceof Monster && !M.masterName && M.curHP > 0);
				
				// 没有归属，发起攻击
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                    // console.warn('killBoss...2');
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                if (monster && !isSelfBelong(monster) && !isNullBelong(monster)) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        // console.warn('killBoss...1.');
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
						return check();
                    }
					else { // 没开黑名单
						console.warn(`boss归属${monster.belongingName},离开不打...`);
                        bossNextWatchTime[bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
				if (!monster) {
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
                    return;
                }
                return check();
            });
        }
        return check();
    }

    async stop() {
		if (!this.isRunning()) return false;
        // SingletonMap.o_eaX.o_lKj(0, TeleportPos.ShenJieArea); 
        await super.stop();
    }
}
// 轮回秘境
class CrossPublicBossTask extends StaticGuaJiTask {
    static name = "CrossPublicBossTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime() && SingletonMap.RoleData.o_lYC > 0) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_NQV)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	refrehBossTime() {
		try { 
			let arrBossCfg = SingletonMap.BossProvider.o_lGJ();
            let lastLayer = arrBossCfg[arrBossCfg.length - 1];
            SingletonMap.o_wB.o_Ohs(lastLayer);
		}
        catch (ex) {
            console.error(ex);
        }
	}

    getNextBoss() {
        let result = null;
        try {
			this.refrehBossTime();
            if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                return null;
            }
			let arrBossCfg = SingletonMap.BossProvider.o_lGJ();
            for (let key in SingletonMap.o_eB9.o_NQV) {
                let arrLayerBossTime = SingletonMap.o_eB9.o_NQV[key];
                // 先找已经刷新的
                let bossTime = arrLayerBossTime.find(M => M.time == 0 && (!CrossPublicBossRefreshTime[key + '_' + M.index] || CrossPublicBossRefreshTime[key + '_' + M.index] <= SingletonMap.ServerTimeManager.o_eYF));
                if (bossTime) {
                    let arrLayerBoss = arrBossCfg.find(M => M.layer == key);
                    if (arrLayerBoss) {
                        let bossCfg = arrLayerBoss.boss.find(M => M.index == bossTime.index);
                        if (bossCfg) {
                            result = bossCfg;
                            break;
                        }
                    }
                }
            }

            if (result == null) {
                let mintimeBoss = null;
                for (let key in SingletonMap.o_eB9.o_NQV) {
                    let arrLayerBossTime = SingletonMap.o_eB9.o_NQV[key];
                    // 找30秒内刷新的
                    let bossTime = arrLayerBossTime.find(M => M.time - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[key + '_' + M.index] || CrossPublicBossRefreshTime[key + '_' + M.index] <= SingletonMap.ServerTimeManager.o_eYF));
                    if (bossTime) {
                        if (mintimeBoss == null || bossTime.time < mintimeBoss.time) {
                            mintimeBoss = bossTime;
                            mintimeBoss.layer = key;
                        }
                    }
                }
                if (mintimeBoss) {
                    let arrLayerBoss = arrBossCfg.find(M => M.layer == mintimeBoss.layer);
                    if (arrLayerBoss) {
                        let bossCfg = arrLayerBoss.boss.find(M => M.index == mintimeBoss.index);
                        if (bossCfg) {
                            result = bossCfg;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isRunning() {
        return this.state == true;
    }

    async enterMap() {
        try {
			if (SingletonMap.MapData.curMapName == '轮回秘境') {
				await this.exitMap();
				await awaitToMap("苍月岛");
			}
			await this.findWayByPos(41, 33, 'findWay', SingletonMap.MapData.curMapName);
			if (this.curNextBoss) {
				SingletonMap.o_wB.o_OiN(this.curNextBoss.layer);
				await awaitToMap("轮回秘境");
			}
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
			if (SingletonMap.MapData.curMapName == '轮回秘境') {
				gameObjectInstMap.ZjmTaskConView.o_OiC({target: gameObjectInstMap.ZjmTaskConView.exitBtn});
				await awaitToMap("苍月岛");
			}
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await xSleep(500);
            if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                this.diePos = null;
                await this.stop();
            }
            else {
                let nextBoss = this.getNextBoss();
                if (nextBoss) {
                    this.curNextBoss = nextBoss;
                    SingletonMap.o_lAu.stop();
                    SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
                    await xSleep(500);
                    await this.findWayByPos(41, 33, 'findWay', SingletonMap.MapData.curMapName);
                    SingletonMap.o_lAu.stop();
                    if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') == -1) {
                        if (this.curNextBoss) {
                            SingletonMap.o_wB.o_OiN(this.curNextBoss.layer);
                        }
                    }
                    if (this.diePos) {
                        await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                    }
                    this.diePos = null;
                }
                else {
                    this.diePos = null;
                    await this.stop();
                }
				clearCurMonster();
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            if (this.isActivityTime()) {
                await this.gotoChanYueDao();
                await this.run();
            }
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async run() {
        try {
			this.refrehBossTime();
            await xSleep(500);
            SingletonMap.o_lAu.stop();
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				clearCurMonster();
                this.curNextBoss = nextBoss;
                await this.enterMap();
                await this.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                SingletonMap.o_lAu.stop();
                await this.waitBossKill(nextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                nextBoss = this.getNextBoss();
				
                QuickPickUpNormal.runPickUp();
            }
			QuickPickUpNormal.runPickUp();
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    isWaitingTime(curBoss) {
        let result = false;
        try {
            if (curBoss) {
                if (SingletonMap.o_eB9.o_NQV[curBoss.layer].find(M => M.index == curBoss.index).time - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    filterMonster() {
        return true;
    }

    async waitBossKill(curBoss) {
        let self = this;
		let pickup = false;
        function check() {
            return xSleep(30).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
                if (!self.isActivityTime()) {
                    return;
                }
				
                // 人物死亡
                if (!isAlive() && SingletonMap.o_n.isOpen('WarnActorReviveWin') || AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 还没回到boss地图
                if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') == -1) {
					await self.enterMap();
                    return check();
                }

				let nextGoods = getNextGoods();
				if (allowPickUp(nextGoods) && pickup == false) {
					QuickPickUpNormal.runPickUp();
					pickup = true;
					await xSleep(1000);
					return check();
				}
                if (SingletonMap.RoleData.o_lYC < 10) { // 令牌不足
                    return;
                }
                
                if (GlobalUtil.compterFindWayPos(curBoss.bossPos[0], curBoss.bossPos[1]).length >= 3) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(curBoss.bossPos[0], curBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    return check();
                }

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3)) {
                        setCurMonster(BlacklistMonitors.badPlayer);
                        SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }

                let bossMonster = GlobalUtil.getMonsterById(curBoss.bossId);
                let monster = bossMonster || self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                // 归属不是自己
                if (monster && self.existsMonster(monster) && !isSelfBelong(monster) && !isNullBelong(monster)) {
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    else {
                        CrossPublicBossRefreshTime[curBoss.layer + '_' + curBoss.index] = SingletonMap.ServerTimeManager.o_eYF + 60000 * 5; //5分钟内不打
                        return;
                    }
                }

                if (self.isWaitingTime(curBoss)) {
                    return check();
                }
				else if (!monster){
					return;
				}

                if (SingletonMap.MapData.curMapName.indexOf('轮回秘境') > -1 && self.findMonter() == null) {
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }
}
// 西游每日活跃
class xiyouXianfuDayTask extends StaticGuaJiTask {
    static name = "xiyouXianfuDayTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();

            if (curHours >= 16 && curHours % 2 == 0
                && (!bossNextWatchTime[xiyouXianfuDayTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[xiyouXianfuDayTask.name])) {
                result = true;
            }
            else if (curHours >= 22 && curHours < 23 && !xiyouXianfuDayTaskLastTime[curMonth + '_' + curDateNum]) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();        	
            await this.gotoXiYouZhuChen();
            await xSleep(500);

            // 领取仙府每日任务
            SingletonMap.o_n.o_EG("XiyouXianfuWin", null, false);
            await xSleep(500);
            let arrDayTask = SingletonMap.o_n.isOpen("XiyouXianfuWin").viewCon.$children.find(M => M.__class__ == 'XiyouXianfuView').o_OhJ._source.filter(M => M.o_Nz_ == true);
            for (var i = 0; i < arrDayTask.length; i++) {
                SingletonMap.o_lQH.o_lIV(arrDayTask[i].stdQuest.id);
                await xSleep(500);
            }
            if (SingletonMap.o_n.isOpen("XiyouXianfuWin")) {
                SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouXianfuWin"));
                await xSleep(500);
            }

            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            // 22点最后一次进入
            if (curHours >= 22) {
                xiyouXianfuDayTaskLastTime[curMonth + '_' + curDateNum] = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 60 * 1000; // 8小时1分钟后再次执行
        bossNextWatchTime[xiyouXianfuDayTask.name] = nextWatchTime;
        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouXianfuWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouXianfuWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 仙府炼丹
class xiyouXianfuLianDanTask extends StaticGuaJiTask {
    static name = "xiyouXianfuLianDanTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23
                && (!bossNextWatchTime[xiyouXianfuLianDanTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[xiyouXianfuLianDanTask.name])) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();          
            await this.gotoXiYouZhuChen();
            await xSleep(500);

            // 炼丹炉
            SingletonMap.o_n.o_EG("XiyouLianDanWin", null, false);
            await xSleep(500);
            // 先领奖励
            let arrLianDanLu = SingletonMap.o_n.isOpen("XiyouLianDanWin").viewCon.$children.find(M => M.__class__ == 'XiyouLiandanView').list.$children.filter(M => M.data.o_yOc && M.endTime != 0 && M.endTime < SingletonMap.ServerTimeManager.o_eYF);
            for (var i = 0; i < arrLianDanLu.length; i++) {
                SingletonMap.o_yKf.o_Und(arrLianDanLu[i].data.index);
                await xSleep(500);
                SingletonMap.o_n.isOpen("XiyouLiandanResultWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLiandanResultWin"));
                await xSleep(500);
            }
            // 开始炼丹
            arrLianDanLu = SingletonMap.o_n.isOpen("XiyouLianDanWin").viewCon.$children.find(M => M.__class__ == 'XiyouLiandanView').list.$children.filter(M => M.data.o_yOc && M.endTime == 0);
            for (var i = 0; i < arrLianDanLu.length; i++) {
                arrLianDanLu[i].o_lP6();
                await xSleep(500);
                for (var j = SingletonMap.o_O.o_UPH.length; j > 0; j--) {
                    // 可以炼丹
                    if (SingletonMap.o_Unw.o_Uh8(SingletonMap.o_O.o_UPH.find(M => M.sort == j).id)) {
                        SingletonMap.o_n.isOpen("XiyouDanfangView").o_O1y({ itemIndex: j - 1 }); // 选中tab
                        await xSleep(500);
                        SingletonMap.o_n.isOpen("XiyouDanfangView").o_eKW(); // 炼丹
                        await xSleep(500);
                        break;
                    }
                }
                await xSleep(500);
            }
            SingletonMap.o_n.isOpen("XiyouLianDanWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLianDanWin"));// 关闭炼丹窗体

            let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 60 * 1000; // 8小时1分钟后再次执行
            bossNextWatchTime[xiyouXianfuLianDanTask.name] = nextWatchTime;
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouLianDanWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouLianDanWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 西游仙府 - 神魔谷地
let xiyouShenMoGuDiTaskLastTime = {};
class XiyouShenMoGuDiTask extends StaticGuaJiTask {
    static name = "XiyouShenMoGuDiTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            if (curHours >= 10 && curHours < 23) {
                if (!bossNextWatchTime[XiyouShenMoGuDiTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[XiyouShenMoGuDiTask.name]) {
                    result = true;
                }
                else if (curHours == 22 && curMinutes >= 30 && !xiyouShenMoGuDiTaskLastTime[curMonth + '_' + curDateNum + '_' + 22]) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoXiYouZhuChen();
            await xSleep(1000);


            // 神魔谷地
            SingletonMap.o_n.o_EG("XiyouShenMoGuDiWin", null, false);
            await xSleep(500);
            // 领取奖励
            if (SingletonMap.XiyouMgr.o_UMf()) {
                SingletonMap.o_yKf.o_UMT();
                await xSleep(500);
            }
            let XiyouShenMoGuDiView = SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin").viewCon.$children.find(M => M.__class__ == 'XiyouShenMoGuDiView');
            // 开始历练
            if (XiyouShenMoGuDiView.startBtn.label == LangManager.o_Ur1[0]) {
                SingletonMap.o_yKf.o_UMM(XiyouShenMoGuDiView.curIdx + 1);
                await xSleep(500);
            }
            // 22点先停止，重新开始
            if (curHours >= 22) {
                xiyouShenMoGuDiTaskLastTime[curMonth + '_' + curDateNum + '_' + 22] = true;
                SingletonMap.o_yKf.o_Ur0(); // 停止历练
                await xSleep(500);
                // 领取奖励
                if (SingletonMap.XiyouMgr.o_UMf()) {
                    SingletonMap.o_yKf.o_UMT();
                    await xSleep(500);
                }
                SingletonMap.o_yKf.o_UMM(XiyouShenMoGuDiView.curIdx + 1);
                await xSleep(500);
            }

            SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin") && SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin"));// 关闭神魔谷地
            let nextWatchTime = curDate.getTime() + 8 * 60 * 60 * 1000 + 5000; // 8小时后
            bossNextWatchTime[XiyouShenMoGuDiTask.name] = nextWatchTime;
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
        if (SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin")) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("XiyouShenMoGuDiWin"));
            await xSleep(500);
        }
        await super.stop();
    }
}

// 西游仙府-炼器
class XiyouXFLianqiTask extends StaticGuaJiTask {
    static name = "XiyouXFLianqiTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23
                && (!bossNextWatchTime[XiyouXFLianqiTask.name] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[XiyouXFLianqiTask.name])) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoXiYouZhuChen();
            await xSleep(1000);

            // 炼器
            SingletonMap.o_n.o_EG("XiyouXFLianqiWin", null, false);
			await xSleep(1000);
			try {
				if (SingletonMap.o_UMF.data.o_UV5()){
					var M = Math.floor((SingletonMap.o_UMF.data.o_UV5().endTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
					// 先领取奖励
					if (M <= 0) {
						SingletonMap.o_UnK.o_URk();
						await xSleep(500);
						closeGameWin("XiyouXFLianqiResultWin");
						await xSleep(500);
					}
				}
			}
			catch(exx){
			}

            let lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 3); // 先找高阶
            if (!lianQiCfg) {
                if (SingletonMap.o_UMF.data.o_UQZ > 0) { // 可以免费刷新
                    SingletonMap.o_UnK.o_UM4();
                    await xSleep(1000);
                    lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 3);
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 2);
                    }
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 1); // 低阶
                    }
                }
                else {
                    lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 2); // 找中阶
                    if (!lianQiCfg) {
                        lianQiCfg = SingletonMap.o_UMF.data.o_UQF.find(M => M.time == 0 && M.level == 1); // 低阶
                    }
                }
            }
            // 开始炼器
            if (lianQiCfg) {
                SingletonMap.o_UnK.o_Uno(lianQiCfg.index, []);
                if (lianQiCfg.level == 3) {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 8 * 60 * 60 * 1000 + 5000; // 8小时后
                }
                else if (lianQiCfg.level == 2) {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 6 * 60 * 60 * 1000 + 5000; // 6小时后
                }
                else {
                    bossNextWatchTime[XiyouXFLianqiTask.name] = curDate.getTime() + 4 * 60 * 60 * 1000 + 5000; // 4小时后
                }
            }
            closeGameWin("XiyouXFLianqiWin"); // 关闭炼器
        }
        catch (ex) {
            console.error(ex);
        }

        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
        closeGameWin("XiyouXFLianqiWin");
        await super.stop();
    }
}

// 西游化缘
class XiyouHuayuanTask extends StaticGuaJiTask {
    huayuanAnswer = [{ 0: 2, 1: 2, 2: 2, 3: 2, 4: 2, 5: 2, 6: 2 },
    { 0: 1, 1: 2, 2: 1, 3: 0, 4: 1, 5: 1, 6: 1 },
    { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0 }];

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            if (curHours >= 10 && curHours < 23) {
                let arrFinishEvent = this.getFinishEvent();
                if (!bossNextWatchTime[XiyouHuayuanTask.name] || bossNextWatchTime[XiyouHuayuanTask.name] <= SingletonMap.ServerTimeManager.o_eYF) {
                    if (this.hasTimes() || (arrFinishEvent && arrFinishEvent.length > 0)) { // 有次数或有已完成的事件
                        result = true;
                    }
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    hasTimes() {
        let result = false;
        try {
            let count = SingletonMap.o_O.o_yCJ.baseBegNum;
            let useTimes = SingletonMap.o_NL6.getCountById(EntimesType.XIYOUHUAYUANTIMES, 1);
            let buyTimes = SingletonMap.o_NL6.getCountById(EntimesType.XIYOUHUAYUANTIMES, 2);
            if (count + buyTimes - useTimes > 0) {
                return true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getFinishEvent() {
        let result = null;
        try {
            result = SingletonMap.o_yCo.o_yE6.events.filter(M => !M.o_ykq);
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }



    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoXiYouZhuChen();
            await xSleep(1000);
            let arrFinishEvent = this.getFinishEvent();
            SingletonMap.o_n.o_EG("XiyouHuayuanWin", null, false);
            if (arrFinishEvent && arrFinishEvent.length > 0) { // 所有事件都完成了，提交事件
                for (var i = 0; i < arrFinishEvent.length; i++) {
                    let eventIndex = arrFinishEvent[i].eventId - 1;
                    let eventItemIndex = arrFinishEvent[i].o_yCc - 1;
                    let answerId = this.huayuanAnswer[eventIndex][eventItemIndex];
                    SingletonMap.o_yCD.o_yCU(arrFinishEvent[i].itemIndex, answerId);
                    await xSleep(500);
                    closeGameWin("XiyouHuayuanTalkEndWin");
                }
            }
            await xSleep(1000);
            arrFinishEvent = this.getFinishEvent();
            if (this.hasTimes() && (arrFinishEvent == null || arrFinishEvent.length == 0)) { //没有进行中的事件，并且有次数
                SingletonMap.o_yCD.o_ys_(SingletonMap.o_yCo.o_yE6.o_yCe); // 开始化缘
            }
            await xSleep(1000);
        }
        catch (ex) {
            console.error(ex);
        }
        bossNextWatchTime[XiyouHuayuanTask.name] = curDate.getTime() + 30 * 60 * 1000;// 30分钟后再次执行
        await this.stop();
    }

    async stop() {
        if (!this.isRunning()) return;
        closeGameWin("XiyouHuayuanWin");
        await super.stop();
    }
}

// 本服皇城争霸
class LocalServerHuangChengZhengBaTask extends StaticGuaJiTask {
    static name = "LocalServerHuangChengZhengBaTask";
    OnlyVitality = false;
    lastTaskTime = "";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            // 每周六20:00 - 20:30
            if (this.lastTaskTime == "" || this.lastTaskTime < SingletonMap.ServerTimeManager.o_eYF) {
                if (weekDay == 6 && curHours == 20 && (curMinutes >= 0 && curSeconds >= 10) && curMinutes < 30) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (weekDay == 6 && curHours == 20 && curMinutes >= 0 && curMinutes < 30) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    initConfig() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.LocalServerHuangChengZhengBaOnlyVitality == 'TRUE') {
                    this.OnlyVitality = true;
                }
                else {
                    this.OnlyVitality = false;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            this.initConfig();
            await this.gotoWangChen(); // 回王城
            await xSleep(500);
            SingletonMap.o_eaX.o_lxG(3); // 去皇宫
            await xSleep(500);
            await awaitToMap("皇城");
            // this.loopCheckHp();
            if (!this.OnlyVitality) {
                await this.run();
            }
			else {
				await xSleep(12000);  //等12秒拿战功活跃度 
			}
            this.lastTaskTime = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 60;
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async run() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    self.findWayIsStop = false;
                    if (!mainRobotIsRunning()) {
                        self.findWayIsStop = true;
                        console.log('mainRobot not Running.........');
                        return;
                    }
                    // 收到停止指令
                    if (self.stopTask == true) {
                        self.findWayIsStop = true;
                        console.log('stopTask.........');
                        return;
                    }

                    // 任务停止
                    if (self.isRunning && !self.isRunning()) {
                        console.log('not isRunning.........');
                        self.findWayIsStop = true;
                        return;
                    }

                    if (!self.isActivityTime()) {
                        self.findWayIsStop = true;
                        console.log('not activity time.........');
                        return;
                    }
                    // 人物死亡
                    if (AliveCheckLock.isLock()) {
                        return check();
                    }
                    if (self.diePos != null) {
                        return check();
                    }

                    if (SingletonMap.MapData.curMapName == '皇城') {
                        SingletonMap.o_OKA.o_NSw(47, 42);
                        return check();
                    }

                    if (SingletonMap.MapData.curMapName == '皇宫') {
                        // 寻找非本行会的人
                        let monster = self.findMonter();
                        if (monster) {
                            // 发起攻击
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else {
                            clearCurMonster();
                            self.attackIsStop = true;
                            SingletonMap.o_OKA.o_NSw(23, 20); // 前往皇座附近
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    findMonter() {
        let monster = null;
        try {
            if (curMonster && curMonster.curHP > 0 && this.existsMonster(curMonster)) {
                monster = curMonster;
            }
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof PlayerActor && !M.masterName && M.curHP > 0 && M.guildName != PlayerActor.o_NLN.guildName) {
                        if (targetMonster == null) {
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                        else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                            // 距离近的优先
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                    }
                }
                if (targetMonster) {
                    monster = targetMonster;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return monster;
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        if (!this.isRunning()) return;
        await super.stop();
    }
}

// 烧猪
class shaoZhuTask extends StaticGuaJiTask {
    static name = "shaoZhuTask";
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isFreeTime() && this.getTimes() > 0) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getTimes() {
        let result = 0;
        try {
            result = SingletonMap.o_OiP.times - SingletonMap.o_OiP.o_lth;
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            let times = this.getTimes();
            for (var i = 0; i < times; i++) {
                SingletonMap.o_lHn.o_NTD(SingletonMap.o_OiP.curLayer);
                await awaitToMap("经验副本");
                await awaitToMap("王城");
                await xSleep(1000);
                if (!mainRobotIsRunning()) {
                    break;
                }
                if (!this.isRunning()) {
                    break;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        if (!this.isRunning()) return;
        closeGameWin("ExpFbBuyWin");
        await super.stop();
    }
}
// 寻宝
class XunBaoTask extends StaticGuaJiTask {
	static name = "XunBaoTask";
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isFreeTime() && this.hasTimes()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	hasTimes() {
        let result = false;
        try {
            result = SingletonMap.o_Nlv.baseCfg.num > SingletonMap.o_Nlv.o_eWD();
			if (result && SingletonMap.o_Ue.o_OFL.find(M => M && M.name && M.name.indexOf('藏宝图') > -1 && M.name.indexOf('藏宝图残页') == -1 && M.name.indexOf('藏宝图秘卷') == -1)) {
				result = true;
			}
			else {
				result = false;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.hasTimes()) {
						try {
							gameObjectInstMap.ZjmTaskConView.o_lmT();
						}
						catch(exx){}
                        return;
                    }
					if (SingletonMap.MapData.curMapName == '藏宝阁') return check();
                    if (gameObjectInstMap.ZjmView.compassView.visible == false) {
						if (self.hasTimes()) {
							SingletonMap.o_Nlv.o_OOL(true);
						}
					}
					else if (gameObjectInstMap.ZjmView.compassView.barCon.visible == false) {
						gameObjectInstMap.ZjmView.compassView.o_eFC();
						// gameObjectInstMap.ZjmView.compassView.o_lXX() // 点击挖宝
					}
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	async enterMap() {
        await xSleep(500);
    }

    async stop() {
		if (!this.isRunning()) return false;
		try {
			SingletonMap.o_Nlv.o_lv1(); // 退出寻宝状态
			await xSleep(300);
			closeGameWin("XunbaotuAutoEndWin");
			await xSleep(300);
			gameObjectInstMap.ZjmTaskConView.o_lmT();
		}
		catch(ex) {}
        await super.stop();
    }
}

// 膜拜城主
class MoBaiChenZhuTask extends StaticGuaJiTask {
    static name = "MoBaiChenZhuTask";
    moBaiState = false;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();

            if (this.isActivityTime() && this.moBaiState == false) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 12 && curMinutes >= 0 && curMinutes < 10) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isOnlyOnlyVitality() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.MoBaiChenZhuOnlyVitality == 'TRUE') {
                    result = true;
                }
                else {
                    result = false;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            this.moBaiState = false;;
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					LastPointWatcher.resetLastPointInfo();
                    if (SingletonMap.o_lAh.o_exz() && self.moBaiState != true) {
                        SingletonMap.o_eqY.o_NGG(0);
                        self.moBaiState = true;
                    }
                    else if (self.isOnlyOnlyVitality() && self.moBaiState) { // 只拿活跃度
                        return;
                    }
                    if (!self.isActivityTime() && self.moBaiState) {
                        return;
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep();
    }

    async stop() {
        if (!this.isRunning()) return;
        await super.stop();
    }
}

// 西游蟠桃
class XiyouPanTaoTask extends StaticGuaJiTask {
    static name = "XiyouPanTaoTask";
	stateMap = {};
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
				let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
				let curMonth = curDate.getMonth();
				let curDateNum = curDate.getDate();
				if (!this.stateMap[curMonth + '_' + curDateNum]) {
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 15) {
                if (curMinutes == 30 && curSeconds >= 10) {
                    result = true;
                }
                else if (curMinutes > 30) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoXiYouZhuChen();
            await xSleep(1000);
			BlacklistMonitors.stop();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	isOnlyVitality() {
		let result = false;
		try {
			let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouPanTaoOnlyVitality == 'TRUE') {
                    result = true;
                }
                else {
                    result = false;
                }
            }
		}
		catch(e) {}
		return result;
	}

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 活动时间结束
                        return true;
                    }
					BlacklistMonitors.stop();
                    if (SingletonMap.MapData.curMapName == '西游降魔') {
                        SingletonMap.o_yKf.o_yZ3();
                        await xSleep(500);
                    }
                    else if (SingletonMap.MapData.curMapName != '蟠桃盛宴') {
                        await this.gotoXiYouZhuChen();
                        await xSleep(500);
                    }
                    else if (SingletonMap.MapData.curMapName == '蟠桃盛宴') {
						LastPointWatcher.resetLastPointInfo();
						if (self.isOnlyVitality()) {
							await xSleep(5000);
							let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
							let curMonth = curDate.getMonth();
							let curDateNum = curDate.getDate();
							self.stateMap[curMonth + '_' + curDateNum] = true;
							return;
						}
						SingletonMap.o_lAu.stop();
                        if (curMonster && !BlacklistMonitors.isBadPlayer(curMonster.roleName) && !SingletonMap.o_O.o_ykw.find(M => M.monId == curMonster.cfgId)) {
                            clearCurMonster();
                        }
                        if (SingletonMap.XiyouMgr.o_yZ7 == 2) { // 没座                            
                            let seatCfg = self.getTargetSeatCfg();
                            if (seatCfg) { // 有空位
                                clearCurMonster();
                                await self.sitSeat(seatCfg);
                            }
                            else if (GlobalUtil.compterFindWayPos(35, 69).length >= 8) { // 前往中心位置
                                await self.findWayByPos(35, 69, 'findWay', SingletonMap.MapData.curMapName);
                            }
                            else {
                                let badSeatPlayer = self.getBadSeatPlayer(); // 先杀有座的
                                while (badSeatPlayer) {
									BlacklistMonitors.stop();
                                    let seatCfg = self.getTargetSeatCfg(); // 有空座了，退出杀人状态
                                    if (seatCfg) {
                                        clearCurMonster();
                                        await self.sitSeat(seatCfg);
                                        break;
                                    }
                                    if (curMonster && !BlacklistMonitors.isBadPlayer(curMonster.roleName) && !SingletonMap.o_O.o_ykw.find(M => M.monId == curMonster.cfgId)) {
                                        clearCurMonster();
                                    }
									SingletonMap.o_lAu.stop();
                                    setCurMonster(badSeatPlayer);
                                    SingletonMap.o_OKA.o_Pc(badSeatPlayer, true);
                                    if (!GlobalUtil.existsMonster(badSeatPlayer)) {
                                        badSeatPlayer = self.getBadSeatPlayer();
                                    }
                                    await xSleep(100);
                                }
								// 杀没座的
								badSeatPlayer = BlacklistMonitors.getBadPlayer();
								while (badSeatPlayer) {
									BlacklistMonitors.stop();
									SingletonMap.o_lAu.stop();
                                    setCurMonster(badSeatPlayer);
                                    SingletonMap.o_OKA.o_Pc(badSeatPlayer, true);
                                    if (!GlobalUtil.existsMonster(badSeatPlayer)) {
                                        badSeatPlayer = BlacklistMonitors.getBadPlayer();
                                    }
                                    await xSleep(100);
                                }
								
                            }
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    sitSeat(seatCfg) {
        let self = this;
        function check() {
            return xSleep(300).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (!self.isActivityTime()) { // 活动时间结束
                    return true;
                }
                if (SingletonMap.XiyouMgr.o_yZ7 != 2) { // 坐上了
                    return true;
                }
                if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] == 1) { // 被别人占领了
                    return true;
                }

                if (PlayerActor.o_NLN.currentX != seatCfg.posTbl.x || seatCfg.posTbl.y != PlayerActor.o_NLN.o_NI3) {
                    SingletonMap.o_OKA.o_NSw(seatCfg.posTbl.x, seatCfg.posTbl.y);
                    return check();
                }
                else {
                    let seat = self.getSeatByCfg(seatCfg);
					if (seat && seat instanceof Monster) {
						SingletonMap.o_OKA.o_Pc(seat, true);
						await xSleep(3000);
					}
					return;
                }
            });
        }
        return check();
    }

    getTargetSeatCfg() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouPanTao != "") {
                    let arrSeatId = appConfig.XiyouPanTao.split('|');
                    for (let i = 0; i < arrSeatId.length; i++) {
                        let seatId = parseInt(arrSeatId[i]);
                        let seatCfg = SingletonMap.o_O.o_ykw.find(M => M.monId == seatId);
                        if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] != 1) {
                            result = seatCfg;
                            break;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
    getSeatByCfg(seatCfg) {
        return SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == seatCfg.monId && M.currentX == seatCfg.posTbl.x && M.o_NI3 == seatCfg.posTbl.y);
    }

    getSeatPlayer(posX, posY) {
        return SingletonMap.o_OFM.o_Nx1.find(M => M.currentX == posX && M.o_NI3 == posY && PlayerActor.o_NLN.guildName != M.guildName && BlacklistMonitors.isBadPlayer(M.roleName));
    }
    getBadSeatPlayer() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouPanTao != "") {
                    let arrSeatId = appConfig.XiyouPanTao.split('|');
                    for (let i = 0; i < arrSeatId.length; i++) {
                        let seatId = arrSeatId[0];
                        let seatCfg = SingletonMap.o_O.o_ykw.find(M => M.monId == seatId);
                        // 先找座位上的
                        if (SingletonMap.XiyouMgr.o_ytO[seatCfg.index - 1] == 1) {
                            let seatPlayer = this.getSeatPlayer(seatCfg.posTbl.x, seatCfg.posTbl.y);
                            if (seatPlayer && BlacklistMonitors.isBadPlayer(seatPlayer.roleName)) {
                                result = seatPlayer;
                                break;
                            }
                        }
                    }

                    if (!result) {
                        result = BlacklistMonitors.getBadPlayer();
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        if (result && !BlacklistMonitors.isWhitePlayer(result.roleName) && !BlacklistMonitors.isWhiteGuild(result.guildName)) {
			console.warn(result.roleName + ':' + BlacklistMonitors.isBadPlayer(result.roleName));
		}
		else {
			result = null;
		}
        return result;
    }

    async enterMap() {
        await xSleep(500);
    }
}

class XiyouCityBossTask extends StaticGuaJiTask {
    static name = "XiyouCityBossTask";
    originalEnableMovePickup = false;

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (isCrossTime()) {
				if ((curHours == 10 && curMinutes >= 30) || curHours > 10) {
					if ($.isEmptyObject(SingletonMap.o_eB9.o_yGa)) {
						result = true;
					}
					else if (this.getNextActivityBoss()) {
						result = true;
					}
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextActivityBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XiyouCityBoss != "") {
                    let XiyouCityBoss = appConfig.XiyouCityBoss.split("|");
                    let bossCfg = SingletonMap.o_eB9.o_yGa;
                    for (var i = 0; i < XiyouCityBoss.length; i++) {
                        let bossId = parseInt(XiyouCityBoss[i]);
                        let boss = SingletonMap.o_O.o_yGM.find(M => M.bossId == bossId);
                        if (bossCfg[boss.index] == 0) {
                            result = boss;
                            break;
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getActivityBossById(bossId) {
        let result = null;
        try {
            let boss = SingletonMap.o_O.o_yGM.find(M => M.bossId == bossId);
            let bossCfg = SingletonMap.o_eB9.o_yGa || {};
            if (bossCfg[boss.index] == 0) {
                result = boss;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
            BlacklistMonitors.start();
            await this.gotoXiYouZhuChen();
            await xSleep(1000);
            let curBoss = this.getNextActivityBoss();
            while (curBoss) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                // 前往boss坐标
                await this.findWayByPos(curBoss.bossPos[0], curBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(100);
                await this.killBoss(curBoss);
                curBoss = this.getNextActivityBoss();
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 16) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00'] = true;
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_30'] = true;
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_16_00'] = true;
            }
            else if (curHours >= 10 && curMinutes < 30) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_00'] = true;
            }
            else if (curHours >= 10 && curMinutes >= 30) {
                xiyouCityBossLastTime[curMonth + '_' + curDateNum + '_10_30'] = true;
            }
        }
        catch (e) {
            console.error(e);
        }

        await this.stop();
    }

    async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }

    killBoss(refreshBoss) {
        let self = this;
        function check() {
            return xSleep(100).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }

                let importantGoods = getImportantGoods();
                if (importantGoods) {
					if (isSuperPri()) {
						SingletonMap.o_lAu.stop();
						startMovePickup(true);
					}
					else if (!SingletonMap.o_lAu.o_lcD){
						SingletonMap.o_lAu.start();
					}
                    return check();
                }

                if (SingletonMap.MapData.curMapName != '西游降魔') { // 不在西游主城
                    await this.gotoXiYouZhuChen();
                    return check();
                }

                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 3) {
                    // 离boss超过3步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }

                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer && GlobalUtil.compterFindWayPos2(refreshBoss.bossPos[0], refreshBoss.bossPos[1], badPlayer.currentX, badPlayer.o_NI3).length <= 3) {	// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget();
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    // self.attackMonster(badPlayer);
                    return check();
                }

                let monster = SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == refreshBoss.bossId);
                if (!monster) { // 怪物消失
                    clearCurMonster();
                    if (self.getActivityBossById(refreshBoss.bossId)) { // 怪物还在，返回
                        await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        monster = SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId == refreshBoss.bossId);
                        if (!monster && self.getActivityBossById(refreshBoss.bossId)) { //怪物处于刷新状态但是目的地没有怪物
                            return;
                        }
                    }
                    else {
                        return;
                    }
                }
                else {
                    BlacklistMonitors.start();
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                }
                return check();
            });
        }
        return check();
    }

    async enterMap() {
        await xSleep(500);
    }

    async backToMap() {
        try {
            if (SingletonMap.MapData.curMapName != '西游降魔') {
                await this.gotoXiYouZhuChen();
            }
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(1000);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

//  跨服1V1
class Ladder1v1Task extends StaticGuaJiTask {
    static name = "Ladder1v1Task";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            let beginTime1 = 1431;
            let endTime1 = 1530;
            let beginTime2 = 1931;
            let endTime2 = 2000;
            try {
                if (appConfig != "") {
                    appConfig = JSON.parse(appConfig);
                    if (appConfig.Ladder1v1Time1 != "") {
                        beginTime1 = parseInt(appConfig.Ladder1v1Time1.replace(":", ""));
                    }
                    if (appConfig.Ladder1v1Time2 != "") {
                        beginTime2 = parseInt(appConfig.Ladder1v1Time2.replace(":", ""));
                    }
                }
            }
            catch (e) {
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (SingletonMap.Ladder1v1Mgr.o_NVM() && SingletonMap.Ladder1v1Mgr.o_ehV() > 0) {
                let curTime = parseInt(curHours + "" + (curMinutes >= 10 ? curMinutes : '0' + curMinutes));
                if (curTime >= beginTime1 && curTime < endTime1) {
                    result = true;
                }
                else if (curTime >= beginTime2 && curTime < endTime2) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
		clearCurMonster();
        function check() {
            return xSleep(2000).then(function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }

                if (!self.isActivityTime() && SingletonMap.MapData.curMapName != '1V1巅峰竞赛') { // 不是活动时间或次数用完了
                    return;
                }
				LastPointWatcher.resetLastPointInfo();
                if (SingletonMap.o_n.isOpen("Ladder1v1ResultWin")) {
					if (SingletonMap.MapData.curMapName == '王城') {
						closeGameWin("Ladder1v1ResultWin");
					}
					return check();
                }
                else if (!SingletonMap.Ladder1v1Mgr.o_O59 && !SingletonMap.o_n.isOpen("Ladder1v1MateWin")
                    && !SingletonMap.o_n.isOpen("Ladder1v1LoadWin")
                    && !SingletonMap.o_n.isOpen("Ladder1v1FinalsResultWin")
                    && SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                    console.warn('开始1v1匹配...');
					clearCurMonster();
                    SingletonMap.o_lVP.sendLadder1v1Mate();
                }
                else if (SingletonMap.MapData.curMapName == '1V1巅峰竞赛') { // 进入赛场
                    // console.warn('1v1比赛进行中...');
                    let monster = GlobalUtil.existsMonster(curMonster) || SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && !M.masterName && M.curHP > 0);
                    if (monster) {
                        setCurMonster(monster);
                        if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
                            SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
                        }
                        else {
                            try {
								console.warn(`发现目标${monster.name},发起攻击`);
                                SingletonMap.o_OKA.o_Pc(monster, true);
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                    else if (GlobalUtil.compterFindWayPos(28, 23).length >= 3) {
                        SingletonMap.o_OKA.o_NSw(28, 23);
                    }
                    else {
                        clearCurMonster();
                    }
                }
                return check();
            });
        }
        return check();
    }
}

//  跨服3V3
class Ladder3v3Task extends StaticGuaJiTask {
    static name = "Ladder3v3Task";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            result = this.isActivityTime() && !SingletonMap.Ladder3v3Mgr.is3v3Map();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            let beginTime1 = 1431;
            let endTime1 = 1530;
            let beginTime2 = 1931;
            let endTime2 = 2000;
            try {
                if (appConfig != "") {
                    appConfig = JSON.parse(appConfig);
                    if (appConfig.Ladder1v1Time1 != "") {
                        beginTime1 = parseInt(appConfig.Ladder1v1Time1.replace(":", ""));
                    }
                    if (appConfig.Ladder1v1Time2 != "") {
                        beginTime2 = parseInt(appConfig.Ladder1v1Time2.replace(":", ""));
                    }
                }
            }
            catch (e) {
            }
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (SingletonMap.Ladder3v3Mgr.o_NVM() && SingletonMap.Ladder3v3Mgr.o_ehV() > 0) {
                let curTime = parseInt(curHours + "" + (curMinutes >= 10 ? curMinutes : '0' + curMinutes));
                if (curTime >= beginTime1 && curTime < endTime1) {
                    result = true;
                }
                else if (curTime >= beginTime2 && curTime < endTime2) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
		clearCurMonster();
        function check() {
            return xSleep(2000).then(function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }
				LastPointWatcher.resetLastPointInfo();
                if (PlayerActor.o_NLN.o_LP) { // 处于幽灵状态
					clearCurMonster();
                    return check();
                }
                if (!self.isActivityTime() && !SingletonMap.Ladder3v3Mgr.is3v3Map()) { // 不是活动时间或次数用完了
                    console.warn('不是活动时间或次数用完了...');
                    return;
                }
                if (SingletonMap.o_n.isOpen("Ladder3v3ResultWin")) {
					if (SingletonMap.MapData.curMapName == '王城') {
						closeGameWin("Ladder3v3ResultWin");
					}
					return check();
                }
                else if (SingletonMap.Ladder3v3Mgr.is3v3Map()) { // 进入赛场
                    // 备战中
                    //if ((SingletonMap.Ladder3v3Mgr.is3v3Map() || SingletonMap.Ladder1v1Mgr.o_yrY() || SingletonMap.JobWarMgr.o_yrY()) && SingletonMap.SceneManager.o_lsJ) {
					//	clearCurMonster();
                    //    return check();
                    //}
                    let monster = GlobalUtil.existsMonster(curMonster) || SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.visible && M.curHP > 0 && SingletonMap.o_h7.o_lDb(M));
                    if (monster) {
                        if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
                            SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
                        }
                        else {
                            try {
                                PlayerActor.o_NLN.o_lkM();
                                setCurMonster(monster);
                                SingletonMap.o_OKA.o_Pc(monster, true);
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                    else if (GlobalUtil.compterFindWayPos(18, 18).length >= 3) {
                        SingletonMap.o_OKA.o_NSw(18, 18);
                    }
                    else {
                        clearCurMonster();
                    }
                }
                else if (!SingletonMap.Ladder3v3Mgr.o_O59 && !SingletonMap.o_n.isOpen("Ladder3v3MateWin")
                    && SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                    SingletonMap.o_lVP.sendLadder3v3Single();
					clearCurMonster();
                }
                return check();
            });
        }
        return check();
    }
}

//  职业巅峰赛1V1
class JobWar1v1Task extends StaticGuaJiTask {
    static name = "JobWar1v1Task";
	taskState = null;
	
    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.isActivityTime() && !this.isRunning() && this.taskState == null) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 19 && curMinutes <= 30) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			this.taskState = false;
			if (SingletonMap.JobWarMgr.o_y61) {
				// 海选
				if (SingletonMap.JobWarMgr.o_y8h() && !SingletonMap.JobWarMgr.o_y77) {
					await this.waitTaskFinish();
				}
				else if (SingletonMap.JobWarMgr.o_y7a() && !SingletonMap.JobWarMgr.o_y77) {// 晋级
					await this.waitTaskFinish2();
				}
				if (SingletonMap.ServerCross.o_ePe) {
					SingletonMap.o_Yr.o_NZ8(); // 退出
					await this.checkServerCrossMap();
				}
			}
        }
        catch (ex) {
            console.error(ex);
        }
		await xSleep(1000);
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 不是活动时间或次数用完了
                        return;
                    }
					LastPointWatcher.resetLastPointInfo();
                    if (SingletonMap.o_n.isOpen("JobWarResultWin")) {
                        closeGameWin("JobWarResultWin");
                    }
                    else if (SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                        SingletonMap.o_y6N.o_y54(); // 进入职业巅峰竞赛主城
                    }
                    else if (SingletonMap.MapData.curMapName == '职业巅峰竞赛主城' && SingletonMap.ServerCross.o_ePe) { // 进入赛场
                        if (!SingletonMap.o_n.isOpen("JobWarMateWin")) {
                            SingletonMap.o_y6N.o_y5B(); // 开始匹配
                        }
                    }
                    let monster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.curHP > 0);
                    if (SingletonMap.MapData.curMapName == '职业巅峰竞赛场') {
                        if (monster) {
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else if (GlobalUtil.compterFindWayPos(30, 17).length >= 3) {
                            SingletonMap.o_OKA.o_NSw(30, 17);
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	isActivityTime2() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 19 && curMinutes < 10) {
				result = true;
			}
			else if(curHours == 19 && curMinutes >= 10 && 20 < curMinutes) {
				result = true;
			}
			else if(curHours == 19 && curMinutes >= 20 && 25 < curMinutes) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}
	
	async waitTaskFinish2() {
        let self = this;
        function check() {
            try {
                return xSleep(1500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 不是活动时间或次数用完了
                        return;
                    }
                    if (SingletonMap.o_n.isOpen("JobWarResultWin")) {
                        closeGameWin("JobWarResultWin");
                    }
                    else if (SingletonMap.MapData.curMapName == '王城') { // 开始匹配
                        SingletonMap.o_y6N.o_y54(); // 进入职业巅峰竞赛主城
                    }
                    else if (SingletonMap.MapData.curMapName == '职业巅峰竞赛主城' && SingletonMap.ServerCross.o_ePe) { // 进入赛场
						if (self.isActivityTime2()) {
							SingletonMap.o_y6N.o_y5d();
						}
                    }
                    let monster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof PlayerActor && M.curHP > 0);
                    if (SingletonMap.MapData.curMapName == '职业巅峰竞赛场') {
                        if (monster) {
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                        }
                        else if (GlobalUtil.compterFindWayPos(30, 17).length >= 3) {
                            SingletonMap.o_OKA.o_NSw(30, 17);
                        }
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 神魔水晶
class ShenMoShuiJinTask extends StaticGuaJiTask {
    static name = "ShenMoShuiJinTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            result = this.isActivityTime();
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (curHours == 21 && curMinutes >= 1) {
                if (this.getTimes() > 0) {
                    result = true;
                }
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    getTimes() {
        let result = 0;
        try {
            var Q = SingletonMap.o_O.o_yH4.yaSongCount || 3;
            result = Q - SingletonMap.o_yy_.times;
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoShenMoZhuChen();
            await xSleep(1000);
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(2000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (self.getTimes() <= 0) return;
                    let npc = SingletonMap.o_OFM.o_Nx1.find(M => M.roleName == '神奇的水晶');
                    if (SingletonMap.o_n.isOpen("NewWorldCrystalWin")) { // 开始押送
                        if (SingletonMap.o_yy_.o_yqi >= 3) {
							if (SingletonMap.Ladder5v5Mgr.isTodayOpen5v5()){
								SingletonMap.o_yyA.o_yHg(); // 原地提交
							}
							else {
								SingletonMap.o_yyA.o_yH3();
							}
                        }
                        else {
                            SingletonMap.o_yyA.o_yqI();
                        }
                    }
                    else if (SingletonMap.o_n.isOpen("NewWorldCrystalSubmitWin")) { // 提交
                        SingletonMap.o_yyA.o_yHg();
                        closeGameWin("NewWorldCrystalSubmitWin");
                    }
                    else if (SingletonMap.o_yy_.o_eGE) { //在运水晶
                        SingletonMap.o_yy_.o_yq6();
                    }
                    else if (npc) {
                        SingletonMap.o_OKA.o_Pc(npc, true);
                    }
                    else {
                        //前往水晶祭坛
                        SingletonMap.o_yy_.o_yHx();
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep(500);
    }

    async stop() {
        if (!this.isRunning()) return;
        await super.stop();
    }
}


// 神界boss
class ShenJieBossTask extends StaticGuaJiTask {
    static name = "ShenJieBossTask";
    curRefreshBoss = null;

    needRun() {
        let result = false;
        try {
            this.init();
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_UbI)) {
                    result = true;
                }
                else if (this.getRefreshBoss()) {
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    init() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieBoss) {
                    this.arrGuaJiMap = appConfig.ShenJieBoss.split('|');
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    getRefreshBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenJieBoss != "") {
                    let arrBossRefreshTimes = this.getArrBossRefreshTimes();
                    let arrShenJieBoss = appConfig.ShenJieBoss.split("|");
                    for (let i = 0; i < arrShenJieBoss.length; i++) {
                        let bossId = parseInt(arrShenJieBoss[i]);
                        if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                            if (arrBossRefreshTimes[bossId] == 0) { // 已经刷新
                                boss = this.getBossCfg(bossId);
                                break;
                            }
                        }
                    }
                    if (boss == null) { // 寻找60秒内刷新的
                        for (let i = 0; i < arrShenJieBoss.length; i++) {
                            try {
                                let bossId = parseInt(arrShenJieBoss[i]);
                                if (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId]) {
                                    let bossRefreshTime = arrBossRefreshTimes[bossId];
                                    if (bossRefreshTime == null) {
                                        continue;
                                    }
                                    if (bossRefreshTime == 0 || bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) { // 已经刷新或30秒后刷新
                                        boss = this.getBossCfg(bossId);
                                        break;
                                    }
                                }
                            }
                            catch (e) {
                                console.error(e);
                            }
                        }
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    getArrBossRefreshTimes() {
        let result = {};
        try {
            let cfg = SingletonMap.o_eB9.o_UbI || {};
            for (var key in cfg) {
                Object.assign(result, cfg[key]);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getArrBossCfg() {
        let arrBossCfg = [];
        try {
            for (var i = 0; i < SingletonMap.o_O.o_Uai.length; i++) {
                for (var j = 0; j < SingletonMap.o_O.o_Uai[i].Layer.length; j++) {
                    arrBossCfg.push(SingletonMap.o_O.o_Uai[i].Layer[j]);
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return arrBossCfg;
    }

    getBossCfg(bossId) {
        let bossCfg = null;
        try {
            let arrBossCfg = this.getArrBossCfg();
            let layerBoss = arrBossCfg.find(M => M.bossList.includes(bossId));
            let bossIndex = layerBoss.bossList.findIndex(M => M == bossId);
            layerBoss.bossPos = layerBoss.bossPosList[bossIndex];
            bossCfg = Object.assign({}, layerBoss);
			// bossCfg.bossPos[1] = bossCfg.bossPos[1] + 5; // y坐标向下偏移5个点
            bossCfg.bossId = bossId;
        }
        catch (e) {
            console.error(e);
        }
        return bossCfg;
    }

    async refreshBossTime() {
        try {
            let arrBossCfg = this.getArrBossCfg();
            for (let i = 0; i < arrBossCfg.length; i++) {
                SingletonMap.o_Uij.o_Umd(arrBossCfg[i].mapId);
                await xSleep(100);
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async enterMap(map) {
        try {
            await this.gotoShenJie();
            SingletonMap.o_Uij.o_UX4(map[0], map[1]);
            this.curMap = map;
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            QuickPickUpNormal.start();
            BlacklistMonitors.start();
            // this.loopCheckHp();
            // this.loopCancleNoBelongMonster();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
                this.hashCode = new Date().getTime();
                //console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
                await this.gotoShenJie();
                await this.run();
            }
        }
        catch (e) {
            console.error(e);
        }
		TaskStateManager.updateTaskEnterState(this.constructor.name);
        await this.stop();
    }

    initState() {
        super.initState();
        this.curRefreshBoss = null;
    }

    async run() {
        try {
            await this.refreshBossTime();
            this.curRefreshBoss = this.getRefreshBoss();
            while (this.curRefreshBoss != null) {
                if (!mainRobotIsRunning()) {
                    break;
                }
                // 停止挂机了
                if (!this.isRunning()) {
                    break;
                }
                if (!this.isActivityTime()) {
                    break;
                }
                // 进入boss地图
                await this.enterMap([this.curRefreshBoss.id, this.curRefreshBoss.layer]);
                SingletonMap.o_lAu.stop();
                clearCurMonster();
                await xSleep(100);
				SingletonMap.o_lAu.stop();
                clearCurMonster();
                await this.killBoss(this.curRefreshBoss);
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                await this.gotoShenJie();
                // this.refreshBossTime();
                this.curRefreshBoss = this.getRefreshBoss();
                // 停止挂机了
                if (!this.isRunning()) {

                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrGuaJiMap.includes(M.cfgId + ""));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        this.afterFindMonster && this.afterFindMonster(monster);
        return monster;
    }

    filterMonster(monster) {
        if (!monster) return false;
        if (!this.arrGuaJiMap.includes(monster.cfgId + "")) {
            return false;
        }
        return true;
    }

    async backToMap() {
        try {
            // console.warn('回到boss地图...');
            let curRefreshBoss = this.curRefreshBoss || this.getRefreshBoss();
            if (curRefreshBoss) {
                if (curRefreshBoss.id && curRefreshBoss.layer) {
                    await this.enterMap([curRefreshBoss.id, curRefreshBoss.layer]);
                    await xSleep(500);
                }
                if (curRefreshBoss.bossPos) {
                    await this.findWayByPos(curRefreshBoss.bossPos[0], curRefreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                }
            }
            //callbackObj.writelog(`回到挂机地图...`);
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(500);
        this.diePos = null;
        // await this.stop();
    }

    async killBoss(refreshBoss) {
        let self = this;
		FirstKnife.setTargetCfgId(refreshBoss.bossId);
        function check() {
            return xSleep(20).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                if (!self.isActivityTime()) {
					return;
                }
				
                // 人物死亡
                if (!isAlive()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
                // 人物死亡
                if (self.diePos != null) {
                    return check();
                }
				// 先打人
				let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer && GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length <= 7) {
					self.findWayIsStop = true;
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }
				
				let monster = GlobalUtil.getMonsterById(refreshBoss.bossId) || self.findMonter();
				
				// 没发现怪，前往boss坐标
				if (!monster && GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 6) {
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    // await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
					SingletonMap.o_OKA.o_NSw(refreshBoss.bossPos[0], refreshBoss.bossPos[1]);
					return check();
                }
				
				// 归属黑名单
				if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
					let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
					if (belongingBadPlayer) {
						self.findWayIsStop = true;
						setCurMonster(belongingBadPlayer);
						SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
						return check();
					}
				}

                // 被打飞了
                if (SingletonMap.MapData.curMapName == '神界主城') {
                    await self.enterMap([refreshBoss.id, refreshBoss.layer]);
                    return check();
                }

                monster = GlobalUtil.getMonsterById(refreshBoss.bossId) || self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
					if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 6)  {
						SingletonMap.o_OKA.o_NSw(refreshBoss.bossPos[0], refreshBoss.bossPos[1]);
					}
					self.findWayIsStop = true;
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                else if (monster && !isSelfBelong(monster)) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
						self.findWayIsStop = true;
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.getBadPlayer()) {
						return check();
					}
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName)) { // 归属是黑名单
						// 先打黑名单归属者
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer && allowAttack(belongingBadPlayer)) {
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							return check();
						}
						else {
							bossNextWatchTime[refreshBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
							console.warn(`boss归属${monster.belongingName},当前模式不能攻击,离开不打...`);
							return;
						}
                    }
                    else {
						await xSleep(1200);
						let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
						if (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster)) {
							self.findWayIsStop = true;
							setCurMonster(tmpMonster);
							SingletonMap.o_OKA.o_Pc(tmpMonster, true);
							return check();
						}
						else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
							return check();
						}
						else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName)) {
							// 先打黑名单归属者
							let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
							if (belongingBadPlayer && allowAttack(belongingBadPlayer)) {
								setCurMonster(belongingBadPlayer);
								SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
								return check();
							}
							else {
								bossNextWatchTime[refreshBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
								console.warn(`boss归属${monster.belongingName},当前模式不能攻击,离开不打...`);
								return;
							}
						}
						else {
							bossNextWatchTime[refreshBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
							console.warn(`boss归属${monster.belongingName},离开不打...`);
							return;
						}
                    }
                }
                // 刷新boss时间
                // self.refreshBossTime();
                try {
					let arrBossRefreshTimes = self.getArrBossRefreshTimes();
                    let bossRefreshTime = arrBossRefreshTimes[refreshBoss.bossId];
                    // 刷新时间小于30秒，等待刷新
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                        return check();
                    }
                    // 刷新时间大于30秒
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 30000) {
                        // console.warn('killBoss...3.');
						console.warn(`刷新时间大于30秒,先离开...`);
                        return;
                    }
                }
                catch (exx) {
                    console.error(exx);
					console.warn(`获取刷新时间异常,先离开...`);
                    return;
                }
                return check();
            });
        }
        return check();
    }
}

// 神魔大战
// 19:30 -20:00 报名
// 20:05 - 20:30 活动
var NWWarMonType = {
	"shengshou": 1,
	"tuteng": 2,
	"shouhu": 3,
	"buff": 4
};

class ShenMoWarTask extends StaticGuaJiTask {
    static name = "ShenMoWarTask";
	arrWeekDayFlag = {
		"1": "weekA",
		"2": "weekB",
		"3": "weekC",
		"4": "weekD",
		"5": "weekE",
		"6": "weekF",
		"0": "weekG"
	};
	isEnd = false;
	
    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.isEnd) return false;
			if (this.isTodayActiv()) {
				result = this.isSignUpTime() || this.isActivityTime();
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isTodayActiv() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let weekDay = curDate.getDay();
			let weekDayFlag = this.arrWeekDayFlag[weekDay + ""];
			let dailyActivCalendar = DailyActivCalendar.getCalendar();
			let weekDayActiv = dailyActivCalendar.find(M => M[weekDayFlag] == '神魔大战');
			if (weekDayActiv) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			let seconds = curDate.getSeconds();
			if (curHours == 20 && ((curMinutes == 5 && seconds >= 6) || curMinutes > 5 && curMinutes < 30)) {
				if(SingletonMap.o_yy_.o_yAG == 1) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name + "_2005")) {
					TaskStateManager.updateTaskEnterState(this.constructor.name + "_2005");
					result = true;
				}
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isSignUpTime() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 19 && curMinutes >= 30) {
				if (SingletonMap.o_yy_.o_yAG == 0) {
					TaskStateManager.updateTaskEnterState(this.constructor.name + "_1930");
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name + "_1930")) {
					TaskStateManager.updateTaskEnterState(this.constructor.name + "_1930");
					result = true;
				}
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}
	
	getArrBossCfg() {
		var M = [];
		try {
			var i = SingletonMap.o_yy_;
			var Q = SingletonMap.o_O.o_yv9;
			var E = SingletonMap.RoleData.o_efG;
			for (var A = 0; A < Q.length; A++) {
				var b = Q[A];
				if (b.camp == E) continue;
				M.push({
					type: NWWarMonType.shengshou,
					camp: b.camp,
					cfg: b,
					state: i.o_yJs(NWWarMonType.shengshou, b.camp)
				})
			}
			Q = SingletonMap.o_O.o_yYW;
			for (var A = 0; A < Q.length; A++) {
				var b = Q[A];
				if (b.camp == E) continue;
				M.push({
					type: NWWarMonType.tuteng,
					camp: b.camp,
					cfg: b,
					state: i.o_yJs(NWWarMonType.tuteng, b.camp)
				})
			}
			Q = SingletonMap.o_O.o_yvp;
			for (var A = 0; A < Q.length; A++) {
				var r = Q[A].camp;
				if (r == E) continue;
				var g = Q[A].Tbl;
				for (var C = 0; C < g.length; C++) M.push({
					type: NWWarMonType.shouhu,
					camp: r,
					cfg: g[C],
					state: i.o_yJs(NWWarMonType.shouhu, r, g[C].index)
				})
			}
		}
		catch(ex) {
		}
		return M;
	}
	
	getNextBoss() {
		let result = null;
		try {
			let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableShenMoZhiZhan == 'TRUE') {
                    let arrShenMoZhiZhanBoss = appConfig.ShenMoZhiZhanBoss.split('|');
					let arrBossCfg = this.getArrBossCfg();
					for (let i = 0;i < arrShenMoZhiZhanBoss.length; i++) {
						let bossId = parseInt(arrShenMoZhiZhanBoss[i]);
						let boss = arrBossCfg.find(M => M.state == true && M.cfg.monsterId == bossId);
						if (boss) {
							result = boss;
							break;
						}
					}
                }
            }
		}
		catch(ex) {
		}
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			await xSleep(1000);
			if (!SingletonMap.o_n.isOpen("NewWorldWarWin")) {
				SingletonMap.o_n.o_EG("NewWorldWarWin");
			}
			await xSleep(1000);
			if (this.isSignUpTime()) {
				SingletonMap.o_yyA.o_yEv();
				SingletonMap.o_yyA.o_yDb();
				SingletonMap.o_yyA.o_yPt();
				SingletonMap.o_yyA.o_yPx();
				await xSleep(500);
			}
			else if (this.isActivityTime()){
                SingletonMap.o_yyA.o_yD5(); // 进入神魔战场
				await this.waitTaskFinish();
			}
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }
	
	isOverTime() {
		let result = false;
		try {
			if (this.isEnd) return true;
			let flag = SingletonMap.o_yy_.o_sD > SingletonMap.ServerTimeManager.o_eYF;
			if (flag) {
				result = false;
			}
			else {
				result = true;
			}
		}
		catch (ex) {
            console.error(ex);
        }
		return result;
	}

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1500).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (self.isOverTime()) { // 不是活动时间或次数用完了
						console.log('神魔大战结束.');
						self.isEnd = true;
                        return;
                    }
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
						return check();
					}
					if (SingletonMap.o_n.isOpen("NewWorldWarWin")) {
						closeGameWin('NewWorldWarWin');
					}
					if (SingletonMap.MapData.curMapName == '王城') {
						SingletonMap.o_n.o_EG("NewWorldWarWin");
						return check();
					}
					LastPointWatcher.resetLastPointInfo();
					let bossCfg = self.getNextBoss();
					if (bossCfg) {
						if (GlobalUtil.compterFindWayPos(bossCfg.cfg.monsterPos.x, bossCfg.cfg.monsterPos.y).length >= 5) {
							await self.findWayByPos(bossCfg.cfg.monsterPos.x, bossCfg.cfg.monsterPos.y, 'findWay', SingletonMap.MapData.curMapName);
						}
						else {
							let boss = GlobalUtil.getMonsterById(bossCfg.cfg.monsterId);
							setCurMonster(boss);
							SingletonMap.o_OKA.o_Pc(boss, true);
						}
					}
					else {
						// 寻找敌对战盟
						let badPlayer = self.findMonter();
						if (badPlayer) {
							if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length >= 5) {
								await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							}
							else {
								setCurMonster(badPlayer);
								SingletonMap.o_OKA.o_Pc(badPlayer, true);
							}
						}
						else {
							clearCurMonster();
						}
					}
                    
                    return check();
                });
            }
            catch (e) {
				return;
            }
        }
        return check();
    }
}

// 峡谷之战
class XiaGuZhiZhanTask extends StaticGuaJiTask {
	static name = "XiaGuZhiZhanTask";
	arrWeekDayFlag = {
		"1": "weekA",
		"2": "weekB",
		"3": "weekC",
		"4": "weekD",
		"5": "weekE",
		"6": "weekF",
		"0": "weekG"
	};
	
    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.isTodayActiv()) {
				if (this.isActivityTime()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isTodayActiv() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let weekDay = curDate.getDay();
			let weekDayFlag = this.arrWeekDayFlag[weekDay + ""];
			let dailyActivCalendar = DailyActivCalendar.getCalendar();
			let weekDayActiv = dailyActivCalendar.find(M => M[weekDayFlag] == '峡谷决战');
			if (weekDayActiv) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();			
			if (curHours == 20 && curMinutes < 20) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        await this.stop();
    }
	
	getNextFlagConfig() {
		let result = null;
		try {
			let arrFlagCfg = SingletonMap.o_O_R.o_eOY();
			let myGuildName = PlayerActor.o_NLN.guildName;
			for(let i = 0;i < arrFlagCfg.length; i++) {
				let flagConfig = arrFlagCfg[i];
				let flagBelongInfo = SingletonMap.o_OhS.o_Njw(flagConfig.index);
				if (flagBelongInfo && (!flagBelongInfo.guildName || flagBelongInfo.guildName.indexOf(PlayerActor.o_NLN.guildName) == -1)) {
					result = flagConfig;
					break;
				}
			}
		}
		catch (ex) {
            console.error(ex);
        }
		return result;
	}

	findMonter() {
        let monster = null;
        try {
            if (curMonster && curMonster.curHP > 0 && this.existsMonster(curMonster)) {
                monster = curMonster;
            }
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof PlayerActor && !M.masterName && M.curHP > 0 && M.guildName != PlayerActor.o_NLN.guildName) {
                        if (targetMonster == null) {
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                        else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                            // 距离近的优先
                            targetMonster = M;
                            minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                            continue;
                        }
                    }
                }
                if (targetMonster) {
                    monster = targetMonster;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return monster;
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) { // 不是活动时间或次数用完了
                        return;
                    }
                    if (SingletonMap.MapData.curMapName == '王城') {
                        SingletonMap.o_Yr.o_NZ8(KfActivity.o_Fb); // 进入峡谷
						return check();
                    }
					LastPointWatcher.resetLastPointInfo();
                    let flagCfg = self.getNextFlagConfig();
					if (flagCfg) {
						if (GlobalUtil.compterFindWayPos(flagCfg.flushArea.x, flagCfg.flushArea.y).length >= 6) {
							await self.findWayByPos(flagCfg.flushArea.x, flagCfg.flushArea.y, 'findWay', SingletonMap.MapData.curMapName);
						}
						
						let flag = GlobalUtil.getMonsterById(flagCfg.monID);
						setCurMonster(flag);
						SingletonMap.o_OKA.o_Pc(flag, true);
						return check();
					}
					else {
						// 寻找敌对战盟
						let badPlayer = self.findMonter();
						if (badPlayer) {
							if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length >= 6) {
								await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							}
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
						}
					}
                    return check();
                });
            }
            catch (e) {
				return;
            }
        }
        return check();
    }
	
	async backToMap() {
        try {
            await xSleep(100);
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}


// 高爆地图
class GaoBaoGuaJiTask extends StaticGuaJiTask {
    static name = "GaoBaoGuaJiTask";
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            let runningTask = SingletonModelUtil.getRunningTask();
            if (SingletonMap.o_NP8.o_lkV > 0 && isFreeTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getMaxLayer() {
		let result = 1;
		try {
			if (SingletonMap.o_NP8.o_lYE >= 50) {
				result = 3;
			}
			else if (SingletonMap.o_NP8.o_lYE >= 10) {
				result = 2;
			} 
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.GaoBaoGuaJi) {
					let maxLayer = this.getMaxLayer();
                    SingletonMap.o_lyZ.o_NHl(maxLayer);
                }
                else {
                    SingletonMap.o_lyZ.o_NHl(1); // 1福利 2高爆1 3高爆2
                }
                await this.waitTaskFinish();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(3000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (SingletonMap.o_NP8.o_lkV <= 0) { // 挂机时间已结束
                        return;
                    }
                    if (!SingletonMap.o_lAu.o_lcD) {
                        SingletonMap.o_lAu.start();
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async enterMap() {
        await xSleep();
    }

    async stop() {
        if (!this.isRunning()) return;
        await super.stop();
    }
}

// 普通玛法
class StaticNormalMafaBossTask extends StaticGuaJiTask {
    static name = "StaticNormalMafaBossTask";
    state = false;
    arrBossPosTemplate = [];
    arrBossName = [];
    arrBossPos = [];
    diePos = null;
    curMapName = null;
    nextRunTime = null;
    bossId = null;
	latestBatch = 0;
	latestBossBatch = 0;

    bosIsAlive() {
        let result = false;
        try {
            let bossTime = normalVillaMafaBossTime[this.bossId];
            if (!bossTime) {
                result = true;
            }
            else {
                let M = Math.floor((bossTime - SingletonMap.ServerTimeManager.o_eYF) / 1e3);
                result = M <= 0 ? true : false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    updateNextRunTime(time) {
        this.nextRunTime = time;
        let nextRunDateStr = new Date(this.nextRunTime).toLocaleString();
        console.warn(`任务${this.constructor.name}下次执行时间${nextRunDateStr}`);
    }

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isCrossTime() && SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL) >= 1) {
				let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                if (this.bossId) {
					if(!normalVillaMafaBossTime[this.bossId] || normalVillaMafaBossTime[this.bossId] - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
						result = true;
					}
                }
				else if (!this.nextRunTime || this.nextRunTime <= curDate.getTime()) {
					result = true;
				}
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			this.initState();
			if (SingletonMap.MapData.curMapName != "玛法幻境") {
				await this.gotoWangChen(); // 先回王城
				await awaitToMap("王城");
			}
            await this.gotoMafaHuanJin();
            if (this.arrBossPosTemplate != null) {
                this.arrBossPos = Object.assign([], this.arrBossPosTemplate);
                QuickPickUpNormal.start();
                //BlacklistMonitors.start();
                //// this.loopCheckHp();
                //this.loopAttackMonter();
                await this.run();
                // console.warn(`${this.constructor.name}任务开始了。。。。。UID=${this.uid}`);
            }
        }
        catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async run() {
        try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			this.latestBossBatch = curDate.getDate();
			this.latestBatch = curDate.getDate();
			
            await this.enterMap();
            await awaitLeaveMap('玛法幻境');
            SingletonMap.o_lAu.stop();
			PlayerActor.o_NLN.o_lkM();
			if (this.bossId && !this.isWaitingTime()) { // 是打boss,boss已击杀或没到刷新时间
				await xSleep(1000);
				// await this.stop();
			}
			else {
				this.curMapName = SingletonMap.MapData.curMapName;
				let curIdx = 0;
				while (this.arrBossPos.length > 0) {
					console.warn(`剩余坐标数量:${this.arrBossPos.length}`);
					if (!this.isRunning()) {
						break;
					}
					if (!mainRobotIsRunning()) {
						break;
					}
					if (SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL) <= 0) {
						console.warn(`星空之力不足, 当拥有星空之力${SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL)}`);
						break;
					}
					
					let minPos = this.arrBossPos[0];
					if (!this.bossId) {
						if (this.arrBossPos.length > 1) {
							minPos = this.arrBossPos.reduce(function (a, b) {
								let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
								let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
								return path1.length >= path2.length ? b : a;
							});
						}
					}
					await this.findWayByPos(minPos.x, minPos.y, 'findWay', SingletonMap.MapData.curMapName);
					await xSleep(50);
					await this.waitBossKill(minPos, curIdx);
					curIdx++;
					this.arrBossPos = this.arrBossPos.filter(M => {
						return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
					});
					QuickPickUpNormal.runPickUp();
					await xSleep(50);
				}
				QuickPickUpNormal.runPickUp();
				await xSleep(500);
			}
        }
        catch (ex) {
            console.error(ex);
        }
    }

    isWaitingTime() {
        let result = false;
        try {
            if (this.bossId && (this.getBossRefreshTime() == 0 || this.getBossRefreshTime() - SingletonMap.ServerTimeManager.o_eYF <= 30000)) {
                result = true; // 60秒内刷新
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	findMonter() {
        let monster = null;
        try {
            monster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && this.filterMonster(M));
        } catch (ex) {
            console.error(ex);
        }
        return monster;
    }

    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossName.find(item => monster.roleName.indexOf(item) > -1 && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async waitBossKill(bossPos, curIdx) {
        let self = this;
        function check() {
            return xSleep(100).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (self.isRunning && !self.isRunning()) {
                    return;
                }
				if (SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL) <= 0) {
					return;
				}
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
				
				if (SingletonMap.MapData.curMapName == '玛法幻境') {
					await self.enterMap();
					return check();					
				}
				
				let bossMonster = null;
                if (self.bossId) {
                    bossMonster = GlobalUtil.getMonsterById(self.bossId);
					if (!bossMonster && self.isWaitingTime() && curIdx == 0) {
						if (GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length >= 6) {
							BlacklistMonitors.stop();
							clearCurMonster();
							await self.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
						}
						return check();
					}
                }
							
                let monster = bossMonster || self.findMonter();

                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(badPlayer.currentX, badPlayer.o_NI3)) {
						self.findWayIsStop = true;
                        setCurMonster(badPlayer);
                        SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(badPlayer.currentX, badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }
				
                // 已经刷新
                if (monster && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
					if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
                        await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
                    }
					//self.findWayIsStop = true;
                    //else {
					setCurMonster(monster);
					SingletonMap.o_OKA.o_Pc(monster, true);
					return check();
                    //}
                }

                if (monster && self.existsMonster(monster) && !isNullBelong(monster) && !isSelfBelong(monster)) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
						self.findWayIsStop = true;
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    else if (GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length <= 6) {
						console.warn(`怪物归属${monster.belongingName},离开不打..`);
                        normalVillaMafaBossTime[monster.cfgId] = SingletonMap.ServerTimeManager.o_eYF + 300000;
                        return;
                    }
                }
				
				if ((!monster || !self.existsMonster(monster)) && GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length >= 6) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(bossPos.x, bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
					return check();
                }
				
				if ((!monster || !self.existsMonster(monster)) && GlobalUtil.compterFindWayPos(bossPos.x, bossPos.y).length <= 6) {
					console.warn(`没有适合攻击的怪物,离开..`);
					return;
				}

                //if (self.curMapName == SingletonMap.MapData.curMapName && self.findMonter() == null) {
                //    return;
                //}
                //else {
                    return check();
                //}
            });
        }
        return check();
    }

    //async stop() {
	//	if (!this.isRunning()) return false;
    //    await super.stop();
    //}

    async enterMap() {
        await xSleep(1000);
    }


    async exitMap() {
        try {
            SingletonMap.o_Ozj.o_elB(); // 回到玛法幻境
            await xSleep(500);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(500);
            if (this.diePos) {
                await this.findWayByPos(this.diePos.x, this.diePos.y, 'findWay', SingletonMap.MapData.curMapName);
                await xSleep(500);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

class MafaChiYueBossTask extends StaticNormalMafaBossTask {
    bossId = 157010;
    static name = "MafaChiYueBossTask";
    arrBossPosTemplate = [{ x: 77, y: 131 }, { x: 53, y: 64 }, { x: 52, y: 45 }, { x: 37, y: 45 }];
    arrBossName = ['绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_GW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaChiYueStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaChiYueStar2Task";
    arrBossPosTemplate = [{ x: 22, y: 108 }, { x: 36, y: 112 }, { x: 66, y: 118 }, { x: 67, y: 140 }, { x: 88, y: 133 }, { x: 94, y: 106 }, { x: 96, y: 84 }, { x: 91, y: 46 }, { x: 66, y: 59 }, { x: 49, y: 63 }, { x: 75, y: 81 }, { x: 20, y: 60 }, { x: 23, y: 20 }];
    arrBossName = ['★','绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(2);
            await xSleep(1000);
            // normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_GW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaChiYueStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaChiYueStar1Task";
    arrBossPosTemplate = [{ x: 19, y: 27 }, { x: 83, y: 21 }, { x: 89, y: 45 }, { x: 78, y: 135 }, { x: 52, y: 98 }, { x: 65, y: 84 }];
    arrBossName = ['★','绝世恶魔', '赤月恶魔'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_eXi(1);
            await xSleep(1000);
            //normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_GW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaMoLongBossTask extends StaticNormalMafaBossTask {
    bossId = 157020;
    static name = "MafaMoLongBossTask";
    arrBossPosTemplate = [{ x: 86, y: 40 }, { x: 63, y: 50 }, { x: 71, y: 39 }];
    arrBossName = ['‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_Ng_[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaMoLongStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaMoLongStar1Task";
    arrBossPosTemplate = [{ x: 39, y: 222 }, { x: 52, y: 180 }, { x: 76, y: 106 }, { x: 88, y: 55 }, { x: 88, y: 21 }, { x: 144, y: 24 }];
    arrBossName = ['★','‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(1);
            await xSleep(1000);
            // normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_Ng_[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaMoLongStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaMoLongStar2Task";
    arrBossPosTemplate = [{ x: 17, y: 126 }, { x: 36, y: 139 }, { x: 55, y: 114 }, { x: 44, y: 97 }, { x: 53, y: 90 }, { x: 31, y: 86 }, { x: 53, y: 65 }, { x: 67, y: 62 }, { x: 82, y: 41 }, { x: 98, y: 26 }, { x: 103, y: 45 }, { x: 83, y: 71 }, { x: 98, y: 98 }, { x: 109, y: 111 }];
    arrBossName = ['★','‖神‖★★魔龙战神', '暗之魔龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_NdF(2);
            await xSleep(1000);
            //normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_Ng_[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaHuoLongBossTask extends StaticNormalMafaBossTask {
    bossId = 157030;
    static name = "MafaHuoLongBossTask";
    arrBossPosTemplate = [{ x: 59, y: 19 }, { x: 13, y: 13 }, {x: 56, y: 73}];
    arrBossName = ['火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_eFv[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaHuoLongStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaHuoLongStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 43 }, { x: 50, y: 32 }, { x: 56, y: 22 }, { x: 63, y: 30 }, { x: 47, y: 18 }, { x: 51, y: 67 }, { x: 62, y: 71 }, { x: 71, y: 55 }, { x: 11, y: 43 }, { x: 17, y: 20 }, { x: 25, y: 8 }];
    arrBossName = ['★','火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(2);
            await xSleep(1000);
            // normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_eFv[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaHuoLongStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaHuoLongStar1Task";
    arrBossPosTemplate = [{ x: 107, y: 219 }, { x: 137, y: 179 }, { x: 82, y: 113 }, { x: 133, y: 100 }, { x: 57, y: 63 }, { x: 57, y: 111 }];
    arrBossName = ['★','火龙教主'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_K8(1);
            await xSleep(1000);
            // normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_eFv[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaDuShiBossTask extends StaticNormalMafaBossTask {
    bossId = 157115;
    static name = "MafaDuShiBossTask";
    arrBossPosTemplate = [{ x: 53, y: 48 }, { x: 9, y: 27 }, { x: 57, y: 71 }, { x: 78, y: 75 }, { x: 8, y: 73 }];
    arrBossName = ['绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_efW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaDuShiStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaDuShiStar1Task";
    arrBossPosTemplate = [{ x: 28, y: 81 }, { x: 42, y: 54 }, { x: 35, y: 38 }, { x: 49, y: 26 }, { x: 67, y: 68 }, { x: 80, y: 68 }, { x: 76, y: 39 }, { x: 66, y: 83 }];
    arrBossName = ['★', '变异','绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(1);
            await xSleep(1000);
            // normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_efW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaDuShiStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaDuShiStar2Task";
    arrBossPosTemplate = [{ x: 28, y: 71 }, { x: 44, y: 55 }, { x: 47, y: 66 }, { x: 56, y: 75 }, { x: 56, y: 87 }, { x: 77, y: 69 }, { x: 86, y: 66 }, { x: 72, y: 57 }, { x: 34, y: 42 }, { x: 47, y: 27 }, { x: 28, y: 28 }, { x: 4, y: 23 }];
    arrBossName = ['★', '变异','绿色巨人', '金刚狼人'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_lNG(2);
            await xSleep(1000);
            //normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_efW[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaXianLingBossTask extends StaticNormalMafaBossTask {
    bossId = 157215;
    static name = "MafaXianLingBossTask";
    arrBossPosTemplate = [{ x: 66, y: 120 }, { x: 52, y: 54 }];
    arrBossName = ['持柱天王', '十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(2);
            await xSleep(1000);
            normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
        }
        catch (ex) {
            console.error(ex);
        }
    }
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_yAb[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
class MafaXianLingStar2Task extends StaticNormalMafaBossTask {
    static name = "MafaXianLingStar2Task";
    arrBossPosTemplate = [{ x: 37, y: 62 }, { x: 67, y: 59 }, { x: 39, y: 38 }, { x: 6, y: 73 }, { x: 8, y: 79 }, { x: 12, y: 84 }, { x: 35, y: 95 }, { x: 40, y: 130 }, { x: 24, y: 156 }, { x: 49, y: 161 }, { x: 62, y: 154 }];
    arrBossName = ['★','持柱天王', '十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(2);
            await xSleep(1000);
            //normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_yAb[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}

class MafaXianLingStar1Task extends StaticNormalMafaBossTask {
    static name = "MafaXianLingStar1Task";
    arrBossPosTemplate = [{ x: 24, y: 93 }, { x: 41, y: 118 }, { x: 29, y: 140 }];
    arrBossName = ['★','持柱天王', '十殿阴司'];
    async enterMap() {
        try {
            SingletonMap.o_Ozj.o_yAP(1);
            await xSleep(1000);
            //normalVillaMafaBossTime[this.bossId] = this.getBossRefreshTime();
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
			super.updateNextRunTime(nextDate);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    //async exitMap() {
    //    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
    //    let nextDate = curDate.getTime() + 2 * 60 * 60 * 1000;
    //    super.updateNextRunTime(nextDate);
    //    await super.exitMap();
    //}
	
	getBossRefreshTime() {
		let result = 0;
		try {
			result = SingletonMap.o_Ntl.o_yAb[2][1];
		}
		catch (ex) {
        }
		return result;
	}
}
// 神龙山庄
class ShenLongShanZhuanTask extends StaticGuaJiTask {
    static name = 'ShenLongShanZhuanTask';
    state = false;
    diePos = null;
    curMapName = null;
    arrBossPos = [];
    arrBossId = [];
    arrBossCfgId = {
        1: 152001, 2: 152001, 3: 152001, 4: 152002, 5: 152002, 6: 152002, 7: 152003, 8: 152003, 9: 152003,
        10: 152004, 11: 152004, 12: 152005, 13: 152005, 14: 152006, 15: 152006, 16: 152007, 17: 152008, 18: 152101,
        19: 152102, 20: 152103, 21: 152104, 22: 152105
    };

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (isCrossTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_eXy)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenLongShanZhuanBoss) {
                    // 先找已经刷新的
                    let arrShenLongShanZhuanBossIndex = appConfig.ShenLongShanZhuanBoss.split('|');
                    for (let i = 0; i < arrShenLongShanZhuanBossIndex.length; i++) {
                        let bossIndex = parseInt(arrShenLongShanZhuanBossIndex[i]);
                        let bossCfg = SingletonMap.o_O.o_N38.find(M => M.index == bossIndex);
                        if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] == 0 &&
                            (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                            result = bossCfg;
                            break;
                        }
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        // 找快要刷新的 时间短的优先
                        for (let i = 0; i < arrShenLongShanZhuanBossIndex.length; i++) {
                            let bossIndex = parseInt(arrShenLongShanZhuanBossIndex[i]);
                            let bossCfg = SingletonMap.o_O.o_N38.find(M => M.index == bossIndex);
                            if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] == 0 &&
                                (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                    || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                                result = bossCfg;
                                break;
                            }
                            else if (bossCfg && SingletonMap.o_eB9.o_eXy[bossCfg.index] - SingletonMap.ServerTimeManager.o_eYF <= 60000 &&
                                (!ShenLongShanZhuanBossNextWatchTime[bossCfg.index]
                                    || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] <= SingletonMap.ServerTimeManager.o_eYF)) {
                                if (mintimeBoss == null || ShenLongShanZhuanBossNextWatchTime[bossCfg.index] < ShenLongShanZhuanBossNextWatchTime[mintimeBoss.index]) {
                                    mintimeBoss = bossCfg;
                                }
                            }
                        }
                        if (result == null) {
                            result = mintimeBoss;
                        }
                    }
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        this.arrBossPos = [];
        this.arrBossId = [];
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenLongShanZhuanBoss) {
                let arrBoss = appConfig.ShenLongShanZhuanBoss.split("|");
                for (let i = 0; i < arrBoss.length; i++) {
                    let bossIndex = parseInt(arrBoss[i]);
                    let bossCfg = SingletonMap.o_O.o_N38[bossIndex - 1];
                    this.arrBossId.push(this.arrBossCfgId[bossCfg.index]);
                }
            }
        }
    }

    async start() {
        try {
			if(this.isRunning()) return;
            if (this.state == true) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            this.init();
			if (!['苍月岛', '神龙山庄', '王城'].includes(SingletonMap.MapData.curMapName)) {
				await this.gotoWangChen(); // 先回王城
			}
			if (!['苍月岛', '神龙山庄'].includes(SingletonMap.MapData.curMapName)) {
				await this.gotoChanYueDao(); // 进入苍月岛
			}
			SingletonMap.o_wB.o_Tl(); // 刷新boss时间
			await xSleep(300);
			QuickPickUpNormal.start();
			BlacklistMonitors.start();
			await this.run();
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async run() {
        try {
            await this.enterMap();
            await xSleep(500);
            SingletonMap.o_lAu.stop();
            this.curMapName = SingletonMap.MapData.curMapName;
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				clearCurMonster();
				BlacklistMonitors.stop();
				SingletonMap.o_lAu.stop();
                // await this.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                //  SingletonMap.o_lAu.stop();
                await this.waitBossKill(nextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(500);
                if (!this.isRunning()) {
                    break;
                }
                if (!mainRobotIsRunning()) {
                    break;
                }
                nextBoss = this.getNextBoss();
            }
            await xSleep(100);
        }
        catch (ex) {
            console.error(ex);
        }
    }

    isAlive() {
        let result = true;
        try {
            result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
            if (!result) {
                this.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
        } catch (ex) {
            console.error(ex);
        }
        return result;
    }

    afterFindMonster() { }

    async enterMap() {
        try {
			if (!['神龙山庄'].includes(SingletonMap.MapData.curMapName)) {
				SingletonMap.o_wB.o_FW();
			}
            await awaitToMap("神龙山庄");
        }
        catch (ex) {
            console.error(ex);
        }
    }

    async exitMap() {
        try {
            await this.gotoChanYueDao();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    findMonter() {
        let monster = null;
        let self = this;
        try {
            let tmpMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster
                && !M.masterName && M.curHP > 100 && self.arrBossId.includes(M.cfgId));
            if (tmpMonster != null) {
                monster = this.filterMonster(tmpMonster) ? tmpMonster : null;
            }
        } catch (ex) {
            console.error(ex);
        }
        return monster;
    }

    async waitBossKill(refreshBoss) {
        let self = this;
		FirstKnife.setTargetCfgId(refreshBoss.bossId);
        function check() {
            return xSleep(30).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
                // 人物死亡
                if (AliveCheckLock.isLock()) {
                    return check();
                }
                QuickPickUpNormal.runPickUp();
                if (GlobalUtil.compterFindWayPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1]).length >= 6) {
                    // 离boss超过6步,返回
                    BlacklistMonitors.stop();
                    clearCurMonster();
                    await self.findWayByPos(refreshBoss.bossPos[0], refreshBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName, refreshBoss.bossId);
                }

                BlacklistMonitors.loopCheckBadPlayer();
                if (BlacklistMonitors.badPlayer != null) {// 开启了黑名单
                    if (GlobalUtil.isAttackDistance(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3)) {
                        setCurMonster(BlacklistMonitors.badPlayer);
                        SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
                    }
                    else {
                        let targetPos = GlobalUtil.getTargetPos(BlacklistMonitors.badPlayer.currentX, BlacklistMonitors.badPlayer.o_NI3);
                        SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                    }
                    return check();
                }

                let monster = self.findMonter();
                // 已经刷新
                if (monster && self.filterMonster(monster)
                    && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                if (monster && self.existsMonster(monster) && !isNullBelong(monster) && !isSelfBelong(monster)) { // 归属不是自己
                    // console.warn('killBoss...1.');
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
						return check();
                    }
                    else {
						ShenLongShanZhuanBossNextWatchTime[refreshBoss.index] = SingletonMap.ServerTimeManager.o_eYF + 300000;
                        return;
                    }
                }

                try {
                    let bossRefreshTime = SingletonMap.o_eB9.o_eXy[refreshBoss.index];
                    // 刷新时间小于30秒，等待刷新
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
                        // console.warn('killBoss...2');
                        return check();
                    }
                    // 刷新时间大于30秒
                    if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
                        // console.warn('killBoss...3.');
                        return;
                    }
                }
                catch (exx) {
                    console.error(exx);
                    return;
                }
                return check();
            });
        }
        return check();
    }


    filterMonster(monster) {
        try {
            if (!monster) return false;
            let boss = this.arrBossId.find(item => monster.cfgId == item && monster.curHP > 100);
            if (boss) {
                return true;
            }
            else {
                return false;
            }
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }

    async backToMap() {
        try {
            await this.enterMap();
            await xSleep(1000);
            if (this.diePos && SingletonMap.MapData.o_lGk(this.diePos.x, this.diePos.y)) {
                await this.findWayByPos(this.diePos.x, this.diePos.y);
            }
        } catch (ex) {
            console.error(ex);
        }
        this.diePos = null;
    }
}

// 押镖
class EscortTask extends StaticGuaJiTask {
    static name = "EscortTask";
    targetLevel = 1; // 目标等级
	buySafe = false; // 买保险
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime() && SingletonMap.o_eXx.o_OP7 > 0) {
                result = true;
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours >= 10 && curHours < 23) {
				let appConfig = getAppConfig();
				if (appConfig != "") {
                    appConfig = JSON.parse(appConfig);
					let beginTime = "";
					let endTime = "";
                    if (appConfig.EscortBeginTime != "") {
                        beginTime = parseInt(appConfig.EscortBeginTime.replace(":", ""));
                    }
                    if (appConfig.EscortEndTime != "") {
                        endTime = parseInt(appConfig.EscortEndTime.replace(":", ""));
                    }
					if (beginTime != "" && endTime != "") {
						let curTime = parseInt(curHours + "" + (curMinutes >= 10 ? curMinutes : '0' + curMinutes));
						if (curTime >= beginTime && curTime < endTime) {
							result = true;
						}
					}
					else if (curHours == 16 && curMinutes >= 30){
						result = true;						
					}
                }
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    init() {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EscortLevel != "") {
                    this.targetLevel = parseInt(appConfig.EscortLevel);
                }
				if (appConfig.BuySafe == "TRUE") {
                    this.buySafe = true;
                }
            }
        }
        catch (ex) {
            this.targetLevel = 1;
            console.warn(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            this.init();
			BlacklistMonitors.stop();
            await this.gotoWangChen();
            await this.gotoChanYueDao();
            await this.waitTaskFinish();
			SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
        }
        catch (ex) {
            console.error(ex);
        }
        closeGameWin('EscortGiftWin'); // 押镖奖励窗口
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(3000).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();						
				}
				
                if (SingletonMap.o_eXx.o_eGE) { // 押镖中
                    if (SingletonMap.o_eXx.o_NDD(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3)) { // 距离镖车过远
                        SingletonMap.o_eXx.o_ljt(); // 飞到镖车
                        await xSleep(300);
                    }
                    if (SingletonMap.o_eXx.o_eGs) {
                        SingletonMap.o_eXx.o_et3 = true;
                        SingletonMap.o_eXx.o_OYc(true);
                        SingletonMap.o_l.o_Ut1('o_l2e', true);
                        await xSleep(300);
                    }
                }
                else { // 没有镖车
                    if (SingletonMap.o_eXx.o_OP7 <= 0) { // 没次数了
                        return;
                    }
                    closeGameWin('EscortGiftWin'); // 押镖奖励窗口
                    await self.gotoChanYueDao();
                    SingletonMap.o_n.o_EG("EscortWin");
                    let npc = SingletonMap.o_OFM.o_Nx1.find(M => M.roleName == '苍月镖头');
                    SingletonMap.o_OKA.o_Pc(npc, true);
                    let times = 0;
                    while (self.targetLevel > SingletonMap.o_eXx.index && times < 4) {
						// 押镖令升级
						if (self.getEscortCount() > 0) {
							SingletonMap.o_NpG.o_ekQ();
							times++;
							await xSleep(500);
						}
						else if (SingletonMap.RoleData.o_NM3(SingletonMap.o_O.o_jv.FreshMoneyType) >= SingletonMap.o_O.o_jv.FreshMoneyValue 
							&& getAppConfigKeyValue("EnableDiamondEscort") == "TRUE") {// 钻石升级
							SingletonMap.o_NpG.o_ekQ();
							times++;
							await xSleep(500);
						}
						else {
							break;
						}
                        
                    }
                    await xSleep(1000);
					try {
						if(SingletonMap.o_n.isOpen("EscortWin") && self.buySafe == true && SingletonMap.RoleData.diamond >= 50) {
							SingletonMap.o_eXx.o_NJp = true;
							SingletonMap.o_n.isOpen("EscortWin").escortView.o_lMi(); //保险运镖
						}
						else {
							SingletonMap.o_NpG.o_Npm(0); // 普通运镖							
						}
					}
					catch(ex){
						console.error(ex);
					}
                    await xSleep(1000);
                }
                return check();
            });
        }
        return check();
    }

    getEscortCount() {
        var M = SingletonMap.o_O.o_jv.FreshNeedItem;
        return M > 100 ? SingletonMap.o_Ue.o_Ngt(M) : SingletonMap.RoleData.o_NM3(M);
    }
}
// 神威狱
class DeityPrisonTask extends StaticGuaJiTask {
    static name = "DeityPrisonTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
                result = true;
            }
        }
        catch (ex) {
            console.warn(ex);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours == 10 && curMinutes < 30 && !DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_10_00']) {
                result = true;
            }
            else if (curHours == 15 && curMinutes >= 30 && !DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_15_30']) {
                result = true;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isOnlyOnlyVitality() {
        let result = false;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.DeityPrisonOnlyVitality == 'TRUE') {
                    result = true;
                }
                else {
                    result = false;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (ex) {
            console.error(ex);
        }
        if (curHours == 10 && curMinutes < 30) {
            DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_10_00'] = true;
        }
        else if (curHours == 15 && curMinutes >= 30) {
            DeityPrisonTaskTime[curMonth + '_' + curDateNum + '_15_30'] = true;
        }
        await this.stop();
    }


    async waitTaskFinish() {
        let self = this;
        function check() {
            return xSleep(1500).then(async function () {
                if (!self.isRunning()) return;
                if (!mainRobotIsRunning()) return;
				if (!self.isActivityTime()) return;
                if (SingletonMap.MapData.curMapName == '王城') {
                    SingletonMap.o_lyZ.o_eVD(1);
                    return check();
                }
				if (self.isOnlyOnlyVitality()) {
					return;
				}
                try {
                    let arrBossCfg = SingletonMap.o_NP8.o_ecq(SingletonMap.o_lyZ.layer);
                    let boss = SingletonMap.o_NP8.bossList.find(M => M.state != 0);
                    if (boss) { // 有boss未击杀
                        let targetMonster = self.existsMonsterById(boss.bossId);
                        if (targetMonster) { // boss在身边
                            setCurMonster(targetMonster);
                            SingletonMap.o_OKA.o_Pc(targetMonster, true);
                        }
                        else {
                            if (arrBossCfg) {
                                let bossCfg = arrBossCfg.boss.find(M => M.BossID == boss.bossId);
                                // 寻路找boss
                                await self.findWayByPos(bossCfg.BossBornPoint.x, bossCfg.BossBornPoint.y, 'findWay', SingletonMap.MapData.curMapName, boss.bossId);
                            }
                        }
                    }
                    else {

                        // 已经是最后一层
                        if (SingletonMap.o_NP8.isMax) {
                            return;
                        }
                        let nextLayer = SingletonMap.o_lyZ.layer + 1;
                        let nextCfg = SingletonMap.o_NP8.o_ecq(nextLayer);
                        let b = nextCfg.expend[0] ? nextCfg.expend[0] : 0;
                        let r = nextCfg.expend[1] ? nextCfg.expend[1] : 0;
                        let itemId = SingletonMap.o_Ue.o_lbU(b, r);
                        // 神威令牌不足
                        if (itemId > 0 && !SingletonMap.o_NP8.o_esz()) {
                            SingletonMap.o_lAu.start();
                            return check();
                        }
                        // 前往下一层
                        let npcCfg = SingletonMap.o_Oos.o_NIi(arrBossCfg.comp);
                        await self.findWayByPos(npcCfg.pos.x, npcCfg.pos.y, 'findWay', SingletonMap.MapData.curMapName);
                        await xSleep(500);
                        SingletonMap.o_lyZ.o_eVD(nextLayer);
                    }
                }
                catch (ex) {
                    console.error(ex);
                }
                return check();
            });
        }
        return check();
    }
}

// 跨服皇陵
class CrossHuangLingTask extends StaticGuaJiTask {
    static name = "CrossHuangLingTask";

    needRun() {
        if (this.isRunning()) return false;
		if (!mainRobotIsRunning()) return false;
        if (SingletonMap.RoleData.o_f4 < 20) return false;
        let result = false;
        try {
            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_N2G)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            if (!isFreeTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getArrBossCfg() {
        let result = [];
        try {
            let bossCfg = SingletonMap.BossProvider.o_Nw8();
            for (var i = 0; i < bossCfg.length; i++) {
                result = result.concat(bossCfg[i].boss);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	isAllowLayer(bossCfg){
		let result = false;
		try {
			if(!bossCfg) return false;
			if (bossCfg.enterCond && bossCfg.enterCond[0]) { 
				var E = SingletonMap.o_eB9.o_UwX(bossCfg.enterCond);
				result = E[0];
			}
			
			if (result && bossCfg.cost) {
				var r = SingletonMap.RoleData.o_f4;
				var g = bossCfg.cost.count;
				result = r >= g ? true : false;
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}

    getNextBoss() {
        let result = null;
        if (SingletonMap.RoleData.o_f4 < 20) return null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.CrossHuangLing) {
                    let arrBossCfg = this.getArrBossCfg();
                    let arrBossId = appConfig.CrossHuangLing.split('|');
                    for (let i = 0; i < arrBossId.length; i++) {
                        let bossId = parseInt(arrBossId[i]);
                        let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
						if (bossCfg && this.isAllowLayer(bossCfg) && SingletonMap.o_eB9.o_N2G[bossCfg.layer]) {
							let bossTime = SingletonMap.o_eB9.o_N2G[bossCfg.layer][bossCfg.index];
							if (bossTime == 0 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) { // 先找已经刷新的
								result = bossCfg;
								break;
							}
						}
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        for (let i = 0; i < arrBossId.length; i++) {
                            let bossId = parseInt(arrBossId[i]);
                            let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
							if (bossCfg && this.isAllowLayer(bossCfg) && SingletonMap.o_eB9.o_N2G[bossCfg.layer]) {
								let bossTime = SingletonMap.o_eB9.o_N2G[bossCfg.layer][bossCfg.index];
								if (bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) {// 找30秒内刷新的
									if (mintimeBoss == null || bossTime < mintimeBoss.time) {
										mintimeBoss = bossCfg;
									}
								}
							}
                        }
                        result = mintimeBoss;
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async refrehBossTime() {
		try {
			for (let i = 1; i <= 8; i++) {
				SingletonMap.o_wB.o_Gq(i);
				await xSleep(100);
			}
		}
		catch (e) {
			console.error(e);
		}
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();         
            await this.gotoWangChen();
			await this.refrehBossTime();
            await xSleep(300);
            let nextBoss = this.getNextBoss();
            if (nextBoss) {
                this.curNextBoss = nextBoss;
                await this.waitTaskFinish(this.curNextBoss);
                QuickPickUpNormal.runPickUp();
                await xSleep(1500);
            }

        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (SingletonMap.RoleData.o_f4 < 20) { // 皇陵体力少于20
                        return true;
                    }

                    if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                        // loading
                        return check();
                    }
                    if (SingletonMap.MapData.curMapName.indexOf('领地') == -1) {
                        SingletonMap.o_wB.o_NGb(nextBoss.layer, nextBoss.index);
                        return check();
                    }
					
					let bossTime = SingletonMap.o_eB9.o_N2G[nextBoss.layer][nextBoss.index];
                    if (bossTime - SingletonMap.ServerTimeManager.o_eYF > 30000) { // 已经击杀
                        return;
                    }
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 7) {
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }

					let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = nextBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
					if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
						// 先打归属者
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer) {
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							return check();
						}
					}
					
                    let badPlayer = BlacklistMonitors.getBadPlayer();
                    if (badPlayer != null && GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length <= 7) {// 先打黑名单
                        setCurMonster(badPlayer);
                        SingletonMap.o_OKA.o_Pc(badPlayer, true);
                        return check();
                    }
					clearCurMonster();
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 7) {
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }
                    
					monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = nextBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                    if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (monster && self.existsMonster(monster) && !isNullBelong(monster) && !isSelfBelong(monster)) {
                        if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                            return check();
                        }
						else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
							// 先打归属者
							let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
							if (belongingBadPlayer) {
								setCurMonster(belongingBadPlayer);
								SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							}
							return check();
						}
                        else {
							await xSleep(2000);
							let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId) || self.findMonter();
							if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster))) {
								setCurMonster(tmpMonster);
								SingletonMap.o_OKA.o_Pc(tmpMonster, true);
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName)) {						
								return check();
							}
							else if (tmpMonster && GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length <= 6 && !BlacklistMonitors.isBadPlayer(tmpMonster.belongingName)) {
								CrossPublicBossRefreshTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
								console.warn(`boss归属${monster.belongingName},离开不打...`);
								return;
							}
                        }
                    }

                    // 刷新时间小于30秒，等待刷新
                    if (bossTime >= 0 || bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
                        return check();
                    }
					else {
						return;
					}
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
		if (!this.isRunning()) return false;
        QuickPickUpNormal.runPickUp();
        await xSleep(500);
        await super.stop();
    }
}

// 上古遗址
class ShanGuYiZhiTask extends StaticGuaJiTask {
    static name = "ShanGuYiZhiTask";
	arrBossRefreshTime = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (SingletonMap.RoleData.o_O4Q < 10) {
				return false;
			}
            if (this.isActivityTime()) {
                if ($.isEmptyObject(this.arrBossRefreshTime)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        return true;
    }

    getArrBossCfg() {
        let result = [];
        try {
            let bossCfg = SingletonMap.BossProvider.o_aQ();
            for (var i = 0; i < bossCfg.length; i++) {
                result = result.concat(bossCfg[i].boss);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async refrehBossTime() {
		try {
			if (!SingletonMap.ServerCross.o_ePe) {
				try {
					for (let i = 1; i <= 3; i++) {
						SingletonMap.Boss2Sys.o_yJ(i);
						await xSleep(100);
					}
				}
				catch (e) {
					console.error(e);
				}
			}
		}
		catch (e) {
            console.error(e);
        }
	}

    getArrBossRefreshTime() {
        let result = [];
        try {
            let bossCfg = SingletonMap.o_eB9.o_l6z;
            for (var i = 1; i <= 3; i++) {
                result = result.concat(bossCfg[i]);
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getMaxLayer() {
		let result = 2;
		try {
			result = SingletonMap.o_NP8.o_lYE >= 50 ? 3 : 2;
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    getNextBoss() {
        let result = null;
        if (SingletonMap.RoleData.o_O4Q < 10) return null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShanGuYiZhi) {
                    let arrBossCfg = this.getArrBossCfg();
                    let arrBossId = appConfig.ShanGuYiZhi.split('|');
                    this.arrBossRefreshTime = this.getArrBossRefreshTime();
					let maxLayer = this.getMaxLayer();
                    for (let i = 0; i < arrBossId.length; i++) {
                        let bossId = parseInt(arrBossId[i]);
                        let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                        let bossTimeCfg = this.arrBossRefreshTime.find(M => M.layer == bossCfg.layer && M.layer <= maxLayer && M.index == bossCfg.index);
						if (!bossTimeCfg) continue;
                        let bossTime = bossTimeCfg.time;
                        if (bossTime == 0 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) { // 先找已经刷新的
                            result = bossCfg;
                            break;
                        }
                    }

                    if (result == null) {
                        let mintimeBoss = null;
                        for (let i = 0; i < arrBossId.length; i++) {
                            let bossId = parseInt(arrBossId[i]);
                            let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
                            let bossTimeCfg = this.arrBossRefreshTime.find(M => M.layer == bossCfg.layer && M.index == bossCfg.index);
							if (!bossTimeCfg) continue;
                            let bossTime = bossTimeCfg.time;
                            if (bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000 && (!CrossPublicBossRefreshTime[bossId] || CrossPublicBossRefreshTime[bossId] <= SingletonMap.ServerTimeManager.o_eYF)) {// 找30秒内刷新的
                                if (mintimeBoss == null || bossTime < mintimeBoss.time) {
                                    mintimeBoss = bossCfg;
                                }
                            }
                        }
                        result = mintimeBoss;
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            if (SingletonMap.MapData.curMapName.indexOf('上古遗迹') == -1) {
                await this.gotoWangChen();
                await xSleep(300);
            }
			await this.refrehBossTime();
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
                this.curNextBoss = nextBoss;
                await this.waitTaskFinish(this.curNextBoss);
                QuickPickUpNormal.runPickUp();
				// await this.refrehBossTime();
                nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
        }
        catch (e) {
            console.error(e);
        }
        QuickPickUpNormal.runPickUp();
        await xSleep(1000);
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
                    if (SingletonMap.RoleData.o_O4Q < 10) { // 体力少于10
                        return true;
                    }

                    if (SingletonMap.MapData.curMapName.indexOf('上古遗迹') == -1) {
                        SingletonMap.Boss2Sys.o_hL(nextBoss.layer);
						await xSleep(500);
                        return check();
                    }

                    if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                        // loading
                        return check();
                    }                    

                    let badPlayer = BlacklistMonitors.getBadPlayer();
                    if (badPlayer != null) {// 先打黑名单
                        setCurMonster(badPlayer);
                        SingletonMap.o_OKA.o_Pc(badPlayer, true);
                        return check();
                    }

                    if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 5) {
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }

                    let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = nextBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                    if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster))) {
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
                    if (monster && self.existsMonster(monster) && !isNullBelong(monster) && !isSelfBelong(monster)) {
                        if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                            setCurMonster(monster);
                            SingletonMap.o_OKA.o_Pc(monster, true);
                            return check();
                        }
                        else {
                            CrossPublicBossRefreshTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                            return;
                        }
                    }
					
					
					self.arrBossRefreshTime = self.getArrBossRefreshTime();
                    let bossTimeCfg = self.arrBossRefreshTime.find(M => M.layer == nextBoss.layer && M.index == nextBoss.index);
                    let bossTime = bossTimeCfg.time;
					if (bossTime > 0 && bossTime - SingletonMap.ServerTimeManager.o_eYF < 30000) {  // 等待刷新
                        return check();
                    }
					else if (bossTime - SingletonMap.ServerTimeManager.o_eYF > 30000) { // 已经击杀
                        return;
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
        QuickPickUpNormal.runPickUp();
        await xSleep(500);
        await super.stop();
    }
}


let moveSleep = 10;
let arrImportGoodName = ['黄金锁子甲碎片','佛陀舍利','81难证明','水星天劫','金星天劫','日轮天劫','火星天劫','木星天劫','天岩草','天青花','混沌玄髓','太阴神髓','太乙星精','霸业', '切割碎片', '『史诗', '碎片', '龙装', '钻石', '宝石', '天工神石', '进阶丹'];
function getImportantGoods() {
    let resultGoods = null;
	try {
		let appConfig = getAppConfig();
		let pickupShenGeJinHua = true;
		let pickupZhuZhaiShenZhuang = 6;
		let pickupHuenGu = 5;
		let pickupHuenHuang = 5;

		if (appConfig != "") {
			appConfig = JSON.parse(appConfig);
			//if (appConfig.PickupShenGeJinHua == 'TRUE') {// 神格精华
			//	pickupShenGeJinHua = true;
			//}
			//if (appConfig.pickupZhuZhaiShenZhuang != "") { // 主宰
			//	pickupZhuZhaiShenZhuang = Number(appConfig.pickupZhuZhaiShenZhuang);
			//}
			//if (appConfig.PickupHuenGu != "") { // 魂骨
			//	pickupHuenGu = Number(appConfig.PickupHuenGu);
			//}
			//if (appConfig.PickupHuenHuang != "") {// 魂环
			//	pickupHuenHuang = Number(appConfig.PickupHuenHuang);
			//}
		}

		if (SingletonMap.o_lpS.o_Onf.length > 0) {
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 77 && Q.stdItem.showQuality >= 7); //主宰装备
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 76 && Q.stdItem.subtype % 10 == 1 && Q.stdItem.showQuality >= 7); //魂环
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 76 && Q.stdItem.subtype % 10 == 1 && Q.stdItem.showQuality >= 6); //魂环
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 76 && Q.stdItem.subtype % 10 != 1 && Q.stdItem.showQuality >= 6); //魂骨
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 76 && Q.stdItem.subtype % 10 != 1 && Q.stdItem.showQuality >= 5); //魂骨
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 77 && Q.stdItem.showQuality >= 6); //主宰装备
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.name.indexOf('赋神石') > -1); //赋神石
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.type == 77 && Q.stdItem.showQuality >= 5); //主宰装备
			}
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.name.indexOf('神格') > -1); //神格
			}
			if (!resultGoods) {
				for (let index = 0; i < arrImportGoodName.length; i++) {
					let cfgName = arrImportGoodName[i];
					resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && Q.stdItem.name.indexOf(cfgName) > -1);
					if (resultGoods) break;
				}
			}
			
			if (!resultGoods) {
				resultGoods = SingletonMap.o_lpS.o_Onf.find(Q => Q && Q.stdItem && isFastPickUpItem(Q.stdItem.name));
			}
		}
	}
	catch(ex){}
    return resultGoods;
}

function getNextGoods() {
    let resultGoods = null;
	try {
		var Q = SingletonMap.o_Ue.o_lpC();
		var L = SingletonMap.SettingMgr.o__j();
		if (SingletonMap.o_lpS.o_Onf.length > 0) {
			let minDistance = null;
			for (var i = 0; i < SingletonMap.o_lpS.o_Onf.length; i++) {
				let goods = SingletonMap.o_lpS.o_Onf[i];
				if (!SingletonMap.o_lLF.o_Nap(goods, L, Q, true)) continue; // 查归属
				//if (minDistance == null && !arrMovePickupHashCode[goods.$hashCode]) {
				if (minDistance == null) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3);
					resultGoods = goods;
				}
				// else if (!arrMovePickupHashCode[goods.$hashCode] && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3)) {
				else if (minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3)) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3);
					resultGoods = goods;
				}
			}
		}
	}catch(ex){}
    return resultGoods;
}

function getNoBelongNextGoods() {
    let resultGoods = null;
	try {
		var Q = SingletonMap.o_Ue.o_lpC();
		var L = SingletonMap.SettingMgr.o__j();
		if (SingletonMap.o_lpS.o_Onf.length > 0) {
			let minDistance = null;
			for (var i = 0; i < SingletonMap.o_lpS.o_Onf.length; i++) {
				let goods = SingletonMap.o_lpS.o_Onf[i];
				//if (minDistance == null && !arrMovePickupHashCode[goods.$hashCode]) {
				if (minDistance == null) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3);
					resultGoods = goods;
				}
				// else if (!arrMovePickupHashCode[goods.$hashCode] && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3)) {
				else if (minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3)) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, goods.currentX, goods.o_NI3);
					resultGoods = goods;
				}
			}
		}
	}
	catch(ex){}
    return resultGoods;
}

async function pickupGoods(goods) {
	window['isPickupGoods'] = true;
	if (!goods) return;
	SingletonMap.o_lLF.o_Oj9 = SingletonMap.o_e.currTimer;
	SingletonMap.o_lLF.o_eoc = false;
	SingletonMap.o_lLF.o_O8Q = goods;
	SingletonMap.o_lLF.o_ejY = SingletonMap.o_e.currTimer + 3e3;
	if (goods) {
		PlayerActor.o_NLN.o_eyi = appInstanceMap.o_l3Q.o_epx;
		SingletonMap.o_l.o_Ut1(appInstanceMap.o_H.o_e90, goods.currentX, goods.o_NI3, new appInstanceMap.o_eQy(appInstanceMap.o_etK.o_N0E, goods));
	}
	function check(){
		return xSleep(10).then(async function () {
			SingletonMap.o_lAu.o_WD();
			PlayerActor.o_NLN.o_eyi = appInstanceMap.o_l3Q.o_epx;
			let result = SingletonMap.o_lpS.o_Onf.find(M => M.$hashCode == goods.$hashCode);
			if (result) {
				SingletonMap.o_l.o_Ut1(appInstanceMap.o_H.o_e90, goods.currentX, goods.o_NI3, new appInstanceMap.o_eQy(appInstanceMap.o_etK.o_N0E, goods));
				return check();
			}
			window['isPickupGoods'] = false;
			return;
		});
	}
	return check();
}

var arrMovePickupHashCode = {};
var lasto_lcD = false;
var enableMovePickup = false;
async function startMovePickup(flag) {
    try {
		if (!isSuperPri()) return;
		//if (enableMovePickup && flag) return;
        if (flag) {
            enableMovePickup = true;
        }
        if (!enableMovePickup) return;
		
        var importantGoods = getImportantGoods();
        if (importantGoods) {
            SingletonMap.o_lAu.stop();
			if (PlayerActor.o_NLN.currentX != importantGoods.currentX || PlayerActor.o_NLN.o_NI3 != importantGoods.o_NI3) {
				await moveToPos(importantGoods.currentX, importantGoods.o_NI3);
			}
            if (PlayerActor.o_NLN.currentX == importantGoods.currentX && PlayerActor.o_NLN.o_NI3 == importantGoods.o_NI3) {
                // await xSleep(moveSleep);
                SingletonMap.o_lLF.o_NMj(importantGoods);
            }
        }
        else {
            var curGoods = getNextGoods();
            //if (curGoods && !arrMovePickupHashCode[curGoods.$hashCode]) {
            if (curGoods) {
                SingletonMap.o_lAu.stop();
				
				if (PlayerActor.o_NLN.currentX != curGoods.currentX || PlayerActor.o_NLN.o_NI3 != curGoods.o_NI3) {
					await moveToPos(curGoods.currentX, curGoods.o_NI3);
				}
				
                //await checkMovePos(item.currentX, item.o_NI3);
                //await xSleep(50);
                if (PlayerActor.o_NLN.currentX == curGoods.currentX && PlayerActor.o_NLN.o_NI3 == curGoods.o_NI3) {
                    // await xSleep(moveSleep);
                    SingletonMap.o_lLF.o_NMj(curGoods);
                    //arrMovePickupHashCode[curGoods.$hashCode] = true;
                }
            }
        }
    }
    catch (e) {
    }
    await xSleep(moveSleep);
    startMovePickup();
}

function stopMovePickup() {
    enableMovePickup = false;
	window['isPickupGoods'] = false;
    arrMovePickupHashCode = {};
}

async function moveToPos(x, y) {
	function check() {
		return xSleep(20).then(async function () {
			if (!enableMovePickup) return;
            SingletonMap.o_lAu.stop();
			if (PlayerActor.o_NLN.currentX == x && PlayerActor.o_NLN.o_NI3 == y) {
				return;
			}
			else {
				var b = SingletonMap.o_etq.o_egw(x, y, PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, true);
				if (b && b.length > 0) {
					SingletonMap.o_NZS.o_NiD(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b[0].dir);
                    PlayerActor.o_NLN.moveTo(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b[0].dir);
					SingletonMap.o_etq.o_lIl(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3);
					await xSleep(moveSleep);
				}
			}
		});
	}
	return check();
}

// 神龙秘境
class CrossShenLongMiJingTask extends StaticGuaJiTask {
    static name = 'CrossShenLongMiJingTask';
    state = false;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
				let arrBossRefreshTime = this.getArrBossRefreshTime();
				if ($.isEmptyObject(arrBossRefreshTime)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
                else if (this.getBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
        }
        return result;
    }

    getBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.ShenLongMiJinBoss) {
                    let arrShenLongMiJingBoss = appConfig.ShenLongMiJinBoss.split("|");
					let arrBossRefreshTime = this.getArrBossRefreshTime();
                    for (let i = 0; i < arrShenLongMiJingBoss.length; i++) {
                        let bossId = parseInt(arrShenLongMiJingBoss[i]);
                        let bossCfg = this.getBossCfg(bossId);
						if (bossCfg) {
							let bossTime = arrBossRefreshTime[bossCfg.index];
							if ((bossTime == 0 || bossTime - SingletonMap.ServerTimeManager.o_eYF < 30000) && (!bossNextWatchTime[bossId] || bossNextWatchTime[bossId] - SingletonMap.ServerTimeManager.o_eYF < 30000)) {
								boss = bossCfg;
								break;
							}
						}
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    isRunning() {
        return this.state == true;
    }

    async backToMap() {
        try {
            await this.enterMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
			SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
			await xSleep(300);
			if (SingletonMap.MapData.curMapName == '苍月岛') {
				SingletonMap.o_wB.o_CI();
			}
            await xSleep(300);
        } catch (ex) {
            console.error(ex);
        }
    }
	
	getArrBossCfg() {
		let result = [];
		try {
			result = SingletonMap.BossProvider.o_C5();
		}
		catch(ex) {
		}
		return result;
	}

    getBossCfg(bossId) {
		let arrBossCfg = this.getArrBossCfg();
        return arrBossCfg.find(M => M.bossId == bossId);
    }
	
	getArrBossRefreshTime() {
		let result = {};
		try {
			result = SingletonMap.o_eB9.o_lfO;
		}
		catch(ex) {
		}
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
				if (SingletonMap.MapData.curMapName != '苍月岛') {
					await this.gotoChanYueDao();
				}
				await this.enterMap();
				await this.loopStartFlow();
            }
        } catch (ex) {
            console.error(ex);
        }
		await this.stop();
    }

    async loopStartFlow() {
        try {
            let curBoss = this.getBoss();
            while (curBoss != null) {
                let bossCfg = this.getBossCfg(curBoss.bossId);
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                if (bossCfg) {
                    await this.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    if (!mainRobotIsRunning() || !this.isRunning()) {
                        break;
                    }
                    await this.killBoss(curBoss.bossId);
                }
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                curBoss = this.getBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    async killBoss(bossId) {
        let self = this;
		FirstKnife.setTargetCfgId(bossId);
        function check() {
            return xSleep(20).then(async function () {
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }

                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
				
				if (SingletonMap.MapData.curMapName == '苍月岛' || SingletonMap.MapData.curMapName == '神龙山庄') {
					await self.backToMap();
					return check();
				}
				
				try {
                    let curBoss = self.getBoss();
                    if (!curBoss) {
                        return;
                    }
                    if (GlobalUtil.compterFindWayPos(curBoss.bossPos[0], curBoss.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(curBoss.bossPos[0], curBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
                }
                catch (ex) {
                    console.error(ex);
                    return;
                }
				
                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }

                let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                if (!monster) {
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
                    return;
                }
				
				// 没有归属，发起攻击
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    // console.warn('killBoss...2');
                    setCurMonster(monster);
                    SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }

                if (monster && !isNullBelong(monster) && !isSelfBelong(monster)) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        // console.warn('killBoss...1.');
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
						return check();
                    }
                    else { // 没开黑名单
                        bossNextWatchTime[bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
                return check();
            });
        }
        return check();
    }
	
	async stop() {
		await super.stop();		
	}
}

// 星空炼狱
class CrossHellBossTask extends StaticGuaJiTask {
    static name = 'CrossHellBossTask';
    state = false;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
				let arrBossRefreshTime = this.getArrBossRefreshTime();
				if ($.isEmptyObject(arrBossRefreshTime)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
                else if (this.getBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
        }
        return result;
    }

    getBoss() {
        let boss = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.XinKongLianYuBoss && SingletonMap.o_e09.count > 0) {
                    let arrBoss = appConfig.XinKongLianYuBoss.split("|");
					let arrBossRefreshTime = this.getArrBossRefreshTime();
                    for (let i = 0; i < arrBoss.length; i++) {
                        let bossId = parseInt(arrBoss[i]);
                        let bossCfg = this.getBossCfg(bossId);
						if (bossCfg) {
							let bossTimeCfg = arrBossRefreshTime[bossCfg.layer];
							if (bossTimeCfg.time == 0 && (!bossNextWatchTime[bossId] || bossNextWatchTime[bossId] - SingletonMap.ServerTimeManager.o_eYF < 30000)) {
								boss = bossCfg;
								break;
							}
						}
                    }
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }

    isRunning() {
        return this.state == true;
    }

    async backToMap() {
        try {
            await this.enterMap();
        } catch (ex) {
            console.error(ex);
        }
    }

    async enterMap() {
        try {
			SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
			let curBoss = this.getBoss();
			if (curBoss) {
				SingletonMap.Boss2Sys.o_Od3(curBoss.layer);
			}
            await xSleep(1000);
        } catch (ex) {
            console.error(ex);
        }
    }
	
    async exitMap() {
        await xSleep(500);
    }
	
	getArrBossCfg() {
		let result = [];
		try {
			result = SingletonMap.o_e09.cfg;
		}
		catch(ex) {
		}
		return result;
	}

    getBossCfg(bossId) {
		let arrBossCfg = this.getArrBossCfg();
        return arrBossCfg.find(M => M.bossId == bossId);
    }
	
	getArrBossRefreshTime() {
		let result = {};
		try {
			result = SingletonMap.o_e09.o_euB;
		}
		catch(ex) {
		}
		return result;
	}

    async run() {
        try {
            this.startUtils();
            BlacklistMonitors.start();
            this.loopCancleNoBelongMonster();
            await this.loopStartFlow();
        } catch (ex) {
            console.error(ex);
        }
    }

	autoByYuanBaoTimes() {
		try {
			if (getAppConfigKeyValue("XinKongYuanBaoTimes") != "TRUE") return;
			let curTimes = SingletonMap.o_e09.o_xS[0] || 0;
			let cfgTimes = 1;
			if (cfgTimes > curTimes && SingletonMap.RoleData.o_NM3(3) >= 500000) {
				SingletonMap.Boss2Sys.o_ODf(1);
			}
		}
		catch (ex) {
            console.error(ex);
        }
	}

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            if (curHours >= 10 && curHours < 23) {
				if (SingletonMap.MapData.curMapName != '苍月岛') {
					await this.gotoChanYueDao();
				}
				SingletonMap.o_n.o_EG("CrossHellBossWin");
				this.autoByYuanBaoTimes();
				await xSleep(500);
                await this.run();
                //console.warn(`开始执行任务${this.constructor.name}  uid=>${this.uid}`);
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    async loopStartFlow() {
        try {
			closeGameWin("CrossHellBossWin");
            let curBoss = this.getBoss();
            while (curBoss != null) {
				if (SingletonMap.MapData.curMapName != '苍月岛') {
					SingletonMap.o_eaX.o_lKj(0, TeleportPos.KfCityArea);
				}
				QuickPickUpNormal.runPickUp();
                let bossCfg = this.getBossCfg(curBoss.bossId);
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
                if (bossCfg) {
                    // await this.findWayByPos(bossCfg.bossPos[0], bossCfg.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    if (!mainRobotIsRunning() || !this.isRunning()) {
                        break;
                    }
                    await this.killBoss(curBoss);
                }
                if (!mainRobotIsRunning() || !this.isRunning()) {
                    break;
                }
				QuickPickUpNormal.runPickUp();
                await xSleep(1000);
                curBoss = this.getBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async killBoss(pCurBoss) {
        let self = this;
		let pickup = false;
        function check() {
            return xSleep(20).then(async function () {
				QuickPickUpNormal.runPickUp();
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (!self.isRunning()) {
                    return;
                }
				let nextGoods = getNextGoods();
				if (allowPickUp(nextGoods) && pickup == false) {
					QuickPickUpNormal.runPickUp();
					pickup = true;
					await xSleep(1000);
					return check();
				}
				if (SingletonMap.o_e09.count <= 0) {
					return;
				}

                // 人物死亡
                if (AliveCheckLock.isLock()) {
					self.findWayIsStop = true;
					self.attackIsStop = true;
					clearCurMonster();
                    return check();
                }
				
				if (SingletonMap.MapData.curMapName == '苍月岛') {
					await xSleep(1000);
					SingletonMap.Boss2Sys.o_Od3(pCurBoss.layer);
					await xSleep(1000);
				}
				
				try {
                    if (GlobalUtil.compterFindWayPos(pCurBoss.bossPos[0], pCurBoss.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(pCurBoss.bossPos[0], pCurBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
                }
                catch (ex) {
                    console.error(ex);
                    return;
                }
				
                let badPlayer = BlacklistMonitors.getBadPlayer();
                if (badPlayer != null) {// 开启了黑名单
                    setCurMonster(badPlayer);
					self.gotoTarget(badPlayer);
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    return check();
                }

                let monster = self.findMonter() || SingletonMap.o_OFM.o_Nx1.find(M => M.cfgId = pCurBoss.bossId && M instanceof Monster && !M.masterName && M.curHP > 100);
                if (!monster) {
                    return;
                }

                if (monster && !isNullBelong(monster) && !isSelfBelong(monster)) { // 归属不是自己
                    if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
                        // console.warn('killBoss...1.');
                        setCurMonster(monster);
                        SingletonMap.o_OKA.o_Pc(monster, true);
                        return check();
                    }
					else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
						return check();
                    }
					else { // 没开黑名单
                        bossNextWatchTime[pCurBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
                        return;
                    }
                }
                // 没有归属，发起攻击
                if (monster && self.filterMonster(monster) && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
                    // console.warn('killBoss...2');
                    setCurMonster(monster);
                    //self.attackMonster(monster);
					SingletonMap.o_OKA.o_Pc(monster, true);
                    return check();
                }
                return check();
            });
        }
        return check();
    }
	
	async stop() {
		await super.stop();		
	}
}

// 宠物boss
class PetBossTask extends StaticGuaJiTask {
    static name = "PetBossTask";
	arrBossRefreshTime = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			this.arrBossRefreshTime = this.getArrBossRefreshTime();
            if (this.isActivityTime()) {
                if ($.isEmptyObject(this.arrBossRefreshTime)) {
                    result = true;
                }
                else if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        return true;
    }

    getArrBossCfg() {
		let result = [];
		try {
			let arr = SingletonMap.BossProvider.o_e73();
			for (let i = 0;i < arr.length; i++) {
				result = result.concat(arr[i].boss);
			}
		}
		catch(e) {}
        return result;
    }

    getArrBossRefreshTime() {
        let result = [];
        try {
			for (var key in SingletonMap.o_eB9.o_NxE) {
				result = result.concat(SingletonMap.o_eB9.o_NxE[key]);
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnablePetBoss == 'TRUE') {
					let arrBossRefreshTime = this.getArrBossRefreshTime();
					let arrBossCfg = this.getArrBossCfg();
					result = arrBossRefreshTime.find(M => M.time == 0 && M.o_2a.ndCir <= SingletonMap.RoleData.o_Oja 
						&& arrBossCfg.find(C => C.bossId == M.o_2a.bossId && $.isEmptyObject(C.cost)));  // 只打免费的
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            if (SingletonMap.MapData.curMapName != '王城' && SingletonMap.MapData.curMapFilePath != 'ChongWuBoss') {
                await this.gotoWangChen();
                await xSleep(300);
            }
            let nextBoss = this.getNextBoss();
            if (nextBoss) {
				QuickPickUpNormal.runPickUp();
                await this.waitTaskFinish(nextBoss);
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (!nextBoss) return;
					if (!self.getNextBoss() || self.getNextBoss().o_2a.layer != nextBoss.o_2a.layer) return;
					if (SingletonMap.MapData.curMapFilePath != 'ChongWuBoss') {
						SingletonMap.o_wB.o_ek4(nextBoss.o_2a.layer, nextBoss.o_2a.index);
						await xSleep(100);
						return check();
					}
					if (GlobalUtil.compterFindWayPos(nextBoss.o_2a.bossPos[0], nextBoss.o_2a.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(nextBoss.o_2a.bossPos[0], nextBoss.o_2a.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
					let targetMonster = self.existsMonsterById(nextBoss.o_2a.bossId);
					if (targetMonster) { // boss在身边
						setCurMonster(targetMonster);
						SingletonMap.o_OKA.o_Pc(targetMonster, true);
						return check();
					}
					else if (SingletonMap.o_n.isOpen("BossVipResultWin")){
						return;						
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
		closeGameWin("BossVipResultWin");
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// vip boss
class VipBossTask extends StaticGuaJiTask {
    static name = "VipBossTask";
	arrBossRefreshTime = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
                if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        return true;
    }

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableVipBoss == 'TRUE' && appConfig.VipBoss != "") {
					let arrConfig = appConfig.VipBoss.split("|");
					for (let i = 0; i < arrConfig.length; i++) {
						let idx = parseInt(arrConfig[i]);
						let arrLayerBoss = SingletonMap.o_eB9.o_O4_[idx];
						if (arrLayerBoss) {
							let curBoss = arrLayerBoss.find(M => M.time == 0 && M.o_2a.ndVip <= SingletonMap.RoleData.o_NY5() && M.o_2a.cost.count <= totalBagItemByName('VIP卷轴'));
							if (curBoss) {
								result = curBoss;
								break;
							}
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            if (SingletonMap.MapData.curMapName != '王城' && SingletonMap.MapData.curMapFilePath != 'vipBOSS') {
                await this.gotoWangChen();
                await xSleep(300);
            }
            let nextBoss = this.getNextBoss();
            if (nextBoss) {
				QuickPickUpNormal.runPickUp();
                await this.waitTaskFinish(nextBoss);
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}	
					if (!nextBoss) return;
					
					if (SingletonMap.MapData.curMapFilePath != 'vipBOSS') {
						SingletonMap.o_wB.o_DO(nextBoss.o_2a.layer, nextBoss.o_2a.index);
						await xSleep(100);
						return check();
					}
					if (GlobalUtil.compterFindWayPos(nextBoss.o_2a.bossPos[0], nextBoss.o_2a.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(nextBoss.o_2a.bossPos[0], nextBoss.o_2a.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                    }
					let targetMonster = self.existsMonsterById(nextBoss.o_2a.bossId);
					if (targetMonster) { // boss在身边
						setCurMonster(targetMonster);
						SingletonMap.o_OKA.o_Pc(targetMonster, true);
						return check();
					}
					else if (SingletonMap.o_n.isOpen("BossVipResultWin")){
						return;						
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
		if (!this.isRunning()) return false;
		closeGameWin("BossVipResultWin");
        await super.stop();
    }
}

// 八卦 boss
class BaGuaBossTask extends StaticGuaJiTask {
    static name = "BaGuaBossTask";
	arrBossRefreshTime = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
                if (this.getNextBoss()) {
                    result = true;
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        return true;
    }

    getNextBoss() {
        let result = null;
        try {
			if (SingletonMap.RoleData.o_lYC < 10) return;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableBaGuaBoss == 'TRUE') {
					let arrBoss = [];
					let layerStr = SingletonMap.BossProvider.o_NEp();
					let arrBossCfg = SingletonMap.BossProvider.o_Njm();
					for(var key in arrBossCfg) {
						arrBoss = arrBoss.concat(arrBossCfg[key]);
					}
					for (var i = 0; i < arrBoss.length; i++) {
						let boss = arrBoss[i];
						if (SingletonMap.RoleData.o_Oja >= boss.ndCir && layerStr.indexOf(boss.ndCir) > -1 
							&& SingletonMap.o_eB9.o_2N[boss.index].time == 0 
							&& (!bossNextWatchTime[boss.bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[boss.bossId])) {
							result = boss;
							break;
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				QuickPickUpNormal.runPickUp();
                await this.waitTaskFinish(nextBoss);
				nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.curMapFilePath != 'mijingBOSS') {
						SingletonMap.o_wB.o_er4(nextBoss.index);
						await xSleep(100);
						return check();
					}
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
					let targetMonster = self.existsMonsterById(nextBoss.bossId);
					if (targetMonster && (isNullBelong(targetMonster) || isSelfBelong(targetMonster))) {
						setCurMonster(targetMonster);
						SingletonMap.o_OKA.o_Pc(targetMonster, true);
						return check();
					}
					else {
						QuickPickUpNormal.runPickUp();
						await xSleep(1000);
						bossNextWatchTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; 
						if (SingletonMap.MapData.curMapFilePath == 'mijingBOSS') {
							gameObjectInstMap.ZjmTaskConView.o_lmT();
						}
						return;			
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
		closeGameWin("BossVipResultWin");
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 神魔祖地
class ShenMoZhuDiBossTask extends StaticGuaJiTask {
    static name = "ShenMoZhuDiBossTask";
	type = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (SingletonMap.RoleData.godEvilCount <= 0) return false;
            if (this.isActivityTime()) {
				if ($.isEmptyObject(SingletonMap.o_yy_.o_yvF)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && (curHours == 23 && curMinutes >= 30 || curHours == 0 && curMinutes <= 30)) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	
	refreshBossTime() {
		try {
			try {
				[1, 2].forEach(async (layer) => {
					SingletonMap.o_yyA.o_yvg(layer, 1);
					await xSleep(100);
					SingletonMap.o_yyA.o_yvg(layer, 2);
					await xSleep(100);
					SingletonMap.o_yyA.o_yvg(layer, 3);
					await xSleep(100);
				});
			}
			catch (e) {
				console.error(e);
			}
		}		
		catch (ex) {
            console.error(ex);
        }
	}
	
	getType() {
		return this.type != null ? this.type : SingletonMap.RoleData.o_efG;	
	}

    getNextBoss() {
        let result = null;
        try {
			if (SingletonMap.RoleData.godEvilCount <= 0) return;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableShenMoZhuDiBoss == 'TRUE') {
					let type = this.getType();
					let arrBossTime = SingletonMap.o_yy_.o_yvF[type];
					
					let arrBossLayerCfg = appConfig.ShenMoZhuDiBoss.split('|');
					for(let i = 0; i < arrBossLayerCfg.length; i++) {
						let layerIndex = arrBossLayerCfg[i];
						if (arrBossTime && arrBossTime[layerIndex]) {
							let boss = arrBossTime[layerIndex].find(M => M.time == 0);
							if (boss && (!bossNextWatchTime[`ShenMoZhuDi_${layerIndex}_${boss.idx}`] || bossNextWatchTime[`ShenMoZhuDi_${layerIndex}_${boss.idx}`] <= SingletonMap.ServerTimeManager.o_eYF)) {
								result = boss;
								result.type = type;
								result.layer = parseInt(layerIndex);
								break;
							}
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            if (!['神族主城', '魔族主城', '王城'].includes(SingletonMap.MapData.curMapName)) {
                await this.gotoWangChen();
                await xSleep(1000);
            }
			if (!['神族主城', '魔族主城'].includes(SingletonMap.MapData.curMapName)) {
				await this.gotoShenMoZhuChen();
				await xSleep(1000);
			}
			if (['神族主城', '魔族主城'].includes(SingletonMap.MapData.curMapName)) {
				this.type = SingletonMap.RoleData.o_efG;
				if (SingletonMap.RoleData.o_efG == 1) {
					await this.findWayByPos(12, 21, 'findWay', SingletonMap.MapData.curMapName);
				}
				else {
					await this.findWayByPos(121, 56, 'findWay', SingletonMap.MapData.curMapName);
				}
			}
			closeGameWin("NewWorldActTipWin");
			this.refreshBossTime();
			await xSleep(1000);
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				QuickPickUpNormal.runPickUp();
                await this.waitTaskFinish(nextBoss);
				await this.refreshBossTime();
				nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	getEnterNpcName() {
		return SingletonMap.RoleData.o_efG == 1 ? '神族祖地' : '魔族祖地';
	}

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}

					if (SingletonMap.RoleData.godEvilCount <= 0) return;
					
					if (SingletonMap.MapData.curMapFilePath != 'gerenBOSS') {
						if (!SingletonMap.o_n.isOpen("NewWorldZuDiWin")) {
							if (SingletonMap.RoleData.o_efG == 1) {
								await self.findWayByPos(12, 21, 'findWay', SingletonMap.MapData.curMapName);
							}
							else {
								await self.findWayByPos(121, 56, 'findWay', SingletonMap.MapData.curMapName);
							}
							SingletonMap.o_OKA.o_Pc(SingletonMap.o_OFM.o_Nx1.find(M => M.roleName== self.getEnterNpcName()), true);
							//SingletonMap.o_n.o_EG("NewWorldZuDiWin");
							await xSleep(500);
							closeGameWin("NewWorldActTipWin");
						}
						else {
							const curMapName = SingletonMap.MapData.curMapName;
							SingletonMap.o_yyA.o_yYC(nextBoss.type, nextBoss.layer, nextBoss.idx);
							await self.changeScene(curMapName);
							await xSleep(500);
						}
						return check();
					}
					
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer != null) {
						setCurMonster(badPlayer);
						SingletonMap.o_OKA.o_Pc(badPlayer, true);
						return check();
					}
					
					if (GlobalUtil.compterFindWayPos(40, 25).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(40, 25, 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
					
					
					let bossMonster = GlobalUtil.getMonsterById(nextBoss.bossId);
					let targetMonster = bossMonster || self.findMonter();
					
					if (targetMonster && (isNullBelong(targetMonster) || isSelfBelong(targetMonster))) {
						setCurMonster(targetMonster);
						SingletonMap.o_OKA.o_Pc(targetMonster, true);
						return check();
					}
					
					if (targetMonster && !isNullBelong(targetMonster) && !isSelfBelong(targetMonster)) { 	
						if (BlacklistMonitors.isWhitePlayer(targetMonster.belongingName)) { // 归属是白名单
							setCurMonster(targetMonster);
							SingletonMap.o_OKA.o_Pc(targetMonster, true);
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(targetMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
							return check();
						}
						else {
							bossNextWatchTime[`ShenMoZhuDi_${nextBoss.layer}_${nextBoss.idx}`] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
							return;
						}
					}
					else {
						QuickPickUpNormal.runPickUp();
						await xSleep(1000);
                        bossNextWatchTime[`ShenMoZhuDi_${nextBoss.layer}_${nextBoss.idx}`] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
						if (SingletonMap.MapData.curMapFilePath == 'gerenBOSS') {
							 SingletonMap.o_yyA.o_yUH(1);
						}
						return;			
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	findMonter() {
        let monster = null;
        try {
            // 归属怪优先
            if (curMonster && isSelfBelong(curMonster)) {
                let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (belongingMonster) {
                    monster = belongingMonster;
                }
            }
            // 还没有归属
            if (curMonster && isNullBelong(curMonster)) {
                let noBelongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (noBelongingMonster) {
                    monster = noBelongingMonster;
                }
            }
			
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof Monster && !M.masterName && M.curHP > 100 && this.filterMonster(M)) {
                        // 没有归属或归属自己或归属白名单
                        if (isNullBelong(M) || isSelfBelong(M) || BlacklistMonitors.isWhitePlayer(M.belongingName)) {
                            if (targetMonster == null) {
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                            else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                                // 距离近的优先
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                        }
                    }
                }

                if (targetMonster) {
                    monster = targetMonster;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        return monster;
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }

    async stop() {
		try {
			if (SingletonMap.MapData.curMapFilePath == 'gerenBOSS') {
				SingletonMap.o_yyA.o_yUH(1);
			}
        }
        catch (e) {
            console.error(e);
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 神魔深渊
class ShenMoShenYuanBossTask extends StaticGuaJiTask {
    static name = "ShenMoShenYuanBossTask";
	taskBossTime = null;
	
    needRun() {
		let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
				if ($.isEmptyObject(SingletonMap.o_yy_.o_yvZ)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && (curHours == 23 && curMinutes >= 30 || curHours == 0 && curMinutes <= 30)) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	getLayer() {
		return SingletonMap.o_NP8.o_lYE >= 100 ? 1 : 0;
	}

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableShenMoShenYuanBoss == 'TRUE') {
					let curLayer = this.getLayer();
					let arrBossCfg = SingletonMap.o_O.o_yvW[curLayer];
					let arrBossTime = SingletonMap.o_yy_.o_yvZ[curLayer + 1];
					let arrBossId = appConfig.ShenMoShenYuanBoss.split('|');
					for(let i = 0; i < arrBossId.length; i++) {
						let bossId = parseInt(arrBossId[i]) + curLayer * 8; //2层比1层大8
						let bossCfg = arrBossCfg.data.find(M => M.bossId == bossId);
						if (arrBossTime) {
							let bossTime = arrBossTime[bossCfg.id];
							if ((bossTime == 0 || bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) && (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId])) {
								result = bossCfg;
								result.layer = curLayer;
								break;
							}
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	isWaitingTime(bossId) {
		let result = false;
		try {
			let curLayer = this.getLayer();
			let arrBossCfg = SingletonMap.o_O.o_yvW[curLayer];
			let arrBossTime = SingletonMap.o_yy_.o_yvZ[curLayer + 1];
			let bossCfg = arrBossCfg.data.find(M => M.bossId == bossId);
			let bossTime = arrBossTime[bossCfg.id];
			if (bossTime == 0 || bossTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
				result = true;
			}
		}
		catch(exx) {
			console.log(exx);
		}
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            //if (!['神族主城', '魔族主城', '王城'].includes(SingletonMap.MapData.curMapName)) {
            //    await this.gotoWangChen();
            //    await xSleep(1000);
            //}
			if (!['神族主城', '魔族主城'].includes(SingletonMap.MapData.curMapName)) {
				await this.gotoShenMoZhuChen();
				await xSleep(1000);
			}
			closeGameWin("NewWorldActTipWin");
			if (['神族主城', '魔族主城'].includes(SingletonMap.MapData.curMapName)) {
				if (SingletonMap.RoleData.o_efG == 1) {
					await this.findWayByPos(10, 19, 'findWay', SingletonMap.MapData.curMapName);
				}
				else {
					await this.findWayByPos(117, 51, 'findWay', SingletonMap.MapData.curMapName);
				}
			}
			if (!SingletonMap.o_n.isOpen("NewWorldAbyssWin")) {
				SingletonMap.o_n.o_EG("NewWorldAbyssWin");
				await xSleep(500);
				var Q = SingletonMap.o_O.o_yvW[this.getLayer()];
				SingletonMap.Boss2Sys.o_yv5(Q.index);
				closeGameWin("NewWorldAbyssWin");
			}
			
			await xSleep(1000);
            let nextBoss = this.getNextBoss();
            if (nextBoss) {
				QuickPickUpNormal.runPickUp();
                await this.waitTaskFinish(nextBoss);
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1000);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop2();
    }

    async waitTaskFinish(pNextBoss) {
        let self = this;
        function check() {
            try {
                return xSleep(100).then(async function () {
					QuickPickUpNormal.runPickUp();
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!self.isActivityTime()) {
                        return true;
                    }
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (!pNextBoss) {
						QuickPickUpNormal.runPickUp();
						await xSleep(1000);
						return;
					}
					if (['神族主城', '魔族主城'].includes(SingletonMap.MapData.curMapName)) {
						if (!SingletonMap.o_n.isOpen("NewWorldAbyssWin")) {
							if (SingletonMap.RoleData.o_efG == 1) {
								await self.findWayByPos(10, 19, 'findWay', SingletonMap.MapData.curMapName);
							}
							else {
								await self.findWayByPos(117, 51, 'findWay', SingletonMap.MapData.curMapName);
							}
							SingletonMap.o_n.o_EG("NewWorldAbyssWin");
							await xSleep(500);
						}
						else {
							const curMapName = SingletonMap.MapData.curMapName;
							closeGameWin("NewWorldActTipWin");
							var Q = SingletonMap.o_O.o_yvW[self.getLayer()];
							SingletonMap.Boss2Sys.o_yv5(Q.index);
							closeGameWin("NewWorldAbyssWin");
							await self.changeScene(curMapName);
							await xSleep(500);
						}
						return check();
					}
					
					let nextGoods = getNextGoods();
                    if (allowPickUp(nextGoods)) {
						QuickPickUpNormal.runPickUp();
                        return check();
                    }
					
					if (GlobalUtil.compterFindWayPos(pNextBoss.bossPos[0], pNextBoss.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(pNextBoss.bossPos[0], pNextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
					
					BlacklistMonitors.badPlayer = BlacklistMonitors.getBadPlayer();
					if (BlacklistMonitors.badPlayer != null) {
						setCurMonster(BlacklistMonitors.badPlayer);
						SingletonMap.o_OKA.o_Pc(BlacklistMonitors.badPlayer, true);
						return check();
					}
					
					if (GlobalUtil.compterFindWayPos(pNextBoss.bossPos[0], pNextBoss.bossPos[1]).length >= 5) {
                        // 离boss超过3步,返回
                        BlacklistMonitors.stop();
                        clearCurMonster();
                        await self.findWayByPos(pNextBoss.bossPos[0], pNextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
                        return check();
                    }
					
					let bossMonster = GlobalUtil.getMonsterById(pNextBoss.bossId);
					let targetMonster = bossMonster || self.findMonter();
					
					if (targetMonster && self.filterMonster(targetMonster) && (isNullBelong(targetMonster) || isSelfBelong(targetMonster))) {
						setCurMonster(targetMonster);
						SingletonMap.o_OKA.o_Pc(targetMonster, true);
						return check();
					}
					
					if (targetMonster && GlobalUtil.compterFindWayPos(pNextBoss.bossPos[0], pNextBoss.bossPos[1]).length <= 5
						&& self.existsMonster(targetMonster) && !isNullBelong(targetMonster) && !isSelfBelong(targetMonster)) { // 归属不是自己
						if (BlacklistMonitors.isWhitePlayer(targetMonster.belongingName)) { // 归属是白名单
							setCurMonster(targetMonster);
							SingletonMap.o_OKA.o_Pc(targetMonster, true);
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(targetMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
							return check();
						}
						else {
							bossNextWatchTime[pNextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 300000; // 是别人归属的boss,5分钟内不打
							return;
						}
					}
					else {
						if (self.isWaitingTime(pNextBoss.bossId)) {
							return check();
						}
						QuickPickUpNormal.runPickUp();
						await xSleep(1000);
						SingletonMap.Boss2Sys.o_yYA();
						if (['神魔深渊一层', '神魔深渊二层'].includes(SingletonMap.MapData.curMapName)) {
							SingletonMap.o_yyA.o_yUH();
							return;
						}
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	findMonter() {
        let monster = null;
        try {
            // 归属怪优先
            if (curMonster && isSelfBelong(curMonster)) {
                let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (belongingMonster) {
                    monster = belongingMonster;
                }
            }
            // 还没有归属
            if (curMonster && isNullBelong(curMonster)) {
                let noBelongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.$hashCode == curMonster.$hashCode);
                if (noBelongingMonster) {
                    monster = noBelongingMonster;
                }
            }
			
            let len = SingletonMap.o_OFM.o_Nx1.length;
            if (monster == null) {
                let targetMonster = null;
                let minDistance = null;
                for (let i = len - 1; i >= 0; i--) {
                    const M = SingletonMap.o_OFM.o_Nx1[i];
                    if (M instanceof Monster && !M.masterName && M.curHP > 100 && this.filterMonster(M)) {
                        // 没有归属或归属自己或归属白名单
                        if (isNullBelong(M) || isSelfBelong(M) || BlacklistMonitors.isWhitePlayer(monster.belongingName)) {
                            if (targetMonster == null) {
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                            else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                                // 距离近的优先
                                targetMonster = M;
                                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                continue;
                            }
                        }
                    }
                }

                if (targetMonster) {
                    monster = targetMonster;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        return monster;
    }

    async backToMap() {
        await xSleep(100);
    }

    async enterMap() {
        await xSleep(100);
    }
	
	async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }

    async stop2() {
		if (!this.isRunning()) return false;
		if (['神魔深渊一层', '神魔深渊二层'].includes(SingletonMap.MapData.curMapName)) {
			SingletonMap.o_yyA.o_yUH();
		}
        await super.stop();
    }
}

// 神器Boss
class ShenQiBossTask extends StaticGuaJiTask {
    static name = "ShenQiBossTask";
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.hasTimes()) {
				if ($.isEmptyObject(SingletonMap.o_eB9.o_Nuj)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	hasTimes() {
		let result = false;
		try {
			if (getAppConfigKeyValue("KillAllShenQiBoss") == "TRUE") {
				return true;
			}
			let M = SingletonMap.o_lVF.o_SW.find(M => M.name == '神器魔王');
			let count = SingletonMap.o_NL6.getCountById(M.relationIndex, M.smallIndex);
			if (M.canAwardCount - count > 0) {
				result = true;
			}
		}
		 catch (e) {
            console.error(e);
        }
        return result;
	}
	
	async refrehBossTime() {
		try {
			let arr = SingletonMap.BossProvider.o_O3W();
			for (var i = 0;i < arr.length; i++) {
				if (!SingletonMap.ServerCross.o_ePe) {
					SingletonMap.o_wB.o_lEj(arr[i].id);
					await xSleep(200);
				}
			}
		}
		 catch (e) {
			console.error(e);
        }
	}
	
	getArrBossCfg() {
		let result = []; 
		try {
			let arrBossCfg = SingletonMap.BossProvider.o_O3W();
			for(let i = 0; i < arrBossCfg.length; i++) {
				let arrLayerBoss = arrBossCfg[i].Layer;
				for (let j = 0; j < arrLayerBoss.length; j++) {
					
					result = result.concat(arrLayerBoss[j]);
					if (arrLayerBoss[j].other) {
						result = result.concat(arrLayerBoss[j].other);
					}
				}
			}
		}
		 catch (e) {
        }
		return result;
	}

    getNextBoss() {
		if (!this.hasTimes()) return null;
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableShenQiBoss == 'TRUE') {
					let resultCfg = null;
					let arrBoss = [];
					let layerCfg = '|' + appConfig.ShenQiBoss + '|';
					let mapBossTimeCfg = SingletonMap.o_eB9.o_Nuj;
					for(let layerKey in mapBossTimeCfg) {
						let strKey = '|' + layerKey + '|';
						if (layerCfg.indexOf(strKey) > -1) {
							let layerBossTimeCfg = mapBossTimeCfg[layerKey];
							for (let bossIdx in layerBossTimeCfg) {
								if (layerBossTimeCfg[bossIdx] == 1 && (!bossNextWatchTime[bossIdx] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossIdx])) {
									resultCfg = {
										layer: layerKey,
										index: bossIdx
									};
									break;
								}								
							}
						}
						if (resultCfg != null) {
							break;
						}
					}
					if (resultCfg != null) {
						let arrBossCfg = this.getArrBossCfg();
						result = arrBossCfg.find(M => M.id == resultCfg.layer && M.index == resultCfg.index && SingletonMap.o_lb7.o_eWl(0, M.mapId)[0]);
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoWangChen();
			await xSleep(300);
			await this.refrehBossTime();
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (!this.hasTimes()) break;
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					break;
				}
				clearCurMonster();
				BlacklistMonitors.stop();
				SingletonMap.o_lAu.stop();
				
				QuickPickUpNormal.runPickUp();
				SingletonMap.o_wB.o_tE(nextBoss.id, nextBoss.index);
				await xSleep(300);
                await this.waitTaskFinish(nextBoss);
				QuickPickUpNormal.runPickUp();
				await xSleep(1200);
				await this.refrehBossTime();
				nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1200);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return;
					}
					SingletonMap.o_lAu.stop();
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					if (!monster && GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 10) {
						await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName, nextBoss.bossId);
						return check();
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							return check();
						}
					}
					if (!monster) {
						monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					}
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
							await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName, nextBoss.bossId);
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					
					if (monster && GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length <= 6) { // 归属不是自己
						// console.warn('killBoss...1.');
						if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else if (BlacklistMonitors.getBadPlayer()) {
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
							return check();
						}
						else {
							await xSleep(1200);
							let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
							if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster))) {
								setCurMonster(tmpMonster);
								SingletonMap.o_OKA.o_Pc(tmpMonster, true);
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
								return check();
							}
							else {
								bossNextWatchTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 5;
								return;
							}
						}
					}
					return;
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }

    async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 本服皇陵
class HuangLingTask extends StaticGuaJiTask {
    static name = "HuangLingTask";
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.hasTimes()) {
				if ($.isEmptyObject(SingletonMap.o_eB9.o_OKb)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	hasTimes() {
		let result = false;
		try {
			let M = SingletonMap.o_lVF.o_SW.find(M => M.name == '皇陵体力');
			let count = SingletonMap.o_NL6.getCountById(M.relationIndex, M.smallIndex);
			if (M.canAwardCount - count > 0 && SingletonMap.RoleData.o_f4 >= 12) {
				result = true;
			}
		}
		 catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getArrBossCfg() {
		let result = []; 
		try {
			result = SingletonMap.BossProvider.o_loR(SingletonMap.o_eB9.o_O6I);
		}
		catch (e) {
        }
		return result;
	}
	
	refrehBossTime() {
		try {
			let arrBossCfg = SingletonMap.BossProvider.o_loR(SingletonMap.o_eB9.o_O6I);
			for (let i = 0; i < arrBossCfg.length; i++) {
				let bossCfg = arrBossCfg[i];
				SingletonMap.o_wB.o_gq(1, bossCfg.layer);
			}
		}
		catch(e){}
	}
	
	getArrBossRefreshTime() {
		let result = [];
		try {
			for(let key in SingletonMap.o_eB9.o_OKb) {
				let layerBossRefreshTime = SingletonMap.o_eB9.o_OKb[key];
				for (let idx in layerBossRefreshTime) {
					result = result.concat(layerBossRefreshTime[idx]);
				}
			}
		}
		catch(e) {
		}
		return result;
	}
	
	isAllowLayer(layer) {
		let result = false;
		try {
			let arrBossCfg = SingletonMap.BossProvider.o_loR(SingletonMap.o_eB9.o_O6I);
			let layerCfg = arrBossCfg.find(M => M.layer == layer);
			var Q = SingletonMap.RoleData.o_Oja;
			var E = layerCfg.ndCir || 0;
			if (Q > E) {
				var A;
				var b = SingletonMap.o_eB9.o_O6I;
				if (b == 2 || b == 3) {
					if (b == 2) { 
						A = 1e4 * SingletonMap.o_OrB.o_ODF + SingletonMap.o_OrB.o_NZu; 
					}
					else if (b == 3) {
						A = 1e4 * SingletonMap.o_OV0.o_OYF + SingletonMap.o_OV0.o_RQ;
					}
					result = A >= 1e4 * layerCfg.ndLv + layerCfg.ndStar ? true: false;
				} else if (b == 4 || b == 5 || b == 6) {
					A = SingletonMap.o_CA.lvList[b - 4 + 1];
					result = A >= layerCfg.ndLv ? true: false;
				} else if (b == 1) {
					A = SingletonMap.RoleDat.realmLv;
					result = A >= layerCfg.ndLv ? true: false;
				}
			}
		}
		catch(e) {
		}
		return result;
	}

    getNextBoss() {
        let result = null;
        try {
			if (!this.hasTimes()) return null;
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableHuangLing == 'TRUE') {
					let resultCfg = null;
					let arrLayer = appConfig.HuangLing.split('|');
					let arrBossCfg = this.getArrBossCfg();
					let arrBossRefreshTime = this.getArrBossRefreshTime();
					for (let i = 0;i < arrLayer.length; i++) {
						let layer = parseInt(arrLayer[i]);
						if (this.isAllowLayer(layer)) {
							let bossTime = arrBossRefreshTime.find(M => M.layer == layer && M.time == 0);
							if (bossTime) {
								let arrBoss = arrBossCfg.find(M => M.layer == bossTime.layer);
								result = arrBoss.boss.find(M => M.index == bossTime.index && M.cost.count <= SingletonMap.RoleData.o_f4);
								if (result) {
									result.layer = layer;
									break;
								}
							}
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoWangChen();
			this.refrehBossTime();
			await xSleep(300);
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (!this.hasTimes()) break;
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					break;
				}
				QuickPickUpNormal.runPickUp();
				SingletonMap.o_wB.o_M3(nextBoss.layer);
				await xSleep(300);
                await this.waitTaskFinish(nextBoss);
				QuickPickUpNormal.runPickUp();
				await xSleep(1200);
				nextBoss = this.getNextBoss();
            }
            QuickPickUpNormal.runPickUp();
            await xSleep(1200);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(300).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.hasTimes()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return;
					}
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 6) {
						SingletonMap.o_OKA.o_NSw(nextBoss.bossPos[0], nextBoss.bossPos[1]);
						return check();
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 5) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 5) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					return;
				
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }

    async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 巅峰竞技
class WarZoneTask extends StaticGuaJiTask {
    static name = "WarZoneTask";
	inited = false;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (SingletonMap.o_eDM.isOpen()) {
				// 已报名并且报名已结束
				if (SingletonMap.ServerTimeManager.o_eYF >= SingletonMap.o_eDM.o_N8n() && this.isActivityTime())  {
					if (this.inited == false) {
						this.inited = true;
						TaskStateManager.updateTaskEnterState(this.constructor.name);
						result = true;
					}
					else if(SingletonMap.o_eDM.o_IU) {
						TaskStateManager.updateTaskEnterState(this.constructor.name);
						result = true;
					}
					else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
						TaskStateManager.updateTaskEnterState(this.constructor.name);
						result = true;
					}
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let curHours = curDate.getHours();
			let curMinutes = curDate.getMinutes();
			if (curHours == 19 && curMinutes < 30) {
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoWangChen();
			SingletonMap.o_Yr.o_NOH();
			await xSleep(1300);
			if (SingletonMap.o_eDM.o_IU) {
				await this.waitTaskFinish();
			}
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(300).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) {
						return;
					}
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					SingletonMap.o_lAu.stop();
					BlacklistMonitors.stop();
					if (SingletonMap.MapData.curMapName == '王城') {
						SingletonMap.o_eDM.o_Ojo(true);
						window["LoginSysresetLogin"]();
						await xSleep(500);
						return check();
					}
					
					if (SingletonMap.MapData.curMapName == '巅峰主城') {
						SingletonMap.o_eqY.o_luS();
						await xSleep(500);
						return check();
					}
					
					if (SingletonMap.MapData.curMapName == '巅峰皇城') {
						SingletonMap.o_OKA.o_NSw(48, 42);
						return check();
					}
					
					if (SingletonMap.MapData.curMapName == '巅峰皇宫') {
						// 靠近皇座
						if (GlobalUtil.compterFindWayPos(23,19).length >= 8) {
							clearCurMonster();
							await self.findWayByPos(23, 19, 'findWay', SingletonMap.MapData.curMapName);
						}
						let badPlayer = self.findBadPlayer();
						if (badPlayer) {
							if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 5) {
								setCurMonster(badPlayer);
								SingletonMap.o_OKA.o_Pc(badPlayer, true);
								return check();
							}
							else {
								clearCurMonster();
								SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
								return check();
							}
						}
					}
				}
				catch (e) {
					console.error(e);
				}
				return check();
			});
        }
        return check();
    }
	
	findBadPlayer() {
		let badPlayer = null;
		try {
			let arrBadPlayer = [];
			let len = SingletonMap.o_OFM.o_Nx1.length;
			for (let i = len - 1; i >= 0; i--) {
				const M = SingletonMap.o_OFM.o_Nx1[i];
				if (!SingletonMap.o_h7.o_lDb(M)) {
					continue;
				}
				if (M && M instanceof PlayerActor && !M.masterName && M.curHP > 0) {
					arrBadPlayer.push(M);
				}
			}

			// 距离近的优先
			let minDistance = null;
			for (let i = 0; i < arrBadPlayer.length; i++) {
				let M = arrBadPlayer[i];
				if (minDistance == null) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
					badPlayer = M;
				}
				else if (minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
					minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
					badPlayer = M;
				}
			}
		}
		catch (e) {
			console.error(e);
		}
		return badPlayer;
	}

    async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 苍月秘境
class CrossCangYueBossTask extends StaticGuaJiTask {
    static name = "CrossCangYueBossTask";
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime() && this.hasTimes()) {
				this.getAward();
				if ($.isEmptyObject(SingletonMap.o_eB9.o_eaW)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getAward() { // 领取任务奖励
		try {
			var M = [];
			var i = SingletonMap.o_eHJ.o_c7();
			var Q;
			for (var E = 0; E < i.length; E++) {
				Q = SingletonMap.QuestMgr.o_Nx0(i[E].id);
				if (!Q) Q = SingletonMap.o_eHJ.o_lUO(i[E].id);
				M.push({
					quest: Q,
					mc: i[E].mc,
					time: i[E].time
				})
			};
			for (var i = 0;i < M.length; i++) {
				if (M[i].quest.o_Nz_) {
					SingletonMap.o_lQH.o_lIV(M[i].quest.stdQuest.id);
				}
			}
		}
		catch(e){}
	}
	
	getTimes(type) {
		// 0:高级 1:超级
		var E = LangManager.bossStr140;
		var A = SingletonMap.o_eB9.o_N2T;
		var b = SingletonMap.BossProvider.o_9v();
		return Math.max(0, b["freeTime" + type] + b["costTime" + type] - (A[type + 1] || 0));
	}
	
	hasTimes() {
		let result = false;
		try {
			let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
				if (appConfig.KillAllCrossCangYueBoss == 'TRUE') {
					// 有怪就打
					return true;
				}
				result = this.getTimes(0) > 5 || this.getTimes(1) > 3;
			}
		}
		 catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getArrBossCfg() {
		let result = []; 
		try {
			result = SingletonMap.BossProvider.o_lIj();
		}
		catch (e) {
        }
		return result;
	}
	
	getBossRefreshTime() {
		let result = [];
		try {
			for(let key in SingletonMap.o_eB9.o_eaW) {
				result.push({
					index: key,
					time: SingletonMap.o_eB9.o_eaW[key]
				});
			}
		}
		catch(e) {
		}
		return result;
	}

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableCrossCangYueBoss == 'TRUE') {
					let arrBossRefreshTime = this.getBossRefreshTime();
					let arrBoss = appConfig.CrossCangYueBoss.split('|');
					for (let idx = 0; idx < arrBoss.length; idx++) {
						let arrBossCfg = this.getArrBossCfg();
						arrBossCfg = arrBossCfg.filter(M => M.bossId == parseInt(arrBoss[idx]));
						for (let i = 0;i < arrBossCfg.length; i++) {
							let bossCfg = arrBossCfg[i];
							let bossTime = arrBossRefreshTime.find(M => M.index == bossCfg.index && M.time == 0);
							if (bossTime) {
								result = bossCfg;
								break;
							}
						}
						if (result) {
							break;
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoChanYueDao();
			await xSleep(300);
			if (SingletonMap.MapData.curMapName == '苍月岛') {
				SingletonMap.o_wB.o_NG6();
				await xSleep(300);
				SingletonMap.o_lQH.o_lIV(8903);
				await xSleep(300);
				SingletonMap.o_lQH.o_lIV(8904);
				await xSleep(300);
			}
			await xSleep(500);
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (!this.hasTimes()) break;
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					break;
				}
				QuickPickUpNormal.runPickUp();
				await xSleep(300);
                await this.waitTaskFinish(nextBoss);
				QuickPickUpNormal.runPickUp();
				await xSleep(1200);
				nextBoss = this.getNextBoss();
            }
			SingletonMap.o_lQH.o_lIV(8903);
			await xSleep(300);
			SingletonMap.o_lQH.o_lIV(8904);
			await xSleep(300);
            QuickPickUpNormal.runPickUp();
            await xSleep(1200);
			QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	getBaoXian() {
		let result = null;
		try {
			if (this.getTimes(1) > 3) {
				result = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && isSelfBelong(M) && M.roleName.indexOf('超级') > -1);
				if (!result) {
					result = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.belongingName == "" && M.roleName.indexOf('超级') > -1);
				}
			}
			else if (this.getTimes(0) > 5) {
				result = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && isSelfBelong(M) && M.roleName.indexOf('高级') > -1);
				if (!result) {
					result = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.belongingName == "" && M.roleName.indexOf('高级') > -1);
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	async waitPickupBaoXian(baoXian) {
		if (!baoXian) return;
		let self = this;
		function check() {            
			return xSleep(500).then(async function () {
				if (!self.isRunning()) return;
				if (!mainRobotIsRunning()) return;				
				let result = SingletonMap.o_OFM.o_Nx1.find(M => M.$hashCode == baoXian.$hashCode && isSelfBelong(M));
				if (!result) {
					return;
				}
				return check();
			});
		}
		return check();
	}
	
	async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(300).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.hasTimes()) {
						return;
					}
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}

					if (SingletonMap.MapData.curMapName == '苍月岛') {
						SingletonMap.o_wB.o_NG6();
						return check();
					}
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 9) {
						await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
						return check();
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 5) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							return check();
						}
					}
					let baoXian = self.getBaoXian();
					if (baoXian)  {
						if (GlobalUtil.compterFindWayPos(baoXian.currentX, baoXian.o_NI3).length >= 5) {
							await self.findWayByPos(baoXian.currentX, baoXian.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
						}
						setCurMonster(baoXian);
						SingletonMap.o_OKA.o_Pc(baoXian, true);
						await xSleep(10000);
					}
					
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
							await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					else {
						await xSleep(1500);
						baoXian = self.getBaoXian();
						if (baoXian)  {
							if (GlobalUtil.compterFindWayPos(baoXian.currentX, baoXian.o_NI3).length >= 5) {
								await self.findWayByPos(baoXian.currentX, baoXian.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							}
							setCurMonster(baoXian);
							SingletonMap.o_OKA.o_Pc(baoXian, true);
							await xSleep(10000);
						}
						else {
							return;
						}
					}
				}
				catch (e) {
					console.error(e);
				}
				return check();
			});
        }
        return check();
    }
	
    async stop() {
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 金猪
class GoldPigTask extends StaticGuaJiTask {
    static name = "GoldPigTask";
	arrBossCfg = [
		{sceneId:"49", pos: "47,29",time: '', map: '1,1'},
		{sceneId:"50", pos:"47,57",time: '', map: '1,2'},
		{sceneId:"51", pos:"77,56",time: '', map: '1,3'},
		{sceneId:"52", pos:"77,56",time: '', map: '1,4'},
		{sceneId:"53", pos:"89,111",time: '', map: '2,1'},
		{sceneId:"54", pos:"110,94",time: '', map: '2,2'},
		{sceneId:"55", pos:"48,46",time: '', map: '2,3'},
		{sceneId:"147", pos:"78,63",time: '', map: '2,4'},
		{sceneId:"56", pos:"65,58",time: '', map: '3,1'},
		{sceneId:"57", pos:"105,88",time: '', map: '3,2'},
		{sceneId:"58", pos:"64,36",time: '', map: '3,3'},
		{sceneId:"148", pos:"121,63", time: '', map: '3,4'},
		{sceneId:"59", pos:"45,45",time: '', map: '4,1'},
		{sceneId:"60", pos:"53,45",time: '', map: '4,2'},
		{sceneId:"61", pos:"63,51",time: '', map: '4,3'},
		{sceneId:"149", pos:"94,74",time: '', map: '4,4'},
		{sceneId:"62", pos:"79,91",time: '', map: '5,1'},
		{sceneId:"63", pos:"94,193",time: '', map: '5,2'},
		{sceneId:"150", pos:"145,98",time: '', map: '5,3'},
		{sceneId:"64", pos:"106,136",time: '', map: '6,1'},
		{sceneId:"65", pos:"98,97",time: '', map: '6,2'},
		{sceneId:"151", pos:"89,139",time: '', map: '6,3'},
		{sceneId:"66", pos:"84,67",time: '', map: '7,1'},
		{sceneId:"67", pos:"78,23",time: '', map: '7,2'},
		{sceneId:"152", pos:"56,36",time: '', map: '7,3'},
		{sceneId:"68", pos:"85,70",time: '', map: '8,1'},
		{sceneId:"69", pos:"53,60",time: '', map: '8,2'},
		{sceneId:"153", pos:"102,118",time: '', map: '8,3'},
		{sceneId:"70", pos:"91,50",time: '', map: '9,1'},
		{sceneId:"71", pos:"25,72",time: '', map: '9,2'},
		{sceneId:"154", pos:"108,50",time: '', map: '9,3'},
		{sceneId:"72", pos:"130,110",time: '', map: '10,1'},
		{sceneId:"73", pos:"67,28", time: '', map: '11,1'}
	];
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.getNextBoss()) {
				TaskStateManager.updateTaskEnterState(this.constructor.name);
				result = true;
			}
			else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
				TaskStateManager.updateTaskEnterState(this.constructor.name);
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    getNextBoss() {
        let result = null;
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.GoldPigBoss) {
					let arrPigBossMapCfg = appConfig.GoldPigBoss.split("|");
					let tmpResult = null;
					for (let i = 0; i < arrPigBossMapCfg.length; i++) {
						let pigBossMapCfg = arrPigBossMapCfg[i];
						tmpResult = this.arrBossCfg.find(M => M.map == pigBossMapCfg && (!M.time || M.time <= SingletonMap.ServerTimeManager.o_eYF));
						if (tmpResult) {
							result = tmpResult;
							break;
						}
					}
                }
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoWangChen();
			await xSleep(300);
            let nextBoss = this.getNextBoss();
            while (nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (this.stopTask) break;
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					break;
				}
				clearCurMonster();
				BlacklistMonitors.stop();
				SingletonMap.o_lAu.stop();
                await this.waitTaskFinish(nextBoss);
				nextBoss.time = SingletonMap.ServerTimeManager.o_eYF + 1000 * 7200;
				nextBoss = this.getNextBoss();
				QuickPickUpNormal.runPickUp();
				await xSleep(800);
            }
            QuickPickUpNormal.runPickUp();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	findMonster() {
		// 归属怪
		if (curMonster && isSelfBelong(curMonster)) {
			let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 100 && M.cfgId >= 148001 && M.cfgId <= 148011 && M.$hashCode == curMonster.$hashCode);
			if (belongingMonster) {
				return belongingMonster;
			}
		}
		return SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.cfgId >= 148001 && M.cfgId <= 148011);	
	}

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (self.stopTask) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.sceneId != nextBoss.sceneId) {
						let arrMap = nextBoss.map.split(',');
						SingletonMap.o_wB.o_l3N(arrMap[0], arrMap[1]);
						return check();
					}
					LastPointWatcher.resetLastPointInfo();
					let arrBossPos = nextBoss.pos.split(',');
					if (GlobalUtil.compterFindWayPos(arrBossPos[0], arrBossPos[1]).length >= 7) {
						await self.findWayByPos(arrBossPos[0], arrBossPos[1], 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							return check();
						}
					}
					
					let	monster = self.findMonster();
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
							await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName, nextBoss.bossId);
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					
					if (monster && isNullBelong(monster) && !isSelfBelong(monster) && GlobalUtil.compterFindWayPos(arrBossPos[0], arrBossPos[1]).length <= 6) { // 归属不是自己
						// console.warn('killBoss...1.');
						if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else if (BlacklistMonitors.getBadPlayer()) {
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
							return check();
						}
						else {
							await xSleep(1200);
							let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
							if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster))) {
								setCurMonster(tmpMonster);
								SingletonMap.o_OKA.o_Pc(tmpMonster, true);
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
								return check();
							}
							else {
								nextBoss.time = SingletonMap.ServerTimeManager.o_eYF + 1000 * 7200;
								return;
							}
						}
					}
                }
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
}

// 神龙巢穴
class ShenLongChaoXueTask extends StaticGuaJiTask {
    static name = "ShenLongChaoXueTask";
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (SingletonMap.Ladder5v5Mgr.isTodayOpen5v5()) return false; // 修罗战场
			if (!this.isOverTime() && this.isActivityTime() && SingletonMap.o_eB9.o_Nye != 2) {
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 21 && curMinutes <= 30) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isOnlyOnlyVitality() {
        return getAppConfigKeyValue("ShenLongChaoXueOnlyVitality")  == 'TRUE';
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoChanYueDao();
			SingletonMap.o_wB.o_NHG(); // 获取魔气情况
			await xSleep(1000);
			if (SingletonMap.o_eB9.o_Ove >= SingletonMap.BossProvider.o_luu) { // 魔气充满了
				this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
				await this.waitTaskFinish();
			}
			this.updateOverTime();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	findMonster() {
		return SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName);	
	}

    async waitTaskFinish() {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.curMapName == '苍月岛') {
						SingletonMap.o_wB.o_NrM();
						return check();
					}
					if (self.isOnlyOnlyVitality()) {
						await xSleep(2000);
						return;
					}
					
					let importantGoods = getImportantGoods() || getNextGoods();
					if (importantGoods) {
						if (isSuperPri() && enableMovePickup == false) {
							SingletonMap.o_lAu.stop();
							startMovePickup(true);
						}
						else if (!SingletonMap.o_lAu.o_lcD){
							SingletonMap.o_lAu.start();
						}
						return check();
					}
					
					if (self.isOverTime()) return;
					if (SingletonMap.o_eB9.o_Nye == 2) return;
					
					if (GlobalUtil.compterFindWayPos(38, 36).length >= 8) {
						await self.findWayByPos(38, 36, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let monster = self.findMonster();
					if (monster)  LastPointWatcher.resetLastPointInfo();
					if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
						// 先打归属者
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer) {
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
						}
						return check();
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					// 寻怪
					monster = self.findMonster();
					if (monster) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 5) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
	
	async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }
}
// 摸金巢穴
class MoJinChaoXueTask extends StaticGuaJiTask {
    static name = "MoJinChaoXueTask";
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (!this.isOverTime() && this.isActivityTime() && SingletonMap.o_eB9.o_fq != 0) {
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let weekDay = curDate.getDay();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (weekDay == 0 && curHours == 21 && curMinutes <= 30) {// 周日
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	isOnlyOnlyVitality() {
        return getAppConfigKeyValue("MoJinChaoXueOnlyVitality")  == 'TRUE';
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoChanYueDao();
			SingletonMap.o_wB.o_tw(); // 获取魔气情况
			await xSleep(1000);
			if (SingletonMap.o_eB9.o_l99 >= SingletonMap.BossProvider.o_O28) {// 魔气充满了
				this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
				await this.waitTaskFinish();
			}
			this.updateOverTime();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	findMonster() {
		return SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName);
	}

    async waitTaskFinish() {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.curMapName == '苍月岛') {
						SingletonMap.o_wB.o_NrM();
						return check();
					}
					if (self.isOnlyOnlyVitality()) {
						await xSleep(2000);
						return;
					}
					
					let importantGoods = getImportantGoods() || getNextGoods();
					if (importantGoods) {
						if (isSuperPri() && enableMovePickup == false) {
							SingletonMap.o_lAu.stop();
							startMovePickup(true);
						}
						else if (!SingletonMap.o_lAu.o_lcD){
							SingletonMap.o_lAu.start();
						}
						return check();
					}
					
					if (self.isOverTime()) return;
					if (SingletonMap.o_eB9.o_fq == 0) return;
					
					if (GlobalUtil.compterFindWayPos(38, 36).length >= 8) {
						await self.findWayByPos(38, 36, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let monster = self.findMonster();
					if (monster)  LastPointWatcher.resetLastPointInfo();
					if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
						// 先打归属者
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer) {
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
						}
						return check();
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					// 寻怪
					monster = self.findMonster();
					if (monster) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 5) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
	
	async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 寻龙秘境
class FindDragonTask extends StaticGuaJiTask {
	static name = "FindDragonTask";
	
	needRun() {
		let result = false;
		try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if(this.isActivityTime()) {
				if ($.isEmptyObject(SingletonMap.o_eB9.o_yU_)) {
					result = true;
				}
				else if (this.getNextBoss()) {
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					result = true;
				}
				if (result) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
				}
			}
		}
		catch(e) {}
		return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			if (!isCrossTime()) return false;
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && (curHours == 23 && curMinutes >= 30 || curHours == 0 && curMinutes <= 30)) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
			// 每周1、4、7开启
			if(!(weekDay == 1 || weekDay == 4 || weekDay == 0)) {
				result = false;
			}
			else if (result && curHours == 10 && curMinutes >= 40)  {
				result = true;
			}
			else if (result && curHours == 21 && curMinutes >= 40) {
				result = true;
			}
			else {
				result = false;
			}
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoShenMoBianJin();
			SingletonMap.Boss2Sys.o_ye7();
			await xSleep(1000);
			this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
			let nextBoss = this.getNextBoss();
            while (nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (!this.isActivityTime()) break;
                await this.waitTaskFinish(nextBoss);
				nextBoss = this.getNextBoss();
            }
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	getNextBoss() {
		let result = null;
		try {
			let selBossCfg = getAppConfigKeyValue("FindDragonBoss");
			let arrSelBossCfg = selBossCfg.split('|');
			let bossCfg = SingletonMap.BossProvider.o_yl7();
			let arrBossId = bossCfg.bossID;
			let arrBossPos = bossCfg.bossPos;
			for (let i = 0;i < arrSelBossCfg.length; i++) {
				let selBossId = arrSelBossCfg[i];
				let bossIndex = arrBossId.findIndex(M => M == selBossId);
				if (SingletonMap.o_eB9.o_yU_[bossIndex]) {
					result = {
						bossId: selBossId,
						bossPos: arrBossPos[bossIndex]
					};
					break;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					SingletonMap.o_lAu.stop();
					if (SingletonMap.MapData.curMapName != '寻龙秘境') {
						SingletonMap.Boss2Sys.o_yeO();
						return check();
					}
					
					if (SingletonMap.o_lpS.o_Onf.length > 0) {
						if (isSuperPri() && enableMovePickup == false) {
							SingletonMap.o_lAu.stop();
							startMovePickup(true);
						}
						else if (enableMovePickup == false){
							SingletonMap.o_lAu.start();
						}
						return check();
					}
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos.x, nextBoss.bossPos.y).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos.x, nextBoss.bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos.x, nextBoss.bossPos.y).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos.x, nextBoss.bossPos.y, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					// 寻怪
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					if (monster) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 5) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
	
	async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 苍月神殿
class ChanYueShenDianTask extends StaticGuaJiTask {
	static name = "ChanYueShenDianTask";
	
	needRun() {
		let result = false;
		try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if(this.isActivityTime() && !this.isOverTime()) {
				if (SingletonMap.o_eB9.o_lC8) {
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					result = true;
				}
				
				if (result) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
				}
			}
		}
		catch(e) {}
		return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 20 && curMinutes >= 40) {
				result = true;
			}
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			if (getAppConfigKeyValue("ChanYueShenDianSweep") == "TRUE") {
				await this.gotoWangChen();
				SingletonMap.o_yFu.o_yB0(4, 0, 0); // 扫荡
			}
			else {
				await this.gotoChanYueDao();
				SingletonMap.o_wB.o_e6I();
				await awaitToMap('苍月神殿');
				this.originalEnableMovePickup = enableMovePickup; // 记录原始状态
				let nextBoss = SingletonMap.BossProvider.o_Tq();
				await this.waitTaskFinish(nextBoss);
			}
			this.updateOverTime();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					
					let importantGoods = getImportantGoods();
					if (importantGoods) {
						if (isSuperPri()) {
							SingletonMap.o_lAu.stop();
							startMovePickup(true);
						}
						else if (!SingletonMap.o_lAu.o_lcD){
							SingletonMap.o_lAu.start();
						}
						return check();
					}
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos.x, nextBoss.bossPos.y).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos.x, nextBoss.bossPos.y, 'findWay', SingletonMap.MapData.curMapName, nextBoss.bossID);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos.x, nextBoss.bossPos.y).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos.x, nextBoss.bossPos.y, 'findWay', SingletonMap.MapData.curMapName, nextBoss.bossID);
					}
					
					// 寻怪
					let monster = GlobalUtil.getMonsterById(nextBoss.bossID);
					if (monster) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 5) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
							return check();
						}
						else {
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
					}
					
					if (!SingletonMap.o_eB9.o_lC8) return; // 怪物已击杀
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
	
	async stop() {
        if (!this.originalEnableMovePickup) {
            stopMovePickup();
        }
		if (!this.isRunning()) return false;
        await super.stop();
    }
}

// 金榜题名
class ChatWorldAnswerTask extends StaticGuaJiTask {
	static name = "ChatWorldAnswerTask";
	needRun() {
		let result = false;
		try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			result = this.isActivityTime();
		}
		catch(e) {}
		return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curMonth = curDate.getMonth();
            let curDateNum = curDate.getDate();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours == 12 && curMinutes >= 4 && curMinutes < 15) {
                result = true;
            }
		}
		catch(e) {}
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }

    async waitTaskFinish() {
        let self = this;
		let answerMap = {};
        function check() {
            try {
                return xSleep(15).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					LastPointWatcher.resetLastPointInfo();
                    if (!self.isActivityTime()) {
                        return;
                    }
					if (SingletonMap.o_l_o.o_eFX > 0 && !answerMap[SingletonMap.o_l_o.o_eFX]) {
						let answer = SingletonMap.o_NCd.o_eCS(SingletonMap.o_l_o.o_e4u());
						SingletonMap.o_NCd.o_N_X(answer);
						answerMap[SingletonMap.o_l_o.o_eFX] = answer;
						await xSleep(1000);
					}
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 异兽吞噬
function AncientShenKunRecy() {
	try {
		let o_eOH = SingletonMap.o_Ue.o_UeH();
		var M = [];
		for (var i = 0, Q = o_eOH; i < Q.length; i++) {
			var E = Q[i];
			if (null != E && E.o_ES)
				M.push(E);
		}
		if (M && M.length > 0) {
		   SingletonMap.o_OPQ.o_UlF(M.length, M);
		}
		closeGameWin("AncientShenKunRecyResultView");
	}
	catch (e) {
		console.error(e);
	}
}
// 星空吞噬
function xingKongTunShi() {
	try {		
		let o_eOH = SingletonMap.o_Ue.o__U();

		var M = [];
		for (var i = 0,Q = o_eOH; i < Q.length; i++) {
			var E = Q[i];
			if (null != E && E.o_ES) M.push(E)
		}

		var i = [];
		var Q = [];
		for (var E = 0; E < M.length; E++)
			if (M[E].userItem.stdItem.type == appInstanceMap.StdItemType.o_Nat)
				i.push(M[E]);
			else
				Q.push(M[E]);
		if (i.length > 0)
			SingletonMap.o_OPQ.o_l9Y(i.length, i, o_Ks.o_ebD);
		if (Q && Q.length > 0)
			SingletonMap.o_OPQ.o_l9Y(Q.length, Q)
	}
	catch (e) {
		console.error(e);
	}
}

class DailyActivCalendar {
	static o_NiW = ["", "weekA", "weekB", "weekC", "weekD", "weekE", "weekF", "weekG"];
	static arrWeekDayFlag = {
		"1": "weekA",
		"2": "weekB",
		"3": "weekC",
		"4": "weekD",
		"5": "weekE",
		"6": "weekF",
		"0": "weekG"
	};
	static getCalendar(){
		let result = "";
		try {
			var M = new Date(SingletonMap.ServerTimeManager.o_eYF);
			var i = SingletonMap.o_O.o_eHL.concat();
			var Q = [];
			var E;
			var A = SingletonMap.ServerTimeManager.o_Nu4;
			var b = A > 0 ? A: SingletonMap.ServerTimeManager.o_yKv;
			var r = SingletonMap.o_lle.o_Il();
			var g = 7 + r - 1;
			for (var C = 0; C < i.length; C++) Q.push(i[C]);
			if (b <= g) {
				var L = r - b;
				var d = M.getDay();
				E = SingletonMap.o_lle.o_OkN();
				0 == d && (d = 7);
				if (b <= r) if (A > 0) {
					if (d - b >= 5) for (var C = 0; C < Q.length; C++) if (Q[C].timeStr == E) Q[C][DailyActivCalendar.o_NiW[6]] = ""
				} else if (d - b >= 3) for (var C = 0; C < Q.length; C++) if (Q[C].timeStr == E) Q[C][DailyActivCalendar.o_NiW[6]] = "";
				d += L;
				if (d >= 1 && d <= 7) for (var C = 0; C < Q.length; C++) if (Q[C].timeStr == E) Q[C][DailyActivCalendar.o_NiW[d]] = LangManager.sabac_str50
			}
			if (SingletonMap.o_lV5.o_OE1()) {
				E = "20:00";
				var U = SingletonMap.o_O.o_e14;
				var W = U.openServerDay - U.openTime.week + 1;
				if (SingletonMap.ServerTimeManager.myServerDay >= W) {
					var V = M.getDay();
					var N = U.openTime.week - V;
					var w = SingletonMap.ServerTimeManager.myServerDay + N;
					if (w >= U.openServerDay) for (var C = 0; C < Q.length; C++) if (Q[C].timeStr == E) Q[C][DailyActivCalendar.o_NiW[U.openTime.week]] = LangManager.sabac_str105
				}
			}
			if (SingletonMap.ServerTimeManager.myServerDay >= 150) for (var C = 0; C < Q.length; C++) {
				E = "21:00";
				if (Q[C].timeStr == E) for (var u = 1; u < 8; u++) Q[C][DailyActivCalendar.o_NiW[u]] = LangManager.o_yRH;
				else if ("20:00" == Q[C].timeStr && SingletonMap.o_yy_.o_yMq()) {
					var m = SingletonMap.o_O.o_yYT.openDayOfWeek || [1, 4, 7];
					for (var u = 0; u < m.length; u++) {
						var v = m[u];
						Q[C][DailyActivCalendar.o_NiW[v]] = LangManager.o_yRk
					}
				}
			}
			if (appInstanceMap.Systemlimit.o_WE(appInstanceMap.o_NCL.LADDER1V1)) for (var C = 0; C < Q.length; C++) if ("14:30" == Q[C].timeStr || "19:30" == Q[C].timeStr) {
				for (var u = 1; u < 7; u++) Q[C][DailyActivCalendar.o_NiW[u]] = LangManager.o_yrX;
				E = "19:30";
				if (Q[C].timeStr == E) Q[C][DailyActivCalendar.o_NiW[7]] = LangManager.o_yrb
			}
			if (SingletonMap.XiyouMgr.isOpen()) for (var C = 0; C < Q.length; C++) {
				E = "15:30";
				if (Q[C].timeStr == E) for (var u = 1; u <= 7; u++) Q[C][DailyActivCalendar.o_NiW[u]] = LangManager.o_yK9
			}
			if (appInstanceMap.Systemlimit.o_WE(appInstanceMap.o_NCL.o_Uoq)) {
				var j = SingletonMap.o_O.o_UPk.lightShenDianOpenDayTbl;
				var Y = {
					timeStr: "21:30"
				};
				for (var u = 1; u <= 7; u++) if (j.indexOf(u) > -1) Y[DailyActivCalendar.o_NiW[u]] = LangManager.o_UZP;
				else Y[DailyActivCalendar.o_NiW[u]] = "";
				Q.push(Y)
			}
			result = Q;
		}
		catch(e){
			console.error(e);
		}
		return result;
	}
}


class GlobalUtil {
    static allowMapGuaJi = false;
    static lastTaskHashCode = null;

    init() {
        GlobalUtil.allowMapGuaJi = false;
    }

    static compterFindWayPos(x1, y1) {
        return SingletonMap.o_etq.o_egw(x1, y1, PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, true);
        // return SingletonMap.o_etq.o_egw(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, x1, y1, true);
    }

    static compterFindWayPos2(x1, y1, x2, y2) {
        return SingletonMap.o_etq.o_egw(x1, y1, x2, y2, true);
        // return SingletonMap.o_etq.o_egw(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, x1, y1, true);
    }

    static getTargetPos(posX, posY) {
        let arrPathPos = GlobalUtil.compterFindWayPos(posX, posY).reverse();
        let resultPos = null;
        if (SingletonMap.ShenJieMgr.o_UEk()) {
            for (let i = 5; i > 0; i--) {
                if (arrPathPos[arrPathPos.length - i]) {
                    resultPos = arrPathPos[arrPathPos.length - i];
                    break;
                }
            }
        }
        else {
            for (let i = 4; i > 0; i--) {
                if (arrPathPos[arrPathPos.length - i]) {
                    resultPos = arrPathPos[arrPathPos.length - i];
                    break;
                }
            }
        }
        return resultPos;
    }

    static isAttackDistance(posX, posY) {
        let arrPathPos = GlobalUtil.compterFindWayPos(posX, posY);
        return arrPathPos.length < 5;
    }

    static existsMonster(monster) {
        let result = false;
        if (monster) {
            result = SingletonMap.o_OFM.o_Nx1.find(M => monster.curHP > 0 && !monster.masterName && M.$hashCode == monster.$hashCode);
        }
        return result;
    }

    static getMonsterById(cfgId) {
        let result = false;
        if (cfgId) {
            result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.cfgId == cfgId);
        }
        return result;
    }
	
	static getMonsterByName(name) {
        let result = false;
        if (name) {
            result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.roleName == name);
        }
        return result;
    }
}

// 修罗幻境
class XiuLuoHuanJinTask extends StaticGuaJiTask {
    static name = "XiuLuoHuanJinTask";
	nextRunTime = null;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime() && this.hasTimes()) {
				if ($.isEmptyObject(SingletonMap.o_eB9.o_Hmw)) {
					result = true;
				}
				else if (this.getJiNaTimes() > 0 && this.getJiNaBoss()) { // 优先缉拿 
					result = true;
				}
				else if (this.getJiNaTimes() <= 0 && this.getNextBoss()) {
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					result = true;
				}
				if (result) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async refrehBossTime() {
		try {
			let arrSource = SingletonMap.o_O.o_Hmc;
			for (let i = 0; i < arrSource.length; i++) {
				SingletonMap.Boss2Sys.o_HmT(arrSource[i].layer);
				await xSleep(200);
			}
		}
		catch (e) {
            console.error(e);
        }
	}
	
	hasTimes() {
		return SingletonMap.UintDataMgr.getValue(86, 1) > 0;
	}
	
	getJiNaTimes() {
		let result = 0;
		try {
			let M = SingletonMap.UintDataMgr.getValue(88, 3)
			let i = SingletonMap.BossProvider.o_lyt().xiuLuoBossBoxDayNum || 5;
			result = Math.max(0, i - M);
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	getJiNaBoss() {
		let result = null;
		try{
			let jiNaBossId = SingletonMap.UintDataMgr.getValue(88, 1);
			let arrBossCfg = this.getArrBossCfg();
			let selBossCfg = arrBossCfg.find(M => M.bossId == jiNaBossId);
			if (selBossCfg && this.isAllowLayer(selBossCfg.layer)) {
				if (SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''] 
				&& SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''][selBossCfg.index + ''] == 0
				&& (!bossNextWatchTime[selBossCfg.bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[selBossCfg.bossId])) {
					result = selBossCfg;
				}
				else if (SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''] 
				&& SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''][selBossCfg.index + ''] - SingletonMap.ServerTimeManager.o_eYF <= 30000
				&& (!bossNextWatchTime[selBossCfg.bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[selBossCfg.bossId])) {
					result = selBossCfg;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	hasJiNaBaoXian() {
		let result = false;
		try {
			let jiNaBossId = SingletonMap.UintDataMgr.getValue(88, 1);
			let i = SingletonMap.UintDataMgr.getValue(88, 2);
			let Q = 2 == i;
			let E = 1 == i;
			result = E && !Q;
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	getBossRefreshTime(bossId) {
		let result = SingletonMap.ServerTimeManager.o_eYF + 99999999;
		try {
			let arrBossCfg = this.getArrBossCfg();
			let bossCfg = arrBossCfg.find(M => M.bossId == bossId);
			if (SingletonMap.o_eB9.o_Hmw[bossCfg.layer + ''] 
				&& SingletonMap.o_eB9.o_Hmw[bossCfg.layer + ''][bossCfg.index + '']) {
					result = SingletonMap.o_eB9.o_Hmw[bossCfg.layer + ''][bossCfg.index + ''];
				}
		}
		catch (ex) {
			console.error(ex);
        }
		return result;
	}
	
	getArrBossCfg() {
		let result = [];
		try {
			let arrLayerBossCfg = SingletonMap.o_O.o_Hmc;
			for (let i = 0;i  < arrLayerBossCfg.length; i++) {
				let layerBossCfg = arrLayerBossCfg[i];
				let tbl = layerBossCfg.Tbl.map(item => {
					return {...item, layer: layerBossCfg.layer};
				});
				result = result.concat(tbl);
			}
		}
		catch(e) {
			console.error(e);
		}
		return result;
	}
	
	getNextBoss() {
		let result = null;
		try {
			let strSelBossCfg = getAppConfigKeyValue("XiuLuoHuanJinBoss");
			let arrSelBossCfg = strSelBossCfg.split("|");
			let arrBossCfg = this.getArrBossCfg();
			for (let i = 0; i < arrSelBossCfg.length; i++) {
				let selBossCfg = arrBossCfg.find(M => M.bossId == arrSelBossCfg[i]);
				if (SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''] 
				&& SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''][selBossCfg.index + ''] == 0
				&& (!bossNextWatchTime[selBossCfg.bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[selBossCfg.bossId])) {
					result = selBossCfg;
					break;
				}
				else if (SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''] 
				&& SingletonMap.o_eB9.o_Hmw[selBossCfg.layer + ''][selBossCfg.index + ''] - SingletonMap.ServerTimeManager.o_eYF <= 30000
				&& (!bossNextWatchTime[selBossCfg.bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[selBossCfg.bossId])) {
					result = selBossCfg;
					break;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	isAllowLayer(layer) {
		let result = false;
		try {
			let arrSource = SingletonMap.o_O.o_Hmc;
			let M = arrSource.find(M => M.layer == layer);
			if (M && SingletonMap.RoleData.o_HT5 >= M.limit) {
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoWangChen();
			await this.refrehBossTime();
			if (this.hasJiNaBaoXian()) {
				SingletonMap.Boss2Sys.o_Han();
				await xSleep(500);
			}
			if (this.getJiNaTimes() > 0) {
				let jiNaBoss = this.getJiNaBoss();
				SingletonMap.o_Yr.o_NZ8(KfActivity.o_Hul, jiNaBoss.layer);
				await awaitToMap('修罗幻境');
				await this.waitTaskFinish(jiNaBoss);
				QuickPickUpNormal.runPickUp();
				await xSleep(1000);
			}
			else {
				let nextBoss = this.getNextBoss();
				let lastLayer = null;
				while(nextBoss) {
					if (!this.isRunning()) break;
					if (!mainRobotIsRunning()) break;
					if (!this.hasTimes()) break;
					if (lastLayer != nextBoss.layer) {
						lastLayer = nextBoss.layer;
						await this.gotoWangChen();
						SingletonMap.o_Yr.o_NZ8(KfActivity.o_Hul, nextBoss.layer);
						await awaitToMap('修罗幻境');
					}
					await this.waitTaskFinish(nextBoss);
					QuickPickUpNormal.runPickUp();
					await xSleep(1000);
					nextBoss = this.getNextBoss();
				}
			}
			if (this.hasJiNaBaoXian()) {
				SingletonMap.Boss2Sys.o_Han();
				await xSleep(500);
			}
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	findMonster() {
		return SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName);	
	}

    async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!isCrossTime()) return;
					if (!self.hasTimes()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}					
					
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						LastPointWatcher.resetLastPointInfo();
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					
					// 寻怪
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					if (monster)  LastPointWatcher.resetLastPointInfo();
					// 已经刷新
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					if (monster && !isNullBelong(monster) && !isSelfBelong(monster)) {
						if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
							// 先打归属者
							let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
							if (belongingBadPlayer) {
								setCurMonster(belongingBadPlayer);
								SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
								return check();
							}
						}
						else {
							await xSleep(1200);
							let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
							if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster))) {
								setCurMonster(tmpMonster);
								SingletonMap.o_OKA.o_Pc(tmpMonster, true);
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
								// 先打归属者
								let belongingBadPlayer = GlobalUtil.getMonsterByName(tmpMonster.belongingName);
								if (belongingBadPlayer) {
									setCurMonster(belongingBadPlayer);
									SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
									return check();
								}
							}
							else {
								bossNextWatchTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 10; // 是别人归属的boss,10分钟内不打
								console.warn(`boss归属${tmpMonster.belongingName},离开不打...`);
								return;
							}
						}
					}
					
					let bossRefreshTime = self.getBossRefreshTime(nextBoss.bossId);
					// 等待刷新
					if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
						return check();
					}
					// 超出等待时间
					if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
						return;
					}
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
}

// 神魔边境4王
class ShenMoBianJinKingTask extends StaticGuaJiTask {
    static name = "ShenMoBianJinKingTask";
	
	needRun() {
		let result = false;
		try {
			if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
                if ($.isEmptyObject(SingletonMap.o_eB9.o_yHl)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
                else if (this.getNextBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
                    result = true;
                }
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
            }
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (monthDay == 1) { // 每月1号休战
                result = false;
            }
            else if (weekDay == 0 && (curHours == 23 && curMinutes >= 30 || curHours == 0 && curMinutes <= 30)) { // 星期日 23:30以后不能进
                result = false;
            }
            else if (weekDay == 1 && curHours < 10) { // 星期一 10点前不能进
                result = false;
            }
            else if (weekDay >= 1 && weekDay <= 6 && ((curHours == 23 && curMinutes >= 30) || (curHours == 0 && curMinutes <= 30))) { // 周123456晚上23.30到00.30
                result = false;
            }
            else {
                result = true;
            }
		}
		 catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	getBossRefreshTime(bossId) {
		let result = SingletonMap.ServerTimeManager.o_eYF + 99999999;
		try {
			let bossCfg = SingletonMap.o_O.o_yU9.find(M => M.bossId == bossId);
			result = SingletonMap.o_eB9.o_yHl[bossCfg.index];
		}
		catch (ex) {
			console.error(ex);
        }
		return result;
	}
	
	getNextBoss() {
		let result = null;
		try {
			let selBossCfg = getAppConfigKeyValue("ShenMoBianJinKing");
			let arrSelBossCfg = selBossCfg.split('|');
			for (let i = 0;i < arrSelBossCfg.length; i++) {
				let bossId = arrSelBossCfg[i];
				let bossCfg = SingletonMap.o_O.o_yU9.find(M => M.bossId == bossId);
				if (SingletonMap.o_eB9.o_yHl[bossCfg.index] == 0
				&& (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId])) {
					result = bossCfg;
					break;
				}
			}
			if (result == null) {
				for (let i = 0;i < arrSelBossCfg.length; i++) {
					let bossId = arrSelBossCfg[i];
					let bossCfg = SingletonMap.o_O.o_yU9.find(M => M.bossId == bossId);
					if (SingletonMap.o_eB9.o_yHl[bossCfg.index] - SingletonMap.ServerTimeManager.o_eYF < 30000
						&& (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId])) {
						result = bossCfg;
						break;
					}
				}
			}
		}catch (e) {
			console.error(e);
		}
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoShenMoBianJin();
			// 刷新boss时间
			SingletonMap.Boss2Sys.o_yU3();
			await xSleep(1000);
			let nextBoss = this.getNextBoss();
			while(nextBoss) {
				closeGameWin("NewWorldActTipWin");
				if (SingletonMap.MapData.curMapName != '神魔边境') {
					await this.gotoShenMoBianJin();
				}
				await this.waitTaskFinish(nextBoss);
				QuickPickUpNormal.runPickUp();
				await xSleep(1000);
				if (this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				nextBoss = this.getNextBoss();
			}
        }
        catch (e) {
            console.error(e);
        }
		QuickPickUpNormal.runPickUp();
		await xSleep(1000);
        await this.stop();
    }
	
	async waitTaskFinish(nextBoss) {
        let self = this;
        function check() {            
			return xSleep(500).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (!self.isActivityTime()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.curMapName != '神魔边境') {
						await this.gotoShenMoBianJin();
					}
					LastPointWatcher.resetLastPointInfo();
					if (GlobalUtil.compterFindWayPos(nextBoss.bossPos[0], nextBoss.bossPos[1]).length >= 8) {
						await self.findWayByPos(nextBoss.bossPos[0], nextBoss.bossPos[1], 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 6) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							clearCurMonster();
							SingletonMap.o_OKA.o_NSw(badPlayer.currentX, badPlayer.o_NI3);
							return check();
						}
					}
					
					// 寻怪
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					// 已经刷新
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) {
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					if (monster && !isNullBelong(monster) && !isSelfBelong(monster)) {
						if (BlacklistMonitors.isWhitePlayer(monster.belongingName)) { // 归属是白名单
							setCurMonster(monster);
							SingletonMap.o_OKA.o_Pc(monster, true);
							return check();
						}
						else if (BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) { // 归属是黑名单
							// 先打归属者
							let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
							if (belongingBadPlayer) {
								setCurMonster(belongingBadPlayer);
								SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
								return check();
							}
						}
						else {
							await xSleep(1200);
							let tmpMonster = GlobalUtil.getMonsterById(monster.cfgId);
							if (tmpMonster && (isNullBelong(tmpMonster) || isSelfBelong(tmpMonster))) {
								setCurMonster(tmpMonster);
								SingletonMap.o_OKA.o_Pc(tmpMonster, true);
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.getBadPlayer()) {
								return check();
							}
							else if (tmpMonster && BlacklistMonitors.isBadPlayer(tmpMonster.belongingName) && BlacklistMonitors.getBadPlayer()) {						
								// 先打归属者
								let belongingBadPlayer = GlobalUtil.getMonsterByName(tmpMonster.belongingName);
								if (belongingBadPlayer) {
									setCurMonster(belongingBadPlayer);
									SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
									return check();
								}
							}
							else {
								bossNextWatchTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 10; // 是别人归属的boss,10分钟内不打
								console.warn(`boss归属${tmpMonster.belongingName},离开不打...`);
								return;
							}
						}
					}
					
					let bossRefreshTime = self.getBossRefreshTime(nextBoss.bossId);
					// 等待刷新
					if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 60000) {
						return check();
					}
					// 超出等待时间
					if (bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF > 60000) {
						return;
					}
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
}



// 西游洞天福地
class XiYouDonTianTask extends StaticGuaJiTask {
    static name = "XiYouDonTianTask";
	
	arrPosMap = {
		"173": [{x:24, y:91, time: 0}, {x:39,y:78, time: 0}, { x:43,y:55, time: 0}, { x:28,y:32, time: 0}, {x:44,y:22, time: 0}, {x:72,y:21, time: 0}, {x:77,y:49, time: 0}, {x:80,y:89, time: 0}, {x:112,y:87, time: 0}, {x:118,y:36, time: 0}, {x:127,y:14, time: 0}],
		"174": [{x:81, y:57, time: 0}, {x:94, y:47, time: 0}, {x:52, y:88, time: 0}, {x:72, y:108, time: 0}, {x:104, y:117, time: 0}, {x:116, y:87, time: 0}, {x:33, y:94, time: 0}, {x:13, y:43, time: 0}, {x:22, y:10}, {x:45, y:21, time: 0}],
		"175": [{x:104,y:24, time: 0}, {x:93,y:18, time: 0}, {x:116,y:95, time: 0}, {x:77,y:105, time: 0}, {x:125,y:60, time: 0}, {x:47,y:79, time: 0}, {x:26,y:97, time: 0}, {x:53,y:26, time: 0}, {x:27,y:45, time: 0}],
		"176": [{x:28,y:137, time: 0}, {x:14,y:160, time: 0}, {x:76,y:161, time: 0}, {x:29,y:108, time: 0}, {x:13,y:86, time: 0},  {x:61,y:107, time: 0}, {x:76,y:79, time: 0}, {x:90,y:65, time: 0}, {x:62,y:58, time: 0}, {x:26,y:29, time: 0}],
		"177": [{x:98,y:89, time: 0 }, {x:64,y:50, time: 0},  {x:55,y:29, time: 0},  {x:32,y:40, time: 0},  {x:8,y:60, time: 0},  {x:39,y:77, time: 0},  {x:65,y:93, time: 0},  {x:107,y:45, time: 0},  {x:111,y:29, time: 0}],
		"178": [{x:24, y:91, time: 0}, {x:39,y:78, time: 0}, { x:43,y:55, time: 0}, { x:28,y:32, time: 0}, {x:44,y:22, time: 0}, {x:72,y:21, time: 0}, {x:77,y:49, time: 0}, {x:80,y:89, time: 0}, {x:112,y:87, time: 0}, {x:118,y:36, time: 0}, {x:127, y:14, time: 0}],
		"179": [{x:81, y:57, time: 0}, {x:94, y:47, time: 0}, {x:52, y:88, time: 0}, {x:72, y:108, time: 0}, {x:104, y:117, time: 0}, {x:116, y:87, time: 0}, {x:33, y:94, time: 0}, {x:13, y:43, time: 0}, {x:22, y:10}, {x:45, y:21, time: 0}],
		"180": [{x:104,y:24, time: 0}, {x:93,y:18, time: 0}, {x:116,y:95, time: 0}, {x:77,y:105, time: 0}, {x:125,y:60, time: 0}, {x:47,y:79, time: 0}, {x:26,y:97, time: 0}, {x:53,y:26, time: 0}, {x:27,y:45, time: 0}],
		"181": [{x:28,y:137, time: 0}, {x:14,y:160, time: 0}, {x:76,y:161, time: 0}, {x:29,y:108, time: 0}, {x:13,y:86, time: 0},  {x:61,y:107, time: 0}, {x:76,y:79, time: 0}, {x:90,y:65, time: 0}, {x:62,y:58, time: 0}, {x:26,y:29, time: 0}],
		"182": [{x:98,y:89, time: 0 }, {x:64,y:50, time: 0},  {x:55,y:29, time: 0},  {x:32,y:40, time: 0},  {x:8,y:60, time: 0},  {x:39,y:77, time: 0},  {x:65,y:93, time: 0},  {x:107,y:45, time: 0},  {x:111,y:29, time: 0}],
		"298": [{x:81, y:57, time: 0}, {x:94, y:47, time: 0}, {x:52, y:88, time: 0}, {x:72, y:108, time: 0}, {x:104, y:117, time: 0}, {x:116, y:87, time: 0}, {x:33, y:94, time: 0}, {x:13, y:43, time: 0}, {x:22, y:10}, {x:45, y:21, time: 0}],
		"299": [{x:28,y:137, time: 0}, {x:14,y:160, time: 0}, {x:76,y:161, time: 0}, {x:29,y:108, time: 0}, {x:13,y:86, time: 0},  {x:61,y:107, time: 0}, {x:76,y:79, time: 0}, {x:90,y:65, time: 0}, {x:62,y:58, time: 0}, {x:26,y:29, time: 0}],
		"300": [{x:104,y:24, time: 0}, {x:93,y:18, time: 0}, {x:116,y:95, time: 0}, {x:77,y:105, time: 0}, {x:125,y:60, time: 0}, {x:47,y:79, time: 0}, {x:26,y:97, time: 0}, {x:53,y:26, time: 0}, {x:27,y:45, time: 0}]
	};
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (isCrossTime() && !this.isEnough()) {
				if (this.hasIdleBoss()) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
				else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
					TaskStateManager.updateTaskEnterState(this.constructor.name);
					result = true;
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	isEnough() {
		let result = false;
		try {
			if (getAppConfigKeyValue("XiYouDonTianOnly30FuBi") == "TRUE" || getAppConfigKeyValue("XiYouDonTianOnly10FuBi") == "TRUE") {
				// 西游每日任务
				var M = SingletonMap.o_O.o_UiV;
				var i = [];
				var Q = M.xiyouxianfudaytask;
				var E;
				for (var A = 0; A < Q.length; A++) {
					E = SingletonMap.QuestMgr.o_Nx0(Q[A]);
					if (!E) E = SingletonMap.o_eHJ.o_lUO(Q[A]);
					i.push(E)
				}
				if (i.length) i.sort(SingletonMap.QuestMgr.o_k9);
				if (getAppConfigKeyValue("XiYouDonTianOnly30FuBi") == "TRUE") {
					let flag = i.find(M => M.stdQuest && M.stdQuest.id == 9202 && M.o_Nz_ == true); // 30币
					if (flag) {
						result = true;
					}
				}
				else if (getAppConfigKeyValue("XiYouDonTianOnly10FuBi") == "TRUE") {
					let flag = i.find(M => M.stdQuest && M.stdQuest.id == 9201 && M.o_Nz_ == true); // 10币
					if (flag) {
						result = true;
					}
				}
			}
		}
		catch (ex) {
            console.error(ex);
        }
		return result;
	}
	
	hasIdlePosBySid(sid) {
		let result = false;
		try {
			let arrPos = this.arrPosMap[sid];
			let idlePos = arrPos.find(M => M.time == 0 || M.time <= SingletonMap.ServerTimeManager.o_eYF);
			if (idlePos){
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	hasPower(sid) {
		let result = false;
		try {
			let layerBossCfg = SingletonMap.o_O.o_yjP[sid];
			if (layerBossCfg && SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_yzl) >= layerBossCfg.cost) {
				result = true;	
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}

    hasIdleBoss() {
        let result = null;
        try {
			let selBossCfg = getAppConfigKeyValue("XiYouDonTian");
			let arrSelBossCfg = selBossCfg.split("|");
			for (let i = 0; i < arrSelBossCfg.length; i++) {
				let sid = arrSelBossCfg[i];
				if (this.hasPower(sid) && this.hasIdlePosBySid(sid)) {
					result = true;
					break;
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getNextArrPosBySid(sid) {
		let result = [];
		try {
			result = this.arrPosMap[sid].filter(M => M.time == 0 || M.time <= SingletonMap.ServerTimeManager.o_eYF);
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getNextArrPos() {
		let result = null;
		try {
			let selBossCfg = getAppConfigKeyValue("XiYouDonTian");
			let arrSelBossCfg = selBossCfg.split("|");
			for (let i = 0; i < arrSelBossCfg.length; i++) {
				let sid = arrSelBossCfg[i];
				let layerBossCfg = SingletonMap.o_O.o_yjP[sid];
				if (layerBossCfg && SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_yzl) >= layerBossCfg.cost
					&& this.hasIdlePosBySid(sid)) {
					let arrPos = result = this.getNextArrPosBySid(sid);
					if (arrPos.length > 0) {
						result = {
							sid: sid,
							arrPos: arrPos
						};
						break;
					}
				}
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
			await this.gotoXiYouZhuChen();
            let nextArrPos = this.getNextArrPos();
            if (nextArrPos && nextArrPos.arrPos.length > 0) {
				// 进入地图
				if (SingletonMap.MapData.sceneId != nextArrPos.sid) {
					await this.exitScene();
					const curMapName = SingletonMap.MapData.curMapName;
					SingletonMap.o_yKf.o_yLP(nextArrPos.sid);
					await changeScene(curMapName);
					SingletonMap.o_lAu.stop();
					clearCurMonster();
				}
				for(let i = 0; i < nextArrPos.arrPos.length; i++) {
					if (!this.isRunning()) break;
					if (!mainRobotIsRunning()) break;
					if (!this.hasPower(nextArrPos.sid)) {
						break;
					}
					if (!isCrossTime()) {
						break;
					}
					if (this.isEnough()) {
						break;
					}
					SingletonMap.o_lAu.stop();
					clearCurMonster();
					let nextPos = nextArrPos.arrPos[i];
					await this.waitTaskFinish(nextArrPos.sid, nextPos);
					nextPos.time = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 60 * 2;
					QuickPickUpNormal.runPickUp();
					await xSleep(800);
				}
            }
        }
        catch (e) {
            console.error(e);
        }
		await await exitScene();
        await this.stop();
    }
	
	isSuperMonster(M) {
		let result = false;
		try {
			 if (M.cfgId >= 163101 && M.cfgId <= 163108) { // 西游毛颖山洞府
				result = false;
			}
			else if (M.cfgId >= 163201 && M.cfgId <= 163208) { // 西游莲花洞府
				result = false;
			}
			else if (M.cfgId >= 163301 && M.cfgId <= 163308) { // 西游狮驼洞府
				result = false;
			}
			else if (M.cfgId >= 163401 && M.cfgId <= 163408) { // 西游无底洞府
				result = false;
			}
			else if (M.cfgId >= 163501 && M.cfgId <= 163508) { // 西游金兜洞府
				result = false;
			}
			else if (M.cfgId >= 165101 && M.cfgId <= 165108) { // 浮屠山
				result = false;
			}
			else if (M.cfgId >= 165201 && M.cfgId <= 165208) { // 万寿山
				result = false;
			}
			else if (M.cfgId >= 165301 && M.cfgId <= 165308) { // 黑风山
				result = false;
			}
			else if (M.cfgId >= 165401 && M.cfgId <= 165408) { // 普陀珞珈山
				result = false;
			}
			else if (M.cfgId >= 165501 && M.cfgId <= 165508) { // 小须弥山
				result = false;
			}
			else if (M.cfgId >= 197001 && M.cfgId <= 197008) { // 方壶仙境
				result = false;
			}
			else if (M.cfgId >= 197017 && M.cfgId <= 197024) { // 迷雾仙境
				result = false;
			}
			else if (M.cfgId >= 197033 && M.cfgId <= 197040) { // 异域仙境
				result = false;
			}
			else {
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}	
	
	findMonster() {
		let result = null;
		if (isSuperPri() || isYearPri()) {
			result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && isSelfBelong(M) && this.isSuperMonster(M) == true);
			if (!result) {
				result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && isNullBelong(M) && this.isSuperMonster(M) == true);
			}
		}
		else {
			result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && isSelfBelong(M));
			if (!result) {
				result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && isNullBelong(M));
			}
		}
		return result;
	}

    async waitTaskFinish(sid, nextPos) {
        let self = this;
        function check() {            
			return xSleep(300).then(async function () {
				try {
					if (!self.isRunning()) return;
					if (!mainRobotIsRunning()) return;
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return;
					}
					if (!self.hasPower(sid)) {
						return;
					}
					if (!isCrossTime()) {
						return;
					}
					if (self.isEnough()) {
						return;
					}
					if (GlobalUtil.compterFindWayPos(nextPos.x, nextPos.y).length >= 6) {
						await self.findWayByPos(nextPos.x, nextPos.y, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						if (GlobalUtil.compterFindWayPos(badPlayer.currentX, badPlayer.o_NI3).length <= 5) {
							setCurMonster(badPlayer);
							SingletonMap.o_OKA.o_Pc(badPlayer, true);
							return check();
						}
						else {
							await self.findWayByPos(badPlayer.currentX, badPlayer.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
							return check();
						}
					}
					
					if (GlobalUtil.compterFindWayPos(nextPos.x, nextPos.y).length >= 6) {
						await self.findWayByPos(nextPos.x, nextPos.y, 'findWay', SingletonMap.MapData.curMapName);
					}
					
					let monster = self.findMonster();
					if (monster) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 3) {
							await self.findWayByPos(monster.currentX, monster.o_NI3, 'findWay', SingletonMap.MapData.curMapName);
						}
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
				}
				catch (e) {
					console.error(e);
				}
				return;
			});
        }
        return check();
    }
}

// 修罗战场
class XiuLuoWarTask extends StaticGuaJiTask {
    static name = "XiuLuoWarTask";

    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;			
            if (SingletonMap.Ladder5v5Mgr.isTodayOpen5v5() && this.isActivityTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 20 && curMinutes >= 50) {
                result = true;
            }
			else if (curHours == 21 && curMinutes <= 25) {
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			SingletonMap.o_n.o_EG("XiuLuoActWin");
			await xSleep(1000);
			await this.signUp();
			closeGameWin("XiuLuoActWin");
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	async signUp() {
		try {
			for (let i = 1; i <= 5; i++) {
				if (getAppConfigKeyValue("XiuLuoSign" + i)  == "TRUE") {
					SingletonMap.o_lVP.sendSignUp5v5(i);
					await xSleep(100);
				}
				if (getAppConfigKeyValue("XiuLuoBeCaption" + i)  == "TRUE") {
					SingletonMap.o_lVP.sendLadder5v5BeCaption(i);
					await xSleep(100);
				}				
			}
		}
		catch (e) {
            console.error(e);
        }
	}
	
	async getLadder5v5Rd()  {
		try {
			// 击杀队长
			let arrCfg = SingletonMap.o_O.Cross5V5DayKillAwardConfig;
			for(let i = 0; i < arrCfg.length; i++) {
				let cfg = arrCfg[i];
				if (!o_h.o_l3H(SingletonMap.Ladder5v5Mgr.o_HXH, cfg.index)) {
					SingletonMap.o_lVP.sendGetLadder5v5Rd(1, cfg.index);
					await xSleep(100);
				}
			}
			// 每周参与
			arrCfg = SingletonMap.o_O.Cross5V5SeasonConfig;
			for(let i = 0; i < arrCfg.length; i++) {
				let cfg = arrCfg[i];
				if (!o_h.o_l3H(SingletonMap.Ladder5v5Mgr.o_HXw, cfg.index)) {
					SingletonMap.o_lVP.sendGetLadder5v5Rd(3, cfg.index);
					await xSleep(100);
				}
			}
			// 今日参与
			arrCfg = SingletonMap.o_O.Cross5V5DayWinAwardConfig;
			for(let i = 0; i < arrCfg.length; i++) {
				let cfg = arrCfg[i];
				if (!o_h.o_l3H(SingletonMap.Ladder5v5Mgr.o_HX2, cfg.index)) {
					SingletonMap.o_lVP.sendGetLadder5v5Rd(2, cfg.index);
					await xSleep(100);
				}
			}
		}
		catch (e) {
            console.error(e);
        }
	}

    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(3000).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (SingletonMap.MapData.curMapName == "王城") {
						SingletonMap.o_n.o_EG("XiuLuoActWin");
					}
					else {
						closeGameWin("XiuLuoActWin");						
					}
					LastPointWatcher.resetLastPointInfo();
                    if (!self.isActivityTime()) {
                        return;
                    }
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
	
	async stop() {
		SingletonMap.o_n.o_EG("XiuLuoActWin");
		await xSleep(1000);
		await this.getLadder5v5Rd();
		closeGameWin("XiuLuoActWin");	
        await super.stop();
    }
}

// 刺激战场
class CijiWarTask extends StaticGuaJiTask {
    static name = "CijiWarTask";
	arrWeekDayFlag = {
		"1": "weekA",
		"2": "weekB",
		"3": "weekC",
		"4": "weekD",
		"5": "weekE",
		"6": "weekF",
		"0": "weekG"
	};
	arrPosTempate = [{x:49, y:45}, {x:52,y:70}, {x:81,y:76},{x:78,y:46},{x:65,y:59}]
	tempPos = [];
	isOver = false;
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (this.isOver == true) return false;
            if (this.isTodayActiv() && this.isActivityTime() && SingletonMap.MapData.curMapName != "刺激战场") {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	isTodayActiv() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let weekDay = curDate.getDay();
			let weekDayFlag = this.arrWeekDayFlag[weekDay + ""];
			let dailyActivCalendar = DailyActivCalendar.getCalendar();
			let weekDayActiv = dailyActivCalendar.find(M => M[weekDayFlag] == '刺激战场');
			if (weekDayActiv) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}

    isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            let curSeconds = curDate.getSeconds();
            if (curHours == 20 && curMinutes <= 20) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }

    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			SingletonMap.o_Yr.o_NZ8(KfActivity.o_l4J);
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
		this.isOver = true;
        await this.stop();
    }
	
	getNextPos() {
		let result = null;
		if (this.tempPos.length == 0) {
			this.tempPos = Object.assign([], this.arrPosTempate);
		}
		// 距离近的优先
		let minPos = this.tempPos.reduce(function (a, b) {
			let path1 = GlobalUtil.compterFindWayPos(a.x, a.y, true);
			let path2 = GlobalUtil.compterFindWayPos(b.x, b.y, true);
			return path1.length > path2.length ? b : a;
		});
		this.tempPos = this.tempPos.filter(M => {
			return M.x + ',' + M.y != minPos.x + ',' + minPos.y;
		});
	}
	
	findMonster() {
		let result = null;
		try {
			let arrPlayer = SingletonMap.o_OFM.o_Nx1.filter(M => M && "PlayerActor" == egret.getQualifiedClassName(M) && M.curHP > 0);
			result = arrPlayer.reduce(function (a, b) {
				return a.curHP > b.curHP ? b : a;
			});
		}
		catch(exx) {
		}
		return result;
	}
	
	isOnlyVitality() {
		let result = false;
		try {
            if (getAppConfigKeyValue("CiJiWarOnlyVitality") != "TRUE") {
				result = true;
			}
			else {
				result = false;
			}
		}
		catch(e) {}
		return result;
	}
	
    async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
						return check();
					}
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (SingletonMap.MapData.curMapName != "刺激战场") {
						SingletonMap.o_Yr.o_NZ8(KfActivity.o_l4J);
						await xSleep(1500);
						return check();
					}
                    if (!self.isActivityTime()) {
                        return;
                    }
					
					if (self.isOnlyVitality()) {
						await xSleep(2000);
						return;
					}
					
					let monster = self.findMonster();
					if (monster) {
						if (GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length >= 6) {
							SingletonMap.o_OKA.o_NSw(monster.currentX, monster.o_NI3);
						}
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					
					let pos = self.getNextPos();
					if (pos) {
						await self.findWayByPos(pos.x, pos.y, 'findWay', SingletonMap.MapData.curMapName);
					}
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
			return check();
        }
        return check();
    }
}

// 黑暗神殿
class ShenJieDarkTask extends StaticGuaJiTask {
    static name = "ShenJieDarkTask";
	posTemplate = [
		{x:79, y:121}, {x:91, y:111}, {x:111, y:125}, {x:97, y:136}, 
		{x:33, y:68}, {x:20, y:78}, {x:38, y:96}, {x:55, y:84}, {x:30, y:35},{x:41, y:29},{x:13, y:8},
		{x:109, y:43}, {x:124, y:31}, {x:147, y:50}, {x:132, y:61},  
		{x:168, y:86}, {x:183, y:74}, {x:205, y:91}, {x:191, y:104},{x:157, y:119},{x:218, y:161}, {x:222, y:169}, {x:208, y:169},
		{x:148, y:153},{x: 135, y: 163}, {x:155, y:179}, {x:169, y:169}];
	floorCfg = null;
	isDebug = false;
	nextRunTime = null;
	
	initFloorCfg() {
		this.floorCfg = JSON.parse(JSON.stringify(SingletonMap.o_O.o_UWW));
	}
	
	clearFloorCfg() {
		this.floorCfg = null;
	}
	
	updateFloorCfg(floor, id, flag) {
		try {
			if (this.floorCfg == null) {
				this.initFloorCfg();
			}
			if (floor && id) {
				this.isDebug && console.warn(`第${floor}层--ID=${id}--${flag}`);
				this.isDebug && console.warn('更新前:' + JSON.stringify(this.floorCfg[floor - 1].npcList));
				let cfg = this.floorCfg[floor - 1].npcList.find(M => M.id == id);
				cfg.flag = flag;
				this.isDebug && console.warn('更新后:' + JSON.stringify(this.floorCfg[floor - 1].npcList));
			}
		}
		catch(ex){
			console.error(ex);
		}
	}
	
    needRun() {
        let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;			
            if (isCrossTime()) {
				if (this.floorCfg == null) {
					this.initFloorCfg();
				}
				if ($.isEmptyObject(SingletonMap.ShenJieMgr.o_Usu)) {
					result = true;
				}
				else if (this.getArrSelBoss().length > 0) {
					result = true;
				}
				else if (!this.nextRunTime || this.nextRunTime <= curDate.getTime()) {
					result = true;
				}
            }
			else {
				if (this.floorCfg) {
					this.clearFloorCfg();
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getArrSelBoss() {
		let result = [];
		try {
			let cfg = "|" + getAppConfigKeyValue("ShenJieDarkBoss") + "|";
			let arrBoss = SingletonMap.ShenJieMgr.o_Usu;
			for (let i = 0; i < SingletonMap.ShenJieMgr.o_Usu.length; i++) {
				let count = arrBoss[i];
				let layer = i + 1;
				let strLayer  = "|" + layer + "|";
				if (count > 0 && cfg.indexOf(strLayer) > -1) {
					result.push(layer);
				}
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getMaxFloor(){
		let result = null;
		try {
			let cfg = getAppConfigKeyValue("ShenJieDarkBoss");
			let arrStrFloor = cfg.split("|");
			let arrNumberFloor = arrStrFloor.map(M => {
				return parseInt(M);
			});
			result  = Math.max(...arrNumberFloor);
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	isSelFloor() {
		let result = false;
		try {
			let curLayer = this.getCurFloor();
			let strLayer  = "|" + curLayer + "|";
			let cfg = "|" + getAppConfigKeyValue("ShenJieDarkBoss") + "|";
			if (cfg.indexOf(strLayer) > -1) {
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getFloorBossCount(layer) {
		let result = 0;
		try {
			if (layer) {
				result = SingletonMap.ShenJieMgr.o_Usu[layer - 1];
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getCurFloor() {
		let result = -1;
		try {
			result = SingletonMap.MapData.sceneId;
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	updateNextRunTime() {
		let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        this.nextRunTime = curDate.getTime() + 2 * 60 * 60 * 1000 + 3  * 60 * 1000;
        let nextRunDateStr = new Date(this.nextRunTime).toLocaleString();
        console.warn(`任务${this.constructor.name}下次执行时间${nextRunDateStr}`);
    }
	
    async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoShenJie();
			await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
		TaskStateManager.updateTaskEnterState(this.constructor.name);
        await this.stop();
    }
	
	async waitTaskFinish() {
		let self = this;
		function check() {
			try {
				return xSleep(500).then(async function () {
					if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!isCrossTime()) return;
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return check();
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					// 进入1层
					if (SingletonMap.MapData.curMapName == "神界主城") {
						SingletonMap.o_Uij.o_Uoe(1);
						return check();
					}
					
					// 是勾选的层
					if (self.isSelFloor()) {
						self.isDebug && console.warn(`是勾选的楼层${self.isSelFloor()}..`);
						if (self.getFloorBossCount(self.getCurFloor()) > 0) { // 有boss
							closeGameWin("ShenJieDarkNpcWin");
							console.warn('是勾选的楼层,开始寻怪..');
							await self.killFloorBoss();
							console.warn(`第${self.getCurFloor()}层怪物已清空..`);
							await xSleep(200);
						}
					}
					
					await self.fetchImportantGoods();
					
					let maxFloor = self.getMaxFloor();
					let curFloor = self.getCurFloor();
					self.isDebug && console.warn(`勾选的最高层${maxFloor},当前层是${curFloor}..`);
					if (curFloor > maxFloor) { // 超过最高层
						console.warn('超过勾选的最高层..');
						closeGameWin("ShenJieDarkNpcWin");
						self.updateNextRunTime();
						return;
					}
					else if (curFloor == maxFloor) {// 到达最高层
						console.warn(`到达勾选最高层:${maxFloor}..`);
						if (self.getFloorBossCount(self.getCurFloor()) > 0) { // 有boss
							closeGameWin("ShenJieDarkNpcWin");
							console.warn('是勾选的楼层,开始寻怪..');
							await self.killFloorBoss();
							console.warn(`第${self.getCurFloor()}层怪物已清空..`);
							await xSleep(200);
							await self.fetchImportantGoods();
						}
						self.updateNextRunTime();
						return;
					}
					await self.fetchImportantGoods();
					await self.goToNextFloor(); // 前往下一层
					return check();
				});
			}
			catch (e) {
				console.error(e);
			}
		}
		return check();
	}
	
	async fetchImportantGoods() {
		PlayerActor.o_NLN.o_lkM();
		SingletonMap.o_lAu.stop();
		function check() {
			return xSleep(500).then(function() {
				let importantGoods = getImportantGoods() || getNextGoods();
				if (importantGoods) {
					if (!SingletonMap.o_lAu.o_lcD){
						PlayerActor.o_NLN.o_lkM();
						SingletonMap.o_lAu.start();
					}
					return check();
				}
				return;
			});
		}
		return check();
	}
	
	async killFloorBoss() {
		console.warn(`寻找第${this.getCurFloor()}层boss`);
		let self = this;
		let arrPos = JSON.parse(JSON.stringify(this.posTemplate));
		function check() {
            try {
                return xSleep(500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!isCrossTime()) return;
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					let importantGoods = getImportantGoods() || getNextGoods();
					if (importantGoods) {
						if (!SingletonMap.o_lAu.o_lcD){
							PlayerActor.o_NLN.o_lkM();
							SingletonMap.o_lAu.start();
						}
						return check();
					}
					if (arrPos.length == 0) {
						console.warn('寻路结束..');
						return;  // 坐标都走完了
					}
					let curFloorBossCount = self.getFloorBossCount(self.getCurFloor());
					if (curFloorBossCount == 0) {
						console.warn('没有boss了..');
						return;  // 没有boss了
					}
					// 先打人
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						setCurMonster(badPlayer);
						self.gotoTarget(badPlayer);
						SingletonMap.o_OKA.o_Pc(badPlayer, true);
						return check();
					}
					// 再打怪
					let monster = self.findMonster();
					if (monster && (isNullBelong(monster) || isSelfBelong(monster) || BlacklistMonitors.isWhitePlayer(monster.belongingName))) {
						setCurMonster(monster);
						self.gotoTarget(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					
					// 到达目的地
					if (arrPos[0].x == PlayerActor.o_NLN.currentX && PlayerActor.o_NLN.o_NI3 == arrPos[0].y) {
						arrPos.shift();
						return check();
					}
					else {
						SingletonMap.o_lAu.stop();
						SingletonMap.o_OKA.o_NSw(arrPos[0].x, arrPos[0].y);
						return check();
					}
                });
            }
            catch (e) {
                console.error(e);
            }
			return check();
        }
        return check();
	}
	
	getShowNextFloor() {
		let result = null;
		try {
			var M = SingletonMap.MapData.sceneId;
			var i = SingletonMap.ShenJieMgr;
			var Q = SingletonMap.o_O.o_UWW[M - 1];
			var E = Q.npcList;
			var A = i.o_UCl > 0;
			if (A) { 
				var nextLayerPos = E[i.o_UCl - 1];
				if (nextLayerPos) {
					result = SingletonMap.o_Oos.o_NIi(nextLayerPos.id);
				}
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	getCurFloorNpcList() {
		let result = null;
		try {
			if (this.floorCfg == null) {
				this.initFloorCfg();
			}
			var M = SingletonMap.MapData.sceneId;
			var Q = this.floorCfg[M - 1];
			result = JSON.parse(JSON.stringify(Q.npcList.filter(M => M.flag != false)));
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	async goToNextFloor() {
		closeGameWin("ShenJieDarkNpcWin");
		let self = this;
		let startLayer = this.getCurFloor();
		console.warn(`当前第${startLayer}层,前往下一层`);
		function check() {
            try {
                return xSleep(500).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (!isCrossTime()) return;
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					if (startLayer < self.getCurFloor()) { // 已经到达下一层
						closeGameWin("ShenJieDarkNpcWin");
						self.isDebug && console.warn(`起始层${startLayer}, 已经到达下一层${self.getCurFloor()}`);
						return;
					}
					
					// 已经探过层，直接前往
					let showNextFloor = self.getShowNextFloor();
					if (showNextFloor) {
						self.isDebug && console.warn(`本层已走过，直接前往${startLayer}-${showNextFloor.id}..`);
						// 传送
						if (SingletonMap.o_n.isOpen("ShenJieDarkNpcWin")) {
							SingletonMap.o_lAu.stop();
							PlayerActor.o_NLN.o_lkM();
							const curMapName = SingletonMap.MapData.curMapName;
							SingletonMap.o_Uij.o_UCo(showNextFloor.id);
							await changeScene(curMapName);
							closeGameWin("ShenJieDarkNpcWin");
							return check();	
						}
						// 前往npc坐标
						SingletonMap.o_l.o_Ut1(appInstanceMap.o_H.o_e90, showNextFloor.pos.x, showNextFloor.pos.y, new appInstanceMap.o_eQy(appInstanceMap.o_etK.o_NAZ, showNextFloor.name));
						return check();
					}
					
					await self.findToNextFloor(startLayer);
                    return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
	}
	
	getMinPosNpc() {
		let result = null;
		try {
			let floorNpcList = this.getCurFloorNpcList();
			let minPosNpc = floorNpcList[0];
			if (floorNpcList.length > 1) {
				let minPosNpc = floorNpcList.reduce(function (a, b) {
					let path1 = GlobalUtil.compterFindWayPos(a.posx, a.posy, true);
					let path2 = GlobalUtil.compterFindWayPos(b.posx, b.posy, true);
					return path1.length >= path2.length ? b : a;
				});
			}
			if (minPosNpc) {
				result = SingletonMap.o_Oos.o_NIi(minPosNpc.id);
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	async findToNextFloor(startLayer) {
		closeGameWin("ShenJieDarkNpcWin");
		let self = this;
		let floorNpcList = this.getCurFloorNpcList();
		this.isDebug && console.warn(`寻路前往下一层，起始层${startLayer}..`);
		function check() {
			return xSleep(500).then(async function () {
				if (!self.isRunning()) return;
				if (!mainRobotIsRunning()) return;
				if (!isCrossTime()) return;
				//人物死亡
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
				
				let minPosNpc = self.getMinPosNpc();
				await self.findToTransferNpc(minPosNpc);
				if (startLayer > self.getCurFloor()) {// 是假的npc
					floorNpcList = floorNpcList.filter(M => M.id != minPosNpc.id);
					console.warn(`假的传送使者，下次不走这条路..第${startLayer}层,${minPosNpc.id}`);
					//console.warn(JSON.stringify(floorNpcList));
					self.updateFloorCfg(startLayer, minPosNpc.id, false);
					return;
				}
				else if (startLayer == self.getCurFloor()) {
					floorNpcList = floorNpcList.filter(M => M.id != minPosNpc.id);
					console.warn(`假的传送使者，下次不走这条路..第${startLayer}层,${minPosNpc.id}`);
					//console.warn(JSON.stringify(floorNpcList));
					self.updateFloorCfg(startLayer, minPosNpc.id, false);
					self.isDebug && console.warn(`楼层未变化，探路...`);
				}
				else { // 是真的npc
					floorNpcList = floorNpcList.filter(M => M.id == minPosNpc.id);
					console.warn(`真的传送使者..第${startLayer}层,${minPosNpc.id}`);
					//console.warn(JSON.stringify(floorNpcList));
					self.updateFloorCfg(startLayer, minPosNpc.id, true);
					return;
				}
				return check();
			});
		}
		return check();
	}
	
	async findToTransferNpc(minPosNpc) {
		closeGameWin("ShenJieDarkNpcWin");
		let self = this;
		let startLayer = this.getCurFloor();
		function check() {
			return xSleep(500).then(async function () {
				if (!self.isRunning()) return;
				if (!mainRobotIsRunning()) return;
				if (!isCrossTime()) return;
				//人物死亡
				if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
					return check();
				}
				// 传送
				if (SingletonMap.o_n.isOpen("ShenJieDarkNpcWin")) {
					console.warn(`到达npc位置，执行传送...${minPosNpc.id}`);
					SingletonMap.o_lAu.stop();
					PlayerActor.o_NLN.o_lkM();
					const curMapName = SingletonMap.MapData.curMapName;
					SingletonMap.o_Uij.o_UCo(minPosNpc.id);
					closeGameWin("ShenJieDarkNpcWin");
					await changeScene(curMapName);
					return;	
				}
				// 前往npc坐标
				SingletonMap.o_l.o_Ut1(appInstanceMap.o_H.o_e90, minPosNpc.pos.x, minPosNpc.pos.y, new appInstanceMap.o_eQy(appInstanceMap.o_etK.o_NAZ, minPosNpc.name));
				return check();
			});
		}
		return check();
	}
	
	findMonster() {
		let result = null;
		try {
			// 归属怪优先
			if (curMonster && isSelfBelong(curMonster)) {
				let belongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.cfgId >= 172001 && M.cfgId <= 172008 && M.$hashCode == curMonster.$hashCode);
				if (belongingMonster) {
					result = belongingMonster;
				}
			}
			// 已选中还没有归属
			if (curMonster && isNullBelong(curMonster)) {
				let noBelongingMonster = SingletonMap.o_OFM.o_Nx1.find(M => M instanceof Monster && !M.masterName && M.curHP > 0 && M.cfgId >= 172001 && M.cfgId <= 172008 && M.$hashCode == curMonster.$hashCode);
				if (noBelongingMonster) {
					result = noBelongingMonster;
				}
			}
			if (result == null) {
				result = SingletonMap.o_OFM.o_Nx1.find(M => M.curHP > 0 && !M.masterName && M.cfgId >= 172001 && M.cfgId <= 172008 && isNullBelong(M));
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
}

// 光明神殿
class ShenJieLightTask extends StaticGuaJiTask {
	static name = "ShenJieLightTask";
	floorCfg = null;
	isDebug = false;
	nextRunTime = null;
	
	isTodayActiv() {
		let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
			let weekDay = curDate.getDay();
			let weekDayFlag = DailyActivCalendar.arrWeekDayFlag[weekDay + ""];
			let dailyActivCalendar = DailyActivCalendar.getCalendar();
			let weekDayActiv = dailyActivCalendar.find(M => M[weekDayFlag] == '光明神殿');
			if (weekDayActiv) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
	}
	
	isActivityTime() {
        let result = false;
        try {
            let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let monthDay = curDate.getDate();
            let weekDay = curDate.getDay();
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
            if (curHours == 21 && curMinutes >= 30) {
				result = true;
			}
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }
	
	initFloorCfg() {
		this.floorCfg = JSON.parse(JSON.stringify(SingletonMap.o_O.o_UCH));
	}
	
	clearFloorCfg() {
		this.floorCfg = null;
	}
	
	updateFloorCfg(floor, id, flag) {
		try {
			if (this.floorCfg == null) {
				this.initFloorCfg();
			}
			if (floor && id) {
				this.isDebug && console.warn(`第${floor}层--ID=${id}--${flag}`);
				this.isDebug && console.warn('更新前:' + JSON.stringify(this.floorCfg[floor - 1].npcList));
				let cfg = this.floorCfg[floor - 1].npcList.find(M => M.id == id);
				cfg.flag = flag;
				this.isDebug && console.warn('更新后:' + JSON.stringify(this.floorCfg[floor - 1].npcList));
			}
		}
		catch(ex){
			console.error(ex);
		}
	}
}

// 圣尊战场boss
class ShenZunZhangChanBossTask extends StaticGuaJiTask {
    static name = "ShenZunZhangChanBossTask";
    curRefreshBoss = null;
	isOver = false;

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
			if (SingletonMap.o_eB9.o_Hxo == 0) return false;
			if (!this.hasCrossBoss()) return false;
			if (!this.hasLocalBoss() && !this.hasCrossBoss()) return false;
			if ($.isEmptyObject(SingletonMap.o_eB9.o_HxM)) {
				result = true;
			}
			else if (this.getRefreshBoss()) {
				result = true;
			}
			else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	getArrBossCfg() {
		let result = [];
		try {
			let arrBossCfg = [];
			let sysBossCfg = SingletonMap.BossProvider.o_HIm();
			for (let i = 0; i < sysBossCfg.length; i++)  {
				let item = sysBossCfg[i];
				let layer = item.Layer.map(M => {
					M.crossFlag = item.crossFlag;
					M.id = item.id;
					return M;
				});
				result = result.concat(layer);
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	hasCrossBoss() {
		let result = false;
		try {
			let strSelBoss = getAppConfigKeyValue("ShenZunZhangChanBoss");
			let arrSelBoss = strSelBoss.split("|");
			for (let i = 0; i < arrSelBoss.length; i++) {
				let bossId = parseInt(arrSelBoss[i]);
				let bossCfg = this.getBossCfg(bossId);
				if (isCrossTime() && bossCfg && bossCfg.crossFlag == 1) {
					result = true;
					break;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	hasLocalBoss() {
		let result = false;
		try {
			let strSelBoss = getAppConfigKeyValue("ShenZunZhangChanBoss");
			let arrSelBoss = strSelBoss.split("|");
			for (let i = 0; i < arrSelBoss.length; i++) {
				let bossId = parseInt(arrSelBoss[i]);
				let bossCfg = this.getBossCfg(bossId);
				if (bossCfg && bossCfg.crossFlag == 0) {
					result = true;
					break;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	getBossCfg(bossId) {
		let result = null;
		try {
			let arrBossCfg = this.getArrBossCfg();
			result = arrBossCfg.find(M => M.bossList.includes(bossId));
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	getBossRefreshTime(bossId) {
		let result = null;
		try {
			let bossCfg = this.getBossCfg(bossId);
			if (bossCfg) {
				let mapBossTime = SingletonMap.o_eB9.o_HxM[bossCfg.mapId];
				if (mapBossTime){
					result = mapBossTime[bossId];
				}
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    getRefreshBoss() {
        let boss = null;
        try {
			let strSelBoss = getAppConfigKeyValue("ShenZunZhangChanBoss");
			let arrSelBoss = strSelBoss.split("|");
			for (let i = 0; i < arrSelBoss.length; i++) {
				let bossId = parseInt(arrSelBoss[i]);
				let bossRefreshTime = this.getBossRefreshTime(bossId);
				// 没时间表示没进过地图
				if ((!bossRefreshTime || bossRefreshTime == 0 ) && (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId])) {
					let bossCfg = this.getBossCfg(bossId);
					if (isCrossTime() && bossCfg.crossFlag == 1 || bossCfg.crossFlag == 0) {
						bossCfg.bossId = bossId;
						bossCfg.bossIndex = bossCfg.bossList.findIndex(M => M == bossId);
						bossCfg.pos = bossCfg.bossPosList[bossCfg.bossIndex];
						boss = bossCfg;
						break;
					}
				}
			}
			
			if (boss == null) { // 寻找30秒内刷新的
				for (let i = 0; i < arrSelBoss.length; i++) {
					let bossId = parseInt(arrSelBoss[i]);
					let bossRefreshTime = this.getBossRefreshTime(bossId);
					if ((!bossRefreshTime || bossRefreshTime == 0 || bossRefreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) && (!bossNextWatchTime[bossId] || SingletonMap.ServerTimeManager.o_eYF >= bossNextWatchTime[bossId])) {
						let bossCfg = this.getBossCfg(bossId);
						if (isCrossTime() && bossCfg.crossFlag == 1 || bossCfg.crossFlag == 0) {
							bossCfg.bossId = bossId;
							bossCfg.bossIndex = bossCfg.bossList.findIndex(M => M == bossId);
							bossCfg.pos = bossCfg.bossPosList[bossCfg.bossIndex];
							boss = bossCfg;
							break;
						}
					}
				}
			}
        }
        catch (e) {
            console.error(e);
        }
        return boss;
    }
	
    async refreshBossTime() {
        try {
            SingletonMap.Boss2Sys.o_HIB(SingletonMap.MapData.sceneId);
            await xSleep(100);
        }
        catch (e) {
            console.error(e);
        }
    }

    async enterMap() {
        try {
			if (this.curRefreshBoss && SingletonMap.MapData.mapId != this.curRefreshBoss.mapId) {
				if (this.curRefreshBoss.crossFlag == 1) {
					SingletonMap.o_Yr.o_NZ8(KfActivity.XJPREDORBOSS, this.curRefreshBoss.id, this.curRefreshBoss.layer);
				}
				else {
					SingletonMap.Boss2Sys.o_Hc5(this.curRefreshBoss.id, this.curRefreshBoss.layer);
				}
			}
			await xSleep(10);
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
			await this.gotoTianJie();
			let nextBoss = this.getRefreshBoss();
			while(nextBoss) {
				if (!this.isRunning()) break;
				if (!mainRobotIsRunning()) break;
				if (this.stopTask) break;
				if (SingletonMap.o_eB9.o_Hxo == 0) break;
				await this.enterMap();
				await this.refreshBossTime();
				await this.waitTaskFinish(nextBoss);
				nextBoss = this.getRefreshBoss();
			}
        }
        catch (e) {
            console.error(e);
        }
		TaskStateManager.updateTaskEnterState(this.constructor.name);
		this.curRefreshBoss = null; 
        await this.stop();
    }

    initState() {
        super.initState();
        this.curRefreshBoss = null;
    }
	
	async waitTaskFinish(nextBoss) {
        let self = this;
		this.curRefreshBoss = nextBoss;
		FirstKnife.setTargetCfgId(nextBoss.bossId);
        function check() {
            try {
                return xSleep(300).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (self.stopTask) return;
					if (SingletonMap.o_eB9.o_Hxo == 0) return;
					if (!nextBoss) return;
					if (nextBoss.crossFlag == 1 && !isCrossTime()) {
						return;
					}
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					await self.enterMap();
					// 先打人
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer && GlobalUtil.compterFindWayPos(nextBoss.pos[0], nextBoss.pos[1]).length <= 7) {
						self.findWayIsStop = true;
						setCurMonster(badPlayer);
						self.gotoTarget(badPlayer);
						SingletonMap.o_OKA.o_Pc(badPlayer, true);
						return check();
					}
					
					let monster = GlobalUtil.getMonsterById(nextBoss.bossId);
					// 没发现怪，前往boss坐标
					if (!monster && GlobalUtil.compterFindWayPos(nextBoss.pos[0], nextBoss.pos[1]).length >= 6) {
						clearCurMonster();
						SingletonMap.o_OKA.o_NSw(nextBoss.pos[0], nextBoss.pos[1]);
						return check();
					}
					// 没归属或归属是自己
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) { 
						if (GlobalUtil.compterFindWayPos(nextBoss.pos[0], nextBoss.pos[1]).length >= 6)  {
							SingletonMap.o_OKA.o_NSw(nextBoss.pos[0], nextBoss.pos[1]);
						}
						self.findWayIsStop = true;
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					// 归属黑名单
					else if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer) {
							self.findWayIsStop = true;
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							return check();
						}
					}
					// 归属白名单
					else if (monster && BlacklistMonitors.isWhitePlayer(monster.belongingName)) {
						self.findWayIsStop = true;
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					else if (monster && !(isNullBelong(monster) || isSelfBelong(monster))) { // 是别人归属的boss,10分钟内不打
						bossNextWatchTime[nextBoss.bossId] = SingletonMap.ServerTimeManager.o_eYF + 1000 * 60 * 10; 
						console.warn(`boss归属${monster.belongingName},离开不打...`);
						return;
					}
					let refreshTime = self.getBossRefreshTime(nextBoss.bossId);
					if (refreshTime - SingletonMap.ServerTimeManager.o_eYF <= 30000) {
						SingletonMap.o_lAu.stop();
						return check();
					}
					return;
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }

    async backToMap() {
        try {
            if (this.curRefreshBoss) {
                await this.enterMap();
                await this.findWayByPos(this.curRefreshBoss.pos[0], this.curRefreshBoss.pos[1], 'findWay', SingletonMap.MapData.curMapName);
            }
        } catch (ex) {
            console.error(ex);
        }
        await xSleep(100);
        this.diePos = null;
    }
}

// 圣尊战场小怪
class ShenZunZhangChanNormalBossTask extends StaticGuaJiTask {
    static name = "ShenZunZhangChanNormalBossTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
			if (this.hasGuaJiMap()) { 
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	hasGuaJiMap() {
		let result = null;
        try {
			let strMap = getAppConfigKeyValue("ShenZunZhangChanNormalBoss");
			let arrMap = strMap.split("|");
			let arrBossCfg = this.getArrBossCfg();
			for (let i = 0; i < arrMap.length; i++) {
				let mapId = parseInt(arrMap[i]);
				let mapCfg = arrBossCfg.find(M => M.mapId == mapId);
				if (mapCfg) {
					if (isCrossTime() && mapCfg.crossFlag == 1 || mapCfg.crossFlag == 0) {
						result = mapCfg;
						break;
					}
				}
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getGuaJiMap(mapId) {
		let result = null;
        try {
			let arrBossCfg = this.getArrBossCfg();
			let mapCfg = arrBossCfg.find(M => M.mapId == mapId);
			let mapInfo = SingletonMap.o_lb7.o_OX1(mapId);
			if (mapInfo.dayrecharge == 0 || SingletonMap.o_NP8.o_lYE >=  mapInfo.dayrecharge) {
				if (isCrossTime() && mapCfg && mapCfg.crossFlag == 1 || mapCfg.crossFlag == 0) {
					result = mapCfg;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	getArrBossCfg() {
		let result = [];
		try {
			let arrBossCfg = [];
			let sysBossCfg = SingletonMap.BossProvider.o_HIm();
			for (let i = 0; i < sysBossCfg.length; i++)  {
				let item = sysBossCfg[i];
				let layer = item.Layer.map(M => {
					M.crossFlag = item.crossFlag;
					M.id = item.id;
					return M;
				});
				result = result.concat(layer);
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}

    async enterMap(arrMap) {
        try {
			if (!arrMap) return;
			let arrBossCfg = this.getArrBossCfg();
			for (let i = 0; i < arrMap.length; i++) {
				let mapId = parseInt(arrMap[i]);
				let mapCfg = arrBossCfg.find(M => M.mapId == mapId);
				let mapInfo = SingletonMap.o_lb7.o_OX1(mapId);
				if (mapInfo.dayrecharge == 0 || SingletonMap.o_NP8.o_lYE >=  mapInfo.dayrecharge) {
					if (isCrossTime() && mapCfg && mapCfg.crossFlag == 1 || mapCfg.crossFlag == 0) {
						if (SingletonMap.MapData.mapId == mapCfg.mapId) {
							break;
						}
						else {
							await this.exitScene();
							if (mapCfg.crossFlag == 1) {
								SingletonMap.o_Yr.o_NZ8(KfActivity.XJPREDORBOSS, mapCfg.id, mapCfg.layer);
							}
							else {
								SingletonMap.Boss2Sys.o_Hc5(mapCfg.id, mapCfg.layer);
							}
							break;
						}
					}
				}
			}
			await xSleep(10);
        } catch (ex) {
            console.error(ex);
        }
    }
	
	async enterMapOnce(arrMap) {
        try {
			let arrBossCfg = this.getArrBossCfg();
			for (let i = 0; i < arrMap.length; i++) {
				let mapId = parseInt(arrMap[i]);
				let mapCfg = arrBossCfg.find(M => M.mapId == mapId);
				if (mapCfg) {
					let mapInfo = SingletonMap.o_lb7.o_OX1(mapId);
					if (mapInfo.dayrecharge == 0 || SingletonMap.o_NP8.o_lYE >=  mapInfo.dayrecharge) {
						if (isCrossTime() && mapCfg.crossFlag == 1 || mapCfg.crossFlag == 0) {
							if (SingletonMap.MapData.mapId == mapCfg.mapId) {
								if (mapCfg.crossFlag == 1) {
									// SingletonMap.o_Yr.o_NZ8(KfActivity.XJPREDORBOSS, mapCfg.id, mapCfg.layer);
									useShuiJiProperty();
								}
								else {
									SingletonMap.Boss2Sys.o_Hc5(mapCfg.id, mapCfg.layer);
								}
								break;
							}
						}
					}
				}
			}
			await xSleep(10);
        } catch (ex) {
            console.error(ex);
        }
    }

    async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
			await this.gotoTianJie();
			await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	getSelectMap() {
		let result = [];
		try {
			let strMap = getAppConfigKeyValue("ShenZunZhangChanNormalBoss");
			result = strMap.split("|");
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	existsBoss() {
		let result = false;
		try {
			let arrBossCfg = this.getArrBossCfg();
			let arrBossList = arrBossCfg.map(M => {return M.bossList});
			let arrBossCfgId = arrBossList.flat(); // 转成1维数组
			for (let i = 0;i < SingletonMap.o_OFM.o_Nx1.length; i++) {
				let item = SingletonMap.o_OFM.o_Nx1[i];
				if (item && arrBossCfgId.includes(item.cfgId)) {
					result = true;
					break;
				}
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
    }
	
	filterMonster(monster) {
		let result = true;
		try {
			let arrBossCfg = this.getArrBossCfg();
			let arrBossList = arrBossCfg.map(M => {return M.bossList});
			let arrBossCfgId = arrBossList.flat(); // 转成1维数组
			if (monster && arrBossCfgId.includes(monster.cfgId)) {
				result = false;
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	async waitTaskFinish() {
        let self = this;
		let arrMap = this.getSelectMap();
        function check() {
            try {
                return xSleep(1000).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (self.stopTask) return;
					if (!self.hasGuaJiMap()) return;
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return check();
					if (arrMap.length == 0) { arrMap = self.getSelectMap();}
					
					//人物死亡
					if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
						return check();
					}
					
					// 返回地图
					if (!isShenZunMap()) {
						await self.enterMap(arrMap);
						return check();
					}
					SingletonMap.o_lAu.stop();
					
					// 发现boss,boss归属还没打完 离开不打
					if (self.existsBoss() && SingletonMap.o_eB9.o_Hxo > 0 && getAppConfigKeyValue("NoSeeShenZunBoss") == "TRUE") {
						// console.warn(`发现boss,boss归属次数还没打完,躲开boss...`);
						await self.enterMapOnce(arrMap);
					}
					
					// 先打人
					let badPlayer = BlacklistMonitors.getBadPlayer();
					if (badPlayer) {
						self.findWayIsStop = true;
						setCurMonster(badPlayer);
						self.gotoTarget(badPlayer);
						SingletonMap.o_OKA.o_Pc(badPlayer, true);
						return check();
					}
					
					let monster = self.findMonter();
					// 没归属或归属是自己
					if (monster && (isNullBelong(monster) || isSelfBelong(monster))) { 
						self.findWayIsStop = true;
						setCurMonster(monster);
						SingletonMap.o_OKA.o_Pc(monster, true);
						return check();
					}
					// 归属黑名单
					else if (monster && BlacklistMonitors.isBadPlayer(monster.belongingName) && BlacklistMonitors.getBadPlayer()) {
						let belongingBadPlayer = GlobalUtil.getMonsterByName(monster.belongingName);
						if (belongingBadPlayer) {
							self.findWayIsStop = true;
							setCurMonster(belongingBadPlayer);
							SingletonMap.o_OKA.o_Pc(belongingBadPlayer, true);
							return check();
						}
					}
					else {
						clearCurMonster();
						PlayerActor.o_NLN.o_lkM();
					}
					
					if (!monster) {
						QuickPickUpNormal.runPickUp();
						await xSleep(800);
						await self.enterMapOnce(arrMap);
					}
					
					let bossThreshold = getAppConfigKeyValue("BossThreshold") || 3;
					bossThreshold = parseInt(bossThreshold);
					// 剩余数量不足 并且 选择了多张图挂机
					if (SingletonMap.o_eB9.o_HIT < bossThreshold && self.getSelectMap().length > 1) {
						arrMap.shift();
						await self.enterMap(arrMap);
						return check();
					}
					return check();
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 扫荡玛法庄主
class mafaZYBossTask extends StaticGuaJiTask {
	static name = "mafaZYBossTask";

    needRun() {
        let result = false;
        try {
            if (this.isRunning()) return false;
            if (!mainRobotIsRunning()) return false;
			if(this.getTimes() > 0) {
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
    }
	
	async start() {
        try {
            if (this.isRunning()) return;
			if (!mainRobotIsRunning()) return false;
			this.initState();
			await this.gotoWangChen();
			await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	getTimes() {
		let result = 0;
		try {
			if (SingletonMap.o_Ue.o_Ngt(SingletonMap.o_O.o_Pu.mafaZYBossChallengeCost.id) >= SingletonMap.o_O.o_Pu.mafaZYBossChallengeCost.count) {
				result = Math.max(0, SingletonMap.o_O.o_Pu.mafaZYBossChallengeTime - SingletonMap.o_NL6.getCountById(EntimesType.MAFAZYBOSSTIME));
			}
		}
		catch (e) {
            console.error(e);
        }
		return result;
	}
	
	async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(300).then(async function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
					if (self.stopTask) return;
					if (SingletonMap.o_n.isOpen("CrossLoadingWin")) return check();
					if (self.getTimes() > 0) {
						if (!SingletonMap.o_n.isOpen("XiyouEntranceWin")) {
							SingletonMap.o_n.o_EG("XiyouEntranceWin", [1]);
							return check();
						}
						if (SingletonMap.o_yLF.o_yzz.o_yBi > 0)
						{
							if (!SingletonMap.o_n.isOpen("XiyouAchieveQuickFinishMFZYBossSuijiTipsWin")) {
								SingletonMap.o_n.o_EG("XiyouAchieveQuickFinishMFZYBossSuijiTipsWin");
								return check();
							}
							
							if (!SingletonMap.o_n.isOpen("XiyouAchieveQuickFinishResultWin")) {
								SingletonMap.o_yFu.o_yB0(1, 0, 0);
								return check();
							}
							else {
								closeGameWin("XiyouAchieveQuickFinishResultWin");
								return check();
							}
						}
						else {
							SingletonMap.o_yFu.o_yLj();
							return check();
						}
					}
					closeGameWin("XiyouAchieveQuickFinishResultWin");
					closeGameWin("XiyouAchieveQuickFinishMFZYBossSuijiTipsWin");
					closeGameWin("XiyouEntranceWin");
					return;
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 买挖矿次数
class BuyMineTimesTask extends StaticGuaJiTask {
	static name = 'BuyMineTimesTask';
	needRun() {
		let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
			if (getAppConfigKeyValue("EnableBuyMineTimes") != "TRUE") return false;
            if (this.hasTimes()) {
                result = true;
            }
			else if (TaskStateManager.isFirtTimeEnter(this.constructor.name)) {
				TaskStateManager.updateTaskEnterState(this.constructor.name);
				result = true;
			}
        }
        catch (e) {
            console.error(e);
        }
        return result;
	}
	
	hasTimes() {
		let result = false;
		try {
			 // 购买次数已达上限
			if (SingletonMap.o_O.o_lgc.dailyBuyCount - SingletonMap.o_e_w.o_lhj <= 0) {
				return false;
			}
			let roleCost = SingletonMap.RoleData.o_NM3(SingletonMap.o_O.o_lgc.dailyBuyCost.id); // 钻石数量
			let buyCost = SingletonMap.o_O.o_lgc.dailyBuyCost.count; // 购买挖矿次数需要多少钻石
			if (roleCost >= buyCost) {
				result = true;
			}
		}
		catch (e) {
            console.error(e);
        }
        return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
            await this.waitTaskFinish();
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	async waitTaskFinish() {
        let self = this;
        function check() {
            try {
                return xSleep(500).then(function () {
                    if (!self.isRunning()) return;
                    if (!mainRobotIsRunning()) return;
                    if (self.hasTimes()) {
						SingletonMap.o_OzT.o_evH();
						return check();
                    }
                    return;
                });
            }
            catch (e) {
                console.error(e);
            }
        }
        return check();
    }
}

// 神龙托管
class ShenLongTGTask extends StaticGuaJiTask {
	static name = 'ShenLongTGTask';
	nextRunTime = null;
	maxTgHours = 12;
	
	needRun() {
		let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.isActivityTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			var Q = SingletonMap.o_O.o_l5j[11];
			var E = SingletonMap.o_eG.o_N5Q(Q.type);
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
			if (E[0] && curHours >= 10 && curHours < 22) {
				if (this.nextRunTime == null || this.nextRunTime >= SingletonMap.ServerTimeManager.o_eYF || this.isLastTGTime()) {
					result = true;
				}
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	isLastTGTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 21 && curMinutes >= 30) {
				result = true;
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	getTGHours() {
		let result = 0;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours >= 10 && curHours < 22) {
				let diffHours = 21 - curHours;
				let diffMinutes = 30 - curMinutes;
				if (diffHours > 0) {
					diffMinutes = diffMinutes + diffHours * 60;
				}
				if (diffMinutes > 0) {
					result = Math.floor(diffMinutes / 60);
				}
			}
		}
		 catch (e) {
            console.error(e);
        }
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			if (this.isLastTGTime()) {
				this.getAward();
				await xSleep(500);
				SingletonMap.Boss2Sy.o_ULn(1, this.maxTgHours); // 神龙托管				
			}
			else {
				let tgHours = this.getTGHours();
				if (tgHours > 0) {
					this.getAward();
					await xSleep(500);
					SingletonMap.Boss2Sy.o_ULn(1, tgHours); // 神龙托管
				}
			}
			this.nextRunTime = SingletonMap.o_eB9.o_ULF.getTime + 5000;
        }
        catch (e) {
            console.error(e);
        }		
        await this.stop();
    }
	
	getAward() {
		try {
			if (SingletonMap.o_eB9.o_ULF.getTime > SingletonMap.ServerTimeManager.o_eYF) {
				SingletonMap.Boss2Sys.o_Uze();
			}
		}
		catch(e){
			console.error(e);
		}
	}
}

// 玛法托管
class MafaTGTask extends StaticGuaJiTask {
	static name = 'MafaTGTask';
	nextRunTime = null;
	maxTgHours = 10;
	nextTGTime = {
		'1': 0,
		'2': 0,
		'3': 0,
		'4': 0,
		'5': 0
	}
	
	needRun() {
		let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (this.hasActivity() && this.isActivityTime() && (this.nextRunTime == null || SingletonMap.ServerTimeManager.o_eYF >= this.nextRunTime)) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			var b = SingletonMap.o_O.o_l5j[12];
			var r = SingletonMap.o_eG.o_N5Q(b.type);
			if (!r[0]) { // 未激活特权
				return;
			}
			
			if (0 == SingletonMap.o_Ntl.o_NxW) {// 没开启玛法别墅
				return;
			}
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
			if (E[0] && curHours >= 10 && curHours < 22) {
				if (this.hasTGMap()) {
					result = true;
				}
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	hasActivity() {
		let result = false;
		try {
			var Q = SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL);
			if (getAppConfigKeyValue('EnableChiYueTG') == 'TRUE' && Q >= 12 ) { // 赤月
				result = true;
			}
			else if (getAppConfigKeyValue('EnableZhangShenTG') == 'TRUE' && Q >= 12 ) { // 战神
				result = true;
			}
			else if (getAppConfigKeyValue('EnableTianLongTG') == 'TRUE' && Q >= 24 ) { // 天龙
				result = true;
			}
			else if (getAppConfigKeyValue('EnableDuShiTG') == 'TRUE' && Q >= 24 ) { // 都市
				result = true;
			}
			else if (getAppConfigKeyValue('EnableXianLinTG') == 'TRUE' && Q >= 24 ) { // 仙灵
				result = true;
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			await this.getAward();
			if (getAppConfigKeyValue('EnableChiYueTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[1].endTime) {
				if (this.getTGHours(1) > 0) {
					SingletonMap.o_Ozj.o_UBy(1, this.getTGHours(1));
					await xSleep(500);
					this.nextTGTime['1'] = SingletonMap.o_Ntl.o_Ujq[1].endTime + 10000;
				}
			}
			if (getAppConfigKeyValue('EnableZhangShenTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[2].endTime) {
				if (this.getTGHours(2) > 0) {
					SingletonMap.o_Ozj.o_UBy(2, this.getTGHours(2));
					await xSleep(500);
					this.nextTGTime['2'] = SingletonMap.o_Ntl.o_Ujq[2].endTime + 10000;
				}
			}
			if (getAppConfigKeyValue('EnableTianLongTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[3].endTime) {
				if (this.getTGHours(3) > 0) {
					SingletonMap.o_Ozj.o_UBy(3, this.getTGHours(3));
					await xSleep(500);
					this.nextTGTime['3'] = SingletonMap.o_Ntl.o_Ujq[3].endTime + 10000;
				}
			}
			if (getAppConfigKeyValue('EnableDuShiTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[4].endTime) {
				if (this.getTGHours(4) > 0) {
					SingletonMap.o_Ozj.o_UBy(4, this.getTGHours(4));
					await xSleep(500);
					this.nextTGTime['4'] = SingletonMap.o_Ntl.o_Ujq[4].endTime + 10000;
				}
			}
			if (getAppConfigKeyValue('EnableXianLinTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[5].endTime) {
				if (this.getTGHours(5) > 0) {
					SingletonMap.o_Ozj.o_UBy(5, this.getTGHours(5));
					await xSleep(500);
					this.nextTGTime['5'] = SingletonMap.o_Ntl.o_Ujq[5].endTime + 10000;
				}
			}
			this.nextRunTime = ServerTimeManager.instance.o_eYF + 60 * 1000 * 60; // 1小时领一次
        }
        catch (e) {
            console.error(e);
        }
        await this.stop();
    }
	
	hasTGMap() {
		let result = false;
		try {
			if (getAppConfigKeyValue('EnableChiYueTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[1].endTime) {
				result = true;
			}
			else if (getAppConfigKeyValue('EnableZhangShenTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[2].endTime) {
				result = true;
			}
			else if (getAppConfigKeyValue('EnableTianLongTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[3].endTime) {
				result = true;
			}
			else if (getAppConfigKeyValue('EnableDuShiTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[4].endTime) {
				result = true;
			}
			else if (getAppConfigKeyValue('EnableXianLinTG') == 'TRUE' && ServerTimeManager.instance.o_eYF >= SingletonMap.o_Ntl.o_Ujq[5].endTime) {
				result = true;
			}
		}
		 catch (e) {
            console.error(e);
        }
		return result;
	}
	
	hasAward(mapId) {
		try {
			var g = ServerTimeManager.instance.o_eYF;
			var C = g >= SingletonMap.o_Ntl.o_Ujq[mapId].endTime;
			var L = SingletonMap.o_Ntl.o_Ujq[mapId].getTime;
			if (!(!C && L > g)) {
				return true;
			}
		}
		catch(e) {
			console.error(e);
		}
		return false;
	}
	
	async getAward() {
		try {
			for(let i = 1; i < 6; i++) {
				// 托管前先领奖励
				loopRunRecy();
				if (this.hasAward(i)) {
					SingletonMap.o_Ozj.o_UKN(i);
					await xSleep(500);
				}
			}
		}
		catch(e) {
			console.error(e);
		}		
	}
	
	getTGHours(mapId) {
		let result = 0;
		try {
			let maxHours = 0;
			let Q = SingletonMap.RoleData.o_NM3(appInstanceMap.MoneyType.o_lCL);
			if  (mapId <= 2) {
				maxHours = Math.floor(Q / 12);
			}
			else if (mapId > 2 && mapId <= 5) {
				maxHours = Math.floor(Q / 24);
			}
			let cfgHours = this.getTGCfgHours(mapId);
			if (maxHours >= cfgHours) {
				result = cfgHours;
			}
			else {
				result = maxHours;
			}
		}
		catch(e) {
			console.error(e);
		}
		return result;
	}
	
	getTGCfgHours(mapId) {
		let result = 0;
		try {
			if (mapId == 1) {
				result = parseInt(getAppConfigKeyValue('ChiYueTGHour'));
			}
			else if (mapId == 2) {
				result = parseInt(getAppConfigKeyValue('ZhangShenTGHour'));
			}
			else if (mapId == 3) {
				result = parseInt(getAppConfigKeyValue('TianLongTGHour'));
			}
			else if (mapId == 4) {
				result = parseInt(getAppConfigKeyValue('DuShiTGHour'));
			}
			else if (mapId == 5) {
				result = parseInt(getAppConfigKeyValue('XianLinTGHour'));
			}
		}
		catch(e) {
			console.error(e);
		}
		return result;
	}
}

// 西游托管
class XiYouTGTask extends StaticGuaJiTask {
	static name = 'XiYouTGTask';
	nextRunTime = null;
	maxTgHours = 6;
	
	needRun() {
		let result = false;
        try {
			if (this.isRunning()) return false;
			if (!mainRobotIsRunning()) return false;
            if (SingletonMap.RoleData.o_N5T() > 0 && this.isActivityTime()) {
                result = true;
            }
        }
        catch (e) {
            console.error(e);
        }
        return result;
	}
	
	isActivityTime() {
		let result = false;
		try {
			if (this.nextRunTime == null || this.nextRunTime >= SingletonMap.ServerTimeManager.o_eYF || this.isLastTGTime()) {
				result = true;
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	isLastTGTime() {
		let result = false;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours == 21 && curMinutes >= 30) {
				result = true;
			}
		}
		catch (e) {
			console.error(e);
		}
		return result;
	}
	
	getTGHours() {
		let result = 0;
		try {
			let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
            let curHours = curDate.getHours();
            let curMinutes = curDate.getMinutes();
			if (curHours >= 10 && curHours < 22) {
				let diffHours = 21 - curHours;
				let diffMinutes = 30 - curMinutes;
				if (diffHours > 0) {
					diffMinutes = diffMinutes + diffHours * 60;
				}
				if (diffMinutes > 0) {
					result = Math.floor(diffMinutes / 60);
				}
			}
		}
		 catch (e) {
            console.error(e);
        }
		return result;
	}
	
	async start() {
        try {
            if (this.isRunning()) return;
            if (!mainRobotIsRunning()) return;
			this.initState();
            await this.gotoWangChen();
			let tgLayer = getAppConfigKeyValue('XiyouTGMap');
			if (this.isLastTGTime()) {
				this.getAward();
				await xSleep(500);
				if (tgLayer) {
					SingletonMap.o_yK.o_UaU(parseInt(tgLayer), this.maxTgHours); // 托管				
				}
			}
			else {
				let tgHours = this.getTGHours();
				if (tgHours > 0) {
					this.getAward();
					await xSleep(500);
					SingletonMap.o_yK.o_UaU(parseInt(tgLayer), tgHours);
				}
			}
			this.nextRunTime = SingletonMap.XiyouMgr.o_l7m.getTime + 10000;
        }
        catch (e) {
            console.error(e);
        }		
        await this.stop();
    }
	
	getAward() {
		try {
			if (SingletonMap.XiyouMgr.o_l7m.getTime < SingletonMap.ServerTimeManager.o_eYF) {
				SingletonMap.o_yKf.o_UEZ();
			}
		}
		catch(e){
			console.error(e);
		}
	}
}

// 不自动检测黑名单的任务
var noAutoCheckBadPlayerTask = [CrossHuangLingTask.name, LianYuBossTask.name, CrossCangYueBossTask.name, XiaGuZhiZhanTask.name, ShenMoWarTask.name, XiyouTianLaoBossTask.name];
function autoCheckBadPlayer() {
	let result = true;
	try {
		let curRunningTask = SingletonModelUtil.getRunningTask();
		if (curRunningTask && noAutoCheckBadPlayerTask.includes(curRunningTask.constructor.name)) {
			result = false;
		}
	}
	catch(e){}
	return result;
}

// 全局参数: 死亡不回城
var arrNoBackTaskMap=[MafaChiYueBossTask.name,MafaChiYueStar2Task.name,MafaChiYueStar1Task.name,MafaMoLongBossTask.name,MafaMoLongStar1Task.name,MafaMoLongStar2Task.name,MafaHuoLongBossTask.name,MafaHuoLongStar2Task.name,MafaHuoLongStar1Task.name,MafaDuShiBossTask.name,MafaDuShiStar1Task.name,MafaDuShiStar2Task.name,MafaXianLingBossTask.name,MafaXianLingStar2Task.name,MafaXianLingStar1Task.name,villaDuShiBossTask.name,villaDuShiStar1Task.name,villaDuShiStar2Task.name,villaHuoLongBossTask.name,villaHuoLongStar2Task.name,villaHuoLongStar1Task.name,villaMoLongBossTask.name,villaMoLongStar1Task.name,villaMoLongStar2Task.name,villaChiYueBossTask.name,villaChiYueStar2Task.name,villaChiYueStar1Task.name,villaXianLingBossTask.name,villaXianLingStar2Task.name,villaXianLingStar1Task.name,villaShenLongZhuanZhuTask.name,villaShenLongWangTask.name,villaShenLongStarTask.name];

// 不检查攻击模式
var arrNoNeedChangeModeMap = "|鸿蒙圣境(噩梦)|鸿蒙圣境(普通)|寻龙秘境|1V1巅峰竞赛|1V1巅峰争霸|神魔战场|跨服3v3天梯赛|巅峰皇城|巅峰主城|巅峰皇宫|蟠桃盛宴|神魔边境|神族主城|魔族主城|皇城|神魔深渊一层|神魔深渊二层|跨服沙皇宫|刺激战场|职业巅峰竞赛主城|职业巅峰竞赛场|峡谷决战|跨服沙巴克|皇宫|魔龙密窟|修罗战场|";

// 例外
var arrExceptionPri = [{
	pf: 'cq7you',
	roleId:	'436582424'
},{
	pf: 'cq7you',
	roleId:	'436588884'
},{
	pf: 'cq7you',
	roleId:	'436498383'
},{
	pf: 'cq7you',
	roleId:	'436489565'
},{
	pf: 'cqbd',
	roleId:	'235331874'
},{
	pf: 'cq7you',
	roleId:	'235326456'
}];

function isExceptionPri() {
	let result = false;
	try {
		let cfg = arrExceptionPri.find(M => M.pf == SingletonMap.ServerChooseData.pf && M.roleId == SingletonMap.ServerChooseData.enterRole.id);
		if (cfg) {
			result = true;
		}
	}
	catch(ex) {}
	return result;
}

(function(){console.warn('init finish...', true);})();
function doTest(){console.error('test...', true);}