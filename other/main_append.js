// 脚本开始
function isEmptyObjectFun(e) {
    var t;
    for (t in e)
        return !1;
    return !0
}

try {
    if ($) {
        $.isEmptyObject = $.isEmptyObject || isEmptyObjectFun
    }
    else {
        window.$ = function () {
            var M = function () { };
            M.isEmptyObject = isEmptyObjectFun;
            return M;
        }
    }
}
catch (ex) {
}

var isYearVip = false;
var isSuperVip = false;
var isSysVip = false;
var isFastSuction = false;
var isDoubleAttck = false;
var isPaid = false;
var isTestPaid = false;
var isTempPaid = false;
var isYearPaid = false;
var isQuarterPaid = false;
var isMonthPaid = false;

function setVipConfig(cfg) {
    isYearVip = cfg.isYearVip == '0SA' ? true : false;
    isSuperVip = cfg.isSuperVip == '0SA' ? true : false;
    isSysVip = cfg.isSysVip == '0SA' ? true : false;
    isFastSuction = cfg.isFastSuction == '0SA' ? true : false;
    isDoubleAttck = cfg.isDoubleAttck == '0SA' ? true : false;
    isPaid = cfg.isPaid == '0SA' ? true : false;
    isTestPaid = cfg.isTestPaid == '0SA' ? true : false;
    isTempPaid = cfg.isTempPaid == '0SA' ? true : false;
    isYearPaid = cfg.isYearPaid == '0SA' ? true : false;
    isQuarterPaid = cfg.isQuarterPaid == '0SA' ? true : false;
    isMonthPaid = cfg.isMonthPaid == '0SA' ? true : false;
}

function isSuperPri() {
    return isPaid == true || isTestPaid == true || isTempPaid == true;
}

function isYearPri() {
    return isYearPaid == true;
}

function isQuarterPri() {
    return isQuarterPaid == true;
}

function isMonthPri() {
    return isMonthPaid == true;
}

function hasPri() {
    return isSuperPri() || isYearPri() || isQuarterPri() || isMonthPri();
}

window.appInstanceMap = {};
function doVerify(pt, roleId) {
    try {
        const _roleId = SingletonMap.ServerChooseData.enterRole.id;
        const _pf = SingletonMap.ServerChooseData.pf;
        if (_roleId != roleId || _pf != pt) {
            confirm('用户信息异常!');
            window.location.href = 'about: blank';
        }
    } catch (ex) {
        console.error(ex);
    }
}

var appConfig = ""; // 托管配置
function getAppConfig() {
    appConfig = appConfig || "";
    return appConfig;
}
function updateAppConfig(cfg) {
    appConfig = cfg;
}

var pringRecvMsg = false;
function enablePringRecvMsg() {
    pringRecvMsg = true;
}

function disablePringRecvMsg() {
    pringRecvMsg = false;
}

var taskFlag = true;
function enableTaskFlag() {
    taskFlag = true;
}

function disableTaskFlag() {
    taskFlag = false;
}

function buf2hex(buffer) {
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

function hex2buf(hex) {
    if (!hex || !hex.match) return;
    var typedArray = new Uint8Array(hex.match(/[\da-f]{2}/gi).map(function (h) {
        return parseInt(h, 16)
    }));
    return typedArray.buffer;
}

function gameMsg(arraybufmsg) {
    var self = this;
    this.data = new DataView(arraybufmsg),
        this._bytes = new Uint8Array(arraybufmsg),
        this._position = 0,
        this.write_position = 0;
    // this.$endian = "bigEndian";
    this.$endian = "littleEndian";

    this.readUnsignedShort = function () {
        var t = this.data.getUint16(this._position, 0 == this.$endian);
        this._position += 2;
        return t;
    }

    this.readUnsignedByte = function () {
        return this._bytes[this._position++] || void 0;
    }

    this.readDouble = function () {
        var t = this.data.getFloat64(this._position, 0 == this.$endian);
        return this.position += 8;
        return t;
    }
    return this;
}

var commandSocket = null;
function setCommandSocket(socket) {
    commandSocket = socket;
}

function printMsgOnErrorOld(d) {
    if (!pringRecvMsg) return;
    (async function () {
        var msg = new gameMsg(d);
        var msgPrefix = msg.readUnsignedShort();
        var msgLength = msg.readUnsignedShort();
        var i = msg.readUnsignedByte();
        var Q = msg.readUnsignedByte();
        var A = i + "-" + Q;
        var b = ["0-2", "0-5", "0-6", "0-7", "0-9", "0-18", "0-35", "0-45", "0-68", "5-7", "125-44", "26-48"];
        if (b.indexOf(A) == -1) {
            const bufhex = buf2hex(d);
            window.console.error(`sysId=${i} msgId=${Q}: ${bufhex}`);
            await xSleep(50);
        }
    })();
}

function printMsgOnError(M, Q, E) {
    if (!Q && !E) {
        printMsgOnErrorOld(M);
        return;
    }
    (async function () {
        if (!pringRecvMsg) return;
        var A = M + "-" + Q;
        var b = ["0-2", "0-5", "0-6", "0-7", "0-9", "0-18", "0-35", "0-45", "0-68", "5-7", "125-44", "26-48"];
        if (b.indexOf(A) == -1) {
            window.console.error(`sysId=${M} msgId=${Q}: ${buf2hex(E.buffer)}`);
            await xSleep(50);
        }
    })();
}

function testMsg() {
    var msg = document.getElementById('msgbox').value;
    commandSocket.send(hex2buf(msg));
}

/** 刷金币:进入行会boss界面后发起 **/
function runTask(isInit) {
    if (isInit) {
        enableTaskFlag();
    }
    if (taskFlag) {
        step1();
    }
}

function step1() {
    commandSocket.send(hex2buf('eecc06009e0ad0010000'));
    setTimeout(function () {
        step2();
    }, 10);
}

function step2() {
    commandSocket.send(hex2buf('eecc0200a002'));
    setTimeout(function () {
        step3();
    }, 10);
}

function step3() {
    commandSocket.send(hex2buf('eecc0200a003'));
    setTimeout(function () {
        runTask();
    }, 10);
}
/** 刷金币结束 **/


var TASK_FLAG = {
    NONE: 'NONE', // 空闲
    XUN_BAO: 'XUN_BAO',  // 寻宝
    SHAO_ZHU: 'SHAO_ZHU', // 烧猪
    CAI_LIAO_FUBEN: 'CAI_LIAO_FUBEN', // 材料副本
    PRIVATE_BOSS: 'PRIVATE_BOSS', // 个人boss
    DAILY_AVTIV: 'DAILY_AVTIV', // 领取每日活跃
    YuanBaoShop: 'YuanBaoShop', // 元宝商店
    GuildShop: 'GuildShop', // 行会商店

    XiYouSheLiShop: 'XiYouSheLiShop', // 西游舍利仙坊
    XiYouFuQiShop: 'XiYouFuQiShop', // 西游福气仙坊
    MafaShop: 'MafaShop', // 玛法商店
    ShenWangShop: 'ShenWangShop' // 本服声望商店
};


async function autoEverydayTask() {
    try {
        startLoopRunRecy();
        for (let i = 1; i <= 22; i++) {
            //  SingletonMap.o_eNr.o_lEq(1); // 挑战个人boss
            SingletonMap.o_eNr.o_NDs(i); // 扫荡个人boss
            startLoopRunRecy();
            await xSleep(500);
        }
        startLoopRunRecy();
        await xSleep(1000);

        SingletonMap.o_Lh.o_li6(1); // 惠民礼包1钻档
        await xSleep(100);
        SingletonMap.o_ln8.o_eOU();
        await xSleep(100);
        // 转生修为丹
        SingletonMap.o_B6.o_eqB(2);
        SingletonMap.o_B6.o_eqB(1);
        SingletonMap.o_B6.o_eqB(1);
        SingletonMap.o_B6.o_eqB(1);
        SingletonMap.o_B6.o_eqB(1);
        SingletonMap.o_B6.o_eqB(1);

        SingletonMap.o_lK4.o_OQ(1); // 每日返利免费档
        await xSleep(500);

        // 领取天界藏经阁
        SingletonMap.o_ObR.o_Ft();
        await xSleep(500);
        startLoopRunRecy();
        await xSleep(1000);

        // 天阶真气丹 10次
        for (let i = 0; i < 10; i++) {
            SingletonMap.o_e08.o_mr(1);
            await xSleep(100);
        }

        await xSleep(500);
        doShoppingNew(); // 元宝商店        
        await xSleep(500);
        doGuildShoppingNew(); // 行会商店
        await xSleep(500);
        getHanHuiBossAward();// 领取行会boss奖励
        await xSleep(500);
        startLoopRunRecy();
        await xSleep(1000);
        // 签到
        qianDao();
        await xSleep(500);

        // 西游舍利仙坊
        doXiYouSheLiShop();
        await xSleep(300);

        // 西游福气仙坊
        doXiYouFuQiShop();
        await xSleep(300);

        // 玛法商城
        doMafaShop();
        await xSleep(300);

        // 本服声望商城
        doShenWangShop();
        await xSleep(300);

        SingletonMap.o_yFu.o_yB0(3, 0, 0); // 世界boss扫荡
        startLoopRunRecy();
        await xSleep(1000);

         SingletonMap.o_yFu.o_yB0(4, 0, 0); // 苍月神殿扫荡
         startLoopRunRecy();
         await xSleep(1000);
         closeGameWin("XiyouAchieveQuickFinishResultWin");

        getShejJieDayAward();// 领取神界每日奖励
        startLoopRunRecy();
        await xSleep(1000);

        console.log('开始材料副本...........');
        await autoCaiLiaoFuBen(); // 材料副本
        console.log('材料副本结束...........');
        await xSleep(1000);
    }
    catch (ex) {
        console.error(ex);
    }
}

// 西游舍利仙坊
async function doXiYouSheLiShop() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouSheLiShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.XiYouSheLiShop))) return; // 今日已完成
                let arrShopItem = appConfig.XiYouSheLiShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_Q0.o_eiR(arrItemCode[0], arrItemCode[1], arrItemCode[2]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.XiYouSheLiShop), true);
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 西游福气仙坊
async function doXiYouFuQiShop() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouFuQiShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.XiYouFuQiShop))) return; // 今日已完成
                let arrShopItem = appConfig.XiYouFuQiShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_Q0.o_eiR(arrItemCode[0], arrItemCode[1], arrItemCode[2]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.XiYouFuQiShop), true);
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 玛法商城
async function doMafaShop() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.MafaShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.MafaShop))) return; // 今日已完成
                let arrShopItem = appConfig.MafaShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_Q0.o_eiR(arrItemCode[0], arrItemCode[1], arrItemCode[2]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.MafaShop), true);
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 本服声望商城
async function doShenWangShop() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenWangShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.ShenWangShop))) return; // 今日已完成
                let arrShopItem = appConfig.ShenWangShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_Q0.o_eiR(arrItemCode[0], arrItemCode[1], arrItemCode[2]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.ShenWangShop), true);
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

async function autoLoopDayTask() {
    try {
        if(!SingletonMap.ServerCross.o_ePe) {
            // 摇钱树
            yaoQianShu();
            await xSleep(1000);

            // 聚宝盆
            juBaoPen();
            await xSleep(1000);

            // 领取在线奖励
            zhaiXianJianLi();
            await xSleep(1000);
        }
    }
    catch (ex) {
    }
    setTimeout(autoLoopDayTask, 60000); // 一分钟一次
}

(function () {
    autoLoopDayTask();
})();

// 领取行会boss奖励
function getHanHuiBossAward() {
    for (var i = 1; i <= 20; i++) {
        SingletonMap.o_lWu.o_e8Y(i);
    }
}

// 领取神界每日奖励
function getShejJieDayAward() {
    try {
        var Q = Math.min(SingletonMap.ShenJieMgr.curLayer, SingletonMap.o_O.o_UiO.length);
        SingletonMap.o_Uij.o_Ui9(Q);
    } catch (ex) { }
}

// 元宝商店
async function doShoppingNew() {
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.YuanBaoShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.YuanBaoShop))) return; // 今日已完成
                let arrShopItem = appConfig.YuanBaoShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_Q0.o_eiR(1, arrItemCode[0], arrItemCode[1]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.YuanBaoShop), true);
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

// 行会商品
async function doGuildShoppingNew() {
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.GuildShop) {
                //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.GuildShop))) return; // 今日已完成
                let arrShopItem = appConfig.GuildShop.split("|");
                for (let i = 0; i < arrShopItem.length; i++) {
                    let arrItemCode = arrShopItem[i].split(",");
                    SingletonMap.o_lWu.o_NWn(arrItemCode[0], arrItemCode[1]);
                    await xSleep(100);
                }
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.GuildShop), true);
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

// 材料副本
async function autoCaiLiaoFuBen() {
    try {
        await gotoWangChen();
        for (var i = 0; i < SingletonMap.o_0e.arr.length; i++) {
            let item = SingletonMap.o_0e.arr[i];
            let layer = -1;
            for (var j = item.arr.length - 1; j >= 0; j--) {
                if (item.arr[j].score == 1) {
                    layer = item.arr[j].layer;
                    break;
                }
            }
            if (layer != -1 && SingletonMap.o_OFS.o_OPh(item.id) == 0) {
                SingletonMap.o_y0.o_l1h(item.id, layer);
                await xSleep(500);
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
    closeGameWin("MaterialsFBResultWin");
    await xSleep(500);
    closeGameWin("MaterialsFBView");
}

// 和平模式
function hePinModel() {
    commandSocket.send(hex2buf("eecc0300180300"));
}

var bagManager = null;
function setBagManager(pInst) {
    bagManager = pInst;
    //console.warn('setBagManager....................');
}

// 背包窗体
var bagWin = null;
function setBagWin(pInst) {
    bagWin = pInst;
    //console.warn('setBagWin....................');
}

// 回收窗体对象
var recyWin = null;
function setRecyWin(pInst) {
    recyWin = pInst;
    //console.warn('setRecyWin....................');
}

var xiyouBagManager = null;
function setXiyouBagManager(pInst) {
    xiyouBagManager = pInst;
    //console.warn('setXiyouBagManager....................');
}

var xiyouBagWin = null;
function setXiyouBagWin(pInst) {
    xiyouBagWin = pInst;
    //console.warn('setXiyouBagWin....................');
}

// 西游回收窗体对象
var xiyouRecyWin = null;
function setXiyouRecyWin(pInst) {
    xiyouRecyWin = pInst;
    //console.warn('setXiyouRecyWin....................');
}

// 分解窗口
var bagResolveView = null;
function setBagResolveView(pInst) {
    bagResolveView = pInst;
    //console.warn('分解');
}

// 八卦分解
var baGuaRecyWin = null;
function setBaGuaRecyWin(pInst) {
    baGuaRecyWin = pInst;
    // console.warn('八卦');
}

// 打开|关闭西游背包
function toggleXiyouBagWin() {
    xiyouBagManager && xiyouBagManager.o_OiC();
}

// 打开|关闭背包
function toggleBagWin() {
    bagManager && bagManager.o_OiC();
}

// 执行背包回收
function runRecy() {
    // 背包未打开
    //if (!bagWin || bagWin.$children.length == 0) {
    //    toggleBagWin();
    //}
    // bagWin && bagWin.o_Oma(1); // 选中回收    
    // recyWin && recyWin.o_lCT(); // 点回收
    if (!hasPri()) {
        return;
    }
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.RecyBag == "TRUE") {
                // 背包回收
                recyBag();
            }
            if (appConfig.RecyLongZhuang == "TRUE") {
                // 龙装回收
                longZhuangRecy();
            }
            if (appConfig.RecyYuHun == "TRUE") {
                // 御魂回收
                try {
                    SingletonMap.PetMgr.o_e7W = parseInt(appConfig.RecyYuHunLv);
                }
                catch(exxx) {}
                yuHuenRecy();
            }
            // 星空吞噬
            if (appConfig.RecyXingKong == "TRUE" && typeof xingKongTunShi == 'function') {
                xingKongTunShi();
            }
            // 异兽吞噬
            if (appConfig.RecyYiShou == "TRUE" && typeof AncientShenKunRecy == 'function') {
                AncientShenKunRecy();
            }
            if (appConfig.RecyJiuZiZhenYan == "TRUE") {
                // 九字真言回收
                jiuZiZhenYanRecy();
            }
            if (appConfig.RecyBaGua) {
                // 八卦回收
                let steps = appConfig.RecyBaGua.split('|');
                recyBaGua(steps);
            }

            if (SingletonMap && SingletonMap.XiyouMgr && !SingletonMap.XiyouMgr.o_yWb() && appConfig.MergerShiShi == "TRUE") {
                // 合成史诗
                mergerShiShi();
            }
            // 在玛法幻境
            if (SingletonMap && SingletonMap.o_Ntl && SingletonMap.o_Ntl.o_bj()) {
                // 合成赤月
                if (appConfig.MergerMafaChiYue == "3") {
                    mergerMafaChiYueLevel3();
                }
                else if (appConfig.MergerMafaChiYue == "2") {
                    mergerMafaChiYueLevel2();
                }
                // 合成战神
                if (appConfig.MergerMafaZhanShen == "3") {
                    mergerMafaZhanShenLevel3();
                }
                else if (appConfig.MergerMafaZhanShen == "2") {
                    mergerMafaZhanShenLevel2();
                }
                // 合成天龙
                if (appConfig.MergerMafaTianLong == "3") {
                    mergerMafaTianLongLevel3();
                }
                else if (appConfig.MergerMafaTianLong == "2") {
                    mergerMafaTianLongLevel2();
                }
                // 合成都市
                if (appConfig.MergerMafaDuShi == "3") {
                    mergerMafaDuShiLevel3();
                }
                else if (appConfig.MergerMafaDuShi == "2") {
                    mergerMafaDuShiLevel2();
                }
                // 合成仙灵
                if (appConfig.MergerMafaXianLin == "3") {
                    mergerMafaXianLinLevel3();
                }
                else if (appConfig.MergerMafaXianLin == "2") {
                    mergerMafaXianLinLevel2();
                }

                // 合成赤月
                if (appConfig.MergerMafaChiYueJianJia == "3") {
                    mergerMafaChiYueJianJiaLevel3();
                }
                else if (appConfig.MergerMafaChiYueJianJia == "2") {
                    mergerMafaChiYueJianJiaLevel2();
                }
                // 合成战神
                if (appConfig.MergerMafaZhanShenJianJia == "3") {
                    mergerMafaZhanShenJianJiaLevel3();
                }
                else if (appConfig.MergerMafaZhanShenJianJia == "2") {
                    mergerMafaZhanShenJianJiaLevel2();
                }
                // 合成天龙
                if (appConfig.MergerMafaTianLongJianJia == "3") {
                    mergerMafaTianLongJianJiaLevel3();
                }
                else if (appConfig.MergerMafaTianLongJianJia == "2") {
                    mergerMafaTianLongJianJiaLevel2();
                }
                // 合成都市
                if (appConfig.MergerMafaDuShiJianJia == "3") {
                    mergerMafaDuShiJianJiaLevel3();
                }
                else if (appConfig.MergerMafaDuShiJianJia == "2") {
                    mergerMafaDuShiJianJiaLevel2();
                }
                // 合成仙灵
                if (appConfig.MergerMafaXianLinJianJia == "3") {
                    mergerMafaXianLinJianJiaLevel3();
                }
                else if (appConfig.MergerMafaXianLinJianJia == "2") {
                    mergerMafaXianLinJianJiaLevel2();
                }

                useMafaBagProps(); // 使用玛法道具
            }
        }
    } catch (ex) {
        console.error(ex);
    }

    useBagProps(); // 自动使用道具
}

// 回收背包
function recyBag() {
    try {
        let M = SingletonMap.o_Ue.o_O9m();
        if (M && M.length > 0) {
            SingletonMap.o_OPQ.o_0z(M.length, M);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 回收
function loopRunRecy() {
    if (autoRecy == false) {
        return;
    }
    try {
        if (!SingletonMap.Ladder3v3Mgr.is3v3Map() && !SingletonMap.Ladder1v1Mgr.is1v1Map()) { // 1v1 3v3不能回收
            runRecy();
            if (SingletonMap && SingletonMap.XiyouMgr && SingletonMap.XiyouMgr.o_yWb()) {
                runXiyouRecy();
            }
            if (SingletonMap && SingletonMap.o_Ntl && SingletonMap.o_Ntl.o_bj()) {
                mafaRecy();
            }
            if (SingletonMap && SingletonMap.ShenJieMgr && SingletonMap.ShenJieMgr.o_UEk()) {
                shenJieRecy();
            }
        }
    } catch (ex) { }
    //setTimeout(function () {
    //    loopRunRecy();
    //}, 10000);
}

// 自动回收
var autoRecy = false;
function startLoopRunRecy(init) {
    try {
        if (init) {
            autoRecy = true;
        }
        if (!hasPri()) {
            return;
        }
        loopRunRecy();
    } catch (e) { }
    if (typeof autoDiscardProp == 'function') {
        autoDiscardProp();
    }
    if (typeof autoSaveWarehouse == 'function') {
        autoSaveWarehouse();
    }
}

var timerHandle = null;
function stopLoopRunRecy() {
    if (timerHandle) {
        clearTimeout(timerHandle);
    }
    autoRecy = false;
}

// 西游回收
function runXiyouRecy() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.RecyXiyou != 'TRUE') { // 没有启用回收
                return;
            }
        }
        var o_eOH = SingletonMap.o_yx0.o_O9m();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E)
        }
        if (M && M.length > 0) {
            SingletonMap.o_OPQ.o_0z(M.length, M, o_Ks.o_yCQ);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 玛法回收
function mafaRecy() {
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.RecyMafa != 'TRUE') { // 没有启用回收
                return;
            }
        }
        var o_eOH = SingletonMap.o_OqG.o_O9m();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E);
        }
        if (M && M.length > 0) {
            SingletonMap.o_OPQ.o_0z(M.length, M, o_Ks.o_lkf);
        }
        //SingletonMap.o_n.o_EG(MafaBagWin); // 打开玛法背包
        //gameObjectInstMap.MafaBagWin.o_Oma(1); // 选中回收tab
        //gameObjectInstMap.MafaRecyView.o_lCT();  // 回收
        //gameObjectInstMap.MafaBagWin.visible = false; // 关闭背包
    } catch (e) {
        console.log(e);
    }
}

// 神界回收
function shenJieRecy() {
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.RecyShenJie != 'TRUE') { // 没有启用回收
                return;
            }
        }
        var o_eOH = SingletonMap.o_UPB.o_O9m();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E);
        }
        if (M && M.length > 0)
            SingletonMap.o_OPQ.o_0z(M.length, M, o_Ks.o_Ubt);

        // SingletonMap.o_n.o_EG(ShenJieBagWin); // 打开神界背包
        // gameObjectInstMap.ShenJieBagWin.o_Oma(1); // 选中回收tab
        // gameObjectInstMap.ShenJieBagRecyView.o_lCT();  // 回收
        // gameObjectInstMap.ShenJieBagWin.visible = false; // 关闭背包
    } catch (e) {
        console.error(e);
    }

    try {
        autoUpShenJie(); // 升级神阶
        useShenWangZhuanXian(); // 开神王转箱
        autoLuShenJian(appConfig.AutoLuShenJian); // 戮神剑
    } catch (e) {
        console.error(e);
    }
    try {
        var autoPropStr = getAppConfigKeyValue("AutoPropName") || usePropStr;
        if (autoPropStr) {
            const arrRegUseProp = autoPropStr.split("|");
            var arrUseProp = [];
            for (let i = 0; i < SingletonMap.o_UPB.o_eiy.length; i++) {
                let bagItem = SingletonMap.o_UPB.o_eiy[i];
                for (let j = 0; j < arrRegUseProp.length; j++) {
                    if (bagItem && arrRegUseProp[j] && bagItem.name.indexOf(arrRegUseProp[j]) != -1) {
                        arrUseProp.push(bagItem.name);
                        break;
                    }
                }
            }
            for (let i = 0; i < arrUseProp.length; i++) {
                useShenJiePropByName(arrUseProp[i], true);
            }
        }
    } catch (e) {
        console.error(e);
    }
}

// 龙装分解
function longZhuangRecy() {
    try {
        var o_eOH = SingletonMap.o_Ue.o_yNc();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E);
        }
        if (M && M.length > 0)
            SingletonMap.o_OPQ.o_0z(M.length, M, o_Ks.o_erU);
    } catch (e) {
        console.log(e);
    }
}

// 回收八卦
function recyBaGua(pSteps) {
    try {
        let steps = [];
        for (var i = 0; i < pSteps.length; i++) {
            steps.push(parseInt(pSteps[i]));
        }
        //var M = SingletonMap.o_e0Q.o_lNA("BaGuaRecyState") || 0;
        //for (var i = 1; i <= 10; i++) {
        //    if (o_h.o_l3H(M, i))
        //        steps.push(i);
        //}
        var i = steps;
        var M = SingletonMap.o_NEw.o_NPk();
        var Q;
        var E = [0, 0, 0, 0, 0];
        var A = [0, 0, 0, 0, 0];
        var b = 10;
        var r = [];
        for (var g = 0; g < M.length; g++) {
            Q = SingletonMap.ItemMgr.o_29(M[g].userItem.stdItem);
            if (Q.steps > b)
                continue;
            if (i.indexOf(Q.steps) > -1)
                r.push(M[g]);
            Q.steps % 2 == 1 ? E[Math.floor(Q.steps / 2)] += 1 : A[Q.steps / 2 - 1] += 1
        }

        var arrM = [];
        for (var i = 0, Q = r; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES) {
                arrM.push(E);
            }
        }
        if (arrM.length > 0) {
            SingletonMap.o_OPQ.o_0z(arrM.length, arrM);
        }
    } catch (e) {
        console.log(e);
    }
}

// 御魂分解
function yuHuenRecy() {
    try {
        var o_eOH = SingletonMap.PetMgr.o_nR();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E);
        }
        if (M && M.length > 0)
            SingletonMap.o_OPQ.o_0z(M.length, M);
    } catch (e) {
        console.log(e);
    }
}

// 真言分解
function jiuZiZhenYanRecy() {
    try {
        var o_eOH = SingletonMap.o_Ue.o_yJk();
        var M = [];
        for (var i = 0, Q = o_eOH; i < Q.length; i++) {
            var E = Q[i];
            if (null != E && E.o_ES)
                M.push(E);
        }
        if (M && M.length > 0)
            SingletonMap.o_OPQ.o_0z(M.length, M);
    } catch (e) {
        console.log(e);
    }
}

var usePropStr = "贡献令|神功结晶|守护结晶|魂珠结晶|伏魔结晶|寻龙币|血符结晶|伏魔结晶|血符结晶|10钻石|20钻石|50钻石|100钻石|裁决结晶|万经验符|50000金币|100000金币|500000金币|小金砖|大金砖|特级金砖|通用经验技能书|地阶藏经阁宝箱|玄阶藏经阁宝箱|天阶藏经阁宝箱|神魔之力|声望卷轴|威望卷轴|万元宝|神龙参与宝箱|橙纹龙晶宝箱|血纹金晶宝箱|神魔结晶宝箱|邪神战斗宝箱|天阶宝物宝箱|万物源气";
function setPropStr(str) {
    usePropStr = str;
}
function existsProp(str){
    return usePropStr.indexOf(str) > -1;
}

var arrBatchPropName = ["神龙归属宝箱", "藏经阁宝箱", "神功结晶", "玫瑰花"];
function useBagProps() {
    try {
        var autoPropStr = getAppConfigKeyValue("AutoPropName") || usePropStr;
        const arrRegUseProp = autoPropStr.split("|");
        var arrUseProp = [];
        for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
            for (let j = 0; j < arrRegUseProp.length; j++) {
                if (arrRegUseProp[j] && SingletonMap.o_Ue.o_OFL[i]
                    && SingletonMap.o_Ue.o_OFL[i].name
                    && SingletonMap.o_Ue.o_OFL[i].name.indexOf(arrRegUseProp[j]) != -1) {
                    arrUseProp.push(SingletonMap.o_Ue.o_OFL[i]);
                    break;
                }
            }
        }
        for (let i = 0; i < arrUseProp.length; i++) {
            const useProp = arrUseProp[i];
            if (useProp == null) continue;
            let L = SingletonMap.o_Ue.o_OMm(useProp) ? useProp.btCount : 1;
            if (useProp.name && (arrBatchPropName.find(M => useProp.name.indexOf(M) != -1))) {
                L = useProp.btCount;
            }
            (function (pUseProp) {
                setTimeout(function () {
                    SingletonMap.o_OPQ.o_Nf7(pUseProp.series, L, pUseProp);
                }, 50);
            })(useProp);
        }
    }
    catch(exx) {}
}


function useMafaBagProps() {
    let arrUsePropStr = [];
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.UseMafaShuJun == "TRUE") {
                // 玛法书卷
                arrUsePropStr.push("玛法书卷");
            }
        }
        let mafaBag = SingletonMap.o_OqG.o_eiy;
        let arrUseProp = [];
        for (let i = 0; i < mafaBag.length; i++) {
            let bagItem = mafaBag[i];
            if (bagItem && bagItem.name && arrUsePropStr.find(item => bagItem.name.indexOf(item) != -1)) {
                arrUseProp.push(bagItem);
            }
        }

        for (let i = 0; i < arrUseProp.length; i++) {
            const useProp = arrUseProp[i];
            if (useProp == null) continue;

            let num = SingletonMap.o_Ue.o_OMm(useProp) ? useProp.btCount : 1;
            SingletonMap.o_OPQ.o_Nf7(useProp.series, num, useProp);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

var curMapId = -1;
var sceneId = -1;
function setCurMapId(mapId, pSceneId) {
    curMapId = mapId;
    sceneId = pSceneId;
}

var recoverHPFlag = false;
function recoverHP(init) {
    if (init) {
        recoverHPFlag = true;
    }
    if (recoverHPFlag) {
        commandSocket.send(hex2buf("eecc0d001f05ed56691901001127010000"));
        setTimeout(function () {
            recoverHP();
        }, 10);
    }
}
function closeRecoverHP() {
    recoverHPFlag = false;
}

// 进入西游安全区
function gotoXiYuoMainMap() {
    // commandSocket.send(hex2buf('eecc0d008b07001f000000000000000000'));
    SingletonMap.o_eaX.o_lKj(0, TeleportPos.XiYouArea);
}

// 进入天界安全区
function gotoTianJieMainMap() {
    commandSocket.send(hex2buf('eecc0200b501'));
}

var isGuaJi = false;
var arrGuaJiMapHex = [];
/*function startGuaJi(mapHex) {//天界挂机
    if (mapHex != "") {
        arrGuaJiMapHex = mapHex.split(',');
        isGuaJi = true;
        isXiYouGuaJi = false;
        loopGuaJiMap(0); // 轮循挂机地图
        setTimeout(attackBelongMonster, 3000);
    }
}*/

function startGuaJi() {//天界挂机
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.TianJieMap) {
                arrGuaJiMapHex = appConfig.TianJieMap.split("|");
                isGuaJi = true;
                isXiYouGuaJi = false;
                loopGuaJiMap(0); // 轮循挂机地图
                setTimeout(attackBelongMonster, 3000);
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

function stopGuaJi() {//停止天界挂机
    arrGuaJiMapHex = [];
    isGuaJi = false;
    stopLoopRunRecy();
    resetSysVal();
}

function stopAllGuaJi() {
    stopGuaJi();
    stopXiYouGuaJi();
    stopShenJieGuaJi();
}

var curGuaJiMapIndex = 0;

function loopGuaJiMap(idx) {
    if (isGuaJi) {
        if (typeof idx != 'undefined') {
            curGuaJiMapIndex = idx;
        }
        if (arrGuaJiMapHex.length == 1) {//只挂1个图
            beforGotoGuaJiCurMap();
        }
        else if (curGuaJiMapIndex < arrGuaJiMapHex.length) {
            gotoTianJieMainMap();
            let curMapInfo = arrGuaJiMapHex[curGuaJiMapIndex];
            let curMap = curMapInfo.split(',');
            SingletonMap.Boss2Sys.o_OV4(curMap[0], curMap[1]);
            curGuaJiMapIndex++;
            setTimeout(function () { // 每张地图挂2分钟
                beforloopGuaJiMap();
            }, 120000);
        }
        else {
            loopGuaJiMap(0);
        }
    }
}

function beforGotoGuaJiCurMap() {
    if (!existsMonster()) {
        gotoGuaJiCurMap();
    }
    else {
        setTimeout(function () {
            beforGotoGuaJiCurMap();
        }, 500);
    }
}

function beforloopGuaJiMap() {
    if (!existsMonster()) {
        loopGuaJiMap(curGuaJiMapIndex);
    }
    else {
        setTimeout(function () {
            beforloopGuaJiMap();
        }, 500);
    }
}

function gotoGuaJiCurMap() {
    setTimeout(function () {
        let curMapInfo = arrGuaJiMapHex[curGuaJiMapIndex];
        let curMap = curMapInfo.split(',');
        SingletonMap.Boss2Sys.o_OV4(curMap[0], curMap[1]);
    }, 500);
}

var isXiYouGuaJi = false;
var arrXiYouGuaJiMapHex = [];
var curXiyouMapHex = "";
/*
function startXiYouGuaJi(mapHex) {//西游挂机
    if (mapHex != "") {
        curXiyouMapHex = mapHex;
        arrXiYouGuaJiMapHex = mapHex.split(',');
        isGuaJi = false;
        isXiYouGuaJi = true;
        curXiYouGuaJiMapIndex = 0;
        loopXiYouGuaJiMap(0); // 轮循挂机地图
        setTimeout(attackBelongMonster, 3000);
    }
}*/
function startXiYouGuaJi() {//西游挂机
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.XiYouMap) {
                arrXiYouGuaJiMapHex = appConfig.XiYouMap.split('|');
                isGuaJi = false;
                isXiYouGuaJi = true;
                curXiYouGuaJiMapIndex = 0;
                loopXiYouGuaJiMap(0); // 轮循挂机地图
                setTimeout(attackBelongMonster, 3000);
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

function stopXiYouGuaJi() {//停止挂机
    arrXiYouGuaJiMapHex = [];
    isXiYouGuaJi = false;
    stopLoopRunRecy();
    resetSysVal();
}

var curXiYouGuaJiMapIndex = 0;
function loopXiYouGuaJiMap(idx) {
    if (isXiYouGuaJi) {
        if (typeof idx != 'undefined') {
            curXiYouGuaJiMapIndex = idx;
        }
        if (arrXiYouGuaJiMapHex.length == 1) {//只挂1个图
            beforGotoXiYouGuaJiCurMap();
        }
        else if (curXiYouGuaJiMapIndex < arrXiYouGuaJiMapHex.length) {
            gotoXiYuoMainMap();

            let curMapInfo = arrXiYouGuaJiMapHex[curXiYouGuaJiMapIndex];
            let curMap = curMapInfo.split(',');
            SingletonMap.o_yKf.o_yda(curMap[0], curMap[1]);
            curXiYouGuaJiMapIndex++;
            setTimeout(function () { // 每张地图挂2分钟
                beforLoopXiYouGuaJiMap();
            }, 120000);
        }
        else {
            loopXiYouGuaJiMap(0);
        }
    }
}

function beforGotoXiYouGuaJiCurMap() {
    if (!existsMonster()) {
        gotoXiYouGuaJiCurMap();
    }
    else {
        setTimeout(function () {
            beforGotoXiYouGuaJiCurMap();
        }, 500);
    }
}

function beforLoopXiYouGuaJiMap() {
    if (!existsMonster()) {
        loopXiYouGuaJiMap(curXiYouGuaJiMapIndex);
    }
    else {
        setTimeout(function () {
            beforLoopXiYouGuaJiMap();
        }, 500);
    }
}

var isShenJieGuaJi = false;
var arrShenJieMap = [];

// 返回 [{x: ..., y:...}]
function getShenJieCaiLiaoMonsterConfig(mapId) {
    let pos = shenJieCaiLiaoMonsterConfig[mapId];
    let arrPos = pos.split("|");
    let arrResult = [];
    for (var i = 0; i < arrPos.length; i++) {
        let posItem = arrPos[i].split(",");
        arrResult.push({
            x: posItem[0],
            y: posItem[1]
        });
    }
    return arrResult;
}

// 神界挂机
function startShenJieGuaJi(mapData) {
    isGuaJi = false;
    isXiYouGuaJi = false;
    isShenJieGuaJi = true;
    if (hasPri()) { // 超级vip挂高暴
        startShenJieVipGuaJi();
    }
    else {
        startShenJieNormalGuaJi();
    }
}

function isSelfPet(M) {
    let result = false;
    try {
        if (M && (M.masterName.indexOf(SingletonMap.RoleData.name) > -1 || M.masterName.indexOf(PlayerActor.o_NLN.roleName) > -1)) {
            result = true;
        }
    }
    catch (ex) {
        console.error(ex);
    }
    return result;
}

function isSelfBelong(M) {
    let result = false;
    if (!M) return false;
    try {
        if (M.belongingName == SingletonMap.RoleData.name || M.belongingName == PlayerActor.o_NLN.roleName) {
            result = true;
        }
    }
    catch (ex) {
        console.error(ex);
    }
    return result;
}

function isNullBelong(M) {
    let result = false;
    try {
        if (M && M.belongingName == "") {
            result = true;
        }
    }
    catch (ex) {
        console.error(ex);
    }
    return result;
}

// 不打非归属怪
function noAttackNoBelongMonster() {
    try {
        if (!isSelfBelong(curMonster)) { // 不打非归属怪
            clearCurMonster();
            SingletonMap.o_lAu.stop();
        }
    } catch (ex) {
        console.error(ex);
    }
}

var isShenJieVipGuaJiMod = true; // false 表示打普通怪, true表示打高暴
var curShenJieVieMap = null;

function gotoShenJieVipGuaJiCurMap() {
    if (curShenJieVieMap) {
        SingletonMap.o_Uij.o_UX4(curShenJieVieMap[0], curShenJieVieMap[1]); // 前往地图
    }
}

// 神界高暴挂机
async function startShenJieVipGuaJi() {
    if (!isShenJieGuaJi) return; // 停止挂机
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenJieMap) {
                arrShenJieMap = appConfig.ShenJieMap.split('|');
                for (let i = 0; i < arrShenJieMap.length; i++) {
                    normalPickUp(); // 捡取
                    let curMap = arrShenJieMap[i].split(',');
                    // 换图，如果只挂一张图就不换
                    if (curShenJieVieMap == null || (curShenJieVieMap && !(curShenJieVieMap[0] == curMap[0] && curShenJieVieMap[1] == curMap[1]))) {
                        curShenJieVieMap = curMap;
                        SingletonMap.o_Uij.o_UX4(curMap[0], curMap[1]); // 前往地图
                        await xSleep(1500);
                    }
                    normalPickUp(); // 捡取
                    noAttackNoBelongMonster(); // 不打非归属怪
                    let monsterConfig = getShenJieCaiLiaoMonsterConfig(curMap);
                    while (monsterConfig.length > 0) {
                        if (!isShenJieGuaJi) return; // 停止挂机
                        // 找出距离最近的坐标
                        let minPos = monsterConfig.reduce(function (a, b) {
                            let aDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, a.x, a.y);
                            let bDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, b.x, b.y);
                            return aDistance > bDistance ? b : a;
                        });
                        noAttackNoBelongMonster(); // 不打非归属怪

                        // 追杀黑名单
                        console.log('追杀黑名单..');
                        await attackBadPlayer(minPos.x, minPos.y);
                        console.log('追杀黑名单结束..');
                        await xSleep(500);

                        normalPickUp(); // 捡取
                        // 寻路
                        console.log(`开始寻路==>index:${monsterConfig.length} x:${minPos.x} y:${minPos.x}`);
                        await doFindWayByPos(minPos.x, minPos.y);
                        console.log(`寻路结束==>x:${minPos.x} y:${minPos.x}`);

                        // 攻击高暴怪
                        console.log('开始寻怪..');
                        await attackShenJieVipMonster();
                        console.log('寻怪结束..');

                        await xSleep(500);
                        normalPickUp(); // 捡取
                        // 删除用过的坐标
                        monsterConfig = monsterConfig.filter(M => {
                            return M.x != minPos.x && M.y != minPos.y;
                        });
                        normalPickUp(); // 捡取
                    }
                }
                isShenJieVipGuaJiMod = false;// 打完一轮，切换成普通怪模式
            }
        }
    } catch (ex) {
        console.error(ex);
    }
    await xSleep(2000);
    startShenJieVipGuaJi();
}

// 寻找黑名单
function attackBadPlayer(backX, backY) {
    function check() {
        return xSleep(300).then(async function () {
            let badPlayer = findBadPlayer();
            if (badPlayer) {
                if (isRemoteAttackDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, badPlayer.currentX, badPlayer.o_NI3)) {
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    setCurMonster(badPlayer);
                }
                else {
                    await doFindWayByPos(badPlayer.currentX, badPlayer.o_NI3, true);
                }
                return check();
            }
            else {
                if (getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, backX, backY) > 6) {
                    await doFindWayByPos(backX, backY, true);
                }
                return;
            }
        });
    };
    return check();
}

function doFindWayByPos(x, y, isAttackBadPlayer) {
    function check() {
        var curX = PlayerActor.o_NLN.currentX;
        var curY = PlayerActor.o_NLN.o_NI3;
        return xSleep(300).then(async function () {
            if (!isShenJieGuaJi) return; // 停止挂机
            SingletonMap.o_lAu.stop();
            // 追击黑名单的时候不要寻怪
            if (isAttackBadPlayer != true) {
                await attackShenJieVipMonster(); // 寻路遇到怪物
            }
            if (getDistance(curX, curY, x, y) < 7) {
                return;
            }
            else {
                SingletonMap.o_lAu.stop();
                SingletonMap.o_OKA.o_NSw(x, y);
                return check();
            }
        });
    }
    return check();
}

// 攻击高暴怪
function attackShenJieVipMonster() {
    let curX = PlayerActor.o_NLN.currentX
    let curY = PlayerActor.o_NLN.o_NI3;
    function check() {
        return xSleep(300).then(async function () {
            if (!isShenJieGuaJi) return; // 停止挂机
            normalPickUp(); // 捡取

            // 追杀黑名单
            // await attackBadPlayer(curX, curY);

            let arrMonster = SingletonMap.o_OFM.o_Nx1;
            let isExists = false;

            // 先打归属怪
            for (let i = 0; i < arrMonster.length; i++) {
                let M = arrMonster[i];
                if (M && M instanceof Monster && !M.masterName && M.curHP > 100 && isSelfBelong(M)) {
                    if (isShenJieVipGuaJiMod) { // 高暴怪模式
                        if (!(M.cfgId >= 167001 && M.cfgId <= 167045)) { // 只打高暴怪
                            SingletonMap.o_OKA.o_Pc(M, true);
                            setCurMonster(M);
                            isExists = true;
                            break;
                        }
                    }
                    else {
                        if (!(M.cfgId >= 167001 && M.cfgId <= 167045)) { // 发现高暴怪
                            isShenJieVipGuaJiMod = true;
                        }
                        SingletonMap.o_OKA.o_Pc(M, true);
                        setCurMonster(M);
                        isExists = true;
                        break;
                    }
                }
            }
            // 打没有归属的
            if (!isExists) {
                for (let i = 0; i < arrMonster.length; i++) {
                    let M = arrMonster[i];
                    if (M && M instanceof Monster && !M.masterName && M.curHP > 100 && !M.belongingName) {
                        if (isShenJieVipGuaJiMod) { // 高暴怪模式
                            if (!(M.cfgId >= 167001 && M.cfgId <= 167045)) { // 只打高暴怪
                                SingletonMap.o_OKA.o_Pc(M, true);
                                setCurMonster(M);
                                isExists = true;
                                break;
                            }
                        }
                        else {
                            if (!(M.cfgId >= 167001 && M.cfgId <= 167045)) { // 发现高暴怪
                                isShenJieVipGuaJiMod = true;
                            }
                            SingletonMap.o_OKA.o_Pc(M, true);
                            setCurMonster(M);
                            isExists = true;
                            break;
                        }
                    }
                }
            }
            if (isExists) {
                normalPickUp(); // 捡取
                return check();
            }
            else {
                normalPickUp(); // 捡取
                return;
            }
        });
    };
    return check();
}


// 神界普通挂机
function startShenJieNormalGuaJi() {
    let appConfig = getAppConfig();
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenJieMap) {
                arrShenJieMap = appConfig.ShenJieMap.split('|');
                isGuaJi = false;
                isXiYouGuaJi = false;
                isShenJieGuaJi = true;
                loopShenJieGuaJiMap(0);
                setTimeout(attackBelongMonster, 3000);
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

var curShenJieGuaJiMapIndex = 0;
function loopShenJieGuaJiMap(idx) {
    if (isShenJieGuaJi) {
        if (typeof idx != 'undefined') {
            curShenJieGuaJiMapIndex = idx;
        }
        if (arrShenJieMap.length == 1) {//只挂1个图
            beforGotoShenJieCurMap();
        }
        else if (curShenJieGuaJiMapIndex < arrShenJieMap.length) {
            gotoShenJieGuaJiCurMap();
            let curMapInfo = arrShenJieMap[curShenJieGuaJiMapIndex];
            let curMap = curMapInfo.split(',');
            SingletonMap.o_Uij.o_UX4(curMap[0], curMap[1]);
            curShenJieGuaJiMapIndex++;
            setTimeout(function () { // 每张地图挂2分钟
                beforGotoShenJieCurMap();
            }, 120000);
        }
        else {
            loopShenJieGuaJiMap(0);
        }
    }
}

function beforGotoShenJieCurMap() {
    if (!existsMonster()) {
        gotoShenJieGuaJiCurMap();
    }
    else {
        setTimeout(function () {
            beforGotoShenJieCurMap();
        }, 500);
    }
}

function gotoShenJieGuaJiCurMap() {
    let mapData = arrShenJieMap[curShenJieGuaJiMapIndex].split(",");
    SingletonMap.o_Uij.o_UX4(mapData[0], mapData[1]);
}

function stopShenJieGuaJi() {//停止挂机
    arrShenJieMap = [];
    isShenJieGuaJi = false;
    stopLoopRunRecy();
    resetSysVal();
}

function existsMonster() {
    try {
        let isExists = false;
        let len = SingletonMap.o_OFM.o_Nx1.length;
        for (let i = len - 1; i >= 0; i--) {
            const M = SingletonMap.o_OFM.o_Nx1[i];
            if (M instanceof Monster && !M.masterName && M.o_O97 == 0 && (isNullBelong(M) || isSelfBelong(M))) {
                isExists = true;
                break;
            }
        }
        return isExists;
    } catch (ex) {
        return false;
    }
    return false;
}

function gotoXiYouGuaJiCurMap() {
    setTimeout(function () {
        // commandSocket.send(hex2buf(arrXiYouGuaJiMapHex[curXiYouGuaJiMapIndex]));
        let curMapInfo = arrXiYouGuaJiMapHex[curXiYouGuaJiMapIndex];
        let curMap = curMapInfo.split(',');
        SingletonMap.o_yKf.o_yda(curMap[0], curMap[1]);
    }, 500);
}

function printLog(msg) {
    console.log(msg);
}

var isShenMoDisEquip = false;
var SingletonMap = {};
var arrShenMoEquipData = [];
const shenMoEquipTypeMap = [1, 4, 6, 7, 14, 18, 46, 33, 34, 35, 36, 39, 42, 43, 44, 47, 50, 57];
//神魔卸装
function doShenMoDisEquip() {
    if (!hasPri() || isMonthPri()) return;
    isShenMoDisEquip = true;
    const equips = SingletonMap.EquipData.equips;
    arrShenMoEquipData = [];
    for (var i = 0; i < equips.length; i++) {
        arrShenMoEquipData.push(equips[i]);
    }
    for (var i = 0; i < equips.length; i++) {
        if (equips[i] != null && shenMoEquipTypeMap.includes(equips[i]._stdItem.type)) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_ldH(equip.series, equip.wItemId);
                }, 50);
            })(equips[i]);
        }
    }
    doJinShenDisEquip();
    disGem();
}

//神魔上装
function doShenMoWearEquip() {
    if (!hasPri() || isMonthPri()) return;
    const equips = SingletonMap.EquipData.equips;
    for (var i = 0; i < arrShenMoEquipData.length; i++) {
        if (equips[i] == null && arrShenMoEquipData[i] != null) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_OBm(equip.series, SingletonMap.o_OqG.o_ty(equip._stdItem.id) ? 1 : 0);
                }, 50);
            })(arrShenMoEquipData[i]);
        }
    }
    doJinShenWearEquip();
    wearGem();
    isShenMoDisEquip = false;
    isShiShiDisEquip = false;
    isJieBiaoDisEquip = false;
}

var isJieBiaoDisEquip = false;
var arrJieBiaoEquipData = [];
const jieBiaoEquipTypeMap = [1];
// 劫镖卸装
function doJieBiaoDisEquip() {
    if (!hasPri() || isMonthPri()) return;
    isJieBiaoDisEquip = true;
    const equips = SingletonMap.EquipData.equips;
    arrJieBiaoEquipData = [];
    for (var i = 0; i < equips.length; i++) {
        arrJieBiaoEquipData.push(equips[i]);
    }
    for (var i = 0; i < equips.length; i++) {
        if (equips[i] != null && jieBiaoEquipTypeMap.includes(equips[i]._stdItem.type)) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_ldH(equip.series, equip.wItemId);
                }, 50);
            })(equips[i]);
        }
    }
    doJinShenDisEquip();
}
// 劫镖上装
function doJieBiaoWearEquip() {
    if (!hasPri() || isMonthPri()) return;
    const equips = SingletonMap.EquipData.equips;
    for (var i = 0; i < arrJieBiaoEquipData.length; i++) {
        if (equips[i] == null && arrJieBiaoEquipData[i] != null) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_OBm(equip.series, SingletonMap.o_OqG.o_ty(equip._stdItem.id) ? 1 : 0);
                }, 50);
            })(arrJieBiaoEquipData[i]);
        }
    }
    doJinShenWearEquip();
    isJieBiaoDisEquip = false;
}


var arrJinShenEquipData = {};
// 金身卸装
function doJinShenDisEquip() {
    if (!hasPri() || isMonthPri()) return;
    arrJinShenEquipData = {};
    const equips = SingletonMap.o_O6y.o_LZ;
    for (var i in equips) {
        let arrEquip = equips[i];
        arrJinShenEquipData[i] = arrJinShenEquipData[i] || [];
        for (var j = 0; j < arrEquip.length; j++) {
            let equip = arrEquip[j];
            arrJinShenEquipData[i].push(equip);
            if (equip != null) {
                (function (pi, pj, pEquip) {
                    setTimeout(function () {
                        SingletonMap.o_ON9.o_lEP(pi, pj, pEquip._stdItem.id);
                    }, 50);
                })(i, j, equip);
            }
        }
    }
}

// 金身上装
function doJinShenWearEquip(times) {
    if (!hasPri() || isMonthPri()) return;
    const equips = SingletonMap.o_O6y.o_LZ;
    for (var i in equips) {
        let arrEquip = equips[i];
        let jinShenEquip = arrJinShenEquipData[i];
        for (var j = 0; j < arrEquip.length; j++) {
            let jinShenEquipData = jinShenEquip[j];
            if (arrEquip[j] == null && jinShenEquipData != null) {
                let equipSeries = getEquipSeries(jinShenEquipData.wItemId);
                if (equipSeries != null) {
                    (function (pi, pj, pEquipSeries) {
                        setTimeout(function () {
                            SingletonMap.o_ON9.o_qB(pi, pj, pEquipSeries);
                        }, 50);
                    })(i, j, equipSeries);
                }
            }
        }
    }
    if (!times || times <= 30) {
        let curTimes = times ? times + 1 : 1;
        setTimeout(function () {
            doJinShenWearEquip(curTimes);
        }, 50);
    }
}

function getEquipSeries(wItemId) {
    try {
        let equip = SingletonMap.o_Ue.o_OFL.find((item) => {
            return item && item.wItemId == wItemId;
        });
        if (equip) {
            return equip.series;
        }
    }
    catch (ex) { }
    return null;
}

function lockLife(flag) {
    try {
        if (hasPri()) {
            if (SingletonMap && SingletonMap.ShenJieMgr && SingletonMap.ShenJieMgr.o_UEk()) { // 在神界
                shenJiLockLife(flag);
            }
            else if (SingletonMap && SingletonMap.XiyouMgr && SingletonMap.XiyouMgr.o_yWb()) { // 在西游
                xiyouLockLife(flag);
            }
        }
        //else if (!SingletonMap.ServerCross.o_ePe) { // 在本服
        //    localServeLockLife(flag);
        //}
    } catch (ex) {
        console.error(ex);
    }
}

/** 西游回血 **/
var lockLifeFlag = false;
var zhenHuShou = null; // 西游护手
var zhenDuenPai = null; // 神界盾牌
function xiyouLockLife(flag) {
    try {
        if (flag) {
            lockLifeFlag = true;
            checkXiYouRenShenGuo(); // 买够1组人参果
        }
        if (!lockLifeFlag) {
            return;
        }
        // 血量低于30%
        if (PlayerActor.o_NLN.curHP / PlayerActor.o_NLN.curHPMax < 0.3) {
            userXiyouJinDang();
        }
        else if (PlayerActor.o_NLN.curHP / PlayerActor.o_NLN.curHPMax < 0.9) {// 血量低于90%
            if (!isFindStrangers()) { // 没有外人，调快速度
                for (var i = 0; i < 5; i++) {
                    userXiyouRenShenGuo();
                }
            } else {
                userXiyouRenShenGuo();
            }
        }
        /*
        let huShou = getZhenHuShou();
        if (huShou != null) {
            zhenHuShou = huShou;
            setTimeout(function () {
                SingletonMap.o_yWP.o_ldH(huShou.series, huShou.wItemId); // 脱
            }, 300);
        }
        else {
            let bagHuShou = getBagHuShou();
            if (bagHuShou != null) {
                setTimeout(function () {
                    SingletonMap.o_yWP.o_OBm(bagHuShou); // 穿
                }, 300);
            }
        }*/
    }
    catch (ex) { }
    //if (lockLifeFlag) {
    //    setTimeout(function () {
    //        lockLife();
    //    }, 30);
    //}
}

function disableLockLife() {
    try {
        lockLifeFlag = false;
        shenJiLockLifeFlag = false;
        localServeLockLifeFlag = false;
        if (SingletonMap && SingletonMap.XiyouMgr && SingletonMap.XiyouMgr.o_yWb()) {
            setTimeout(function () {
                if (zhenHuShou != null) {
                    let huShou = getZhenHuShou();
                    if (huShou == null) {
                        setTimeout(function () {
                            SingletonMap.o_yWP.o_OBm(zhenHuShou);
                        }, 300);
                    }
                }
            }, 300);
        }
        else if (SingletonMap && SingletonMap.ShenJieMgr && SingletonMap.ShenJieMgr.o_UEk() && SingletonMap.o_UbV && SingletonMap.o_UE5) {
            setTimeout(function () {
                if (zhenDuenPai != null) {
                    let curDuenPai = getWornShenJiDuenPai();
                    if (curDuenPai == null) {
                        SingletonMap.o_UE5.o_OBm(zhenDuenPai.series, 1);
                    }
                }
            }, 500);
        }
    }
    catch (ex) { }
}

// 使用金丹
function userXiyouJinDang() {
    let daoju = getXiyouBagDaoJu('一品金丹');
    //if (!daoju) {
    //    daoju = getXiyouBagDaoJu('二品金丹');
    //}
    //if (!daoju) {
    //    daoju = getXiyouBagDaoJu('三品金丹');
    //}
    if (daoju) {
        SingletonMap.SettingMgr.o_l5T = {};
        SingletonMap.o_yWR.o_lsD(daoju.series, 1, daoju);
    }
    else {
        userXiyouRenShenGuo();
    }
}
// 使用人参果
function userXiyouRenShenGuo() {
    let daoju = getXiyouBagDaoJu('千年人参果');
    if (!daoju) {
        daoju = getXiyouBagDaoJu('人参果');
    }
    if (daoju) {
        SingletonMap.SettingMgr.o_l5T = {};
        SingletonMap.o_yWR.o_lsD(daoju.series, 1, daoju);
    }
}

// 使用神液
function userShenJieShenYe() {
    let daoju = getShenJieBagDaoJu('万年生命神液');
    if (!daoju) {
        daoju = getShenJieBagDaoJu('生命神液');
    }
    if (daoju) {
        SingletonMap.o_UTD.o_lsD(daoju.series, 1, daoju);
    }
}
// 使用神界道具
async function useShenJiePropByName(name, batch) {
    let daoju = getShenJieBagDaoJu(name);
    while (daoju) {
        if (batch && daoju.btCount) {
            SingletonMap.o_UTD.o_lsD(daoju.series, daoju.btCount, daoju);
        }
        else {
            SingletonMap.o_UTD.o_lsD(daoju.series, 1, daoju);
        }
        daoju = getShenJieBagDaoJu(name);
        await xSleep(100);
    }
}

// 使用神王转箱
let shenWangZhuanXianNextTime = null;
async function useShenWangZhuanXian() {
    try {
        if (shenWangZhuanXianNextTime != null && shenWangZhuanXianNextTime > SingletonMap.ServerTimeManager.o_eYF) return;
        if (!SingletonMap.ShenJieMgr.o_UEk()) return;
        shenWangZhuanXianNextTime = SingletonMap.ServerTimeManager.o_eYF + 10000; // 10 秒后可以再开
        let times = 0;
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.ShenWangZhuanXianTimes != "") {
                times = parseInt(appConfig.ShenWangZhuanXianTimes);
            }
            else {
                return;
            }

            let daoju = getShenJieBagDaoJu('神王转箱');
            if (daoju) {
                SingletonMap.o_OPQ.o_Nf7(daoju.series, 1, daoju); // 打开转箱窗口
                await xSleep(800);
                SingletonMap.o_OPQ.o_ODk(7); // 免费转
                await xSleep(300);
                for (var i = 0; i < times; i++) {
                    SingletonMap.o_OPQ.o_ODk(7); // 钻石转一次
                    await xSleep(300);
                }
                closeGameWin("BagTurntableWin");
            }
        }
    }
    catch (e) {
        console.error(e);
    }

}

// 升级神阶
function autoUpShenJie() {
    try {
        if (!SingletonMap.ShenJieMgr.o_UEk()) return;
        let curCfg = SingletonMap.o_O.o_UEK[SingletonMap.RoleData.o_UXt]; // 取当前等级配置
        var i = SingletonMap.RoleData.o_UbY;
        if (i >= curCfg.exp) {
            if (0 != Object.keys(curCfg.cost).length) {
                var Q = curCfg.cost;
                var E = 0;
                if (Q.id > 100) { // MoneyType.o_gy = 100
                    E = SingletonMap.o_Ue.o_Ngt(Q.id);
                }
                else {
                    E = SingletonMap.RoleData.o_NM3(Q.id);
                }
                if (E >= Q.count) {
                    SingletonMap.o_Uij.o_UTP();
                }
                else {
                    // SingletonMap.o_Ue.o_Qy(Q.id);
                }
            } else SingletonMap.o_Uij.o_UTP();
        }
    }
    catch (e) {
        console.error(e);
    }
}

// 使用金创药
function userJinChuanYao() {
    let daoju = getBagjinChuanYao('特级金创药');
    if (daoju) {
        SingletonMap.o_OPQ.o_Nf7(daoju.series, 1, daoju);
    }
}

// 获取背包金创药
function getBagjinChuanYao(name) {
    var result = null;
    for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
        let bagItem = SingletonMap.o_Ue.o_OFL[i];
        if (bagItem && bagItem.name.indexOf(name) != -1) {
            result = bagItem;
            break;
        }
    }
    return result;
}

// 获取西游道具
function getXiyouBagDaoJu(name) {
    var result = null;
    if (SingletonMap.XiyouMgr.o_yWb()) {
        for (let i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
            let bagItem = SingletonMap.o_yx0.o_eiy[i];
            if (bagItem && bagItem.name.indexOf(name) != -1) {
                result = bagItem;
                break;
            }
        }
    }
    return result;
}

// 获取神界道具
function getShenJieBagDaoJu(name) {
    var result = null;
    if (SingletonMap.ShenJieMgr.o_UEk()) {
        for (let i = 0; i < SingletonMap.o_UPB.o_eiy.length; i++) {
            let bagItem = SingletonMap.o_UPB.o_eiy[i];
            if (bagItem && bagItem.name.indexOf(name) != -1) {
                result = bagItem;
                break;
            }
        }
    }
    return result;
}

function getZhenHuShou() {
    // return SingletonMap.XiyouEquipData.equips[11];
    var huShou = null;
    for (var i = 0; i < SingletonMap.XiyouEquipData.equips.length; i++) {
        let equip = SingletonMap.XiyouEquipData.equips[i];
        if (equip != null && /.*[天蓬の护手]$/.test(equip.name)) {
            huShou = equip;
            break;
        }
    }
    return huShou;
}

function getBagHuShou() {
    var huShou = null;
    for (var i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
        let equip = SingletonMap.o_yx0.o_eiy[i];
        if (equip != null && zhenHuShou != null && equip.wItemId == zhenHuShou.wItemId) {
            huShou = equip;
            break;
        }
    }
    return huShou;
}

function checkHuShou() {
    try {
        if (SingletonMap.XiyouEquipData && SingletonMap.XiyouEquipData.equips && SingletonMap.XiyouMgr.o_yWb()) { // 在西游
            zhenHuShou = getZhenHuShou();
            if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.toggleBtnToggleHuShou) {
                if (zhenHuShou != null) {
                    callbackObj.toggleBtnToggleHuShou("1");
                } else {
                    callbackObj.toggleBtnToggleHuShou("0");
                }
            }
        }
        else if (SingletonMap.ShenJieMgr.o_UEk()) { // 在神界
            zhenDuenPai = getWornShenJiDuenPai();
            if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.toggleBtnToggleHuShou) {
                if (zhenDuenPai != null) {
                    callbackObj.toggleBtnToggleHuShou("1");
                } else {
                    callbackObj.toggleBtnToggleHuShou("0");
                }
            }
        }
        else if (!SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb()) {
            if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.toggleBtnToggleHuShou) {
                callbackObj.toggleBtnToggleHuShou("0");
            }
        }
    } catch (ex) {
        console.error(ex);
    }
    setTimeout(checkHuShou, 500);
}
(function () {
    // checkHuShou();
})();
/** 西游回血结束 **/

/** 神界回血 **/
var shenJiLockLifeFlag = false;
async function shenJiLockLife(flag) {
    try {
        if (flag) {
            shenJiLockLifeFlag = flag;
            checShenJieShenMinYaoYe(); // 买2组药水
        }
        if (!shenJiLockLifeFlag) {
            return;
        }

        if (PlayerActor.o_NLN.curHP / PlayerActor.o_NLN.curHPMax < 0.9) { // 血量低于90%
            userShenJieShenYe();
        }
        /*
        let curDuenPai = getWornShenJiDuenPai();
        if (curDuenPai == null) {
            setTimeout(function () {
                if (zhenDuenPai) {
                    SingletonMap.o_UE5.o_OBm(zhenDuenPai.series, 1);  // 穿盾
                }
            }, 300);
        } else {
            zhenDuenPai = curDuenPai;
            setTimeout(function () {
                SingletonMap.o_UE5.o_ldH(curDuenPai.series, curDuenPai.wItemId); // 脱盾
            }, 300);
        }
        */
    } catch (ex) { }
    //if (shenJiLockLifeFlag) {
    //    setTimeout(shenJiLockLife, 50);
    //}
}

var localServeLockLifeFlag = false;
function localServeLockLife(flag) {
    try {
        return;
        if (flag) {
            localServeLockLifeFlag = flag;
            checTeJiJinChuanYao(); // 买够5组药水
        }
        if (!localServeLockLifeFlag) {
            return;
        }

        // 判断血量，低于90%开始回血
        if (PlayerActor.o_NLN.curHP / PlayerActor.o_NLN.curHPMax < 0.9) {
            userJinChuanYao();
        }
    } catch (ex) {
        console.error(ex);
    }
}

function getWornShenJiDuenPai() { // true已穿截盾牌
    if (SingletonMap && SingletonMap.o_UbV && SingletonMap.o_UbV.equips) {
        return SingletonMap.o_UbV.equips[1]  // pos 1盾是
    }
    return null;
}
/** 神界回血结束 **/

/* 神界自动召唤本体 */
function checkShenJieBenTi() {
    try {
        if (SingletonMap.ShenJieMgr.o_UEk()) {
            let benTi = SingletonMap.o_OFM.o_Nx1.find(item => item.masterName == PlayerActor.o_NLN.roleName);
            if (!benTi) {
                SingletonMap.o_UE5.o_UcV();
            }
        }
    } catch (e) { }
    setTimeout(checkShenJieBenTi, 1000);
}
(function () {
    checkShenJieBenTi();
})();
/* 神界自动召唤本体结束 */

var arrXiYouEquipData = [];
var isXiYouDisEquip = false;
// 西游卸装
function doXiYouDisEquip() {
    if (!hasPri() || isMonthPri()) return;
    isXiYouDisEquip = true;
    const equips = SingletonMap.XiyouEquipData.equips;
    arrXiYouEquipData = [];
    for (var i = 0; i < equips.length; i++) {
        arrXiYouEquipData.push(equips[i]);
    }
    for (var i = 0; i < equips.length; i++) {
        if (equips[i] != null) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_yWP.o_ldH(equip.series, equip.wItemId); // 卸法器
                }, 50);
            })(equips[i]);
        }
    }
}
// 西游上装
function doXiYouWearEquip() {
    if (!hasPri() || isMonthPri()) return;
    isXiYouDisEquip = false;
    const equips = SingletonMap.XiyouEquipData.equips;
    for (var i = 0; i < arrXiYouEquipData.length; i++) {
        if (equips[i] == null && arrXiYouEquipData[i] != null) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_yWP.o_OBm(equip); // 穿法器
                }, 50);
            })(arrXiYouEquipData[i]);
        }
    }
}


var arrWearXiYou = []; // 记住已经戴的装备
function initWearXiYou() {
    try {
        const equips = SingletonMap.XiyouEquipData.equips;
        arrWearXiYou = [];
        for (var i = 0; i < equips.length; i++) {
            arrWearXiYou.push(equips[i]);
        }
        if (arrWearXiYou.length == 0) {
            setTimeout(initWearXiYou, 1000);
        }
    }
    catch (ex) {
        // console.error(ex);
        setTimeout(initWearXiYou, 1000);
    }
}

// 西游自动穿被打掉的装备
function autoWearXiYou() {
    if (isXiYouDisEquip) return; // 手动卸装不要自动穿
    try {
        const equips = SingletonMap.XiyouEquipData.equips;
        for (var i = 0; i < arrWearXiYou.length; i++) {
            if (equips[i] == null && arrWearXiYou[i] != null) {
                (function (equip) {
                    setTimeout(function () {
                        SingletonMap.o_yWP.o_OBm(equip); // 穿法器
                    }, 50);
                })(arrWearXiYou[i]);
            }
        }
    }
    catch (ex) {
        // console.error(ex);
    }
}

// 记住已装备的神器
var arrShenQi = [];
function initShenQi() {
    try {
        for (var i = 79; i <= 90; i++) {
            if (SingletonMap && SingletonMap.EquipData && SingletonMap.EquipData.equips[i] != null) {
                arrShenQi[i] = SingletonMap.EquipData.equips[i];
            }
        }
        if (arrShenQi.length == 0) {
            setTimeout(initShenQi, 1000);
        }
    }
    catch (ex) {
        //console.error(ex);
        setTimeout(initShenQi, 1000);
    }
}


// 自动穿神器
function autoWearShenQi() {
    try {
        if (isShenMoDisEquip) return;
        for (var i = 79; i <= 90; i++) {
            if (SingletonMap && SingletonMap.EquipData && SingletonMap.EquipData.equips[i] == null) {
                if (arrShenQi[i] != null) { //先读取初始记录
                    SingletonMap.o_lqa.o_OBm(arrShenQi[i].series, SingletonMap.o_OqG.o_ty(arrShenQi[i]._stdItem.id) ? 1 : 0);
                }
                else {
                    var M = SingletonMap.EquipData.o_Oju(i);
                    if (M) {
                        SingletonMap.o_lqa.o_OBm(M.series);
                    }
                }
            }
        }
    } catch (ex) {
        //console.error(ex);
    }
}

// 记住已装备的祈祷
var arrQiDao = [];
function initQiDao() {
    try {
        for (var i = 99; i <= 104; i++) {
            if (SingletonMap && SingletonMap.EquipData && SingletonMap.EquipData.equips[i] != null) {
                arrQiDao[i] = SingletonMap.EquipData.equips[i];
            }
        }
        if (arrQiDao.length == 0) {
            setTimeout(initQiDao, 1000);
        }
    }
    catch (ex) {
        //console.error(ex);
        setTimeout(initQiDao, 1000);
    }
}

// 自动穿祈祷
function autoWearQiDao() {
    try {
        if (isShiShiDisEquip) return;
        for (var i = 99; i <= 104; i++) {
            if (SingletonMap.EquipData.equips[i] == null) {
                if (arrQiDao[i] != null) { //先读取初始记录
                    SingletonMap.o_lqa.o_OBm(arrQiDao[i].series, SingletonMap.o_OqG.o_ty(arrQiDao[i]._stdItem.id) ? 1 : 0);
                }
                else {
                    var M = SingletonMap.EquipData.o_luz(i);
                    if (M) {
                        SingletonMap.o_lqa.o_OBm(M.series);
                    }
                }
            }
        }
    } catch (ex) {
        //console.error(ex);
    }
}

// 记住已装备的官印、勋章
var arrGuanYinXunZhang = [];
function initGuanYinXunZhang() {
    try {
        for (var i = 32; i <= 33; i++) {
            if (SingletonMap.EquipData.equips[i] != null) {
                arrGuanYinXunZhang[i] = SingletonMap.EquipData.equips[i];
            }
        }
        if (arrGuanYinXunZhang.length == 0) {
            setTimeout(initGuanYinXunZhang, 1000);
        }
    }
    catch (ex) {
        //console.error(ex);
        setTimeout(initGuanYinXunZhang, 1000);
    }
}
// 自动穿戴官印、勋章
function autoGuanYinXunZhang() {
    try {
        if (isShenMoDisEquip) return;
        for (var i = 32; i <= 33; i++) {
            if (SingletonMap && SingletonMap.EquipData && SingletonMap.EquipData.equips[i] == null) {
                if (arrGuanYinXunZhang[i] != null) { //先读取初始记录
                    SingletonMap.o_lqa.o_OBm(arrGuanYinXunZhang[i].series, SingletonMap.o_OqG.o_ty(arrGuanYinXunZhang[i]._stdItem.id) ? 1 : 0);
                }
                else {
                    var M = SingletonMap.EquipData.o_Oju(i);
                    if (M) {
                        SingletonMap.o_lqa.o_OBm(M.series);
                    }
                }
            }
        }
    } catch (ex) {
        //console.error(ex);
    }
}

// 记住已装备的百战
var arrBaiZhang = [];
function initBaiZhang() {
    try {
        for (var i = 73; i <= 78; i++) {
            if (SingletonMap.EquipData.equips[i] != null) {
                arrBaiZhang[i] = SingletonMap.EquipData.equips[i];
            }
        }
        if (arrBaiZhang.length == 0) {
            setTimeout(initBaiZhang, 1000);
        }
    }
    catch (ex) {
        //console.error(ex);
        setTimeout(initBaiZhang, 1000);
    }
}
// 自动穿戴百战
function autoBaiZhang() {
    try {
        if (isShenMoDisEquip) return;
        for (var i = 73; i <= 78; i++) {
            if (SingletonMap && SingletonMap.EquipData && SingletonMap.EquipData.equips[i] == null) {
                if (arrBaiZhang[i] != null) { //先读取初始记录
                    SingletonMap.o_lqa.o_OBm(arrBaiZhang[i].series, SingletonMap.o_OqG.o_ty(arrBaiZhang[i]._stdItem.id) ? 1 : 0);
                }
                else {
                    var M = SingletonMap.EquipData.o_Oju(i);
                    if (M) {
                        SingletonMap.o_lqa.o_OBm(M.series);
                    }
                }
            }
        }
    } catch (ex) {
        //console.error(ex);
    }
}

// 自动穿普通装备
async function autoWearNormalEquip() {
    try {
        if (isShenMoDisEquip) return; // 神魔卸的不穿
        if (isJieBiaoDisEquip) return; // 劫镖卸的不穿
        if (SingletonMap.XiyouMgr.o_yWb()) return; // 西游不穿
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.EnableAutoWearNormalEquip == 'TRUE') {
                if (SingletonMap.o_n.isOpen("ZjmEqUseWin")) {
                    SingletonMap.o_n.isOpen("ZjmEqUseWin").o_fA();
                    await xSleep(1000);
                    if (SingletonMap.o_n.isOpen("RoleWin")) {
                        SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen("RoleWin"));
                    }
                }
            }
        }
    }
    catch (ex) {
    }
}

// 4秒自动穿一次
function runAutoWearTask() {
    try {
        autoWearShenQi();
        autoWearQiDao();
        autoWearXiYou();
        autoGuanYinXunZhang();
        autoBaiZhang();
        autoWearNormalEquip();
    } catch (ex) {
        console.error(ex);
    }
    setTimeout(runAutoWearTask, 4000);
}

// 自动穿装备
(function () {
    initShenQi();
    initQiDao();
    initWearXiYou();
    initGuanYinXunZhang();
    initBaiZhang();
    runAutoWearTask();
})();

// 接收各种系统对象
var gameObjectInstMap = {};
function setGameObjectInst(name, inst) {
    gameObjectInstMap[name] = inst;
}
// 关闭窗体时删除对应对象
function removeGameObjectInst(hashCode) {
    if (true) return;
    if (gameObjectInstMap) {
        for (let key in gameObjectInstMap) {
            if (gameObjectInstMap[key].$hashCode == hashCode) {
                delete gameObjectInstMap[key];
            }
        }
    }
}

async function runTaskShaoZhu() {
    // if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU))) return; // 今日已完成

    SingletonMap.o_lHn.o_NTD(gameObjectInstMap.ExpFbView.curLayer); //发起挑战  每次挑战 90秒时间
    await awaitShaoZhu();
    if (gameObjectInstMap.ExpFbBuyWin) {
        SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbBuyWin);
        delete gameObjectInstMap.ExpFbBuyWin;
    }
    if (gameObjectInstMap.ExpFbView) {
        SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbView);
        delete gameObjectInstMap.ExpFbView;
    }
    await xSleep(1000);
    // 打开经验副本
    SingletonMap.o_n.o_EG(window.ExpFbView, [{
        isNotOpen: true
    }]);
    await xSleep(1000);
    var M = SingletonMap.o_OiP.o_lth;
    var i = SingletonMap.o_OiP.times;
    gameObjectInstMap.ExpFbView.o_kV = i - M > 0 ? i - M : 0;

    if (gameObjectInstMap.ExpFbView.o_kV > 0) { // 挑战次数
        runTaskShaoZhu();
    }
    else {
        localStorage.setItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU), true);
    }
}

function awaitShaoZhu() {
    function check() {
        return xSleep(1000).then(function () {
            if (gameObjectInstMap.ExpFbBuyWin && gameObjectInstMap.ExpFbBuyWin.visible) {
                var M = SingletonMap.o_OiP.o_lth;
                var i = SingletonMap.o_OiP.times;
                gameObjectInstMap.ExpFbView.o_kV = i - M > 0 ? i - M : 0;
                if (gameObjectInstMap.ExpFbView.o_kV <= 0) {
                    console.log('烧猪结束了...');
                    localStorage.setItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU), true);

                }
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}
// 烧猪
async function checkShaoZhu() {
    SingletonMap.o_eaX.o_lxG(2);
    await awaitToMap("王城");
    //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU))) return; // 今日已完成
    // 打开经验副本
    SingletonMap.o_n.o_EG(window.ExpFbView, [{
        isNotOpen: true
    }]);
    if (!gameObjectInstMap.ExpFbView) return;

    var M = SingletonMap.o_OiP.o_lth;
    var i = SingletonMap.o_OiP.times;
    gameObjectInstMap.ExpFbView.o_kV = i - M > 0 ? i - M : 0;

    if (gameObjectInstMap.ExpFbView.o_kV > 0) { // 挑战次数
        runTaskShaoZhu();
    } else {
        await xSleep(1000);
        if (gameObjectInstMap.ExpFbBuyWin) {
            SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbBuyWin);
            delete gameObjectInstMap.ExpFbBuyWin;
        }
        await xSleep(1000);
        if (gameObjectInstMap.ExpFbView) {
            SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbView);
            delete gameObjectInstMap.ExpFbView;
        }
        await xSleep(1000);
        localStorage.setItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU), true);
    }
    function check() {
        return xSleep(1000).then(async function () {
            if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.SHAO_ZHU))) { // 今日已完成
                await xSleep(1000);
                if (gameObjectInstMap.ExpFbBuyWin) {
                    SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbBuyWin);
                    delete gameObjectInstMap.ExpFbBuyWin;
                }
                await xSleep(1000);
                if (gameObjectInstMap.ExpFbView) {
                    SingletonMap.o_n.o_eAN(gameObjectInstMap.ExpFbView);
                    delete gameObjectInstMap.ExpFbView;
                }
                await xSleep(1000);
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}

// 领取活跃度奖励 1分钟检查一次
function checkDailyActivReward() {
    try {
        for (var i = 0; i < SingletonMap.o_lVF.o_O12.length; i++) {
            let item = SingletonMap.o_lVF.o_O12[i];
            if (SingletonMap.o_eHG.o_eYi(item) == 1) {
                SingletonMap.o_eJH.o_eKj(item.index);
            }
        }
    } catch (ex) {
    }
    setTimeout(checkDailyActivReward, 60000);
}

// 经验瓶 1分钟检查一次
function runJinYanPingTask() {
    try {
        if (!SingletonMap.ServerCross.o_ePe && SingletonMap.o_eC.o_J1() > 0) {
            for (let i = 0; i < SingletonMap.o_eC.o_J1(); i++) {
                SingletonMap.o_OuG.o_eaO(1);
            }
        }
    } catch (ex) {
    }
    setTimeout(runJinYanPingTask, 60000);
}
// 自动领经验瓶
(function () {
    runJinYanPingTask();
    runRecvMail();
    checkDailyActivReward();
})();

// 自动收邮件 10分钟收一次
function runRecvMail() {
    try {
        if (SingletonMap.o_e95.o_ejk.find(M => M.items.length && 2 != M.state)) {
            SingletonMap.o_e95.o_eow();
        }
    } catch (ex) {
    }
    setTimeout(runRecvMail, 1000 * 60 * 10);
}

// 福利大厅-在线奖励
function zhaiXianJianLi() {
    try {
        if (SingletonMap && SingletonMap.ServerCross && !SingletonMap.ServerCross.o_ePe) {
            for (let i = 1; i < 8; i++) {
                SingletonMap.o_Nsb.o_db(i);
            }
        }
    } catch (ex) { }
    setTimeout(zhaiXianJianLi, 60000);
}

// 藏宝图
async function xunBao() {
    try {
        SingletonMap.o_eaX.o_lxG(2);
        await awaitToMap("王城");
        //if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.XUN_BAO))) return; // 今日已完成
        if (SingletonMap.o_Nlv.baseCfg.num > SingletonMap.o_Nlv.o_eWD()) {
            let flag = SingletonMap.o_Nlv.o_OOL(true);
            if (flag == true) { // 开始寻宝
                await waitXunBaoOver();
                await xSleep(1500);
                SingletonMap.o_n.o_eAN(gameObjectInstMap.XunbaotuAutoEndWin);
                delete gameObjectInstMap.XunbaotuAutoEndWin;
                await xSleep(1500);
                gameObjectInstMap.ZjmTaskConView.o_lmT();
                // gameObjectInstMap.XunbaotuAutoEndWin.visible = false;
                // 寻宝结束 写入local storage
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.XUN_BAO), true);
                console.log("// 寻宝结束 写入local storage");
            } else {
                // 寻宝结束 写入local storage
                console.log("// 寻宝结束 写入local storage");
                localStorage.setItem(getCurUserDayFlag(TASK_FLAG.XUN_BAO), true);
            }
        }
        else {
            // 寻宝结束 写入local storage
            console.log("// 寻宝结束 写入local storage");
            localStorage.setItem(getCurUserDayFlag(TASK_FLAG.XUN_BAO), true);
        }
    } catch (ex) {
        console.error(ex);
    }

    function check() {
        return xSleep(1000).then(function () {
            if (localStorage.getItem(getCurUserDayFlag(TASK_FLAG.XUN_BAO))) {
                return; // 今日已完成
            }
            else {
                return check();
            }
        });
    }
    return check();
}
// XunbaotuAutoEndWin 出现表示寻宝结束
function waitXunBaoOver() {
    function checkXunBaoNum() {
        return xSleep(1000).then(function () {
            // SingletonMap.o_lAu.stop();
            if (gameObjectInstMap.XunbaotuAutoEndWin && gameObjectInstMap.XunbaotuAutoEndWin.visible) {
                return;
            }
            else {
                return checkXunBaoNum();
            }
        });
    }
    return checkXunBaoNum();
}

// 摇钱树
function yaoQianShu() {
    if (SingletonMap && SingletonMap.o_OgZ && SingletonMap.o_OgZ.o_NII && SingletonMap.o_OgZ.o_NII.freeTime > 0) {
        for (let i = 0; i < SingletonMap.o_OgZ.o_NII.freeTime; i++) {
            SingletonMap.o_Lh.o_lV(1, 0);
        }
    }
}

// 聚宝盆
function juBaoPen() {
    if (SingletonMap && SingletonMap.o_OgZ && SingletonMap.o_OgZ.o_N3h && SingletonMap.o_OgZ.o_N3h.freeTime > 0) {
        for (let i = 0; i < SingletonMap.o_OgZ.o_N3h.freeTime; i++) {
            SingletonMap.o_Lh.o_lV(2, 0);
        }
    }
}

// 在线奖励
//function zhaiXianJianLi() {
//    for (let i = 1; i < 8; i++) {
//        SingletonMap.o_Nsb.o_db(i);
//    }
//}

// 签到
async function qianDao() {
    try {
        SingletonMap.o_Nsb.o_Vm(SingletonMap.o_O6o.o_lWt);
        await xSleep(300);
        for (var i = 1; i < 6; i++) {
            SingletonMap.o_Nsb.o_NKe(i);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

function xSleep(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

var arrPlayerActor = [];
var playerActor = [];
function setPlayerActor(pPlayerActor) {
    arrPlayerActor.push(pPlayerActor);
}

function checkRoleData() {
    if (SingletonMap.RoleData && SingletonMap.RoleData.id) {
        let curPlayerActor = arrPlayerActor.filter((item) => item.o_NiY == SingletonMap.RoleData.id);
        if (curPlayerActor.length > 0) {
            playerActor.push(curPlayerActor.o_NLN);
        }
        if (playerActor.length == 0) {
            setTimeout(checkRoleData, 300);
        }
    }
    else {
        setTimeout(checkRoleData, 300);
    }
}
(function () {
    checkRoleData();
})();

// 绕圈
var isMoveCircle = false;
function stopMoveCircle() {
    isMoveCircle = false;
}

function doMoveCircle() {
    isMoveCircle = true;
    var curX = PlayerActor.o_NLN.currentX;
    var curY = PlayerActor.o_NLN.o_NI3;
    var arrPathPos = [];
    arrPathPos.push({ x: curX + 10, y: curY });
    arrPathPos.push({ x: curX + 10, y: curY + 10 });
    arrPathPos.push({ x: curX, y: curY + 10 });
    arrPathPos.push({ x: curX, y: curY });
    startCircle(arrPathPos);
}

function startCircle(arrPathPos) {
    var curX = PlayerActor.o_NLN.currentX;
    var curY = PlayerActor.o_NLN.o_NI3;
    if (curY == arrPathPos[0].y && curX < arrPathPos[0].x) {// 往右
        SingletonMap.o_OKA.o_NSw(arrPathPos[0].x, arrPathPos[0].y);
    }
    else if (curX == arrPathPos[1].x && curY < arrPathPos[1].y) {// 右下
        SingletonMap.o_OKA.o_NSw(arrPathPos[1].x, arrPathPos[1].y);
    }
    else if (curY == arrPathPos[2].y && curX > arrPathPos[2].x) {// 右下往左
        SingletonMap.o_OKA.o_NSw(arrPathPos[2].x, arrPathPos[2].y);
    }
    else if (curX == arrPathPos[3].x && curY > arrPathPos[3].y) {// 右下往左
        SingletonMap.o_OKA.o_NSw(arrPathPos[3].x, arrPathPos[3].y);
    }
    else {
        SingletonMap.o_OKA.o_NSw(arrPathPos[3].x, arrPathPos[3].y);
    }
    if (isMoveCircle) {
        setTimeout(function () {
            startCircle(arrPathPos);
        }, 50);
    }
}
// 绕圈结束


var isShiShiDisEquip = false; // true表示是手动卸装备
// 脱史诗
var arrShiShiEquipData = [];
function doShiShiDisEquip() {
    if (!hasPri() || isMonthPri()) return;
    isShiShiDisEquip = true;
    const equips = SingletonMap.EquipData.equips;
    arrShiShiEquipData = [];
    for (var i = 0; i < equips.length; i++) {
        arrShiShiEquipData.push(equips[i]);
    }
    for (var i = 0; i < equips.length; i++) {
        if (equips[i] != null && !shenMoEquipTypeMap.includes(equips[i]._stdItem.type)) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_ldH(equip.series, equip.wItemId);
                }, 50);
            })(equips[i]);
        }
    }
}
// 穿史诗
function doShiShiWearEquip() {
    if (!hasPri() || isMonthPri()) return;
    const equips = SingletonMap.EquipData.equips;
    for (var i = 0; i < arrShiShiEquipData.length; i++) {
        if (equips[i] == null && arrShiShiEquipData[i] != null) {
            (function (equip) {
                setTimeout(function () {
                    SingletonMap.o_lqa.o_OBm(equip.series, SingletonMap.o_OqG.o_ty(equip._stdItem.id) ? 1 : 0);
                }, 50);
            })(arrShiShiEquipData[i]);
        }
    }
    isShiShiDisEquip = false;
}

var arrPickUpWhite = ["正道丿软中华", "蚂蚁啃骨头", "眺望的ジ黑龙", "熊出没丶岩岩", "[129]千少っ初晴", "何金银", "简单的╃浩言", "阿落", "倾城", "[338]你的心★欢乐", "撞击的清辞", "[342]扈◎少洋气", "沈メ沧澜"];
var arrPickUpItemId = {}; // 记录道具ID，避免重复触发
var arrPickUpItemName = ["黄金锁子甲碎片", "如意金箍棒碎片", "天蓬の护手碎片", "天河の之心碎片", "悟能の恶骨碎片", "天蓬の护臂碎片", "天蓬の头盔碎片", "九齿の钉耙碎片", "火焰碎片", "切割碎片"];
var arrPickUpItemNameByManager = ["黄金锁子甲碎片", "如意金箍棒碎片", "天蓬の护手碎片", "天河の之心碎片", "悟能の恶骨碎片", "天蓬の护臂碎片", "天蓬の头盔碎片", "九齿の钉耙碎片", "火焰碎片", "切割碎片", "碎片", "霸业", "钻石", "天工神石", "龙魂石", "龙炎石", "龙装", "史诗"];
function isFastPickUpItem(name) {
    if (SingletonMap.RoleData.name == '简单的╃浩言') {
        return arrPickUpItemNameByManager.findIndex(item => item == name || name.indexOf(item) > -1) > -1;
    }
    return arrPickUpItemName.findIndex(item => item == name || name.indexOf(item) > -1) > -1;
}
function isWhiteRoleName(name) {
    return isFastSuction == true || arrPickUpWhite.findIndex(item => name.replace(/\[\d+区\]/, '') == item) > -1;
}
function fastPickUp() {
    try {
        if (isFastSuction == true && SingletonMap && PlayerActor && PlayerActor.o_NLN
            // && isWhiteRoleName(PlayerActor.o_NLN.roleName)
            && SingletonMap.RoleData && SingletonMap.RoleData.o_NoB() == 3
            && SingletonMap.o_lpS && SingletonMap.o_lpS.o_Onf
            && SingletonMap.o_lLF
            && SingletonMap.MapData
            && ['西游降魔'].includes(SingletonMap.MapData.curMapName)
        ) {
            //console.log('西游降魔');
            for (var i = 0; i < SingletonMap.o_lpS.o_Onf.length; i++) {
                var Q = SingletonMap.o_lpS.o_Onf[i];
                if (Q && Q.stdItem && Q.stdItem.name
                    && isFastPickUpItem(Q.stdItem.name)
                    && !arrPickUpItemId[Q.$hashCode + ""]) {
                    arrPickUpItemId[Q.$hashCode + ""] = Q.$hashCode + "";
                    SingletonMap.o_lLF.o_NMj(Q, 3);

                }
            }
            //if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.fastPickUpEnabled) {
            //    callbackObj.fastPickUpEnabled();
            //    return;
            //}
        }
    } catch (ex) {
        //if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.fastPickUpDisabled) {
        //    callbackObj.fastPickUpDisabled();
        //}
    }
    //if (typeof callbackObj !== "undefined" && callbackObj !== null && callbackObj.fastPickUpDisabled) {
    //    callbackObj.fastPickUpDisabled();
    //}
}


function normalPickUp2() {
    SingletonMap.o_e.doOnce(10, SingletonMap.o_eCl,
        function (M) {
            for (var i = 0; i < M.length; i++) {
                var Q = M[i];
                SingletonMap.o_lLF.o_NMj(M[i], 2);
            }
        },
        [SingletonMap.o_lpS.o_Onf]);
}

// 寻龙秘境
function normalPickUpXunLongMiJin() {
    try {
        // if (isFindStrangers()) return; // 有外人不捡
        if (isFastSuction == true && SingletonMap.MapData && ['寻龙秘境'].includes(SingletonMap.MapData.curMapName)) {
            SingletonMap.o_e.doOnce(5, SingletonMap.o_eCl,
                function (M) {
                    for (var i = 0; i < M.length; i++) {
                        var Q = M[i];
                        if (Q && Q.stdItem && Q.stdItem.name && (Q.stdItem.name == "龙魂石" || Q.stdItem.name == "龙炎石")
                            && !arrPickUpItemId[Q.$hashCode + ""]) {
                            SingletonMap.o_lLF.o_NMj(M[i], 3);
                            arrPickUpItemId[Q.$hashCode + ""] = Q.$hashCode + "";
                        }
                    }
                },
                [SingletonMap.o_lpS.o_Onf]);
        }
    } catch (ex) {
        console.error(ex);
    }
}

var arrNormalPickUpId = {};
function normalPickUpShenJie() {
    try {
        if (isFastSuction == true) {
            let isExists = false;
            let appConfig = getAppConfig();

            let pickupShenGeJinHua = false;
            let pickupZhuZhaiShenZhuang = 99;
            let pickupHuenGu = 99;
            let pickupHuenHuang = 99;

            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.PickupShenGeJinHua == 'TRUE') {// 神格精华
                    pickupShenGeJinHua = true;
                }
                if (appConfig.pickupZhuZhaiShenZhuang != "") { // 主宰
                    pickupZhuZhaiShenZhuang = Number(appConfig.pickupZhuZhaiShenZhuang);
                }
                if (appConfig.PickupHuenGu != "") { // 魂骨
                    pickupHuenGu = Number(appConfig.PickupHuenGu);
                }
                if (appConfig.PickupHuenHuang != "") {// 魂环
                    pickupHuenHuang = Number(appConfig.PickupHuenHuang);
                }
            }


            for (var i = 0; i < SingletonMap.o_lpS.o_Onf.length; i++) {
                var Q = SingletonMap.o_lpS.o_Onf[i];
                if (Q && Q.stdItem && !arrPickUpItemId[Q.$hashCode + ""]) {
                    if (pickupShenGeJinHua && Q.stdItem.name.indexOf('神格') > -1) {
                        SingletonMap.o_lLF.o_NMj(Q, 3);
                    }
                    else if (Q.stdItem.type == 77 && Q.stdItem.showQuality >= pickupZhuZhaiShenZhuang) { // 2阶主宰
                        SingletonMap.o_lLF.o_NMj(Q, 3);
                        arrPickUpItemId[Q.$hashCode + ""] = Q.$hashCode + "";
                    }
                    else if (Q.stdItem.type == 76) {
                        if (Q.stdItem.subtype % 10 == 1 && Q.stdItem.showQuality >= pickupHuenHuang) { // 十万年及以上魂环
                            SingletonMap.o_lLF.o_NMj(Q, 3);
                            arrPickUpItemId[Q.$hashCode + ""] = Q.$hashCode + "";
                        }
                        else if (Q.stdItem.subtype % 10 != 1 && Q.stdItem.showQuality >= pickupHuenGu) { // 十万年及以上魂骨
                            SingletonMap.o_lLF.o_NMj(Q, 3);
                            arrPickUpItemId[Q.$hashCode + ""] = Q.$hashCode + "";
                        }
                    }
                }
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

function normalFastPickUp() {
    try {
        if (isFastSuction == true) {
            if (SingletonMap.MapData.curMapName.indexOf('光明神殿') > -1 || SingletonMap.MapData.curMapName.indexOf('黑暗神殿') > -1) {
                normalPickUpShenJie();
            }
            else if (['寻龙秘境'].includes(SingletonMap.MapData.curMapName)) {
                normalPickUpXunLongMiJin();
            }
            else if (['西游降魔'].includes(SingletonMap.MapData.curMapName)) {
                fastPickUp();
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

// 用于挂机拾取
function normalPickUp() {
    try {
        if (SingletonMap && SingletonMap.o_lpS && SingletonMap.o_lpS.o_Onf
            && SingletonMap.RoleData && SingletonMap.RoleData.o_NoB() == 3
            && SingletonMap.o_e && SingletonMap.o_eCl && SingletonMap.o_lLF && SingletonMap.o_Ue
            && SingletonMap.MapData && !['西游降魔'].includes(SingletonMap.MapData.curMapName)) {
            SingletonMap.o_e.doOnce(10, SingletonMap.o_eCl,
                function (M) {
                    var b = SingletonMap.o_Ue.o_lpC();
                    for (var i = 0; i < M.length; i++) {
                        var Q = M[i];
                        if (arrNormalPickUpId[Q.$hashCode + ""]) {
                            continue;
                        }
                        if (!SingletonMap.o_lLF.o_OrJ(Q, b)) continue; // 判断归属

                        if (Q && Q.stdItem && Q.stdItem.name
                            && !isFastPickUpItem(Q.stdItem.name)) {
                            arrNormalPickUpId[Q.$hashCode + ""] = Q.$hashCode + "";
                            SingletonMap.o_lLF.o_NMj(M[i], 3);
                        } else if (Q && Q.$name && ['1', '3'].includes(Q.$name)) { // 金币元宝
                            arrNormalPickUpId[Q.$hashCode + ""] = Q.$hashCode + "";
                            SingletonMap.o_lLF.o_NMj(M[i], 3);
                        }
                    }
                },
                [SingletonMap.o_lpS.o_Onf]);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

function checkJianDonXi(x, y) {
    function check() {
        xSleep(50).then(function () {
            if (x == PlayerActor.o_NLN.currentX && y == PlayerActor.o_NLN.o_NI3) {
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}

// 统计传送石
function totalShuiJiShi() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
        let bagItem = SingletonMap.o_Ue.o_OFL[i];
        if (bagItem && bagItem.name == '传送石') {
            count += bagItem.btCount;
        }
    }
    return count;
}
// 统计背包道具数量
function totalBagItemByName(itemName) {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
        let bagItem = SingletonMap.o_Ue.o_OFL[i];
        if (bagItem && bagItem.name == itemName) {
            count += bagItem.btCount;
        }
    }
    return count;
}

// 统计西游随机符
function totalXiYouShuiJiFu() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
        let bagItem = SingletonMap.o_yx0.o_eiy[i];
        if (bagItem && bagItem.name == '西游随机符') {
            count += bagItem.btCount;
        }
    }
    return count;
}

// 检查人参果数量是否有1组
let buyFlag = false;
async function checkXiYouRenShenGuo() {
    const count = totalXiYouRenShenGuo();
    const tagetCount = 5000 - count;
    if (!hasTwoGroupRenShenGuo()) { // 不够1组
        if (buyFlag == true) return;
        buyFlag = true;
        const buyTimes = tagetCount / 99;
        for (let i = 0; i < buyTimes; i++) {
            if (!lockLifeFlag || hasTwoGroupRenShenGuo()) {
                break;
            }
            runXiyouRecy();
            await buyRenSHenGuo();
        }
        buyFlag = false;
    }
}


function hasTwoGroupRenShenGuo() {
    const count = totalXiYouRenShenGuo();
    return count > 5000;
}

// 统计西游千年人参果 - 每个恢复1万血
function totalXiYouRenShenGuo() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
        let bagItem = SingletonMap.o_yx0.o_eiy[i];
        if (bagItem && bagItem.name == '千年人参果') {
            count += bagItem.btCount;
        }
    }
    return count;
}

// 购买千年人参果
function buyRenSHenGuo() {
    SingletonMap.o_Q0.o__A(10, 99);
    return xSleep(100).then(function () {
        return;
    });
}

// 检查十年生命神液量是否有1组
async function checShenJieShenMinYaoYe() {
    const count = totalShenJieShenMinYaoYe();
    const tagetCount = 5000 - count;
    if (!hasTwoGroupShenMinYaoYe()) { // 不够1组
        if (buyFlag == true) return;
        buyFlag = true;
        const buyTimes = tagetCount / 99;
        for (let i = 0; i < buyTimes; i++) {
            if (!shenJiLockLifeFlag || hasTwoGroupShenMinYaoYe()) {
                break;
            }
            shenJieRecy();
            await buyShenMinYaoYe();
        }
        buyFlag = false;
    }
}

// 是否有1组
function hasTwoGroupShenMinYaoYe() {
    const count = totalShenJieShenMinYaoYe();
    return count > 5000;
}

// 统计神界千年生命神液
function totalShenJieShenMinYaoYe() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_UPB.o_eiy.length; i++) {
        let bagItem = SingletonMap.o_UPB.o_eiy[i];
        if (bagItem && bagItem.name == '万年生命神液') {
            count += bagItem.btCount;
        }
    }
    return count;
}

// 购买万年生命神液
function buyShenMinYaoYe() {
    SingletonMap.o_Q0.o__A(18, 99);
    return xSleep(100).then(function () {
        return;
    });
}

// 检查金创药是否有5组
async function checTeJiJinChuanYao() {
    const count = totalTeJiJinChuanYao();
    const tagetCount = 99 * 5 - count;
    if (!hasFiveGroupTeJiJinChuanYao()) { // 数量不够
        const buyTimes = tagetCount / 99;
        for (let i = 0; i < buyTimes; i++) {
            if (hasFiveGroupTeJiJinChuanYao()) {
                break;
            }
            await buyTeJiJinChuanYao();
        }
    }
}

// 是否有5组金创药
function hasFiveGroupTeJiJinChuanYao() {
    const count = totalTeJiJinChuanYao();
    const tagetCount = 99 * 5 - count;
    return tagetCount < 99;
}

// 统计特级金创药
function totalTeJiJinChuanYao() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
        let bagItem = SingletonMap.o_Ue.o_OFL[i];
        if (bagItem && bagItem.name == '特级金创药') {
            count += bagItem.btCount;
        }
    }
    return count;
}

// 购买特级金创药
function buyTeJiJinChuanYao() {
    SingletonMap.o_Q0.o__A(4, 99);
    return xSleep(100).then(function () {
        return;
    });
}

// 统计神界随机令牌
function totalShuiJiLinPai() {
    let count = 0;
    for (let i = 0; i < SingletonMap.o_UPB.o_eiy.length; i++) {
        let bagItem = SingletonMap.o_UPB.o_eiy[i];
        if (bagItem && bagItem.name == '神界随机令牌') {
            count += bagItem.btCount;
        }
    }
    return count;
}

var buyShuiJiHandle = null; // 购买随机石标识
function useShuiJiProperty(flag) { // flag=true表示是手动触发
    window.clearTimeout(buyShuiJiHandle); // 避免因背包满重复执行购买
    if (!flag && isStopMainRobot == true) return; // 停止挂机了
    let property = null;
    // 在西游地图
    if (SingletonMap.XiyouMgr.o_yWb()) {
        for (let i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
            let bagItem = SingletonMap.o_yx0.o_eiy[i];
            if (bagItem && bagItem.name == '西游随机符') {
                property = bagItem;
                break;
            }
        }
        if (property == null) { // 自动购买随机符
            SingletonMap.o_Q0.o__A(13, 50);
        }
    }
    /*else if (SingletonMap.ShenJieMgr.o_UEk()) {// 在神界
        for (let i = 0; i < SingletonMap.o_UPB.o_eiy.length; i++) {
            let bagItem = SingletonMap.o_UPB.o_eiy[i];
            if (bagItem && bagItem.name == '神界随机令牌') {
                property = bagItem;
                break;
            }
        }
        if (property == null) { // 神界随机令牌
            SingletonMap.o_Q0.o__A(19, 50);
        }
    }*/
    else {
        for (let i = 0; i < SingletonMap.o_Ue.o_OFL.length; i++) {
            let bagItem = SingletonMap.o_Ue.o_OFL[i];
            if (bagItem && bagItem.name == '传送石') {
                property = bagItem;
                break;
            }
        }
        if (property == null) { // 自动购买传送石
            SingletonMap.o_Q0.o__A(7, 50);
        }
    }
    if (property == null) {
        buyShuiJiHandle = setTimeout(useShuiJiProperty, 500);
    } else {
        normalPickUp();
        SingletonMap.o_OPQ.o_Nf7(property.series, 1, property); // 使用随机道具
        /*setTimeout(function() {
            SingletonMap.o_OPQ.o_Nf7(property.series, 1, property); // 使用随机道具
            setTimeout(function() {
                normalPickUp();
                isShuiJi = false;
                clearCurMonster();
            }, 1000);            
        }, 1000);*/
    }
}

var buyShuiJiMiGongHandle = null;
function useShuiJiPropertyMiGong(flag) { // flag=true表示是手动触发
    window.clearTimeout(buyShuiJiMiGongHandle); // 避免因背包满重复执行购买
    let property = null;
    if (!isPaid) return;
    // if (SingletonMap.MapData.curMapName == '永恒迷宫') {
    for (let i = 0; i < SingletonMap.o_yx0.o_eiy.length; i++) {
        let bagItem = SingletonMap.o_yx0.o_eiy[i];
        if (bagItem && bagItem.name == '西游随机符') {
            property = bagItem;
            break;
        }
    }
    if (property == null) { // 自动购买随机符
        SingletonMap.o_Q0.o__A(13, 50);
    }

    if (property == null) {
        buyShuiJiMiGongHandle = setTimeout(useShuiJiPropertyMiGong, 500);
    } else {
        normalPickUp();
        SingletonMap.o_OPQ.o_Nf7(property.series, 1, property); // 使用随机道具
    }
    // }
}

function isAttackMap() {
    //if (SingletonMap && SingletonMap.MapData) {
    // return !['西游降魔', '神魔战场'].includes(SingletonMap.MapData.curMapName);
    //    return !['西游降魔'].includes(SingletonMap.MapData.curMapName);
    //}
    return true;
}

function getDistance(x1, y1, x2, y2) {
    return Math.sqrt(Math.abs(x1 - x2) * Math.abs(x1 - x2) + Math.abs(y1 - y2) * Math.abs(y1 - y2));
}

function isNormalHitDistance(x1, y1, x2, y2) {
    return getDistance(x1, y1, x2, y2) <= 2;
}

function isAttackDistance(x1, y1, x2, y2) {
    return getDistance(x1, y1, x2, y2) < 3;
}

function isRemoteAttackDistance(x1, y1, x2, y2) {
    return getDistance(x1, y1, x2, y2) < 10;
}

function getSkill(skillName) {
    let skill = null;
    try {
        for (var key in SkillTypes) {
            if (SingletonMap.o_h7.o_Mr(SkillTypes[key]) && SingletonMap.o_h7.o_Mr(SkillTypes[key]).stdSkill.name.indexOf(skillName) != -1) {
                skill = SingletonMap.o_h7.o_Mr(SkillTypes[key]);
                break;
            } else if (SingletonMap.o_h7.o_JA(SkillTypes[key]) && SingletonMap.o_h7.o_JA(SkillTypes[key]).stdSkill.name.indexOf(skillName) != -1) {
                skill = SingletonMap.o_h7.o_JA(SkillTypes[key]);
                break;
            }
        }
        //if (skill && (skill.o_VM == 0 || skill.o_VM < SingletonMap.o_e.currTimer)) {
        return skill;
        //}
    }
    catch (ex) { }
    return null;
}

function getFaShiHeJiSkill(skillName) {
    let skill = null;
    for (var key in SkillTypes) {
        if (SingletonMap.o_h7.o_JA(SkillTypes[key]) && SingletonMap.o_h7.o_JA(SkillTypes[key]).stdSkill.name.indexOf(skillName) != -1) {
            skill = SingletonMap.o_h7.o_JA(SkillTypes[key]);
            break;
        }
    }
    return skill;
}

function getPowerSkill(skillName) {
    let skill = null;
    if (SingletonMap && SingletonMap.o_egD && SingletonMap.o_egD.o_lon) {
        for (var key in SingletonMap.o_egD.o_lon) {
            if (key && SingletonMap.o_egD.o_lon[key] && SingletonMap.o_egD.o_lon[key].stdSkill.name.indexOf(skillName) != -1) {
                skill = SingletonMap.o_egD.o_lon[key];
                break;
            }
        }
    }
    return skill;
}


var curMonster = null;
function setCurMonster(M) {
    if (M == null) return;
    try {
        if ((M.curHP > 100 || (M.curHP > 0 && M.roleName.indexOf('旗') > -1) || (M.curHP > 0 && M.roleName.indexOf('镖车') > -1))
            && M.roleName.indexOf('宝箱') == -1
            && M.roleName.indexOf('矿位') == -1
            && M.roleName.indexOf('矿工') == -1
            && M.roleName.indexOf('神奇的水晶') == -1
            && M.roleName.indexOf('炸弹') == -1
            && M.roleName.indexOf('加成') == -1
            && M.roleName.indexOf('口粮') == -1
            && M.roleName.indexOf('皇座') == -1
            && M.roleName != '龙珠'
            && M.roleName != '魔灵草'
            && M.roleName != '星脉灵石'
            && SingletonMap.MapData.curMapName != '王城'
            && !isSeat(M)) {
            if (SingletonMap.MapData && SingletonMap.MapData.curMapName && SingletonMap.MapData.curMapName.indexOf) {
                if ( SingletonMap.MapData.curMapName.indexOf('别墅')> -1) {
                    curMonster = M;
                }
                else if (SingletonMap.ShenJieMgr.o_UEk() && SingletonMap.MapData.curMapName.indexOf('神界主城') == -1) {
                    curMonster = M;
                }
                else if (SingletonMap.o_h7.o_lDb(M)) {
                    curMonster = M;
                }
            }
            else if (SingletonMap.o_h7.o_lDb(M)) {
                curMonster = M;
            }
        }
    }
    catch (e) {
        console.error(e);
    }
}

function isSeat(M) {
    if (SingletonMap.MapData.curMapName == '蟠桃盛宴' && M instanceof Monster && (M.roleName.indexOf('天王') > -1 || M.roleName.indexOf('天尊') > -1 || M.roleName.indexOf('大帝') > -1)) {
        return true;
    }
    return false;
}

function clearCurMonster() {
    curMonster = null;
}

var isFullPower = false;
function enableFullPower() {
    isFullPower = true;
}

function disableFullPower() {
    isFullPower = false;
}

/** 监听人物列表 **/
var playerMap = [];
function refreshPlayerList() {
    try {
        playerMap = [];
        let len = SingletonMap.o_OFM.o_Nx1.length;
        let isChange = false;
        for (let i = 0; i < len; i++) {
            let M = SingletonMap.o_OFM.o_Nx1[i];
            if (M instanceof PlayerActor && !playerMap.includes[M.roleName]) {
                playerMap.push(M.roleName);
                isChange = true;
            }
        }
        if (isChange && typeof callbackObj !== "undefined" && callbackObj !== null) {
            callbackObj.onJsRoleover(playerMap.join("###"));
        }
    } catch (ex) { }
}

function isFindStrangers() { //检测身边是否出现陌生人
    let isExists = false;
    let appConfig = getAppConfig();
    appConfig = appConfig.replaceAll("|", "#");
    try {
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
        }
        appConfig.PowerWhiteList = ["正道丿软中华", "帝尊", "眺望的ジ黑龙", "熊出没丶岩岩", "熊出没丶吉吉",
            "噗", "[129]千少っ初晴", "何金银", "简单的╃浩言", "阿落", "倾城", "[338]你的心★欢乐", "撞击的清辞",
            "[342]扈◎少洋气", "风吹裙子飘", "沈メ沧澜", "闲书", "罪oo爱", "紫00风", "★充十块玩玩★", "欧阳",
            "全‖承天", "謨曦", "[342]扈◎少洋气", "★充十块玩玩★", "醉メ清风", "困龙宇蕴", "青丝绕指柔", "肖肖☆",
            "充钱也赢不了", "石岘熊哥", "永恒丶逆天行", "錵卷", "永恒丶K仔", "永恒丶三叹", "卍軍哥卐"];
        const curPowerWhiteList = "#" + appConfig.PowerWhiteList.join("#") + "#"; // 内置蟠桃群成员
        let len = SingletonMap.o_OFM.o_Nx1.length;
        for (let i = 0; i < len; i++) {
            let M = SingletonMap.o_OFM.o_Nx1[i];
            if (M && M.roleName == PlayerActor.o_NLN.roleName) {
                continue;
            }
            if (M && M instanceof PlayerActor) {
                if (!BlacklistMonitors.isPowerWhitePlayer(M.roleName)) {
                    isExists = true;
                    break;
                }
            }
        }
    } catch (ex) {
        isExists = true;
    }
    return isExists;
}

function isDoubleAttackPlayer(M) {
    return isDoubleAttck && M instanceof PlayerActor && M.curHP > 100;
}

class SkillLock {
    static state = false;

    static isLock() {
        return SkillLock.state == true;
    }

    static lock() {
        SkillLock.state = true;
        // console.warn('skill lock....');
    }

    static unLock() {
        SkillLock.state = false;
        // console.warn('skill unLock....');
    }
}

const arrFullSkillMap = ["藏经阁", "北斗七星君", "八仙过海", "三清天尊", "四大天王", "如来", "天宫", "玉皇大帝", "职业巅峰竞赛场"];

function resetSysVal() {
    isFindWay = false; // 是否正在寻路
    isExistsMonster = false; // 是否存在怪物
    lastHitTimestamp = null; // 最近一次发起攻击的时间
}

// 查找黑名单玩家
function findBadPlayer() {
    let badPlayer = null;
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = appConfig.replaceAll("|", "#");
            appConfig = JSON.parse(appConfig);
        }
        let arrBadPlayer = [];
        let len = SingletonMap.o_OFM.o_Nx1.length;
        for (let i = len - 1; i >= 0; i--) {
            const M = SingletonMap.o_OFM.o_Nx1[i];
            if (M && M.roleName == PlayerActor.o_NLN.roleName) {
                continue;
            }
            if (M && M instanceof PlayerActor && !M.masterName && M.curHP > 0) {
                // 攻击任何人，不打白名单
                if (appConfig.AttackEveryOne == "TRUE" 
                    && !BlacklistMonitors.isWhitePlayer(M.roleName)
                    && !BlacklistMonitors.isWhiteGuild(M.guildName) 
                    && SingletonMap.o_h7.o_lDb(M)) {
                    arrBadPlayer.push(M);
                    continue;
                }
                // 黑名单行会
                if (BlacklistMonitors.isBlackGuild(M.guildName) 
                    && SingletonMap.o_h7.o_lDb(M)
                    && !BlacklistMonitors.isWhitePlayer(M.roleName)
                    && !BlacklistMonitors.isWhiteGuild(M.guildName)) {
                    arrBadPlayer.push(M);
                    continue;
                }
                // 黑名单
                if (BlacklistMonitors.isBadPlayer(M.roleName)
                    && !BlacklistMonitors.isWhitePlayer(M.roleName)
                    && !BlacklistMonitors.isWhiteGuild(M.guildName)
                    && SingletonMap.o_h7.o_lDb(M)) {
                    arrBadPlayer.push(M);
                }
            }
        }

        // 距离近的优先
        let minDistance = null;
        for (let i = 0; i < arrBadPlayer.length; i++) {
            let M = arrBadPlayer[i];
            if (minDistance == null) {
                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                badPlayer = M;
            }
            else if (minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) {
                minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                badPlayer = M;
            }
        }
    } catch (ex) {
        console.error(ex);
    }
    return badPlayer;
}

function checkLastHitTimestamp() {

}

var isFindWay = false;
var isExistsMonster = false;
var isShuiJi = false;
var lastHitTimestamp = null;
function attackBelongMonster() {
    try {
        if (lastHitTimestamp == null) {
            lastHitTimestamp = Date.parse(new Date()); // 记录当前时间戳
        }
        isExistsMonster = curMonster == null ? false : true;
        if (isXiYouGuaJi || isGuaJi || isShenJieGuaJi) {
            normalPickUp();

            // 还在追杀中
            if (curMonster && SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode)) {
                setTimeout(attackBelongMonster, 1500);
                return;
            }
            // 先攻击黑名单
            let badPlayer = findBadPlayer();
            if (badPlayer) {
                console.log(`发现黑名单人员 ==> ${badPlayer.roleName}`);
                if (isAttackDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, badPlayer.currentX, badPlayer.o_NI3)) {
                    SingletonMap.o_OKA.o_Pc(badPlayer, true);
                    setCurMonster(badPlayer);
                    isFindWay = false;
                }
                else {
                    isFindWay = true;
                    doFindWay(badPlayer);
                }
                setTimeout(attackBelongMonster, 1500);
                return;
            }

            if (curMonster && !isSelfBelong(curMonster)) { // 挂机不打非归属怪
                clearCurMonster();
                SingletonMap.o_lAu.stop();
                isExistsMonster = false;
            }
            if (SingletonMap && SingletonMap.o_OFM && SingletonMap.o_lAu && SingletonMap.o_OFM.o_Nx1 && SingletonMap.RoleData && !isShuiJi) {
                let len = SingletonMap.o_OFM.o_Nx1.length;
                // 没有手动选中怪物
                if (!isExistsMonster) {
                    for (let i = len - 1; i >= 0; i--) {
                        const M = SingletonMap.o_OFM.o_Nx1[i];
                        if (M instanceof Monster && !M.masterName && M.o_O97 == 0 && M.curHP > 100) {
                            // 先打归属自己的怪
                            if (isSelfBelong(M)) {
                                SingletonMap.o_lAu.stop();
                                SingletonMap.o_OKA.o_Pc(M, true);
                                setCurMonster(M);
                                isExistsMonster = true;
                                isFindWay = false;
                                break;
                            }
                        }
                    }
                }
                if (!isExistsMonster && !isFindWay) { // 没有归属怪物并且没在寻路
                    let targetMonster = null;
                    let minDistance = null;
                    for (let i = len - 1; i >= 0; i--) {
                        const M = SingletonMap.o_OFM.o_Nx1[i];
                        if (M instanceof Monster && !M.masterName && M.o_O97 == 0 && M.curHP > 100) {
                            // 打没有归属的怪
                            if (M.belongingName == '') {
                                if (targetMonster == null) {
                                    targetMonster = M;
                                    minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                    continue;
                                }
                                else if (minDistance && minDistance > getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3)) { // 距离近的优先
                                    targetMonster = M;
                                    minDistance = getDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, M.currentX, M.o_NI3);
                                    continue;
                                }
                            }
                        }
                    }
                    if (targetMonster) {
                        if (isAttackDistance(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, targetMonster.currentX, targetMonster.o_NI3)) {
                            SingletonMap.o_OKA.o_Pc(targetMonster, true);
                            setCurMonster(targetMonster);
                            isFindWay = false;
                        }
                        else {
                            isFindWay = true;
                            normalPickUp(); // 寻路前先捡取
                            doFindWay(targetMonster);
                        }
                        isExistsMonster = true;
                    }
                }
                if (!isExistsMonster && !isFindWay) {
                    clearCurMonster();
                    normalPickUp(); //离开前先捡取
                    xSleep(2000).then(function () {
                        if (!isShuiJi) {
                            isShuiJi = true;
                            useShuiJiProperty();
                        }
                    });
                }
            }
            normalPickUp(); //离开前先捡取
            if (isShuiJi) {
                xSleep(1000).then(function () {
                    attackBelongMonster();
                });
            }
            else if (isExistsMonster) {
                lastHitTimestamp = Date.parse(new Date());// 更新最近一次打怪时间戳
                xSleep(2000).then(function () {
                    attackBelongMonster();
                });
            } else {
                clearCurMonster();
                let monsterNum = getShenYuMonsterNum();
                if (monsterNum && monsterNum < 5) { // 剩余boss小于5只
                    toNextMap();
                    xSleep(2000).then(function () {
                        attackBelongMonster();
                    });
                }
                else {
                    xSleep(2000).then(function () {
                        attackBelongMonster();
                    });
                }
            }
        }
    } catch (ex) {
        console.error(ex);
    }
}

// 获取剩余boss数量
function getShenYuMonsterNum() {
    if (SingletonMap.XiyouMgr.o_yWb()) { // 西游
        return SingletonMap.o_eB9.o_ysG;
    }
    if (SingletonMap.ShenJieMgr.o_UEk()) { // 神界
        return SingletonMap.o_eB9.o_UEY;
    }
}

// 切换挂机地图
function toNextMap() {
    if (SingletonMap.XiyouMgr.o_yWb()) { // 西游
        beforLoopXiYouGuaJiMap();
    }
    else if (SingletonMap.ShenJieMgr.o_UEk()) { // 神界
        beforGotoShenJieCurMap();
    }
    else {
        beforloopGuaJiMap();// 天界
    }
}

function doFindWay(pMonster) {
    try {
        if (isFindWay) {
            var curX = PlayerActor.o_NLN.currentX;
            var curY = PlayerActor.o_NLN.o_NI3;
            if (Math.abs(curX - pMonster.currentX) + Math.abs(curY - pMonster.o_NI3) > 6) {
                SingletonMap.o_lAu.stop();
                SingletonMap.o_OKA.o_NSw(pMonster.currentX, pMonster.o_NI3);
                setTimeout(function () {
                    doFindWay(pMonster);
                }, 500);
            } else {
                SingletonMap.o_OKA.o_Pc(pMonster, true);
                setTimeout(function () {
                    isFindWay = false;
                }, 300);
            }
        }
    } catch (ex) {
    }
}

let isCurMapGuaJi = false;
function fuHuo() {
    if (isGuaJi) {
        commandSocket.send(hex2buf("eecc03008b2300"));
        setTimeout(function () {
            loopGuaJiMap(curGuaJiMapIndex);
        }, 1000);
    }
    else if (isXiYouGuaJi) {
        commandSocket.send(hex2buf("eecc03008b2300"));
        setTimeout(function () {
            gotoXiYouGuaJiCurMap();
        }, 1000);
    }
    else if (isShenJieGuaJi) {
        commandSocket.send(hex2buf("eecc03008b2300"));
        if (hasPri()) { // 神界vip挂机
            setTimeout(function () {
                gotoShenJieVipGuaJiCurMap();
            }, 1000);
        }
        else {// 普通神界挂机
            setTimeout(function () {
                gotoShenJieGuaJiCurMap();
            }, 1000);
        }
    }
}

/** 监听血量，0血复活 **/
function checkHp() {
    try {
        if (typeof PlayerActor != 'undefined' && PlayerActor.o_NLN && PlayerActor.o_NLN.curHP <= 0) {
            fuHuo();
        }
    } catch (ex) {
        console.error(ex);
    }
    setTimeout(checkHp, 1000);
}


function checkMode() {
    try {
        if (SingletonMap && SingletonMap.Ladder3v3Mgr && SingletonMap.Ladder1v1Mgr && SingletonMap.JobWarMgr) {
            if (SingletonMap.Ladder3v3Mgr.is3v3Map() || SingletonMap.Ladder1v1Mgr.o_yrY() || SingletonMap.JobWarMgr.o_yrY()) {
                return;
            }
        }
        if (SingletonMap && SingletonMap.MapData && SingletonMap.MapData.mapId >= 1140001 && SingletonMap.MapData.mapId < 1230001) {
            return;
        }
        if (SingletonMap && SingletonMap.MapData && arrNoNeedChangeModeMap.indexOf("|" + SingletonMap.MapData.curMapName + "|") == -1) {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                // 强制模式
                if (appConfig.AttackModeQianZhi) {
                    if (appConfig.AttackModeQianZhi != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModeQianZhi));
                    }
                }
                // 在西游
                else if (appConfig.AttackModelXiYou && SingletonMap.XiyouMgr.o_yWb()) {
                    if (appConfig.AttackModelXiYou != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelXiYou));
                    }
                }
                // 光明神殿
                else if (appConfig.AttackModelGuanMinShenDiang && SingletonMap.MapData.curMapName.indexOf('光明神殿') != -1) {
                    if (appConfig.AttackModelGuanMinShenDiang != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelGuanMinShenDiang));
                    }
                }
                // 在神界
                else if (appConfig.AttackModelShenJie && SingletonMap.ShenJieMgr.o_UEk()) {
                    if (appConfig.AttackModelShenJie != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelShenJie));
                    }
                }
                // 轮回boss
                else if (appConfig.AttackModelLunHui && SingletonMap.MapData.curMapName.indexOf('轮回秘境') != -1) {
                    if (appConfig.AttackModelLunHui != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelLunHui));
                    }
                }
                // 神龙山庄
                else if (appConfig.AttackModelShenLongShanZhuan && SingletonMap.MapData.curMapName.indexOf('神龙山庄') != -1) {
                    if (appConfig.AttackModelShenLongShanZhuan != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelShenLongShanZhuan));
                    }
                }
                // 玛法幻境
                else if (appConfig.AttackModelMafaHuanJin && SingletonMap.o_Ntl.o_bj()) {
                    if (appConfig.AttackModelMafaHuanJin != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelMafaHuanJin));
                    }
                }
                // 跨服皇陵
                else if (appConfig.AttackModelKuaFuHuanLing && SingletonMap.MapData.curMapFilePath == 'ShenYuBoss-KF') {
                    if (appConfig.AttackModelKuaFuHuanLing != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelKuaFuHuanLing));
                    }
                }
                // 苍月岛
                else if (appConfig.AttackModelChanYueDao && SingletonMap.MapData.curMapName == '苍月岛') {
                    if (appConfig.AttackModelChanYueDao != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelChanYueDao));
                    }
                }
                // 苍月秘境
                else if (appConfig.AttackModelChanYueMiJin && SingletonMap.MapData.curMapName == '苍月秘境') {
                    if (appConfig.AttackModelChanYueMiJin != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelChanYueMiJin));
                    }
                }
                // 神龙巢穴
                else if (appConfig.AttackModelShenLongCaoXue && SingletonMap.MapData.curMapName == '神龙巢穴') {
                    if (appConfig.AttackModelShenLongCaoXue != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModelShenLongCaoXue));
                    }
                }
                // 摸金巢穴
                else if (appConfig.AttackModeMoJinCaoXue && SingletonMap.MapData.curMapName == '摸金巢穴') {
                    if (appConfig.AttackModeMoJinCaoXue != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModeMoJinCaoXue));
                    }
                }
                // 修罗幻境
                else if (appConfig.AttackModeXiuLuoHuanJin && SingletonMap.MapData.curMapName == '修罗幻境') {
                    if (appConfig.AttackModeXiuLuoHuanJin != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.AttackModeXiuLuoHuanJin));
                    }
                }
                // 其他跨服
                else if (appConfig.CrossServeAttackModel && SingletonMap.ServerCross.o_ePe) { // 跨服换模式
                    if (appConfig.CrossServeAttackModel != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.CrossServeAttackModel));
                    }
                }
                // 本服
                else if (appConfig.LocalServeAttackModel && !SingletonMap.ServerCross.o_ePe) { // 本服换模式
                    if (appConfig.LocalServeAttackModel != SingletonMap.o_bA.o_OYs) {
                        SingletonMap.o_bA.o_ug(parseInt(appConfig.LocalServeAttackModel));
                    }
                }
            }
        }
    }
    catch (ex) {
        // console.error(ex);
    }
    // setTimeout(checkMode, 2000);
}


/** 监听血量结束 **/

function moveTo(x, y) {
    var curX = PlayerActor.o_NLN.currentX;
    var curY = PlayerActor.o_NLN.o_NI3;
    if (curX != x || curY != y) {
        SingletonMap.o_OKA.o_NSw(x, y);
        setTimeout(function () {
            moveTo(x, y);
        }, 100);
    }
}

var arrBossRoleName = ["火德真君", "巨灵神", "混天大圣", "六耳猕猴", "欲界执行官", "欲界典狱长", "欲界将军", "虎师异兽", "巨斧守卫", "弓警守卫", "巨魔神", "地狱妖猴"];
function findBoss() {
    let boss = null;
    if (SingletonMap && SingletonMap.o_OFM && SingletonMap.o_OFM.o_Nx1) {
        let len = SingletonMap.o_OFM.o_Nx1.length;
        let minTimestamp = -1;
        for (let i = 0; i < len; i++) {
            let M = SingletonMap.o_OFM.o_Nx1[i];
            if (M instanceof Monster && !M.masterName && arrBossRoleName.includes(M.roleName)) {
                if (minTimestamp == -1 || M.o_O97 < minTimestamp) {
                    minTimestamp = M.o_O97;
                    boss = M;
                }
            }
        }
    }
    // 超过2分钟复活的不等
    if (boss && boss.o_O97 > 0 && (boss.o_O97 - Date.parse(new Date())) > 120000) {
        boss = null;
    }
    return boss;
}

/** 西游boss **/
var arrXiYouMapBoss = [{
    name: "魏征",
    id: 1,
    layer: 1,
    posX: 41,
    posY: 22,
    refreshTimestamp: 0
}, {
    name: "魏征",
    id: 1,
    layer: 2,
    posX: 41,
    posY: 22,
    refreshTimestamp: 0
}, {
    name: "炎帝",
    id: 1,
    layer: 1,
    posX: 114,
    posY: 76,
    refreshTimestamp: 0
}, {
    name: "炎帝",
    id: 1,
    layer: 2,
    posX: 114,
    posY: 76,
    refreshTimestamp: 0
}, {
    name: "白龙",
    id: 2,
    layer: 1,
    posX: 24,
    posY: 52,
    refreshTimestamp: 0
}, {
    name: "白龙",
    id: 2,
    layer: 2,
    posX: 24,
    posY: 52,
    refreshTimestamp: 0
}, {
    name: "黄狮",
    id: 2,
    layer: 1,
    posX: 104,
    posY: 115,
    refreshTimestamp: 0
}, {
    name: "黄狮",
    id: 2,
    layer: 2,
    posX: 104,
    posY: 115,
    refreshTimestamp: 0
}, {
    name: "朱灵官",
    id: 3,
    layer: 1,
    posX: 26,
    posY: 47,
    refreshTimestamp: 0
}, {
    name: "紫薇北极大帝",
    id: 3,
    layer: 1,
    posX: 117,
    posY: 97,
    refreshTimestamp: 0
}, {
    name: "黑熊精",
    id: 4,
    layer: 1,
    posX: 88,
    posY: 67,
    refreshTimestamp: 0
}, {
    name: "六耳猕猴",
    id: 4,
    layer: 1,
    posX: 17,
    posY: 91,
    refreshTimestamp: 0
}, {
    name: "九头附马",
    id: 5,
    layer: 1,
    posX: 76,
    posY: 92,
    refreshTimestamp: 0
}, {
    name: "通风大圣",
    id: 5,
    layer: 1,
    posX: 107,
    posY: 45,
    refreshTimestamp: 0
}, {
    name: "银角大王",
    id: 6,
    layer: 1,
    posX: 124,
    posY: 140,
    refreshTimestamp: 0
}, {
    name: "洞阴大帝",
    id: 6,
    layer: 1,
    posX: 88,
    posY: 22,
    refreshTimestamp: 0
}, {
    name: "孔雀大明菩萨",
    id: 7,
    layer: 1,
    posX: 33,
    posY: 155,
    refreshTimestamp: 0
}, {
    name: "灾惑天君",
    id: 7,
    layer: 1,
    posX: 29,
    posY: 34,
    refreshTimestamp: 0
}, {
    name: "巨灵神",
    id: 8,
    layer: 1,
    posX: 6,
    posY: 76,
    refreshTimestamp: 0
}, {
    name: "巨灵神",
    id: 8,
    layer: 2,
    posX: 6,
    posY: 76,
    refreshTimestamp: 0
}, {
    name: "火德真君",
    id: 8,
    layer: 1,
    posX: 106,
    posY: 13,
    refreshTimestamp: 0
}, {
    name: "火德真君",
    id: 8,
    layer: 2,
    posX: 106,
    posY: 13,
    refreshTimestamp: 0
}, {
    name: "六耳猕猴",
    id: 9,
    layer: 1,
    posX: 27,
    posY: 83,
    refreshTimestamp: 0
}, {
    name: "六耳猕猴",
    id: 9,
    layer: 2,
    posX: 27,
    posY: 83,
    refreshTimestamp: 0
}, {
    name: "混天大圣",
    id: 9,
    layer: 1,
    posX: 75,
    posY: 25,
    refreshTimestamp: 0
}, {
    name: "混天大圣",
    id: 9,
    layer: 2,
    posX: 75,
    posY: 25,
    refreshTimestamp: 0
}, {
    name: "欲界执行官",
    id: 10,
    layer: 1,
    posX: 52,
    posY: 27,
    refreshTimestamp: 0
}, {
    name: "欲界典狱长",
    id: 10,
    layer: 1,
    posX: 37,
    posY: 77,
    refreshTimestamp: 0
}, {
    name: "欲界将军",
    id: 11,
    layer: 1,
    posX: 19,
    posY: 60,
    refreshTimestamp: 0
}, {
    name: "虎狮异兽",
    id: 11,
    layer: 1,
    posX: 82,
    posY: 22,
    refreshTimestamp: 0
}
];

async function loopXiYouBoss() {
    // 判断地图，先回到西游主城
    for (let i = 0; i < arrXiYouMapBoss.length; i++) {
        let bossCfg = arrXiYouMapBoss[i];
        if (bossCfg.refreshTimestamp != 0 && bossCfg.refreshTimestamp > getServerTime()) { // 还没刷新
            continue;
        }
        await new Promise((resolve, reject) => {
            SingletonMap.o_yKf.o_yda(bossCfg.id, bossCfg.layer);
            setTimeout(function () {
                checkXiYouBoss(bossCfg);
                let bossInst = SingletonMap.o_OFM.o_Nx1.find(item => item.roleName = bossCfg.name);
                if (bossInst) { // 找到boss
                    if (bossInst instanceof Monster && !bossInst.masterName && bossInst.curHP > 100 && !M.belongingName) { // 攻击boss
                        console.log(" 攻击boss");
                        attackBoss(bossInst);
                    }
                    else { // 怪物已死亡, 更新刷新时间
                        console.log("怪物已死亡, 更新刷新时间");
                        bossCfg.refreshTimestamp = bossInst.o_O97;
                        resolve();
                    }
                }
                else {
                    console.log("没找到boss");
                    resolve(); // 没找到boss
                }
            }, 1000);
        });
        gotoXiYuoMainMap();
    }
}

async function attackBoss(bossInst) {
    await new Promise((resolve, reject) => {
        SingletonMap.o_lAu.stop();
        SingletonMap.o_OKA.o_Pc(bossInst, true);
        setCurMonster(bossInst);
        checkBossHP(bossInst, resolve);
    });
}

function checkBossHP(bossInst, resolve) {
    if (bossInst.curHP <= 0) {
        resolve();
    }
    else {
        setTimeout(function () {
            checkBossHP(bossInst, resolve);
        }, 300);
    }
}

async function checkXiYouBoss(bossCfg) {
    console.log("checkXiYouBoss...");
    await new Promise((resolve, reject) => {
        SingletonMap.o_lAu.stop();
        SingletonMap.o_OKA.o_NSw(bossCfg.posX, bossCfg.posY);
        checkFindWay(bossCfg.posX, bossCfg.posY, resolve);
    });
}

function checkFindWay(x, y, resolve) {
    console.log("checkFindWay...");
    var curX = PlayerActor.o_NLN.currentX;
    var curY = PlayerActor.o_NLN.o_NI3;
    if (curX == x && curY == y) {
        console.log("到达目的地...");
        resolve();
    } else {
        setTimeout(function () {
            checkFindWay(x, y, resolve);
        }, 300);
    }
}

function checkEnterRole() {
    if (typeof callbackObj !== "undefined" && callbackObj !== null && SingletonMap && SingletonMap.o_n 
            && SingletonMap.ServerChooseData && SingletonMap.ServerChooseData.enterRole) {
        const serverId = SingletonMap.ServerChooseData.enterRole.serverId;
        const name = SingletonMap.ServerChooseData.enterRole.name;
        const roleId = SingletonMap.ServerChooseData.enterRole.id;
        const pf = SingletonMap.ServerChooseData.pf;
        if (pf && serverId && name && roleId && !SingletonMap.o_n.isOpen("CrossLoadingWin")) {
            callbackObj.onJsEnterRole(pf, serverId, roleId, name);
        } else {
            setTimeout(checkEnterRole, 500);
        }
    } else {
        setTimeout(checkEnterRole, 500);
    }
}
(function () {
    checkEnterRole();
})();

async function refreshGuildList() {
    try {
        SingletonMap.o_lWu.o_Nsj();
        await xSleep(500);
        if (typeof callbackObj !== "undefined" && callbackObj !== null 
            && SingletonMap && SingletonMap.GuildDataMgr && SingletonMap.GuildDataMgr.guildList
            && SingletonMap.GuildDataMgr.guildList.length > 0) {
            callbackObj.onRefreshGuildList(SingletonMap.GuildDataMgr.guildList.map(M => M.guildName));
        }
     }
    catch(e){
    }   
}

function watiExitMafaDuShi() {
    // commandSocket.send(hex2buf('eecc0200b36b'));
    function doWatiExitMafaDuShi() {
        return xSleep(1000).then(function () {
            //if (!isXinKongZhiLiTask) return;
            if (SingletonMap.MapData.curMapName.indexOf("别墅") == -1) {
                return;
            }
            else {
                return doWatiExitMafaDuShi();
            }
        });
    }
    return doWatiExitMafaDuShi();
}
/**刷星空之力结束 **/

class MoveLock {
    static state = false;
    static stoped = false;

    static isLock() {// true值表示不可移动
        return MoveLock.state == true;
    }

    static lock() {
        MoveLock.state = true;
        //console.warn('move lock....');
    }

    static unLock() {
        MoveLock.state = false;
        //console.warn('move unLock....');
    }

    static isStop() {
        return MoveLock.stoped == true;
    }

    static stop() {
        MoveLock.stoped = true;
    }

    static resume() {
        MoveLock.stoped = false;
    }
}

let repeatTime = 7000; // 超出时长重发指令

function isCrossTime() {
    let result = false;
    try {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();

        if (curHours >= 10 && curHours < 23) {
            result = true;
        }
    }
    catch (e) {
    }
    return result;
}

function isCrossMap(mapId) {
    let result = false;
    try {
        return mapId.indexOf('kf') != -1;
    }
    catch (e) {
    }
    return result;
}

/** 神魔边境 **/
var shenMoBianJinGuaJi = true;

// 人物是否处于静止不动
var lastRolePositionInfo = {};
function roleIsStatic() {

}

function getCurTime() {
    return new Date().getTime();
}

async function awaitToMap(mapName) {
    let beginTime = getCurTime();
    function check() {
        return xSleep(100).then(function () {
            try {
                // 辅助停止
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }
                if (!mapName) {
                    return;
                }
                // curMapName=> 王城  mapName=>王城
                if (mapName && SingletonMap.MapData.curMapName.indexOf(mapName) > -1) {
                    return;
                }
                // curMapName=> 王城  mapName=>玛法幻境|王城
                if (mapName && mapName.indexOf(SingletonMap.MapData.curMapName) > -1) {
                    return;
                }
            }
            catch (e) {
                return;
            }
            return check();
        });
    }
    return check();
}

// 离开地图
async function awaitLeaveMap(mapName) {
    function check() {
        return xSleep(100).then(function () {
            try {
                // 辅助停止
                if (!mainRobotIsRunning()) {
                    return;
                }
                if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                    // loading
                    return check();
                }
                if (SingletonMap.MapData.curMapName != mapName) {
                    return;
                }
            }
            catch (e) {
                return;
            }
            return check();
        });
    }
    return check();
}

async function awaitToShenMoDaLu() {
    function check() {
        return xSleep(100).then(function () {
            // 辅助停止
            if (!mainRobotIsRunning()) {
                return;
            }
            if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                // loading
                return check();
            }
            if (["神族主城", "魔族主城"].includes(SingletonMap.MapData.curMapName)) {
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}

async function awaitToShenMoBianJin() {
    function check() {
        return xSleep(100).then(function () {
            // 辅助停止
            if (!mainRobotIsRunning()) {
                return;
            }
            if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                // loading
                return check();
            }
            if (["神魔边境"].includes(SingletonMap.MapData.curMapName)) {
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}

/** 神魔边境结束 **/

/* 练功房1小时领一次 */
function autoLianGongFang() {
    try {
        if (SingletonMap && SingletonMap.o_O95) {
            SingletonMap.o_O95.o_OdO();
            SingletonMap.o_O95.o_lIZ();
        }
    } catch (ex) {
        console.error(ex);
    }
    setTimeout(autoLianGongFang, 3600000);
}
(function () {
    autoLianGongFang();
})();


/** 丢出装备  **/
function throwEquipment() {
    try {
        let len = SingletonMap.o_Ue.o_OFL.length;
        for (let i = 0; i < len; i++) {
            let M = SingletonMap.o_Ue.o_OFL[i];
            if (M) {
                let cfg = SingletonMap.ItemMgr.o_29(M._stdItem);
                if (cfg && (cfg.steps == 62 || cfg.steps == 64)) { // 天1=62、天2=64  cfg.sLv 表示是加几的
                    // 扔装备
                    SingletonMap.o_OPQ.o_OxY(M);
                }
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

/** 打印协议 **/
function printXieYi() {
    for (var i = 0; i < SingletonMap.o_P.o_Nx5.length; i++) {
        if (SingletonMap.o_P.o_Nx5[i]) {
            for (var j = 0; j < SingletonMap.o_P.o_Nx5[i].length; j++) {
                console.log('注册协议接口' + i + '-' + j + ':' + SingletonMap.o_P.o_Nx5[i][j]);
            }
        }
    }
}

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, // 月份
        "d+": this.getDate(), // 日
        "h+": this.getHours(), // 小时
        "m+": this.getMinutes(), // 分
        "s+": this.getSeconds(), // 秒
        "q+": Math.floor((this.getMonth() + 3) / 3), // 季度
        "S": this.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

// 任务标识
function getCurUserDayFlag(taskName) {
    const serverId = SingletonMap.ServerChooseData.enterRole.serverId;
    const name = SingletonMap.ServerChooseData.enterRole.name;
    const roleId = SingletonMap.ServerChooseData.enterRole.id;
    const pf = SingletonMap.ServerChooseData.pf;
    return pf + '_' + serverId + '_' + roleId + '_' + new Date().Format("yyyy_MM_dd") + '_' + taskName;
}

window["isNoNeedGLog"] = true;
var isDebug = true;
var isDebugTool = false;

var funWarn = console.warn;
console.warn = function () {
    let msg = getCurTimeStr() + ' ' + arguments[0];
    isDebug && funWarn(msg);
}
//var funError = console.error;
//console.error = function () {
//    let msg = getCurTimeStr() + ' ' + arguments[0];
//    isDebug && funError(msg);
//}
window.onerror = function (message, source, lineno, colno, error) {
    return true;
}
window.addEventListener("unhandledrejection", function (e) {
    e.preventDefault();
    return true;
});
window.onload = function (msg, url, line) {
    window.onerror = function (message, source, lineno, colno, error) {
        return true;
    }
    if (!isDebugTool) return;
    var commandWin = document.createElement("div");
    commandWin.style.position = 'absolute';
    commandWin.style.bottom = "1px";
    commandWin.style.right = "1px";
    commandWin.style.zIndex = 99;
    commandWin.innerHTML = "";
    // commandWin.innerHTML += "<button onclick='runTask(true)'>刷金币</button><button onclick='disableTaskFlag()'>停止刷金币</button>";
    //commandWin.innerHTML += "<input type='text' id='msgbox'/><button onclick='testMsg()'>testMsg</button><button onclick='disablePringRecvMsg()'>关闭日志</button><button onclick='enablePringRecvMsg()'>打印日志</button>";
    commandWin.innerHTML += "<button onclick='disablePringRecvMsg()'>关闭日志</button><button onclick='enablePringRecvMsg()'>打印日志</button>";
    document.body.appendChild(commandWin);
}

function printMsgOnWarnOld(d) {
    var msg = new gameMsg(d);
    var msgPrefix = msg.readUnsignedShort();
    var msgLength = msg.readUnsignedShort();
    var i = msg.readUnsignedByte();
    var Q = msg.readUnsignedByte();

    const bufhex = buf2hex(d);

    // 手动模式,道士法师移动中，停止攻击
    //try {
    //    if ((i == 12 && Q == undefined) && !mainRobotIsRunning() && SingletonMap.RoleData.job != 1) {
    //        clearCurMonster();
    //    }
    //} catch(ex) {
    //}
    if (!pringRecvMsg) return;
    // 19-2切护手、1-1、1-2移动
    // 19-3 限制的切护手
    var A = i + "-" + Q;
    var b = ["26-48", "138-1", "255-128", "8-7"];
    if (b.indexOf(A) == -1) {
        window.console.warn(`sysId=${i} msgId=${Q}: ${bufhex}`);
    }
}
function printMsgOnWarn(M, Q, E) {
    if (!Q || !E) {
        // printMsgOnWarnOld(M);
        return;
    }

    if (!pringRecvMsg) return;
    var A = M + "-" + Q;
    var b = ["26-48", "138-1", "255-128", "8-7"];
    if (b.indexOf(A) == -1) {
        window.console.warn(`sysId=${M} msgId=${Q}: ${buf2hex(E.buffer)}`);
    }
}

function printMsgOnErrorOld(d) {
    if (!pringRecvMsg) return;
    var msg = new gameMsg(d);
    var msgPrefix = msg.readUnsignedShort();
    var msgLength = msg.readUnsignedShort();
    var i = msg.readUnsignedByte();
    var Q = msg.readUnsignedByte();
    var A = i + "-" + Q;
    var b = ["0-2", "0-5", "0-6", "0-7", "0-9", "0-18", "0-35", "0-45", "0-68", "5-7", "125-44", "26-48"];
    if (i && Q && b.indexOf(A) == -1) {
        const bufhex = buf2hex(d);
        window.console.error(`sysId=${i} msgId=${Q}: ${bufhex}`);
    }
}

function printMsgOnError(M, Q, E) {
    if (!Q && !E) {
        printMsgOnErrorOld(M);
        return;
    }

    if (!pringRecvMsg) return;
    var A = M + "-" + Q;
    var b = ["0-2", "0-5", "0-6", "0-7", "0-9", "0-18", "0-35", "0-45", "0-68", "5-7", "125-44", "26-48"];
    if (b.indexOf(A) == -1) {
        window.console.error(`sysId=${M} msgId=${Q}: ${buf2hex(E.buffer)}`);
    }
}

function closePayIframe() {
    document.getElementById('payIframe').src = '';
    document.getElementById("payIframeBox").style.display = 'none';
}

function gamePayBD(itemsid, amount) {
    try
    {
        var paramUrl = "?gameId=24508850";
        paramUrl += "&activeAccount=" + amount;
        paramUrl += "&token=" + urlParam.token;
        paramUrl += "&cpServerId=" + SingletonMap.ServerChooseData.srvid;
        paramUrl += "&cpExtra=" + window["RoleDataId"]() + "_" + itemsid;
        paramUrl += "&time=" + new Date().getTime();
        var payUrl = 'https://wan.baidu.com/webpayt' + paramUrl;
        callbackObj.onGamePay(payUrl);
    }
    catch(ex) {
    }
}

function WxgameVerDataSendPayBD(M) {
    var i = SingletonMap.o_eG.o_iF(M);
    if (i)
        if (false)
            if (SingletonMap.RoleData.o_NM3(window.appInstanceMap.MoneyType.o_s7) >= i.rmb)
                SingletonMap.o_n.o_EG('RechargeReplaceWin', [i.rechargeId, i.rmb]);
            else
                SingletonMap.o_54.o_HL(o_NYB.o_eBw, "@recharge " + i.rechargeId + " " + i.rmb);
        else {
            if (SingletonMap.ServerChooseData.isClosePayWidthTip) {
                alert(LangManager.o_l6Y);
                return
            } else if (SingletonMap.ServerChooseData.isClosePayWidthNoTip)
                return;
            if (SingletonMap.RoleData.o_NM3(window.appInstanceMap.MoneyType.o_s7) >= i.rmb)
                SingletonMap.o_n.o_EG('RechargeReplaceWin', [i.rechargeId, i.rmb]);
            else if (window["gamePay"])
                window["gamePay"](i.rechargeId, i.rmb)
        }
    else
        alert(LangManager.recharge_str14)
};

function rewritePay() {
    try {
        if (SingletonMap.ServerChooseData.pf == 'cqbd') {
            window["gamePay"] = gamePayBD;
            window["WxgameVerDataSendPay"] = WxgameVerDataSendPayBD;
        }
        else {
            setTimeout(rewritePay, 1000);
        }
    }
    catch (exx) {
        setTimeout(rewritePay, 1000);
    }
}

(function () {
    rewritePay();
})();


function getFastSkillHitTimes() {
    if (hasPri()) {
        return 1500;
    }
    return 30000;
}
function getFastNormalHitTimes() {
    if (hasPri()) {
        return 1500;
    }
    return 30000;
}

function isAutoSkill() {
    let result = false;
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
            if (appConfig.EnableAutoSkill == 'TRUE') {
                result = true;
            }
        }
    }
    catch(ex){
    }
    return result;
}

function fastNormalHit() {
    try {
        if (isAutoSkill() && SingletonMap && SingletonMap.o_OFM && SingletonMap.o_OFM.o_Nx1) {
            if (hasPri()) {
                if (curMonster != null && LastPointWatcher.existsMonster(curMonster)) {
                    let selectMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode);
                    if (selectMonster && selectMonster instanceof Monster && selectMonster.curHP > 0) {
                        sendUseNormalHit(selectMonster);
                    }
                    else if (isDoubleAttck && selectMonster && selectMonster instanceof PlayerActor && selectMonster.curHP > 0) {  // 双倍攻击
                        sendUseNormalHit(selectMonster);
                    }
                    else {
                        clearCurMonster();
                    }
                }
            }
        }
    } catch (ex) {
        console.error(ex);
    }
    setTimeout(fastNormalHit, getFastNormalHitTimes());
}

function sendUseNormalHit(M) {
    try {
        if (isSafeArea()) return;
        if (!M) return;
        if (SkillLock.isLock()) return;

        let curRunningTask = SingletonModelUtil.getRunningTask();
        if (curRunningTask && curRunningTask.constructor.name == YeWaiGuaJiTask.name) {
            return;
        }
        if (curRunningTask && curRunningTask.constructor.name == tianJieMatchGuaJiTask.name) {
            return;
        }
        // 法师道士交给人工处理
        // if (SingletonMap.RoleData.job != 1 && M instanceof PlayerActor && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb()) return;
        if (M instanceof PlayerActor) return; // 低调打架
        if (PlayerActor.o_NLN.curHP <= 100) return;
        if (SingletonMap.MapData.curMapName == '王城') return;
        let currentX = PlayerActor.o_NLN.currentX;
        let currentY = PlayerActor.o_NLN.o_NI3;
        if (isAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap()) {
            //if (isNormalHitDistance(currentX, currentY, M.currentX, M.o_NI3)) {
            //    (!isFindStrangers() || isFullPower) && !isSafeArea() && SingletonMap.o_CW.sendNormalHit(M.roleId);
            //}
            (!isFindStrangers() || isFullPower) && !isSafeArea() && SingletonMap.o_CW.sendNormalHit(M.roleId);
        }
    } catch (ex) {
        console.error(ex);
    }
}

function fastSkillHit() {
    try {
        if (isAutoSkill() && !SkillLock.isLock() && SingletonMap && SingletonMap.RoleData && SingletonMap.o_OFM && SingletonMap.o_OFM.o_Nx1) {
            if (hasPri()) { // 超级vip,系统vip开启
                //if (!isFindStrangers() && (SingletonMap.MapData && SingletonMap.MapData.curMapName.indexOf("高爆地图") > -1)) {
                //    useSkill(PlayerActor.o_NLN, getPowerSkill('陨石流星'));
                //}
                // 优先攻击手动选中的怪物
                if (curMonster != null && curMonster.$hashCode && LastPointWatcher.existsMonster(curMonster)) {
                    let selectMonster = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == curMonster.$hashCode);
                    if (selectMonster && selectMonster instanceof Monster && selectMonster.curHP > 0) {
                        sendUseSkill(selectMonster);
                    }
                    else if (isDoubleAttck && selectMonster && selectMonster instanceof PlayerActor && selectMonster.curHP > 0) {  // 双倍攻击
                        sendUseSkill(selectMonster);
                    }
                    else if (selectMonster && selectMonster instanceof PlayerActor && selectMonster.curHP > 0 && ['职业巅峰竞赛场'].includes(SingletonMap.MapData.curMapName)) { // 竞技
                        sendUseSkill(selectMonster);
                    }
                    else {
                        clearCurMonster();
                    }
                }
            }
        }
    } catch (ex) {
        // console.error(ex);
    }
    setTimeout(fastSkillHit, getFastSkillHitTimes());
}

function sendUseSkill(M) {
    if (!M) return;
    try {
        if (PlayerActor.o_NLN.curHP <= 100) return;
        if (SingletonMap.MapData.curMapName == '王城') return;
        try {
            SingletonMap.o_OKA.o_Pc(M);
        }
        catch (ex) {
        }
        // 法师道士交给人工处理
        // if (SingletonMap.RoleData.job != 1 && M instanceof PlayerActor && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb()) return;
        let currentX = PlayerActor.o_NLN.currentX;
        let currentY = PlayerActor.o_NLN.o_NI3;
        let heji = false;
        if (isRemoteAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap() && !isSafeArea()) {
            // 没盾放合击
            if (!M.darkDunVal && !M.fireDunVal && !M.godDunValue && !M.goldDunVal && !M.landDunVal && !M.lightDunVal) {
                heji = true;
            }

            // let MI = SingletonMap.o_OFM.o_MQ(SingletonMap.o_OFM.o_Nx1[0].roleId);
            let MI = SingletonMap.o_OFM.o_MQ(M.roleId);
            var Q = MI ? MI.cfgId : 0;
            if (getSkill('疾星斩')) { // 战士
                Q = 0;
            }
            if (Q) {
                var A = SingletonMap.o_eB9.o_OVc[Q];
                if (!isNaN(A) && A > 0) {
                    heji = false;
                }
                // 黄盾不放合击
                else if (MI.o_OtA && M.damageWhenHP > 0) {
                    heji = false;
                }
            }
            if (heji) {
                if (!SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && getSkill('雷霆之怒')) {
                    // useSkill(M, getSkill('雷霆之怒'));
                    if (SingletonMap.RoleData.o_lU >= SingletonMap.o_Omd.maxAnger && !SkillLock.isLock()) {
                        //stopSkill = true; // 有合击先不放其它技能
                        //gameObjectInstMap.GeneralWebAngerlIcon.o_OiC(null); 按下空格
                        if (!SkillLock.isLock()) {
                            useSkill(M, getSkill('雷霆之怒'));
                            return;
                        }
                    }
                }
                if (!SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && getSkill('噬魂毒焰')) {
                    if (SingletonMap.RoleData.o_lU >= SingletonMap.o_Omd.maxAnger && !SkillLock.isLock()) {
                        if (!SkillLock.isLock()) {
                            useSkill(M, getSkill('噬魂毒焰'));
                            return;
                        }
                    }
                }
            }
        }

        if (isRemoteAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap()) {
            heji = false;

            // 没盾放合击
            if (!M.darkDunVal && !M.fireDunVal && !M.godDunValue && !M.goldDunVal && !M.landDunVal && !M.lightDunVal) {
                heji = true;
            }
            // console.log("合击==>" + heji);
            if (heji) {
                if (!SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && isAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && getSkill('疾星斩')) {
                    if (SingletonMap.RoleData.o_lU >= SingletonMap.o_Omd.maxAnger && !SkillLock.isLock()) {
                        useSkill(M, getSkill('疾星斩'));
                        return;
                    }
                }
            }
        }

        if (isAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap()) {
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && useSkill(M, getSkill('刺杀剑法')); // 刺杀
            !SkillLock.isLock() && (!isFindStrangers() || isFullPower) && !SingletonMap.ShenJieMgr.o_UEk() && useSkill(M, getSkill('半月剑法')); // 没有陌生人，或强制火力时施放，或者是双倍攻击打人
            (arrFullSkillMap.includes(SingletonMap.MapData.curMapName) || SingletonMap.Ladder3v3Mgr.is3v3Map() || SingletonMap.Ladder1v1Mgr.is1v1Map()) && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('狂野冲撞'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('怒斩天下'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('万剑归宗'));

            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('战狂地钉'));

            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('擒龙手'));

            curMonster && !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && useSkill(M, getSkill('烈焰剑法'));
            (!isFindStrangers() || isFullPower || isDoubleAttackPlayer(M)) && !SingletonMap.ShenJieMgr.o_UEk() && useSkill(M, getSkill('逐日剑法'));

            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('开天剑法'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('战神领域'));

        }

        if (isRemoteAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap()) {
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('雷电术')); // 雷电术
            !SkillLock.isLock() && (!isFindStrangers() || isFullPower || isDoubleAttackPlayer(M)) && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('冰咆哮')); // 没有陌生人，或强制火力时施放
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('寒冰掌'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('流星火雨'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('灭天火'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('法神领域'));

            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('鸿蒙毒术'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('暴炎火符'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('嗜血神术'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('道神领域'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('太极封印'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('群毒诡术'));
            !SkillLock.isLock() && !SingletonMap.ShenJieMgr.o_UEk() && !SingletonMap.XiyouMgr.o_yWb() && useSkill(M, getSkill('幽灵道符'));
        }

        if (SingletonMap.ShenJieMgr.o_UEk() && isRemoteAttackDistance(currentX, currentY, M.currentX, M.o_NI3) && isAttackMap()) {
            useSkill(M, getSkill('神雷擎天'));
            (!isFindStrangers() || isFullPower) && useSkill(M, getSkill('鳳凰翔天')); // 没有陌生人，或强制火力时施放
            useSkill(M, getSkill('帝炎焚天'));
            useSkill(M, getSkill('神剑灭世'));
            useSkill(M, getSkill('聖光降世'));
        }

    } catch (ex) {
       // console.error(ex);
    }
}

var skillTime = {};
var arrFastSkillHeJiTeam = ["疾星斩", "噬魂毒焰", "雷霆之怒", "万剑归宗", "天怒惊雷"];
var arrFastSkillTeam = ["怒斩天下", "暴炎火符", "灭天火"];
var stopSkill = false;
var curSkillTime = null;
function useSkill(monster, skill) {
    try {
        if (isSafeArea()) return;
        if (SkillLock.isLock()) return;
        if (!monster) return;
        if (!skill) return;
        if (skill.o_U_G) return;
        if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
            return;
        }
        if (curSkillTime && SingletonMap.ServerTimeManager.o_eYF - curSkillTime <= 500) return;
        
        if (SingletonMap && SingletonMap.o_e && SingletonMap.o_lAu) {
            if (monster && skill) {
                LastPointWatcher.resetLastPointInfo();
                skill.o_U_G = false;
                //给每种技能设定时间
                //else if (SingletonMap.RoleData.o_lU * 3 >= SingletonMap.o_Omd.maxAnger && arrFastSkillTeam.includes(skill.stdSkill.name) && (!skillTime[skill.stdSkill.name] || skillTime[skill.stdSkill.name] < SingletonMap.ServerTimeManager.o_eYF)) {
                if (SingletonMap.RoleData.o_lU  >= SingletonMap.o_Omd.maxAnger && arrFastSkillHeJiTeam.includes(skill.stdSkill.name) && (!skillTime[skill.stdSkill.name] || skillTime[skill.stdSkill.name] < SingletonMap.ServerTimeManager.o_eYF)) {
                    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                    skillTime[skill.stdSkill.name] = curDate.getTime() + 2000;
                    curSkillTime = SingletonMap.ServerTimeManager.o_eYF + 2000;
                    // console.warn(skill.stdSkill.name);
                    SkillLock.lock();
                    let r = new egret.Point(monster.o_e8E, monster.o_ePN);
                    SingletonMap.o_lAu.attack(monster, skill, { $hashCode: r.$hashCode, x: r.x, y: r.y });
                    setTimeout(function() {
                        SkillLock.unLock();
                    }, 500)
                }
                else if (arrFastSkillTeam.includes(skill.stdSkill.name) && (!skillTime[skill.stdSkill.name] || skillTime[skill.stdSkill.name] < SingletonMap.ServerTimeManager.o_eYF)) {
                    let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
                    skillTime[skill.stdSkill.name] = curDate.getTime() + 2000;
                    curSkillTime = SingletonMap.ServerTimeManager.o_eYF + 2000;

                    SkillLock.lock();
                    let r = new egret.Point(monster.o_e8E, monster.o_ePN);
                    //SingletonMap.o_lAu.attack(monster, skill, { $hashCode: r.$hashCode, x: r.x, y: r.y });
                    
                    SingletonMap.o_lAu.attack(monster, skill, { $hashCode: r.$hashCode, x: r.x, y: r.y });
                    setTimeout(function () {
                        SkillLock.unLock();
                    }, 500)
                }
                else if ((skill.o_VM == 0 || skill.o_VM < SingletonMap.o_e.currTimer) 
                    && !arrFastSkillHeJiTeam.includes(skill.stdSkill.name)
                    && !arrFastSkillTeam.includes(skill.stdSkill.name)) {
                    curSkillTime = SingletonMap.ServerTimeManager.o_eYF;
                    // skill.o_U_G = true;技能处于关闭状态
                    // let r = new egret.Point(monster.o_e8E, monster.o_ePN);
                    //if (arrFastSkillTeam.includes(skill.stdSkill.name)) {
                    //      let E = SingletonMap.o_etq.o_Noc(PlayerActor.o_NLN.currentX, PlayerActor.o_NLN.o_NI3, monster.o_e8E, monster.o_ePN);
                    //      SingletonMap.o_CW.sendUseSkill(skill.skillId, monster.roleId, r.x, r.y, E, false, true);
                    //SingletonMap.o_lAu.attack(monster, skill, null);
                    //     return;
                    //}
                    // SkillLock.lock();
                    // SingletonMap.o_lAu.attack(monster, skill, { $hashCode: r.$hashCode, x: r.x, y: r.y });
                    SkillLock.lock();
                    
                    let r = new egret.Point(monster.o_e8E, monster.o_ePN);
                    SingletonMap.o_lAu.attack(monster, skill, { $hashCode: r.$hashCode, x: r.x, y: r.y });
                    setTimeout(function () {
                        SkillLock.unLock();
                    }, 500)
                    // SingletonMap.o_lAu.attack(monster, skill);
                    //SkillLock.unLock();
                }
            }
        }
    }
    catch (e) {
      //  console.log(e);
    }
}

(function () {
    fastNormalHit();
    fastSkillHit();
})();

let arrLogBoss = [];
// 收集boss
function logMonster() {
    try {
        let arrMonster = SingletonMap.o_OFM.o_Nx1.filter(item => { return (item instanceof Monster && !item.masterName) });
        for (let i = 0; i < arrMonster.length; i++) {
            let m = arrMonster[i];
            let boss = {};
            for (let key in m) {
                if (key && m[key] && typeof m[key] != 'function' && typeof m[key] != 'object') {
                    boss[key] = m[key];
                }
            }
            arrLogBoss.push(boss);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

function printLogBoss() {
    let bossMap = {};
    for (var i = 0; i < arrLogBoss.length; i++) {
        if (!bossMap[arrLogBoss[i].cfgId]) {
            console.log(arrLogBoss[i].cfgId + '=' + arrLogBoss[i].roleName);
            bossMap[arrLogBoss[i].cfgId] = true;
        }
    }
}

// 标记神界材料boss
function markShenJieCaiLiaoMonster() {
    try {
        if (isSuperPri() || isYearPri()) {
            let arrMonster = SingletonMap.o_OFM.o_Nx1;
            for (let i = 0; i < arrMonster.length; i++) {
                let M = arrMonster[i];
                if (M && M instanceof Monster && !M.masterName) {
                    if (M.cfgId >= 167001 && M.cfgId <= 167045) { // 神界普通怪
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 169001 && M.cfgId <= 169005) {// 神界炼狱普通怪
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 163101 && M.cfgId <= 163108) { // 西游毛颖山洞府
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 163201 && M.cfgId <= 163208) { // 西游莲花洞府
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 163301 && M.cfgId <= 163308) { // 西游狮驼洞府
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 163401 && M.cfgId <= 163408) { // 西游无底洞府
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 163501 && M.cfgId <= 163508) { // 西游金兜洞府
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 165101 && M.cfgId <= 165108) { // 浮屠山
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 165201 && M.cfgId <= 165208) { // 万寿山
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 165301 && M.cfgId <= 165308) { // 黑风山
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 165401 && M.cfgId <= 165408) { // 普陀珞珈山
                        M.$alpha = 0.2;
                    }
                    else if (M.cfgId >= 165501 && M.cfgId <= 165508) { // 小须弥山
                        M.$alpha = 0.2;
                    }
                }
            }
        }
    }
    catch (ex) {
       // console.error(ex);
    }
    setTimeout(markShenJieCaiLiaoMonster, 500);
}

(function () {
    markShenJieCaiLiaoMonster();
})();

let instanceUID = 0;
class SingletonModelUtil {
    static instanceMap = {};
    static curUid = null;
    static getInstance(classModel) {
        let clsName = classModel.name || classModel.constructor.name;
        if (!SingletonModelUtil.instanceMap[clsName]) {
            SingletonModelUtil.instanceMap[clsName] = new classModel();
        }
        return SingletonModelUtil.instanceMap[clsName];
    }

    static removeTask(classModel) {
        let clsName = classModel.name || classModel.constructor.name;
        delete SingletonModelUtil.instanceMap[clsName];
    }

    static async clearAllTaskInstance() {
        for (let key in SingletonModelUtil.instanceMap) {
            SingletonModelUtil.instanceMap[key] = null;
        }
        SingletonModelUtil.instanceMap = {};
    }

    static stopPlayerAction() {
        try {
            clearCurMonster();
            QuickPickUpNormal.stop();
            BlacklistMonitors.stop();
            PlayerActor && PlayerActor.o_NLN && PlayerActor.o_NLN.o_lkM();
            SingletonMap && SingletonMap.o_lAu && SingletonMap.o_lAu.stop();
        }
        catch (ex) {
            console.error(ex);
        }
    }

    static async stopAllTask() {
        // console.warn(`执行任务停止方法...`);
        clearCurMonster();
        MoveLock.stop();
        SingletonModelUtil.stopPlayerAction();
        for (let key in SingletonModelUtil.instanceMap) {
            try {
                if (key != 'TaskQueueManage') {
                    let taskInstance = SingletonModelUtil.instanceMap[key];
                    taskInstance.attackIsStop = true;
                    taskInstance.findWayIsStop = true;
                    taskInstance.stopTask = true;
                    taskInstance.stopFindWay = true;
                    taskInstance.diePos = null;
                    if (taskInstance && taskInstance.isRunning && taskInstance.isRunning()) {
                        await taskInstance.stop();
                        console.warn(`停止任务... ${taskInstance.constructor.name}  UID=${taskInstance.uid} ${JSON.stringify(getGameMsg())}`);
                    }
                }
            }
            catch (ex) {
                console.error(ex);
            }
        }
        function check() {
            return xSleep(50).then(function () {
                let isExists = false;
                for (let key in SingletonModelUtil.instanceMap) {
                    try {
                        let taskInstance = SingletonModelUtil.instanceMap[key];
                        if (taskInstance && taskInstance.isRunning && taskInstance.isRunning()) {
                            isExists = true;
                            break;
                        }
                    }
                    catch (ex) {
                        console.error(ex);
                    }
                }
                if (isExists == false) {
                    // console.warn(`任务停止完毕...`);
                    MoveLock.resume();
                    return;
                }
                else {
                    return check();
                }
            });
        }
        return check();
    }
    static awaitTaskComplete(taskInstance) {
        function check() {
            return xSleep(500).then(function () {
                if (taskInstance == null) {
                    return;
                }
                if (taskInstance.isRunning && !taskInstance.isRunning()) {
                    return;
                }
                return check();
            });
        }
        return check();
    }

    static getRunningTask() {
        let task = null;
        try {
            for (let key in SingletonModelUtil.instanceMap) {
                let taskInstance = SingletonModelUtil.instanceMap[key];
                if (taskInstance && taskInstance.isRunning && taskInstance.isRunning()) {
                    task = taskInstance;
                    break;
                }
            }
        } catch (ex) {
            console.error(ex);
        }
        return task;
    }

    static existsRunningTask() {
        let result = false;
        for (let key in SingletonModelUtil.instanceMap) {
            let taskInstance = SingletonModelUtil.instanceMap[key];
            if (taskInstance && taskInstance.isRunning && taskInstance.isRunning()) {
                result = true;
                break;
            }
        }
        return result;
    }
}

class RecyTask {
    static start() {
        stopLoopRunRecy();
        startLoopRunRecy();
    }

    static stop() {
        stopLoopRunRecy();
    }
}

// 挂机拾取
class QuickPickUpNormal {
    static arrExpireRes = {};
    static state = false;
    static start() {
        QuickPickUpNormal.arrExpireRes = {};
        //QuickPickUpNormal.stop();
        QuickPickUpNormal.state = true;
        QuickPickUpNormal.runPickUp();
    }

    static runPickUp() {
        try {
            //if (!mainRobotIsRunning()) {
            //    QuickPickUpNormal.arrExpireRes = {};
            //    return;
            //}
            var b = SingletonMap.o_Ue.o_lpC();
            if (SingletonMap && SingletonMap.MapData && SingletonMap.RoleData && SingletonMap.RoleData.o_NoB() > 0
                && !['西游降魔', '苍月神殿', '寻龙秘境', '神龙巢穴', '摸金巢穴', '永恒迷宫'].includes(SingletonMap.MapData.curMapName) 
                && SingletonMap.MapData.curMapName.indexOf("黑暗神殿") == -1
                && SingletonMap.MapData.curMapName.indexOf("光明神殿") == -1) {
                SingletonMap.o_e.doOnce(10, SingletonMap.o_eCl,
                    function (M) {
                        for (var i = 0; i < M.length; i++) {
                            var Q = M[i];
                            if (!Q) return;
                            if (!SingletonMap.o_lLF.o_OrJ(Q, b)) continue; // 不符合拾取规则
                            if (QuickPickUpNormal.arrExpireRes[Q.$hashCode + ""]) { // 已过期
                                continue;
                            }
                            QuickPickUpNormal.arrExpireRes[Q.$hashCode + ""] = Q.$hashCode + "";
                            SingletonMap.o_lLF.o_NMj(M[i], SingletonMap.RoleData.o_NoB());
                        }
                    },
                    [SingletonMap.o_lpS.o_Onf]);
            }
        } catch (ex) {
            //console.error(ex);
        }
        //if (QuickPickUpNormal.state && isStopMainRobot != true) {
        //    setTimeout(QuickPickUpNormal.runPickUp, 300);
        //}
    }

    static stop() {
        //QuickPickUpNormal.state = false;
        QuickPickUpNormal.arrExpireRes = {};
    }
}

// 100毫秒检测一次,由后端调用
function loopBLackListCheck100() {
    BlacklistMonitors.loopCheckBadPlayer();
}


class BlacklistMonitors {
    static state = false;
    static badPlayer = null;
    static stop() {
        BlacklistMonitors.state = false;
        BlacklistMonitors.badPlayer = null;
    }

    static start() {
        //if (this.isRunning()) return;
        //BlacklistMonitors.state = true;
        //BlacklistMonitors.loopCheckBadPlayer();
    }

    static isRunning() {
        return BlacklistMonitors.state == true;
    }

    static isFound() {
        return BlacklistMonitors.badPlayer != null;
    }

    static isBadPlayer(name) {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = appConfig.replaceAll("|", "#").replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "");
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableBlackList == 'TRUE') {
                    appConfig.BlackList = "#" + appConfig.BlackList || "";
                    let reg = new RegExp("#.*" + name.replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "") + "#", "ig");
                    return reg.test(appConfig.BlackList) && !isSafeArea();
                }
            }
        }
        catch (e) {
        }
        return false;
    }

    static isWhitePlayer(name) {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = appConfig.replaceAll("|", "#").replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "");
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableWhiteList == 'TRUE') {
                    appConfig.WhiteList = "#" + appConfig.WhiteList || "";
                    let reg = new RegExp("#.*" + name.replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "") + "#", "ig");
                    return reg.test(appConfig.WhiteList);
                }
            }
        }
        catch (e) {
        }
        return false;
    }

    static isWhiteGuild(name) {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableGuildWhiteList == 'TRUE') {
                    let WhiteList = "|" + appConfig.GuildWhiteList + "|";
                    return name !="" && WhiteList.indexOf(name) > -1;
                }
            }
        }
        catch (e) {
        }
        return false;
    }

    static isBlackGuild(name) {
        try {
            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                if (appConfig.EnableGuildBlackList == 'TRUE') {
                    let GuildList = "|" + appConfig.GuildBlackList + "|";
                    return name != "" && GuildList.indexOf(name) > -1;
                }
            }
        }
        catch (e) {
        }
        return false;
    }

    static isPowerWhitePlayer(name) {
        try {
            let powerWhiteList = ["正道丿软中华", "帝尊", "眺望的ジ黑龙", "熊出没丶岩岩", "熊出没丶吉吉",
                "噗", "[129]千少っ初晴", "何金银", "简单的╃浩言", "阿落", "倾城", "[338]你的心★欢乐", "撞击的清辞",
                "[342]扈◎少洋气", "风吹裙子飘", "沈メ沧澜", "闲书", "罪oo爱", "紫00风", "★充十块玩玩★", "欧阳",
                "全‖承天", "謨曦", "[342]扈◎少洋气", "★充十块玩玩★", "醉メ清风", "困龙宇蕴", "青丝绕指柔", "肖肖☆",
                "充钱也赢不了", "石岘熊哥", "永恒丶逆天行", "錵卷", "永恒丶K仔", "永恒丶三叹", "卍軍哥卐"];
            powerWhiteList = "#" + powerWhiteList.join("#") + "#";
            powerWhiteList = powerWhiteList.replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "");
            let reg = new RegExp("#.*" + name.replace(/\[\d+区\]/ig, "").replace(/\[\d+\]/ig, "") + "#", "ig");
            return reg.test(powerWhiteList);
        }
        catch (e) {
        }
        return false;
    }

    static getBadPlayer() {
        try {
            if (!mainRobotIsRunning()) {
                return;
            }

            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                // if (appConfig.EnableBlackList == 'TRUE' && !isSafeArea()) {
                    let foundBadPlayer = null;
                    if (BlacklistMonitors.badPlayer) {
                        foundBadPlayer = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == BlacklistMonitors.badPlayer.$hashCode && item.curHP > 0);
                    }

                    let curBadPlayer = foundBadPlayer || findBadPlayer();
                    // if (curBadPlayer && !isSomeCamp(curBadPlayer) && SingletonMap.o_h7.o_lDb(curBadPlayer)) {
                    if (curBadPlayer && SingletonMap.o_h7.o_lDb(curBadPlayer)) {
                        return curBadPlayer;
                    }
                    else {
                        return null;
                    }
                //}
                //else {
                //    return null;
                //}
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    static checkBadPlayer() {
        try {
            if (!mainRobotIsRunning()) {
                return;
            }

            let appConfig = getAppConfig();
            if (appConfig != "") {
                appConfig = JSON.parse(appConfig);
                //if (appConfig.EnableBlackList == 'TRUE' && !isSafeArea()) {
                    let foundBadPlayer = null;
                    if (BlacklistMonitors.badPlayer) {
                        foundBadPlayer = SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == BlacklistMonitors.badPlayer.$hashCode && item.curHP > 0);
                    }

                    let curBadPlayer = foundBadPlayer || findBadPlayer();
                    if (curBadPlayer && SingletonMap.o_h7.o_lDb(curBadPlayer)) {
                        BlacklistMonitors.badPlayer = curBadPlayer;

                        if (GlobalUtil.isAttackDistance(curBadPlayer.currentX, curBadPlayer.o_NI3)) {
                            setCurMonster(curBadPlayer);
                            SingletonMap.o_OKA.o_Pc(curBadPlayer, true);
                        }
                        else {
                            let targetPos = GlobalUtil.getTargetPos(curBadPlayer.currentX, curBadPlayer.o_NI3);
                            SingletonMap.o_OKA.o_NSw(targetPos.posX, targetPos.posY);
                        }
                    }
                    else {
                        BlacklistMonitors.badPlayer = null;
                    }
               // }
               // else {
               //     BlacklistMonitors.badPlayer = null;
               // }
            }
        } catch (ex) {
            console.error(ex);
        }
    }

    static loopCheckBadPlayer() {
        try {
            if (!mainRobotIsRunning()) {
                return;
            }
            if (SingletonMap && SingletonMap.MapData && SingletonMap.MapData.curMapName
                && ['西游降魔', '蟠桃盛宴', '神魔战场'].includes(SingletonMap.MapData.curMapName)) return;
            if (SingletonMap && SingletonMap.MapData && SingletonMap.MapData.curMapFilePath == 'ShenYuBoss-KF') return;
            if (!autoCheckBadPlayer()) {
                return;
            }
            BlacklistMonitors.checkBadPlayer();
        } catch (ex) {
            console.error(ex);
        }

        //if (BlacklistMonitors.isRunning() && isStopMainRobot != true) {
        //    setTimeout(BlacklistMonitors.loopCheckBadPlayer, 100);
        //}
    }
}

// 是相同阵营
function isSomeCamp(badPlayer) {
    if (!badPlayer) return false;
    let result = false;
    try {
        if (SingletonMap.MapData.curMapName == '神魔边境' && SingletonMap.o_yy_.serverList && SingletonMap.o_yy_.serverList.length > 0) {
            let mySelf = SingletonMap.o_yy_.serverList.find(M => M.serverId == PlayerActor.o_NLN.serverId);
            let otherPlayer = SingletonMap.o_yy_.serverList.find(M => M.serverId == badPlayer.serverId);
            result = mySelf.camp == otherPlayer.camp;
        }
    } catch (ex) {
        console.error(ex);
    }
    return result;
}

// 是否在安全区
function isSafeArea() {
    if (SingletonMap.MapData.curMapName == '神界主城') return true;
    if (SingletonMap.MapData.curMapName == '天界主城') return true;
    if (SingletonMap.MapData.curMapName == '神族主城') return true;
    if (SingletonMap.MapData.curMapName == '魔族主城') return true;
    if (SingletonMap.MapData.curMapName == '蟠桃盛宴') return true;
    if (SingletonMap.MapData.curMapName == '王城') return true;
    return false;
}

// 神界挂机
function startShenJieGuaJi() {
    let instance = SingletonModelUtil.getInstance(ShenJieGuaJiTask);
    instance.start();
}

// 停止神界挂机
function stopShenJieGuaJi() {
    let instance = SingletonModelUtil.getInstance(ShenJieGuaJiTask);
    instance.stop();
}

// 开始刷星空
function runXinKongZhiLiTask() {
    if (!hasPri()) {
        return;
    }
    let instance = SingletonModelUtil.getInstance(XinKongZhiLiTask);
    instance.start();
}
// 停止刷星空
function stopXinKongZhiLiTask() {
    let instance = SingletonModelUtil.getInstance(XinKongZhiLiTask);
    instance.stop();
}
// 刷星空之力结束

// 强制回收背包所有御魂、神器、绿色祈祷
function recvBagYuHun() {
    try {
        let arr = SingletonMap.o_Ue.o_OFL.filter(item => item && item._stdItem && [29, 35, 38].includes(item._stdItem.type));
        let recyArr = [];
        for (var i = 0; i < arr.length; i++) {
            recyArr.push({
                o_ES: true,
                userItem: arr[i]
            });
        }
        SingletonMap.o_OPQ.o_0z(recyArr.length, recyArr);
    }
    catch (ex) {
        console.error(ex);
    }
}

// 合成史诗
function mergerShiShi() {
    try {
        SingletonMap.o_Ue.o_Ocb();
        let arrItem = SingletonMap.o_Ue.o_OFL.filter(item => item && item._stdItem && item._stdItem.type == 19);
        if (arrItem.length == 0) return;
        let itemMap = {};
        for (let i = 0; i < arrItem.length; i++) {
            itemMap[arrItem[i].wItemId] = itemMap[arrItem[i].wItemId] || 0;
            itemMap[arrItem[i].wItemId] = itemMap[arrItem[i].wItemId] + arrItem[i].btCount;
        }
        for (let key in itemMap) {
            let level = parseInt((key - 360000) / 100);
            if (level >= 5 && itemMap[key] >= 2) { // 合6级以上
                let times = Math.trunc(itemMap[key] / 2);
                for (let i = 0; i < times; i++) {
                    SingletonMap.o_OPQ.o_NC8(Number(key) + 100, 1, -1);
                }
            }
            else if (itemMap[key] >= 3) { // 合2-6级
                let times = Math.trunc(itemMap[key] / 3);
                for (let i = 0; i < times; i++) {
                    SingletonMap.o_OPQ.o_NC8(Number(key) + 100, 1, -1);
                }
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 合成玛法3级
function mergerMafa3Level(type, level2minId, level2maxId, level1minId, level1maxId) {
    try {
        let arrMainItem = getMafaLevelItem(type, level2minId, level2maxId);
        let arrDeputyItem = getMafaLevelItem(type, level1minId, level1maxId);
        if (arrMainItem.length > 0 && arrDeputyItem.length > 0) { // 2阶1阶都有
            SingletonMap.o_e5j.o_9U([{
                type: 0,
                pos: 0,
                user: arrMainItem[0],
                count: 1
            }, {
                type: 0,
                pos: 0,
                user: arrDeputyItem[0],
                count: 1
            }]);
        }
        else if (arrDeputyItem.length > 1) { // 有两个1阶
            SingletonMap.o_e5j.o_9U([{
                type: 0,
                pos: 0,
                user: arrDeputyItem[0],
                count: 1
            }, {
                type: 0,
                pos: 0,
                user: arrDeputyItem[1],
                count: 1
            }]);
        }
        arrMainItem = getMafaLevelItem(type, level2minId, level2maxId);
        arrDeputyItem = getMafaLevelItem(type, level1minId, level1maxId);
        if ((arrMainItem.length > 0 && arrDeputyItem.length > 0) || arrDeputyItem.length > 1) {
            setTimeout(function () {
                mergerMafa3Level(type, level2minId, level2maxId, level1minId, level1maxId);
            }, 1000);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

// 合成玛法2级
function mergerMafa2Level(type, minId, maxId) {
    try {
        let arrMainItem = getMafaLevelItem(type, minId, maxId);
        if (arrMainItem.length > 1) {
            SingletonMap.o_e5j.o_9U([{
                type: 0,
                pos: 0,
                user: arrMainItem[0],
                count: 1
            }, {
                type: 0,
                pos: 0,
                user: arrMainItem[1],
                count: 1
            }]);
        }
        arrMainItem = getMafaLevelItem(type, minId, maxId);
        if (arrMainItem.length > 1) {
            setTimeout(function () {
                mergerMafa2Level(type, minId, maxId);
            }, 1000);
        }
    }
    catch (ex) {
        console.error(ex);
    }
}

function getMafaLevelItem(type, minId, maxId) {
    return SingletonMap.o_OqG.o_eiy.filter(item => item && item._stdItem && item._stdItem.type == type && item._stdItem.showQuality == 4 && item._stdItem.id >= minId && item._stdItem.id <= maxId);
}

// 赤月3阶
function mergerMafaChiYueLevel3() {
    //mergerMafa3Level(42, 530501, 530501, 530401, 530401); // 武器
    //mergerMafa3Level(42, 530502, 530502, 530402, 530402); // 甲
    mergerMafa3Level(42, 530503, 530507, 530403, 530407); // 其它
}
// 赤月3阶剑甲
function mergerMafaChiYueJianJiaLevel3() {
    mergerMafa3Level(42, 530501, 530501, 530401, 530401); // 武器
    mergerMafa3Level(42, 530502, 530502, 530402, 530402); // 甲
}
// 赤月2阶
function mergerMafaChiYueLevel2() {
    mergerMafa2Level(42, 530403, 530407); // 其它
}
// 赤月2阶剑甲
function mergerMafaChiYueJianJiaLevel2() {
    mergerMafa2Level(42, 530401, 530401); // 武器
    mergerMafa2Level(42, 530402, 530402); // 甲
}

// 战神3阶
function mergerMafaZhanShenLevel3() {
    mergerMafa3Level(43, 533043, 533047, 533033, 533037); // 其它
}
// 战神2阶
function mergerMafaZhanShenLevel2() {
    mergerMafa2Level(43, 533033, 533037); // 其它
}
// 战神3阶剑甲
function mergerMafaZhanShenJianJiaLevel3() {
    mergerMafa3Level(43, 533041, 533041, 533031, 533031); // 武器
    mergerMafa3Level(43, 533042, 533042, 533032, 533032); // 甲
}
// 战神2阶剑甲
function mergerMafaZhanShenJianJiaLevel2() {
    mergerMafa2Level(43, 533031, 533031); // 武器
    mergerMafa2Level(43, 533032, 533032); // 甲
}

// 天龙3阶
function mergerMafaTianLongLevel3() {
    mergerMafa3Level(44, 534043, 534047, 534033, 534037); // 其它
}
// 天龙2阶
function mergerMafaTianLongLevel2() {
    mergerMafa2Level(44, 534033, 534037); // 其它
}
// 天龙3阶剑甲
function mergerMafaTianLongJianJiaLevel3() {
    mergerMafa3Level(44, 534041, 534041, 534031, 534031); // 武器
    mergerMafa3Level(44, 534042, 534042, 534032, 534032); // 甲
}
// 天龙2阶剑甲
function mergerMafaTianLongJianJiaLevel2() {
    mergerMafa2Level(44, 534031, 534031); // 武器
    mergerMafa2Level(44, 534032, 534032); // 甲
}

// 都市3阶
function mergerMafaDuShiLevel3() {
    mergerMafa3Level(47, 535113, 535117, 535103, 535107); // 其它
}
// 都市2阶
function mergerMafaDuShiLevel2() {
    mergerMafa2Level(47, 535103, 535107); // 其它
}
// 都市3阶剑甲
function mergerMafaDuShiJianJiaLevel3() {
    mergerMafa3Level(47, 535111, 535111, 535101, 535101); // 武器
    mergerMafa3Level(47, 535112, 535112, 535102, 535102); // 甲
}
// 都市2阶剑甲
function mergerMafaDuShiJianJiaLevel2() {
    mergerMafa2Level(47, 535101, 535101); // 武器
    mergerMafa2Level(47, 535102, 535102); // 甲
}

// 仙灵3阶
function mergerMafaXianLinLevel3() {
    mergerMafa3Level(57, 538023, 538027, 538013, 538017); // 其它
}
// 仙灵2阶
function mergerMafaXianLinLevel2() {
    mergerMafa2Level(57, 538013, 538017); // 其它
}
// 仙灵3阶剑甲
function mergerMafaXianLinJianJiaLevel3() {
    mergerMafa3Level(57, 538021, 538021, 538011, 538011); // 武器
    mergerMafa3Level(57, 538022, 538022, 538012, 538012); // 甲
}
// 仙灵2阶剑甲
function mergerMafaXianLinJianJiaLevel2() {
    mergerMafa2Level(57, 538011, 538011); // 武器
    mergerMafa2Level(57, 538012, 538012); // 甲
}
// 卸宝石
async function disGem() {
    if (!hasPri() || isMonthPri()) return;
    try {
        for (let pos = 0; pos < 10; pos++) {
            for (let grid = 0; grid < 6; grid++) {
                SingletonMap.o_ldS.o_NuB(pos, grid);
                await xSleep(10);
            }
        }
    }
    catch (ex) {
        consol.log(ex);
    }
}
// 装宝石
function wearGem() {
    if (!hasPri() || isMonthPri()) return;
    try {
        SingletonMap.o_l72.o_ext();
    }
    catch (ex) {
        consol.log(ex);
    }
}

var villaMafaBossTime = {};
function updateVillaMafaBossTime(data) {
    try {
        villaMafaBossTime[data.cfg.bossID] = data.time;
    } catch (ex) {
        console.error(ex);
    }
}

function updateMafaBossData(data) {
    try {
        // console.log(data);
    } catch (ex) {
        console.error(ex);
    }
}

var bossNextWatchTime = {}; // 记录boss下次观察时间 key: cfgId, value: 时间戳
function isFreeTime() {
    let result = false;
    try {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();
        if ((curHours >= 0 && curMinutes >= 0 && 10 > curHours)
            || (curHours == 23 && curMinutes >= 0)
        ) {
            result = true;
        }
    }
    catch (e) {
        console.error(e);
        result = false;
    }
    return result;
}

// 轮回boss
let CrossPublicBossRefreshTime = {};
// 西游仙府每日任务
let xiyouXianfuDayTaskLastTime = {};
// 西游四帝
let xiyouCityBossLastTime = {};

function closeGameWin(winName) {
    try {
        if (SingletonMap.o_n.isOpen(winName)) {
            SingletonMap.o_n.o_eAN(SingletonMap.o_n.isOpen(winName));
        }
    }
    catch (e) {
        console.log(e);
    }
}


// 自动领取戮神剑
function autoLuShenJian(type) {
    try {
        if (type == "") return;
        if (!SingletonMap.ShenJieMgr.o_UEk()) return; // 不在神界地图
        if (SingletonMap.o_UuA.o_J1() > 0) { // 还有可领取的次数
            if (type == '1' && SingletonMap.RoleData.diamond >= 1500) {
                SingletonMap.o_UEL.o_Uul(1); // 双倍领取
            }
            else {
                SingletonMap.o_UEL.o_Uul(0); // 免费领取
            }
        }
    }
    catch (e) {
        console.log(e);
    }
}

class AliveCheckLock {
    static state = false;

    static isLock() {
        return AliveCheckLock.state == true;
    }

    static lock() {
        AliveCheckLock.state = true;
        // console.warn('alive lock....');
    }

    static unLock() {
        AliveCheckLock.state = false;
        // console.warn('alive unLock....');
    }
}

function isAlive() {
    let result = true;
    try {
        result = PlayerActor.o_NLN.curHP <= 0 ? false : true;
    } catch (ex) {
        console.error(ex);
    }
    return result;
}

function reviveFinish() {
    function check() {
        return xSleep(1000).then(function () {
            if (PlayerActor && PlayerActor.o_NLN && PlayerActor.o_NLN.curHP > 0) {
                return;
            }
            else {
                return check();
            }
        });
    }
    return check();
}

// 存活检测
async function runCheckAlive() {
    try {
        if (!mainRobotIsRunning()) return;
        if (AliveCheckLock.isLock()) return;
        if (SingletonMap.Ladder3v3Mgr.is3v3Map() || SingletonMap.Ladder1v1Mgr.is1v1Map() || SingletonMap.JobWarMgr.o_yrY()) return;
        if (!isAlive() && SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
            AliveCheckLock.lock();
            let curRunningTask = SingletonModelUtil.getRunningTask();
            if (curRunningTask && curRunningTask.uid == SingletonModelUtil.curUid) {
                curRunningTask.diePos = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3 };
            }
            // 玛法不用复活丹
            if (curRunningTask && getAppConfigKeyValue("MafaNoPropRevive") == "TRUE" && curRunningTask.constructor.name.indexOf('Mafa') > -1) {
                SingletonMap.o_eaX.o_e5d(0);
                await reviveFinish();
                await xSleep(1000);
            }
            // 别墅不用复活丹
            else if (curRunningTask && getAppConfigKeyValue("VillaMafaNoPropRevive") == "TRUE" && curRunningTask.constructor.name.indexOf('villa') > -1) {
                SingletonMap.o_eaX.o_e5d(0);
                await reviveFinish();
                await xSleep(1000);
            }
            else if (curRunningTask && arrNoBackTaskMap.includes(curRunningTask.constructor.name)) {
                let reliveItemCount = SingletonMap.o_Ue.o_Ngt(SingletonMap.o_O.o_OGv.reliveItemId);
                if (reliveItemCount > 0) {
                    SingletonMap.o_eaX.o_e5d(1);
                    await reviveFinish();
                    await xSleep(1000);
                }
                else {
                    SingletonMap.o_eaX.o_e5d(0);
                    await reviveFinish();
                    await xSleep(1000);
                }
            }
            else {
                SingletonMap.o_eaX.o_e5d(0);
                await reviveFinish();
                await xSleep(1000);
            }
            if (curRunningTask && curRunningTask.uid == SingletonModelUtil.curUid) {
                if (!arrNoBackTaskMap.includes(curRunningTask.constructor.name) && curRunningTask.backToMap) {
                    await curRunningTask.backToMap();
                    console.warn('复活返回地图...');
                    curRunningTask.diePos = null;
                }
            }
            AliveCheckLock.unLock();
        }
    }
    catch (e) {
        console.log(e);
    }

    AliveCheckLock.unLock();
}

function taskIsAllowRun(curTaskInstance) {
    let result = false;
    try {
        let runningTask = SingletonModelUtil.getRunningTask();
        if (!mainRobotIsRunning()) {
            result = false;
        }
        else if (!runningTask) {
            result = true;
        }
        else if (!curTaskInstance.needRun()) {
            result = false;
        }
        else if (curTaskInstance.isRunning()) {
            result = false;
        }
        else if (getTaskWeight(curTaskInstance.constructor.name) < getTaskWeight(runningTask.constructor.name)) {
            result = true;
        }
    }
    catch (ex) {
        console.error(ex);
    }
    return result;
}

class TaskQueueManage {
    static name = "TaskQueueManage";
    queue = [];

    getQueueSize() {
        return this.queue.length;
    }

    canAddTask(taskInstance) {
        let runningTask = SingletonModelUtil.getRunningTask();
        if (runningTask) {
            if (getTaskWeight(runningTask.constructor.name) > getTaskWeight(taskInstance.constructor.name)) {
                return true;
            }
            else {
                return null;
            }
        }
        else {
            return true;
        }
    }

    addTask(taskInstance) {
        let taskName = taskInstance.constructor.name;
        if (!this.queue.find(item => item.name == taskName)) {
            // console.log(JSON.stringify(this.queue));
            // console.warn(`添加任务==>${taskName}`);
            this.queue.push({
                name: taskName,
                task: taskInstance
            });
        }
    }

    nextTask() {
        let arrPrepTask = [];
        if (this.queue.length == 0) {
            return null;
        }
        for (let i = 0; i < this.queue.length; i++) {
            let taskInst = this.queue[i].task;
            // let tmpTask = SingletonModelUtil.getInstance(taskCls);
            //if (taskInst.isRunning && !taskInst.isRunning() && taskInst.needRun && taskInst.needRun()) {
            if (taskInst.needRun && taskInst.needRun()) {
                arrPrepTask.push(taskInst);
            }
        }

        if (arrPrepTask.length == 0) {
            return null;
        }

        let highestWeightTask = null;
        for (let i = 0; i < arrPrepTask.length; i++) {
            if (highestWeightTask == null) {
                highestWeightTask = arrPrepTask[i];
                continue;
            }
            if (getTaskWeight(highestWeightTask.constructor.name) > getTaskWeight(arrPrepTask[i].constructor.name)) {
                highestWeightTask = arrPrepTask[i];
            }
        }

        let runningTask = SingletonModelUtil.getRunningTask();
        if (runningTask) {
            if (getTaskWeight(runningTask.constructor.name) > getTaskWeight(highestWeightTask.constructor.name)) {
                return highestWeightTask;
            }
            else {
                return null;
            }
        }
        else if (highestWeightTask) {
            return highestWeightTask;
        }
        return null;
    }

    removeTask(taskName) {
        let targetTask = this.queue.find(item => item.name == taskName);
        if (targetTask) {
            targetTask.attackIsStop = true;
            targetTask.findWayIsStop = true;
            targetTask.stopTask = true;
            targetTask.stopFindWay = true;
            targetTask.diePos = null;
            if (targetTask.isRunning && targetTask.isRunning()) {
                (async () => {
                    await targetTask.stop();
                })();
                console.warn(`停止任务... ${taskInstance.constructor.name}  UID=${taskInstance.uid} ${JSON.stringify(getGameMsg())}`);
            }
            console.warn(`删除任务==>${taskName} queue size=${this.queue.length}`);
        }
        this.queue = this.queue.filter(item => item.name != taskName);
    }
}

var normalVillaMafaBossTime = {};

// 神龙山庄
let ShenLongShanZhuanBossNextWatchTime = {}

// 神威狱
let DeityPrisonTaskTime = {};

class LastPointWatcher {
    static curPoint = null;
    static curPointTime = null;
    static lastPointTime = null;

    static existsMonster(monster) {
        try {
            if (monster == null) {
                return false;
            }
            return SingletonMap.o_OFM.o_Nx1.find(item => item.$hashCode == monster.$hashCode && item.curHP > 100) != undefined && SingletonMap.o_h7.o_lDb(monster);
        } catch (ex) {
            console.error(ex);
        }
        return false;
    }
    static nearMonster(monster) {
        let result = false;
        try {
            if (monster) {
                result = GlobalUtil.compterFindWayPos(monster.currentX, monster.o_NI3).length < 8;
            }
        }
        catch (ex) {
            console.error(ex);
        }
        return result;
    }


    static updateLastPointInfo() {
        try {
            if (LastPointWatcher.curPoint == null) {
                LastPointWatcher.curPoint = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3, mapName: SingletonMap.MapData.curMapName };
                LastPointWatcher.curPointTime = SingletonMap.ServerTimeManager.o_eYF;
                LastPointWatcher.lastPointTime = SingletonMap.ServerTimeManager.o_eYF;
            }
            else if (!LastPointWatcher.existsMonster(curMonster)
                && !LastPointWatcher.nearMonster()
                && LastPointWatcher.curPoint.x == PlayerActor.o_NLN.currentX
                && LastPointWatcher.curPoint.y == PlayerActor.o_NLN.o_NI3
                && LastPointWatcher.curPoint.mapName == SingletonMap.MapData.curMapName
                && !SingletonMap.Ladder3v3Mgr.is3v3Map()
                && !SingletonMap.Ladder1v1Mgr.is1v1Map()) {
                LastPointWatcher.lastPointTime = SingletonMap.ServerTimeManager.o_eYF;
            }
            else {
                LastPointWatcher.curPoint = { x: PlayerActor.o_NLN.currentX, y: PlayerActor.o_NLN.o_NI3, mapName: SingletonMap.MapData.curMapName };
                LastPointWatcher.curPointTime = SingletonMap.ServerTimeManager.o_eYF;
                LastPointWatcher.lastPointTime = SingletonMap.ServerTimeManager.o_eYF;
            }
        }
        catch (ex) {
            console.error(ex);
        }
    }

    static resetLastPointInfo() {
        LastPointWatcher.curPoint = null;
        LastPointWatcher.curPointTime = null;
        LastPointWatcher.lastPointTime = null;
    }

    static async checkActivity() {
        LastPointWatcher.updateLastPointInfo();
        if (LastPointWatcher.curPointTime != null && LastPointWatcher.lastPointTime != null) {
            let timeDifference = LastPointWatcher.lastPointTime - LastPointWatcher.curPointTime;
            if (timeDifference >= 60000) { // 发呆超过60秒
                let msg = {
                    MoveLock: MoveLock.isLock(),
                    RobotLock: RobotLock.isLock(),
                    AliveCheckLock: AliveCheckLock.isLock(),
                    curMapName: SingletonMap.MapData.curMapName,
                    curMonster: curMonster ? curMonster.roleName : ''
                };
                let curRunningTask = SingletonModelUtil.getRunningTask();
                if (curRunningTask) {
                    msg = Object.assign(msg, {
                        stopTask: curRunningTask.stopTask,
                        findWayIsStop: curRunningTask.findWayIsStop,
                        attackIsStop: curRunningTask.attackIsStop,
                        stopFindWay: curRunningTask.stopFindWay,
                        diePos: curRunningTask.diePos,
                        taskName: curRunningTask.constructor.name
                    });
                }
                //callbackObj.writelog("ERROR-发呆超过60秒,重启任务 ==>" + JSON.stringify(msg));
                console.error(`ERROR-发呆超过60秒,重启任务 ==> ${JSON.stringify(msg)}`);
                await SingletonModelUtil.stopAllTask();
                MoveLock.unLock();
                MoveLock.resume();
                RobotLock.unLock();
                AliveCheckLock.unLock();
                LastPointWatcher.resetLastPointInfo();
            }
        }
    }
}

function getGameMsg(){
    let msg = {
        MoveLock: MoveLock.isLock(),
        RobotLock: RobotLock.isLock(),
        AliveCheckLock: AliveCheckLock.isLock(),
        curMapName: SingletonMap.MapData.curMapName,
        curMonster: curMonster ? curMonster.roleName : ''
    };
    let curRunningTask = SingletonModelUtil.getRunningTask();
    if (curRunningTask) {
        msg = Object.assign(msg, {
            stopTask: curRunningTask.stopTask,
            findWayIsStop: curRunningTask.findWayIsStop,
            attackIsStop: curRunningTask.attackIsStop,
            stopFindWay: curRunningTask.stopFindWay,
            diePos: curRunningTask.diePos,
            taskName: curRunningTask.constructor.name
        });
    }
    return msg;
}

let curMapMonsterCount = 0;
function setCurMapMonsterCount(count) {
    curMapMonsterCount = count || 0;
}

function existsCanAttackTarget() {
    try {
        var M = SingletonMap.o_OFM.o_Nx1;
        var i;
        for (var Q = 0; Q < M.length; Q++) {
            i = M[Q];
            if ("PlayerActor" == egret.getQualifiedClassName(i)) if (SingletonMap.o_h7.o_lDb(i)) {
                // 有可以攻击的人
                return true;
            }
        }
        for (var Q = 0; Q < M.length; Q++) {
            i = M[Q];
            if ("Monster" == egret.getQualifiedClassName(i)) if (SingletonMap.o_h7.o_lDb(i)) {
                return true;
            }
        }
        return false;
    }
    catch (ex) {
        console.error(ex);
    }
}

function getCurTimeStr() {
    try {
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();
        let milliseconds = date.getMilliseconds();

        month = month < 10 ? "0" + month : month;
        day = day < 10 ? "0" + day : day;
        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        milliseconds = milliseconds < 100 ? "0" + milliseconds : milliseconds;

        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
    }
    catch (e) {
    }
}

var isInSafeAreaFlag = null;
function inAndOutSafeArea(M) {
    try {
        if (M && M[0]) {
            if (M[0] == LangManager.o_OIx) {
                isInSafeAreaFlag = true;
            }
            else if (M[0] == LangManager.o_e4I) {
                isInSafeAreaFlag = false;
            }
        }
    }
    catch (ex) {
        console.log(ex);
    }
}
function isInSafeArea() {
    return isInSafeAreaFlag == true;
}

let arrCdkey = ['vip$YEAR', 'vip$YEAR0501', 'vip$YEAR0502', 'vip$YEAR0503', 'VIP666', 'bzsc$YEAR'];
async function useCdkey() {
    try {
        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curYear = curDate.getFullYear();
        for (var j = 2022; j <= curYear; j++) {
            for (var i = 0; i < arrCdkey.length; i++) {
                let cdkey = arrCdkey[i].replace('$YEAR', j);
                SingletonMap.o_Os4.o_yGr(cdkey);
                await xSleep(200);
            }
        }
    }
    catch (e) {
    }
}

class RobotLock {
    static state = false;

    static isLock() {// true值表示不可移动
        return RobotLock.state == true;
    }

    static lock() {
        RobotLock.state = true;
        // console.warn('Robot lock....');
    }

    static unLock() {
        RobotLock.state = false;
        // console.warn('Robot unLock....');
    }
}

function initTaskQueueCfg(appConfigProperty, taskCls, subProperty) {
    let taskQueueManage = SingletonModelUtil.getInstance(TaskQueueManage);
    if (appConfigProperty == 'TRUE' && (typeof subProperty == 'undefined' || (subProperty && subProperty != ""))) {
        let curTaskInstance = SingletonModelUtil.getInstance(taskCls);
        taskQueueManage.addTask(curTaskInstance);
    }
    else {
        taskQueueManage.removeTask(taskCls.name);
        SingletonModelUtil.removeTask(taskCls);
    }
}

function getAppConfigJSON() {
    let result = "";
    try {
        let appConfig = getAppConfig();
        if (appConfig != "") {
            result = JSON.parse(appConfig);
        }
    } catch (ex) {
    }
    return result;
}

function getAppConfigKeyValue(key) {
    let result = "";
    try {
        let appConfigJSON = getAppConfigJSON();
        if (appConfigJSON) {
            result = appConfigJSON[key];
        }
    } catch (ex) {
    }
    return result;
}

// 值越小优先级越高
var taskWeightCfg = {
    'XiuLuoWarTask': 300, // 修罗战场
    'CijiWarTask': 400,// 刺激战场
    'ChatWorldAnswerTask': 400, // 金榜题名
    'ShenMoWarTask': 400, // 神魔大战
    'XiaGuZhiZhanTask': 400, // 峡谷决战
    'JobWar1v1Task': 400, // 1v1 职业巅峰赛
    'Ladder1v1Task': 400, // 跨服1v1
    'Ladder3v3Task': 400, // 跨服3v3
    'LocalServerHuangChengZhengBaTask': 400, // 本服皇城
    'MoBaiChenZhuTask': 800,  // 膜拜城主
    'XiyouPanTaoTask': 800, // 蟠桃
    'EscortTask': 800, // 押镖
    'DeityPrisonTask': 800, // 神威狱
    'ShenLongChaoXueTask': 800,// 神龙巢穴
    'MoJinChaoXueTask': 800,// 摸金巢穴

    'CrossShenLongMiJingTask': 900, // 神龙秘境
    'MafaXianLingBossTask': 900, // 玛法仙灵
    'MafaDuShiBossTask': 900, // 玛法都市
    'MafaHuoLongBossTask': 900, // 玛法火龙
    'MafaMoLongBossTask': 900, // 玛法魔龙
    'MafaChiYueBossTask': 900, // 玛法赤月
    'ShenLongShanZhuanTask': 900, // 神龙山庄

    'MafaXianLingStar1Task': 900, // 玛法仙灵星怪
    'MafaXianLingStar2Task': 900, // 玛法仙灵星怪
    'MafaDuShiStar1Task': 900, // 都市星怪
    'MafaDuShiStar2Task': 900,// 都市星怪
    'MafaHuoLongStar1Task': 900, //火龙星怪
    'MafaHuoLongStar2Task': 900, //火龙星怪
    'MafaMoLongStar1Task': 900, // 魔龙星怪
    'MafaMoLongStar2Task': 900, // 魔龙星怪
    'MafaChiYueStar1Task': 900, // 赤月星怪
    'MafaChiYueStar2Task': 900, // 赤月星怪

    'villaXianLingBossTask': 1000,
    'villaDuShiBossTask': 1000,
    'villaHuoLongBossTask': 1000,
    'villaMoLongBossTask': 1000,
    'villaChiYueBossTask': 1000,
    'villaShenLongZhuanZhuTask': 1000,
    'villaShenLongWangTask': 1000,

    'villaXianLingStar1Task': 1000,
    'villaXianLingStar2Task': 1000,
    'villaDuShiStar2Task': 1000,
    'villaDuShiStar1Task': 1000,
    'villaHuoLongStar2Task': 1000,
    'villaHuoLongStar1Task': 1000,
    'villaMoLongStar2Task': 1000,
    'villaMoLongStar1Task': 1000,
    'villaChiYueStar2Task': 1000,
    'villaChiYueStar1Task': 1000,
    'villaShenLongStarTask': 1000,
    
    'XiyouCityBossTask': 1100, // 西游4帝
    'ChanYueShenDianTask': 1200, // 苍月神殿
    'PetBossTask': 2000, // 宠物boss
    'BaGuaBossTask': 2100, // 八卦
    'ShenQiBossTask': 2150, // 神器boss
    'VipBossTask': 2200, // vip boss
    'HuangLingTask': 2250, // 本服皇陵
    'CrossPublicBossTask': 2900, // 轮回boss
    'CrossHellBossTask': 2950, // 星空炼狱
    'shaoZhuTask': 3000, // 烧猪    
    'ShenMoShuiJinTask': 3000, // 神魔水晶
    'CrossHuangLingTask': 3400, // 跨服皇陵

    'ShenJieBossTask': 3500, // 神界boss
    'CrossCangYueBossTask': 3550, // 苍月秘镜
    
    'ShenMoBianJinKingTask': 3600, // 神魔边境4王
    'LianYuBossTask': 3900, // 炼狱试炼
    'xiyouJiuChongTianGuaJiBoss': 4000, //西游boss
    'WuXingBossTask': 4100, // 五行boss
    'XiyouTianLaoBossTask': 4200, // 西游天牢
    
    'XiuLuoHuanJinTask': 4250, // 修罗幻境

    'ShenMoShenYuanBossTask': 4300, // 神魔深渊
    'ShenMoZhuDiBossTask': 4400, //  神魔祖地

    'xiyouXianfuDayTask': 5000, // 西游仙府-每日任务
    'xiyouXianfuLianDanTask': 5000, // 西游仙府-炼丹
    'XiyouShenMoGuDiTask': 5000, // 西游仙府 - 神魔谷地
    'XiyouXFLianqiTask': 5000,  // 西游仙府 - 炼器
    'XiyouHuayuanTask': 5000, // 西游化缘

    'ShanGuYiZhiTask': 6000, // 上古遗址

    'FindDragonTask': 6100, // 寻龙秘境
    'GoldPigTask': 6500, // 福利金猪

    'MineTask': 8000, // 挖矿
    'BossExpTask': 8000, // 经验boss
    'GuildBossTask': 8000, // 行会boss
    'XunBaoTask': 8000,
    'XiYouDonTianTask': 8050,  // 西游洞天

    'XinKongZhiLiTask': 8100, // 星空之力

    'GaoBaoGuaJiTask': 9000, // 高爆挂机

    'ShenJieGuaJiNormalTask': 9100, // 神界普通挂机
    'ShenJieGuaJiTask': 9100, // 神界精英挂机
    'xiYouMatchGuaJiTask': 9100, // 西游九重天挂机
    'shenMoBianJinTask': 9100,  // 神魔边境
    'tianJieMatchGuaJiTask': 9200, //  天界挂机
    'YeWaiGuaJiTask': 9200 //野外boss
}

function getTaskWeight(taskName) {
    var curTaskWeightCfg = getTaskWeightCfg();
    return curTaskWeightCfg[taskName] || 9999;
}

var customTaskWeightCfg = {};
function setCustomTaskWeightCfg(cfg) {
    try {
        if(isSuperPri() || isYearPri()) {
            customTaskWeightCfg = Object.assign({}, cfg);
        }
        else {
            customTaskWeightCfg = {};
        }
    }
    catch(e){
        console.error('自定义优先级异常!' + e.message);
    }
}

function getTaskWeightCfg() {
    try {
        if (getAppConfigKeyValue('EnableTaskWeight') == 'TRUE' && !$.isEmptyObject(customTaskWeightCfg)) {
            return customTaskWeightCfg;
        }
    }
    catch (e) {
    }
    return taskWeightCfg;
}

var isStopMainRobot = false;
async function stopMainRobot(isInit) {
    try {
        RobotLock.lock();
        PlayerActor && PlayerActor.o_NLN && PlayerActor.o_NLN.o_lkM();
        SingletonMap && SingletonMap.o_lAu && SingletonMap.o_lAu.stop();
    }
    catch (ex) {
        console.error(ex);
    }
    try {
        SingletonModelUtil.curUid = null;
        isStopMainRobot = true;
        shenJieNextVipMonsterTime = null; // 重置高暴时间
    }
    catch (ex) {
        console.error(ex);
    }
    try {
        LastPointWatcher.resetLastPointInfo();
        await SingletonModelUtil.stopAllTask();
    } catch (ex) {
        console.error(ex);
    }
    try {
        MoveLock.resume();
        MoveLock.unLock();
        RobotLock.unLock();
        AliveCheckLock.unLock();
    } catch (ex) {
        console.error(ex);
    }
}

function mainRobotIsRunning() {
    try {
        QuickPickUpNormal.runPickUp();
    }
    catch(ex){
    }
    return isStopMainRobot != true;
}

var reconnectLastHashCode = null;
function loopAutoCheck1000() {
    try {
        if (commandSocket && commandSocket.thisObject && commandSocket.thisObject._connected
            && commandSocket.thisObject.$hashCode != reconnectLastHashCode && SingletonMap.MapData && SingletonMap.MapData.curMapName) {
            reconnectLastHashCode = commandSocket.thisObject.$hashCode;
            // console.warn('初始化.........1');
            for (let key in window.appInstanceMap) {
                let M = window.appInstanceMap[key];
                if (!M._instance && !M._instanceCreat && key.indexOf('o_') == 0) {
                    M.instance;
                }
            }
        }
    }
    catch (ex) {
    }
    try {
        if (SingletonMap && SingletonMap.XiyouMgr && SingletonMap.XiyouMgr.o_yWb
            && !SingletonMap.o_n.isOpen("CrossLoadingWin")
            && !SingletonMap.XiyouMgr.o_yWb()
            && !SingletonMap.ShenJieMgr.o_UEk()
            && !SingletonMap.o_UK6.o_UG0()
            && !PlayerActor.o_NLN.o_wI
            && PlayerActor.o_NLN.o_Nx7()
            && !SingletonMap.Ladder3v3Mgr.is3v3Map()
            && !SingletonMap.Ladder1v1Mgr.is1v1Map()
            && SingletonMap.MapData.curMapName.indexOf("黑暗神殿") == -1
            && SingletonMap.MapData.curMapName.indexOf("光明神殿") == -1
            && !['跨服沙皇宫'].includes(SingletonMap.MapData.curMapName)
            && !SingletonMap.o_lAu.o_NRY
            && !SingletonMap.o_eXx.o_eGE
            && !SingletonMap.o_yy_.o_eGE) { // 运水晶不上马
                SingletonMap.o_eJ6.o_d8(1);
        }
    } catch (ex) {
    }
    try {
        if (commandSocket && commandSocket.thisObject && commandSocket.thisObject._connected
            && SingletonMap.MapData && SingletonMap.MapData.curMapName) {
            QuickPickUpNormal.runPickUp();
            checkMode();
        }
    }
    catch (ex) {
    }
}

function loopAutoCheck50() {
    try {
        PlayerActor.o_NLN.moveSpeed = 70;
    }
    catch (e) { }
}

var lastWarnForceWin = null;
var lastLoginRoleSelectWin = null;
function loopAutoCheck10000(reconnection) {
    try {
        autoBuyMiBao();
        autoLoopSmallTask();
    }
    catch (ex) {
    }
    try {
        if (SingletonMap.o_n.isOpen("CrossLoadingWin")) {
            return;
        }
        if (reconnection == 'TRUE' && mainRobotIsRunning()) {
            if (SingletonMap.o_n.isOpen("WarnForceWin") && SingletonMap.o_n.isOpen("WarnForceWin").$hashCode != lastWarnForceWin) {
                lastWarnForceWin = SingletonMap.o_n.isOpen("WarnForceWin").$hashCode;
                SingletonMap.o_n.isOpen("WarnForceWin").o_OwF();
                console.warn('断线重连.........step-1');
            }
            else if (SingletonMap.o_n.isOpen("LoginRoleSelectWin") && SingletonMap.o_n.isOpen("LoginRoleSelectWin").$hashCode != lastLoginRoleSelectWin) {
                lastLoginRoleSelectWin = SingletonMap.o_n.isOpen("LoginRoleSelectWin").$hashCode;
                SingletonMap.o_n.isOpen("LoginRoleSelectWin").o_Npo();
                console.warn('断线重连.........step-2');
            }
        }
    } catch (ex) {
    }
}

var gameCfgResetTime = {};

async function runMainRobot(order) {
    try {
        let curTaskInstance = null;
        if (!hasPri()) {
            return;
        }
        try {
            if(SingletonMap.o_n.isOpen("CrossLoadingWin")) {
                return;
            }
            if (SingletonMap.o_n.isOpen('WarnActorReviveWin')) {
                return;
            }
            if (SingletonMap.MapData.curMapName == '红名监狱') {
                return;
            }
        }
        catch(exxx) {
        }

        if (typeof PlayerActor == 'undefined') return;
        if (typeof PlayerActor != 'undefined' && (!PlayerActor || !PlayerActor.o_NLN)) { // 掉线了
            return;
        }

        let curRunningTask = SingletonModelUtil.getRunningTask();
        if (!curRunningTask) {
            RobotLock.unLock();
            MoveLock.resume();
            MoveLock.unLock();
            AliveCheckLock.unLock();
        }
        
        // 检测是否在发呆
        await LastPointWatcher.checkActivity();

        let curDate = new Date(SingletonMap.ServerTimeManager.o_eYF);
        let curMonth = curDate.getMonth();
        let curDateNum = curDate.getDate();
        let curHours = curDate.getHours();
        let curMinutes = curDate.getMinutes();

        // 重置玛法boss时间
        if ((curHours == 23 || curHours == 10) && !gameCfgResetTime[curMonth + '_' + curDateNum + '_' + curHours]) {
            gameCfgResetTime[curMonth + '_' + curDateNum + '_' + curHours] = true;
            await SingletonModelUtil.stopAllTask();
            await gotoWangChen();
            MoveLock.unLock();
            AliveCheckLock.unLock();
            SingletonModelUtil.stopPlayerAction();
        }

        if (MoveLock.isStop()) return;
        if (AliveCheckLock.isLock()) return;
        // if (MoveLock.isLock()) return;
        if (RobotLock.isLock()) return;
        if (SceneRobotLock.isLock()) return;
        RobotLock.lock();
        isStopMainRobot = false;
        
        let taskQueueManage = SingletonModelUtil.getInstance(TaskQueueManage);

        let appConfig = getAppConfig();
        if (appConfig != "") {
            appConfig = JSON.parse(appConfig);
        }

        if (!(appConfig.EnableBlackList == 'TRUE' && !isSafeArea())) {
            BlacklistMonitors.stop();
        }

        // 开启了别墅
        if (appConfig.EnableVillaBoss == 'TRUE' && appConfig.VillaBoss != '') {
            appConfig.VillaBoss = appConfig.VillaBoss.replaceAll(',', '|');
            let mafaTask = appConfig.VillaBoss.split('|');

            for (let i = 0; i < mafaTask.length; i++) {
                let taskName = mafaTask[i];
                let taskCls = eval(taskName);
                curTaskInstance = SingletonModelUtil.getInstance(taskCls);
                taskQueueManage.addTask(curTaskInstance);
            }
        }
        else {
            for (var key in taskWeightCfg) {
                if (key.indexOf('villa') > -1) {
                    taskQueueManage.removeTask(key);
                    SingletonModelUtil.removeTask(eval(key));
                }
            }
        }

        // 神龙山庄
        initTaskQueueCfg(appConfig.EnableShenLongShanZhuanBoss, ShenLongShanZhuanTask, appConfig.ShenLongShanZhuanBoss);
        // 玛法
        if (appConfig.EnableMafaBoss == 'TRUE' && appConfig.MafaBoss != '') {
            appConfig.MafaBoss = appConfig.MafaBoss.replaceAll(',', '|');
            let mafaTask = appConfig.MafaBoss.split('|');

            for (let i = 0; i < mafaTask.length; i++) {
                let taskName = mafaTask[i];
                let taskCls = eval(taskName);
                curTaskInstance = SingletonModelUtil.getInstance(taskCls);
                taskQueueManage.addTask(curTaskInstance);
            }
        }
        else {
            for (var key in taskWeightCfg) {
                if (key.indexOf('Mafa') > -1) {
                    taskQueueManage.removeTask(key);
                    SingletonModelUtil.removeTask(eval(key));
                }
            }
        }

        // 神界精英挂机
        initTaskQueueCfg(appConfig.EnableShenJieChuanQiZuDi, ShenJieGuaJiTask, appConfig.ShenJieMap);
        // 神界普通挂机
        initTaskQueueCfg(appConfig.EnableShenJieChuanQiZuDiNormal, ShenJieGuaJiNormalTask, appConfig.ShenJieNormalMap);

        // 天界挂机
        initTaskQueueCfg(appConfig.EnableTianJieGuaJi, tianJieMatchGuaJiTask);

        // 神魔边境
        initTaskQueueCfg(appConfig.EnableShenMoBianJin, shenMoBianJinTask);

        // 西游挂机
        initTaskQueueCfg(appConfig.EnableXiYouJiuChongTian, xiYouMatchGuaJiTask);

        // 刷星空之力
        if (hasPri() && !isMonthPri()) {
            initTaskQueueCfg(appConfig.EnableXingKongZhiLi, XinKongZhiLiTask);
        }

        // 西游boss
        initTaskQueueCfg(appConfig.EnableXiYouJiuChongTianBoss, xiyouJiuChongTianGuaJiBoss);
        // 西游天牢boss
        initTaskQueueCfg(appConfig.EnableXiyouTianLaoBoss, XiyouTianLaoBossTask);
        // 西游化缘
        initTaskQueueCfg(appConfig.EnableXiyouHuayuan, XiyouHuayuanTask);
        // 神界五行boss
        initTaskQueueCfg(appConfig.EnableShenJieWuXingBoss, WuXingBossTask);
        // 西游仙府
        initTaskQueueCfg(appConfig.EnableXiyouXianfuDayTask, xiyouXianfuDayTask);
        // 西游仙府-炼丹
        initTaskQueueCfg(appConfig.EnableXiyouXianfuLianDanTask, xiyouXianfuLianDanTask);
        // 西游仙府-神魔谷地
        initTaskQueueCfg(appConfig.EnableXiyouShenMoGuDiTask, XiyouShenMoGuDiTask);
        // 西游仙府-炼器
        initTaskQueueCfg(appConfig.EnableXiyouXFLianqiTask, XiyouXFLianqiTask);
        // 个人boss
        initTaskQueueCfg(appConfig.EnableBossExp, BossExpTask);
        // 行会boss
        initTaskQueueCfg(appConfig.EnableGuildBoss, GuildBossTask);
        // 挖矿
        initTaskQueueCfg(appConfig.EnableMine, MineTask);
        // 烧猪
        initTaskQueueCfg(appConfig.EnableShaoZhu, shaoZhuTask);
        // 膜拜城主
        initTaskQueueCfg(appConfig.EnableMoBaiChenZhu, MoBaiChenZhuTask);
        // 本服皇城战
        initTaskQueueCfg(appConfig.EnableLocalServerHuangChengZhengBa, LocalServerHuangChengZhengBaTask);
        // 西游4帝
        initTaskQueueCfg(appConfig.EnableXiyouCityBoss, XiyouCityBossTask, appConfig.XiyouCityBoss);
        // 1v1职业巅峰赛
        initTaskQueueCfg(appConfig.EnableJobWar1v1, JobWar1v1Task);
        // 跨服1v1
        initTaskQueueCfg(appConfig.EnableLadder1v1, Ladder1v1Task);
        // 跨服3v3
        initTaskQueueCfg(appConfig.EnableLadder1v1, Ladder3v3Task);
        // 神魔水晶
        initTaskQueueCfg(appConfig.EnableShenMoShuiJin, ShenMoShuiJinTask);
        // 神界boss
        initTaskQueueCfg(appConfig.EnableShenJieBoss, ShenJieBossTask, appConfig.ShenJieBoss);
        // 高爆地图
        initTaskQueueCfg(appConfig.EnableGaoBaoGuaJi, GaoBaoGuaJiTask, appConfig.GaoBaoGuaJi);
        // 野外boss
        initTaskQueueCfg(appConfig.EnableYeWaiBoss, YeWaiGuaJiTask, appConfig.YeWaiBoss);
        // 押镖
        initTaskQueueCfg(appConfig.EnableEscort, EscortTask);

        // 神威狱
        initTaskQueueCfg(appConfig.EnableDeityPrison, DeityPrisonTask);
        // 轮回boss
        initTaskQueueCfg(appConfig.EnableCrossPublicBoss, CrossPublicBossTask);

        // 蟠桃盛宴
        initTaskQueueCfg(appConfig.EnableXiyouPanTao, XiyouPanTaoTask);

        // 跨服皇陵  
        initTaskQueueCfg(appConfig.EnableCrossHuangLing, CrossHuangLingTask, appConfig.CrossHuangLing);
        // 上古遗址
        initTaskQueueCfg(appConfig.EnableShanGuYiZhi, ShanGuYiZhiTask, appConfig.ShanGuYiZhi);
        // 寻宝
        initTaskQueueCfg(appConfig.EnableXunBao, XunBaoTask);
        // 炼狱
        initTaskQueueCfg(appConfig.EnableLianYuBoss, LianYuBossTask, appConfig.LianYuBoss);
        // 神龙秘境
        initTaskQueueCfg(appConfig.EnableShenLongMiJinBoss, CrossShenLongMiJingTask, appConfig.ShenLongMiJinBoss);
        // 星空炼狱
        initTaskQueueCfg(appConfig.EnableXinKongLianYuBoss, CrossHellBossTask, appConfig.XinKongLianYuBoss);
        // 宠物boss
        initTaskQueueCfg(appConfig.EnablePetBoss, PetBossTask);
        // VipBoss
        initTaskQueueCfg(appConfig.EnableVipBoss, VipBossTask, appConfig.VipBoss);
        // 八卦
        initTaskQueueCfg(appConfig.EnableBaGuaBoss, BaGuaBossTask);
        // 神魔祖地
        initTaskQueueCfg(appConfig.EnableShenMoZhuDiBoss, ShenMoZhuDiBossTask, appConfig.ShenMoZhuDiBoss);
        // 神魔深渊
        initTaskQueueCfg(appConfig.EnableShenMoShenYuanBoss, ShenMoShenYuanBossTask, appConfig.ShenMoShenYuanBoss);
        // 峡谷决战
        initTaskQueueCfg(appConfig.EnableXiaGuZhiZhan, XiaGuZhiZhanTask);
        // 神魔之战
        initTaskQueueCfg(appConfig.EnableShenMoZhiZhan, ShenMoWarTask);
        // 神器boss
        initTaskQueueCfg(appConfig.EnableShenQiBoss, ShenQiBossTask, appConfig.ShenQiBoss);
        // 本服皇陵
        initTaskQueueCfg(appConfig.EnableHuangLing, HuangLingTask, appConfig.HuangLing);
        // 苍月秘境
        initTaskQueueCfg(appConfig.EnableCrossCangYueBoss, CrossCangYueBossTask, appConfig.CrossCangYueBoss);
        // 金榜题名
        initTaskQueueCfg(appConfig.EnableChatWorldAnswer, ChatWorldAnswerTask);

        // 金猪
        initTaskQueueCfg(appConfig.EnableGoldPigBoss, GoldPigTask, appConfig.GoldPigBoss);
        // 神龙巢穴
        initTaskQueueCfg(appConfig.EnableShenLongChaoXue, ShenLongChaoXueTask);
        // 摸金巢穴
        initTaskQueueCfg(appConfig.EnableMoJinChaoXue, MoJinChaoXueTask);
        // 修罗幻境
        initTaskQueueCfg(appConfig.EnableXiuLuoHuanJin, XiuLuoHuanJinTask, appConfig.XiuLuoHuanJinBoss);
        // 神魔边境4王
        initTaskQueueCfg(appConfig.EnableShenMoBianJinKing, ShenMoBianJinKingTask, appConfig.ShenMoBianJinKing);
        // 寻龙秘境
        initTaskQueueCfg(appConfig.EnableFindDragon, FindDragonTask, appConfig.FindDragonBoss);
        // 苍月神殿
        initTaskQueueCfg(appConfig.EnableChanYueShenDian, ChanYueShenDianTask);
        // 西游洞天
        initTaskQueueCfg(appConfig.EnableXiYouDonTian, XiYouDonTianTask, appConfig.XiYouDonTian);
        // 修罗战场
        initTaskQueueCfg(appConfig.EnableXiuLuoWar, XiuLuoWarTask);
        // 刺激战场
        initTaskQueueCfg(appConfig.EnableCiJiWar, CijiWarTask);

        let nextTask = taskQueueManage.nextTask();
        if (nextTask && nextTask.isRunning && !nextTask.isRunning()) {
            SingletonModelUtil.stopPlayerAction();
            await SingletonModelUtil.stopAllTask();
            await exitScene();
            clearCurMonster();
            if (mainRobotIsRunning() && nextTask.isRunning && !nextTask.isRunning()) {
                console.warn(`order...${order} 启动新任务... ${nextTask.constructor.name}  UID=${nextTask.uid} ${JSON.stringify(getGameMsg())}`);
                SingletonModelUtil.curUid = new Date().getTime();
                nextTask.uid = SingletonModelUtil.curUid;
                MoveLock.unLock();
                MoveLock.resume();
                nextTask.start();
            }
        }
    }
    catch (ex) {
        console.error(ex);
    }
    try {
        RobotLock.unLock();
    }
    catch (ex) {
        console.error(ex);
    }
}
// 脚本结束